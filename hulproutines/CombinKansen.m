function Pf = CombinKansen(PfVakken)
% Somregel voor het combineren van onafhankelijke kansen (NaN waarden
% worden als kans 0 meegenomen) 
%
% Ontwikkeld door HKV en Deltares in opdracht van RWS-WVL, maart 2017

% cell naar array
if iscell(PfVakken)
    N = length(PfVakken);
    for i = 1:N
        PfV(i,1) = cell2mat(PfVakken(i));
    end
    PfVakken = PfV;
end

PfVakken(isnan(PfVakken)) = 0; % NaN waarden overschrijven met 0

Kans1 = PfVakken(1,1);
if length(PfVakken(1,:))>1
    Kans2 = PfVakken(1,2);
    Pf = Kans1+Kans2-Kans1*Kans2; % somregel
    for i=3:length(PfVakken) 
        Kans1 = Pf;
        Kans2 = PfVakken(1,i);    
        Pf = Kans1+Kans2-Kans1*Kans2;
    end
else
    Pf = Kans1;
end

