function [HYD] = maak_h_pdf_pre(invoer,logfile)
% Script voert aantal voorbewerkingen uit op de QT relaties en de QH relaties 
% -Interpolatie/extrapolatie QT relatie
% -Maandelijks ov. freq naar jaarlijks cdf
% -Correctie QH relatie: waterstand stijgt met de afvoer: QH_reparatie_1.m
% -Correctie QH relatie: waterstand stijgt met de tijd: QH_reparatie_2.m
% -Toevoegen bodemdaling
%
% Ontwikkeld door HKV en Deltares in opdracht van RWS-WVL, maart 2017

if strcmp(invoer.belastingtype,'QTQH') % default
    disp('inlezen werklijn afvoer en QH relaties')
    
    QTscenarios = invoer.QTscenarios;
    QHscenarios = invoer.QHscenarios;

    %% inlezen QT en discretisatie/extrapolatie
    QTscendata(size(QTscenarios,1)).ID = 0; % initialisatie
    for sc=1:size(QTscenarios,1)
        for zj = 1:size(QTscenarios,2)-2
            % werklijn laden
            filename = QTscenarios{sc,2+zj};
            try 
                Lob_klimaat1 = dlmread(filename,';',1,0);
            catch
                Lob_klimaat1 = dlmread([invoer.prog_dir 'databases/' filename],';',1,0);
            end

            % Maandelijks ov. freq naar kleinere stappen van Q
            if ~exist('QT1','var')
                %Extrapolatie naar overschrijdingskans van 0.9 om Q_min te
                %bepalen
                Q_min = interp1(log10(Lob_klimaat1(:,2)),Lob_klimaat1(:,1),log10(0.9),'linear','extrap');          
                Q_int = Q_min:invoer.Qdisc_step:max(Lob_klimaat1(:,1))+invoer.Qdisc_high;
                QT1(:,2) = Q_int';
                QT1(:,1) = NaN;
            end

            rc(zj) = (log10(Lob_klimaat1(end,2))-log10(Lob_klimaat1(end-1,2))) / (Lob_klimaat1(end,1)-Lob_klimaat1(end-1,1));
            QTscendata(sc).ID = QTscenarios{sc,1};
            QTscendata(sc).Tstart = QTscenarios{sc,2};
            QTscendata(sc).filenames{zj} = filename;
            QTscendata(sc).zichtjaar(zj) = str2double(filename(end-7:end-4)); % laatste 4 karakters van csv bestand moeten gelijk zijn aan het zichtjaar
            QTscendata(sc).data1{1,zj} = [Lob_klimaat1(:,1) Lob_klimaat1(:,2)];
            QTscendata(sc).data2{1,zj} = QT1;
            
        end
        QTscendata(sc).rc = min(rc);
        % QTscendata(sc).rc = mean(rc);
        clear rc
    end

    
    if invoer.extrapolatie==1 % loglinear extrapoleren
        for sc=1:size(QTscenarios,1)
            for zj = 1:size(QTscenarios,2)-2
                tmp(:,2:3) = QTscendata(sc).data1{1,zj};
                Qint = QTscendata(sc).data2{1,zj}(:,2);
                QTscendata(sc).data2{1,zj}(:,3) = 10.^interp1(tmp(:,2),log10(tmp(:,3)),Qint,'linear','extrap');

            end
        end    
    elseif invoer.extrapolatie==2 % loglinear extrapoleren met parallelle frequentielijnen
        for sc=1:size(QTscenarios,1)
            for zj = 1:size(QTscenarios,2)-2
                tmp(:,2:3) = QTscendata(sc).data1{1,zj};
                tmp(end+1,1) = tmp(end,1);
                tmp(end,2) = tmp(end-1,2)+10; % afvoerniveau toevoegen
                tmp(end,3) = 10.^(log10(tmp(end-1,3))+10*QTscendata(sc).rc); % bijbehorende overschrijdingskans
                Qint = QTscendata(sc).data2{1,zj}(:,2); % discretisatie afvoeren
                QTscendata(sc).data2{1,zj}(:,3) = 10.^interp1(tmp(:,2),log10(tmp(:,3)),Qint,'linear','extrap');
                clear tmp
            end
        end
    end

    %% Maandelijks ov. freq naar jaarlijks cdf
    for sc=1:size(QTscenarios,1)
        for zj = 1:size(QTscenarios,2)-2
            tmp =  QTscendata(sc).data2{1,zj}(:,3); % ov.freq
            Qint = QTscendata(sc).data2{1,zj}(:,2); % afvoeren
            QTscendata(sc).Q_cdf{1,zj}(:,1) = Qint;
            QTscendata(sc).Q_cdf{1,zj}(:,2) = exp(-tmp(:,1).*6);
        end
    end

    %% QH relaties
    QHscendata(size(QHscenarios,1)).ID = 0; % initialisatie
    for sc=1:size(QHscenarios,1)
        for zj = 1:size(QHscenarios,2)-2
            % Qh relatie laden
            filename = QHscenarios{sc,2+zj};
            try 
                QHdata = dlmread(filename,';',1,0);
            catch
                QHdata = dlmread([invoer.prog_dir 'databases/QH/' filename],';',1,0);
            end
            QHscendata(sc).ID = QHscenarios{sc,1};
            QHscendata(sc).Tstart = QHscenarios{sc,2};
            QHscendata(sc).filenames{zj} = filename;
            QHscendata(sc).zichtjaar(zj) = str2double(filename(end-7:end-4)); % laatste 4 karakters van csv bestand moeten gelijk zijn aan het zichtjaar
            
            % QH per zichtjaar repareren naar monotoon stijgende waterstand met
            % de afvoer in de functie QH_reparatie_1
            if sc==1 && invoer.rep_1==1
                QHdata = QH_reparatie_1(QHdata,invoer.rep_incr,logfile);
            end
            QHscendata(sc).data1{1,zj} = QHdata;

            QHscendata(sc).data2{1,zj} = QHdata;        
        end
    end
    % repareren voor afnemende waterstanden in de tijd in de functie
    % QH_reparatie_2
    if invoer.rep_2==1
        QHscendata = QH_reparatie_2(QHscendata,invoer.rep_incr,logfile);
    end
    
    % bepaal startjaren QHscenario's
    for i=1:length(QHscendata)
        QdHjaren(i) = QHscendata(i).Tstart;
    end
    
    HYD.db_bodemdaling = dlmread(invoer.db_bodemdaling,';',1,0);
    HYD.QTscendata = QTscendata;
    HYD.QHscendata = QHscendata;
    HYD.HTscendata = [];
    HYD.QdHjaren = QdHjaren;

elseif strcmp(invoer.belastingtype,'HT')
    % functionaliteit nog niet getest
    disp('inlezen waterstandsfrequentielijnen per dijkvak')
    
    HTscenarios = invoer.HTscenarios;
    %% inlezen HT
    HTscendata(size(HTscenarios,1)).ID = 0; % initialisatie
    for sc=1:size(HTscenarios,1)
        for zj = 1:size(HTscenarios,2)-2
            % freqlijn laden
            filename = HTscenarios{sc,2+zj};
            freqlijnen = dlmread(filename,';',1,0);
            HTscendata(sc).ID = HTscenarios{sc,1};
            HTscendata(sc).Tstart = HTscenarios{sc,2};
            HTscendata(sc).filenames{zj} = filename;
            HTscendata(sc).zichtjaar(zj) = str2double(filename(end-7:end-4)); % laatste 4 karakters van csv bestand moeten gelijk zijn aan het zichtjaar
            HTscendata(sc).data1{1,zj} = freqlijnen;
        end
    end
    
    HYD.db_bodemdaling  = dlmread(invoer.db_bodemdaling,';',1,0);
    HYD.QTscendata      = [];
    HYD.QHscendata      = [];
    HYD.HTscendata      = HTscendata;
    HYD.QdHjaren        = 0;
    
end
