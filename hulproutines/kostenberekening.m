function [VAR] = kostenberekening(VNKvakken,v,T,t,VAR,TMP,invoer)
% Bepaal versterkingskosten op basis van versterkingsopgave in zichtjaar (t) voor vak (v) met hulproutine: kostenberekening.m:
% -Interpoleer kosten o.b.v. KOSWAT kostendatabase;
% -Bij piping: opgave maximeren op vooraf gedefinieerde (maximale) bermlengte
% -Contante waarde berekenen
% -Restwaarde aan eind van periode berekenen: add_restwaarde.m:
%
% Ontwikkeld door HKV en Deltares in opdracht van RWS-WVL, maart 2017

% rij om resultaten weg te schrijven
row = TMP.row;

% bepaal opgave voor kostenberekening
if t == 1
    dH_cost = VAR.dH_corr(v,t);
    dS_cost = VAR.dS_corr(v,t);
    dP_cost = VAR.dP_corr(v,t);
else
    dH_cost = VAR.dH_corr(v,t)-VAR.dH_corr(v,t-1);
    dS_cost = VAR.dS_corr(v,t)-VAR.dS_corr(v,t-1);
    dP_cost = VAR.dP_corr(v,t)-VAR.dP_corr(v,t-1);
end

dP_cost_max     = VAR.pipcost(VAR.pipcost(:,1)==VNKvakken(v),3);
InnovativePerKM = VAR.pipcost(VAR.pipcost(:,1)==VNKvakken(v),2) * VAR.prijsindex;

if invoer.constlength == 1
    if sum(VAR.db_constlength.vak==VNKvakken(v)) == 1
        max_kwelscherm  = max(VAR.db_constlength.max_kwelscherm(VAR.db_constlength.vak==VNKvakken(v)),4);
        max_stabwand    = max(VAR.db_constlength.max_stabwand(VAR.db_constlength.vak==VNKvakken(v)),5);
    else
        max_kwelscherm  = 99;
        max_stabwand    = 99;    
    end
end

if dP_cost > dP_cost_max % innovatieve pipingoplossing (geen berm)
    % variant email Peter de Grave 25-09-2017
    temp1 = interp3cost(dH_cost,dS_cost,dP_cost,VNKvakken(v),VAR.cost_basis,VAR.prijsindex,T(t));
    temp2 = interp3cost(dH_cost,dS_cost,dP_cost_max,VNKvakken(v),VAR.cost_basis,VAR.prijsindex,T(t));
    
    if invoer.constlength == 1
        temp1 = correctcost(temp1, VAR.prijsindex, max_kwelscherm, max_stabwand);
        temp2 = correctcost(temp2, VAR.prijsindex, max_kwelscherm, max_stabwand);
    end
    
    cost_out = temp1;    
    
    % bepalen welke maatregel over welk traject wordt gekozen voor piping
    EurKm_berm = 999;
    EurKm_afgekapt = 999;
    EurKm_scherm = 999;
    
    L_pip_tot = temp1{1,8} + temp1{1,10};   % totale trajectlengte waar pipingoplossing moet worden gevonden
        
    L_berm_max = temp1{1,8};    % maximale trajectlengte waarover een grondberm kan worden aangelegd ivm ruimte
    if L_berm_max > 0
        EurKm_berm = temp1{1,9}/temp1{1,8};
    end
        
    L_afgekapt_max = temp2{1,8};     % maximale trajectlengte me afgekapte berm
    if L_afgekapt_max > 0
        EurKm_afgekapt = temp2{1,9}/temp2{1,8} + InnovativePerKM;
    end
    
    if temp1{1,10} > 0
        EurKm_scherm = temp1{1,11}/temp1{1,10};
    end
    
    if EurKm_berm <= EurKm_afgekapt && EurKm_berm <= EurKm_scherm
        if EurKm_afgekapt <= EurKm_scherm   % berm - afgekapt - scherm
            L_berm = L_berm_max;
            L_afgekapt = L_afgekapt_max - L_berm;
            L_scherm = L_pip_tot - L_berm - L_afgekapt;
        else                                % berm - scherm - afgekapt
            L_berm = L_berm_max;
            L_afgekapt = 0;
            L_scherm = L_pip_tot - L_berm;
        end
    elseif EurKm_afgekapt <= EurKm_berm && EurKm_afgekapt <= EurKm_scherm % Als afgekapt goedkoopste optie wordt, dan L_berm is altijd nul.
            L_berm = 0; % afgekapt - berm - scherm of afgekapt - scherm - berm
            L_afgekapt = L_afgekapt_max;
            L_scherm = L_pip_tot - L_afgekapt;
    elseif EurKm_scherm <= EurKm_berm && EurKm_scherm <= EurKm_afgekapt % Als scherm goedkoopste optie wordt, dan altijd scherm kieze (minste ruimte).
        L_berm = 0;
        L_afgekapt = 0;
        L_scherm = L_pip_tot;
    end
    
    cost_out{1,8}  = L_berm + L_afgekapt;                                   % grondmaatregel (met of zonder afkappen en vzg)
    cost_out{1,9}  = L_berm * EurKm_berm + L_afgekapt * EurKm_afgekapt;
    cost_out{1,10} = L_scherm;                                              % kwelscherm
    cost_out{1,11} = L_scherm * EurKm_scherm;
    cost_out{1,14} = min(temp1{1,14}, temp2{1,14} + temp2{1,13} * InnovativePerKM);     % stabwand
    cost_out{1,17} = min(temp1{1,17}, temp2{1,17} + temp2{1,16} * InnovativePerKM);     % kistdam
    cost_out{1,21} = min(temp1{1,21}, temp2{1,21} + temp2{1,20} * InnovativePerKM);     % cc
    cost_out{1,22} = min(temp1{1,22}, temp2{1,22});  % infrastructuur (email d.d. 22-7-2019)
    cost_out{1,7}  = sum(cell2mat([cost_out(1,9),cost_out(1,11),cost_out(1,14),cost_out(1,17),cost_out(1,21),cost_out(1,22)]));  % totaal
    cost_out{1,28} = L_afgekapt;
    cost_out{1,29} = L_afgekapt * EurKm_afgekapt;    
else
    cost_out = interp3cost(dH_cost,dS_cost,dP_cost,VNKvakken(v),VAR.cost_basis,VAR.prijsindex,T(t));
    if invoer.constlength == 1
        cost_out = correctcost(cost_out, VAR.prijsindex, max_kwelscherm, max_stabwand);
    end
end

% rearrange the order of fields
cost_out = [cost_out(1,1) cost_out(1,23:27) cost_out(1,2:22) cost_out(1,28:29)];

VAR.cost_out_new(1+row,:) = cost_out;

% wegschrijven naar matrix
if isempty(VAR.cost_out_new{1+row,12})
    VAR.kosten_dijk(v,t) = 0;
else
    VAR.kosten_dijk(v,t) = VAR.cost_out_new{1+row,12};
end
