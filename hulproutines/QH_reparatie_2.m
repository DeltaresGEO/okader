function QHscendata = QH_reparatie_2(QHscendata,diff,logfile)
% repareren voor afnemende waterstanden in de tijd
%
% Ontwikkeld door HKV en Deltares in opdracht van RWS-WVL, maart 2017

sc=1; % alleen sc==1 (referentie)
vakken = unique(QHscendata(sc).data2{1,1}(:,1));
for v=1:length(vakken)
    ind = find(QHscendata(sc).data2{1,1}(:,1)==vakken(v));
    rep = 0;
    for i=2:size(ind,1) % alle afvoerniveaus
        for zj = 2:size(QHscendata(sc).data2,2)    
            if QHscendata(sc).data2{1,zj}(ind(i),3)>=QHscendata(sc).data2{1,zj-1}(ind(i),3)
                 % stijgend
            else % repareren
                QHscendata(sc).data2{1,zj}(ind(i),3) = QHscendata(sc).data2{1,zj-1}(ind(i),3) + diff;
                rep = 1;
            end
        end
    end
    if rep==1
        disp(['Warning: QH relatie vak ' num2str(vakken(v)) ' gecorrigeerd (reparatie 2)'])
        fileID = fopen(logfile,'a');
        fprintf(fileID,'%s\n',['Warning: QH relatie vak ' num2str(vakken(v)) ' gecorrigeerd (reparatie 2)']);
        fclose(fileID);
    end
end

% nog eens de afzonderlijke zichtjaren repareren indien nodig
for zj = 1:size(QHscendata(sc).data2,2)    
    for v=1:length(vakken)
        ind = find(QHscendata(sc).data2{1,zj}(:,1)==vakken(v));
        QHvak = QHscendata(sc).data2{1,zj}(ind,:);        
        rep = 0;
        for i=2:size(QHvak,1)
            if QHvak(i,3)>QHvak(i-1,3)
                continue
            else
                QHvak(i,3) = QHvak(i-1,3) + diff;
                rep = 1;
            end
        end
        if rep==1
            disp(['Warning: QH relatie vak ' num2str(vakken(v)) ' gecorrigeerd (reparatie 3)'])
            fileID = fopen(logfile,'a');
            fprintf(fileID,'%s\n',['Warning: QH relatie vak ' num2str(vakken(v)) ' gecorrigeerd (reparatie 3)']);
            fclose(fileID);
        end
        QHscendata(sc).data2{1,zj}(ind,:) = QHvak;
    end
end
