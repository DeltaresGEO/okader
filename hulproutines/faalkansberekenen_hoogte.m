function Pf_data = faalkansberekenen_hoogte(STERKTE,HYD,vak,str,link_to_figures,jaar,t0,mechanism,invoer)
%
%
% Ontwikkeld door HKV en Deltares in opdracht van RWS-WVL, maart 2017

fc_data3 = STERKTE.fc_data3;
xdata_wb = HYD.h_pdf(:,1);
ydata_wb = HYD.h_pdf(:,2);
H_bs = HYD.H_bs_h;

Pf_data = cell(1,2);
vak_data = fc_data3{2};
ws = fc_data3{1};
pf_data = fc_data3{3};
dext = 0:invoer.dH_stap:invoer.dH_max;
for i = 1:length(dext)
    try
        pf_vak = pf_data(vak_data == vak,:);
        if isempty(pf_vak) % proberen opgesplitste vakken te vangen
            pf_vak = pf_data(vak_data == round(vak/10),:);
        end
        f1 = find(~isnan(pf_vak) == 1);
        y_temp = pf_vak(f1);
        x_temp = ws(f1)+dext(i);
    catch
        y_temp = ones(1,length(xdata_wb)).*NaN;
        x_temp = y_temp;
    end
    [pf_s,xdata_wb_new,ydata_wb_new] = editFC(x_temp,y_temp,xdata_wb,ydata_wb,invoer.fkbr_nn);
    if invoer.db_afkap_h ==2
        pf_s(xdata_wb_new<H_bs) = 0;
    end
    product = pf_s.*ydata_wb_new;
    Pf = trapz(xdata_wb_new,product);
    Pf_data{1}(i) = Pf;
    Pf_data{2}(i) = dext(i);
    
    % plotten figuren hpdf en fragility curve
    if invoer.plot_pf_h == 1 || (invoer.plot_pf_h == 2 && i==1) || (invoer.plot_pf_h == 3 && i==1 && jaar==t0) || (invoer.plot_pf_h == 4 && i==1 && (strcmp(str,'ontwerp') || jaar==t0) )
    	plot_pf_wb(xdata_wb_new,ydata_wb_new,pf_s,product,HYD,Pf,mechanism,vak,dext,i,jaar,link_to_figures,str)
    end
end

end