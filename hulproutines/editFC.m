function [pf_s,xdata_wb_new,ydata_wb_new] = editFC(x_temp,y_temp,xdata_wb,ydata_wb,n)
%
%
% Ontwikkeld door HKV en Deltares in opdracht van RWS-WVL, maart 2017

if length(find(isnan(y_temp))) == length(y_temp) % alleen NaN values in FC
    xdata_wb_new = xdata_wb;
    ydata_wb_new = ydata_wb;
    pf_s = ones(length(xdata_wb_new),1).*NaN;
else
    method = 3;  % als 1 of 2, dan eerst interpolatie aanpassen naar log10
    if method == 1;
        xdata_wb_new = linspace(min(xdata_wb),max(xdata_wb),n);
        ydata_wb_new = interp1(xdata_wb,ydata_wb,xdata_wb_new);
        ydata_wb_new(ydata_wb_new<0) = 0;
        ydata_wb_new(ydata_wb_new>1) = 1;
        try
            pf_s = interp1(x_temp,y_temp,xdata_wb_new,'linear','extrap');
            pf_s(pf_s<0) = 0;
            pf_s(pf_s>1) = 1;
        catch
            pf_s = zeros(1,length(xdata_wb_new));
            for i = 1:length(xdata_wb_new)
                idx1 = find(x_temp >= xdata_wb_new(i));
                idx2 = find(x_temp <= xdata_wb_new(i));
                if ~isempty(idx1) && ~isempty(idx2)
                    y1 = y_temp(idx1(1));
                    y2 = y_temp(idx2(end));
                    x1 = x_temp(idx1(1));
                    x2 = x_temp(idx2(end));
                    xn = xdata_wb_new(i);
                    pf_s(i) = y1-(x1-xn)/(x1-x2)*(y1-y2);
                elseif ~isempty(idx1) && isempty(idx2)
                    y1 = y_temp(idx1(1));
                    y2 = y_temp(idx1(2));
                    x1 = x_temp(idx1(1));
                    x2 = x_temp(idx1(2));
                    xn = xdata_wb_new(i);
                    ta = abs(y2-y1)/abs(x2-x1);
                    pf_s(i) = y1-(x1-xn)*ta;
                    if pf_s(i) < 0
                        pf_s(i) = 0;
                    end
                    if pf_s(i) > y1
                        return;
                    end
                elseif isempty(idx1) && ~isempty(idx2)
                    y1 = y_temp(idx2(end-1));
                    y2 = y_temp(idx2(end));
                    x1 = x_temp(idx2(end-1));
                    x2 = x_temp(idx2(end));
                    xn = xdata_wb_new(i);
                    ta = abs(y2-y1)/abs(x2-x1);
                    pf_s(i) = y2+(xn-x2)*ta;
                    if pf_s(i) > 1
                        pf_s(i) = 1;
                    end
                    if pf_s(i) < y1
                        return;
                    end
                end
            end
            pf_s(pf_s<0) = 0;
            pf_s(pf_s>1) = 1;
        end

    elseif method == 2
        xdata_wb_new = x_temp;
        ydata_wb_new = interp1(xdata_wb,ydata_wb,xdata_wb_new,'linear','extrap');
        ydata_wb_new(ydata_wb_new>1) = 1;
        ydata_wb_new(ydata_wb_new<0) = 0;
        pf_s = y_temp;

    elseif method == 3
        % Check xmin en xmax
        xmin = max([min(xdata_wb) min(x_temp)-5]); % aanpassing 08-06-2016
        xmax = min([max(xdata_wb) max(x_temp)]);
        xdata_wb_new = linspace(xmin,xmax,n);
        % Extrapolating wb
        ydata_wb_new = 10.^interp1(xdata_wb,log10(ydata_wb),xdata_wb_new,'linear','extrap');
        ydata_wb_new(ydata_wb_new<0) = 0;
        % Extrapolating fc
        [~,ia,~] = unique(x_temp);
        x_temp_def = x_temp(ia);
        y_temp_def = y_temp(ia);
        pf_s = 10.^interp1(x_temp_def,log10(y_temp_def),xdata_wb_new,'linear','extrap');
        pf_s(pf_s<0) = 0;
        pf_s(pf_s>1) = 1;
    end
end
end