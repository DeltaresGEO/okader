function TEMP = faalkansbepaling1(M,t,Pf_mstab_data,Pf_pip_data,Pf_hoogte_data,v,VAR,TEMP)
% faalkans bepalen voor alle mechanismen, aansturen van faalkansbepaling2.m
% waarin daarwerkelijke berekening wordt gedaan.
%
% Ontwikkeld door HKV en Deltares in opdracht van RWS-WVL, maart 2017

for m_ind=1:length(M)
    % FC pakken voor v,t,m
    m  = M(m_ind);
    if t == 1
        dext = 0;
        if m == 1
            TEMP.Pf_1 = faalkansbepaling2(Pf_mstab_data,dext);
        elseif m == 2
            TEMP.Pf_2 = faalkansbepaling2(Pf_pip_data,dext);
        else
            TEMP.Pf_3 = faalkansbepaling2(Pf_hoogte_data,dext);
        end
     else
        if m == 1
            dext = VAR.dS(v,t-1);
            TEMP.Pf_1 = faalkansbepaling2(Pf_mstab_data,dext);
        elseif m == 2
            if VAR.check_const(v) == 0
                dext = VAR.dP(v,t-1);
                TEMP.Pf_2 = faalkansbepaling2(Pf_pip_data,dext);
            else
                TEMP.Pf_2 = 0;
            end
        else
            dext = VAR.dH(v,t-1);
            TEMP.Pf_3 = faalkansbepaling2(Pf_hoogte_data,dext);
        end
    end
end
