function printdHdSdP(invoer,M,T,VAR,v,vaknaam,link_to_figures,VNKvakken)
% Plotten van ontwikkeling dijkversterkingsdimensies in de tijd
%
% Ontwikkeld door HKV en Deltares in opdracht van RWS-WVL, maart 2017

% dS = invoer.mac_factor.*VAR.dS;
% dP = invoer.pip_factor.*VAR.dP;
% dH = VAR.dH;
dS = VAR.dS_corr;
dP = VAR.dP_corr;
dH = VAR.dH_corr;
check_const = VAR.check_const;
Pf_2_t = VAR.Pf_2_t;

s = figure('visible','off');
if length(M)==1
    if M==1
        plot(T,dS(v,:),'r','Linewidth',1.4);
        if max(dS(v,:))==0
            ylim([0 1]);
        end
        legend({'Versterking Macro Stabiliteit'},'Location','NorthEastOutside','FontSize',8);
    elseif M==2
        plot(T,dP(v,:),'b','Linewidth',1.4);
        hold on
        if max(dP(v,:))==0
            ylim([0 1]);
        end
        if check_const(v) == 1
            t_constr = min(find(Pf_2_t(v,:) == 0));
            plot([T(t_constr) T(t_constr)],[0 max(dP(v,:))],'k--','LineWidth',1.4)
            legend({'Versterking Piping','Constructieve Maatregel Piping'},'Location','NorthEastOutside','FontSize',8);
        else
            legend({'Versterking Piping'},'Location','NorthEastOutside','FontSize',8);
        end
    else
        plot(T,dH(v,:),'g','Linewidth',1.4);
        if max(dH(v,:))==0
            ylim([0 1]);
        end
        legend({'Versterking Hoogte'},'Location','NorthEastOutside','FontSize',8);
    end
elseif length(M)==2 % twee mechanismen
    if M(1)==1
        [xT, mac, hbn] = plotyy(T,dS(v,:),T,dH(v,:),'plot');
        ylabel(xT(1),'Breedte Stabiliteitsberm') % left y-axis
        ylabel(xT(2),'Verhoging Kruinhoogte') % right y-axis
        set(mac,'color','red','Linewidth',1.4)
        set(hbn,'color','g','Linewidth',1.4)
        set(xT(1),'ycolor','k') 
        set(xT(2),'ycolor','k')
        legend({'Versterking Macro Stabiliteit','Versterking Hoogte'},'Location','NorthEastOutside','FontSize',8);
        if max(dH(v,:))==0
            set(xT(2),'ylim',[0 1]);
        end
        if max([dS(v,:)])==0
            set(xT(1),'ylim',[0 1]);
        end
    else
        [xT, mac, hbn] = plotyy(T,dP(v,:),T,dH(v,:),'plot');
         hold on
        ylabel(xT(1),'Breedte Pipingberm') % left y-axis
        ylabel(xT(2),'Verhoging Kruinhoogte') % right y-axis
        set(mac,'color','b','Linewidth',1.4)
        set(hbn,'color','g','Linewidth',1.4)
        set(xT(1),'ycolor','k') 
        set(xT(2),'ycolor','k')
        if check_const(v) == 1
            t_constr = min(find(Pf_2_t(v,:) == 0));
            plot([T(t_constr) T(t_constr)],[0 max(dP(v,:))],'k--','LineWidth',1.4)
            legend({'Versterking Piping','Constructieve Maatregel Piping','Versterking Hoogte'},'Location','NorthEastOutside','FontSize',8);
        else
            legend({'Versterking Piping','Versterking Hoogte'},'Location','NorthEastOutside','FontSize',8);
        end
        if max(dH(v,:))==0
            set(xT(2),'ylim',[0 1]);
        end
        if max([dP(v,:)])==0
            set(xT(1),'ylim',[0 1]);
        end
    end
else % alle drie de mechanismen
        [xT, mac, hbn] = plotyy(T,dS(v,:),T,dH(v,:),'plot');
        ylabel(xT(1),'Breedte Piping of Stabiliteitsberm') % left y-axis
        ylabel(xT(2),'Verhoging Kruinhoogte') % right y-axis
        set(mac,'color','red','Linewidth',1.4)
        set(hbn,'color','g','Linewidth',1.4)
        set(xT(1),'ycolor','k') 
        set(xT(2),'ycolor','k')
        hold on
        plot(T,dP(v,:),'b','Linewidth',1.4)
        %legend({'Versterking Macro Stabiliteit','Versterking Piping','Versterking Hoogte'},'Location','NorthEastOutside','FontSize',8);
        if check_const(v) == 1
            t_constr = min(find(Pf_2_t(v,:) == 0));
            plot([T(t_constr) T(t_constr)],[0 max(dP(v,:))],'k--','LineWidth',1.4)
            legend({'Versterking Macro Stabiliteit','Versterking Piping','Constructieve Maatregel Piping','Versterking Hoogte'},'Location','NorthEastOutside','FontSize',8);
        else
            legend({'Versterking Macro Stabiliteit','Versterking Piping','Versterking Hoogte'},'Location','NorthEastOutside','FontSize',8);
        end
        ylm = 25*ceil(max([dS(v,:), dP(v,:)])/25);
        if ylm>0
            set(xT(1),'ylim',[0 ylm],'ytick',[0:25:ylm]);
        end
        if max(dH(v,:))==0
            set(xT(2),'ylim',[0 1]);
        end
        if max([dS(v,:), dP(v,:)])==0
            set(xT(1),'ylim',[0 1]);
        end
end
grid on;
title(['Versterking, Vak ' vaknaam],'interpreter','none');
xlabel('Tijd [jaar]');
set(gca,'YScale','linear');
print([link_to_figures '\dext met t_' num2str(VNKvakken(v))],'-dpng');
close(s);
