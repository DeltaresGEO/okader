function [VAR] = add_restwaarde(VAR,T,t,v,invoer)
% in laatste jaar restwaarde toevoegen op basis van kosten laatste
% versterking in de restlevensduur.
%
% Ontwikkeld door HKV en Deltares in opdracht van RWS-WVL, maart 2017

ontw_duur = VAR.ontwerpduur(VAR.ontwerpduur(:,1)==VAR.VNKvakken(v),2);

ind_versterk = find(VAR.kosten_dijk(v,:)>0,1,'last');
if ~isempty(ind_versterk)
%     jlv = T(1) + ind_versterk -1; % jaar laatste versterking
    jlv = T(ind_versterk); % jaar laatste versterking
    klv = VAR.kosten_dijk(v,ind_versterk); % kosten laatste versterking
    restwaarde = -klv*(ontw_duur-min(T(end)-jlv(end),ontw_duur))/ontw_duur;
else
    restwaarde = 0;
end
VAR.kosten_dijk(v,t+1) = restwaarde;
VAR.kosten_dijk_cw(v,t+1) = VAR.kosten_dijk(v,t+1)/(1+invoer.discontovoet/100)^(T(t)-invoer.basisjaar); % restwaarde contant

end