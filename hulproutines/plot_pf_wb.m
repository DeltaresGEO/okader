function plot_pf_wb(ws_data,ws_pdf,fr_cdf,product,HYD,Pf,mechanism,vak_vnk,db,i,T,link_to_figures,str)
% script maakt figuur van de kansdichtheidsfunctie van de waterstand, de
% fragility curve en het product van die twee. Van het product wordt ook
% een csv bestand weggeschreven waarmee achteraf zelf de waterstand met de
% hoogste kansbijdrage kan worden bepaald.
%
% Ontwikkeld door HKV en Deltares in opdracht van RWS-WVL, maart 2017

% Plots
s = figure('visible','off');
yyaxis left
plot(ws_data,ws_pdf,'-r',HYD.h_pdf(:,1),HYD.h_pdf(:,2),'--g','LineWidth',1);
ylabel('kansdichtheid per jaar [1/m]');
yyaxis right
plot(ws_data,fr_cdf,'-b','LineWidth',1);
grid on;
legend({'Hpdf extrap.','Hpdf orig.','FC'},'Location','NorthEast','FontSize',8);
xlabel('Waterstand [NAP]');
ylabel('conditionele faalkans [-]');
if strcmp(str,'normal')
    title(['Hpdf en FC ' mechanism ' (Pf=' num2str(Pf,'%.2e') '), vak ' num2str(vak_vnk) ', d=' num2str(db(i)) 'm, t=' num2str(T)],'fontsize',8);
    saveas(s,[link_to_figures '\wb en fc_' mechanism '_' num2str(vak_vnk) '_d_' num2str(db(i)) 'm_t_' num2str(T) '.png']);
elseif strcmp(str,'ontwerp')
    title(['Hpdf(ontwerp) en FC ' mechanism ' (Pf=' num2str(Pf,'%.2e') '), vak ' num2str(vak_vnk) ', d=' num2str(db(i)) 'm, t=' num2str(T)],'fontsize',8);
    saveas(s,[link_to_figures '\wb ontwerp en fc_' mechanism '_' num2str(vak_vnk) '_d_' num2str(db(i)) 'm_t_' num2str(T) '.png']);
end
close(s);

s = figure('visible','off');
plot(ws_data,product,'-b');
grid on;
xlabel('waterstand [m+NAP]');
ylabel('kansdichtheid per jaar [1/m]');
if strcmp(str,'normal')
    title(['product Hpdf en FC ' mechanism ' (Pf=' num2str(Pf,'%.2e') '), vak ' num2str(vak_vnk) ', d=' num2str(db(i)) 'm, t=' num2str(T)],'fontsize',8);
    saveas(s,[link_to_figures '\product wb en fc_' mechanism '_' num2str(vak_vnk) '_d_' num2str(db(i)) 'm_t_' num2str(T) '.png']);
elseif strcmp(str,'ontwerp')
    title(['product Hpdf(ontwerp) en FC ' mechanism ' (Pf=' num2str(Pf,'%.2e') '), vak ' num2str(vak_vnk) ', d=' num2str(db(i)) 'm, t=' num2str(T)],'fontsize',8);
    saveas(s,[link_to_figures '\product wb ontwerp en fc_' mechanism '_' num2str(vak_vnk) '_d_' num2str(db(i)) 'm_t_' num2str(T) '.png']);
end
close(s);

% product ook opslaan als csv
outputdata  = [ws_data;product];
if strcmp(str,'normal')
    outputfile  = [link_to_figures '\product wb en fc_' mechanism '_' num2str(vak_vnk) '_d_' num2str(db(i)) 'm_t_' num2str(T) '.csv'];
elseif strcmp(str,'ontwerp')
    outputfile  = [link_to_figures '\product wb ontwerp en fc_' mechanism '_' num2str(vak_vnk) '_d_' num2str(db(i)) 'm_t_' num2str(T) '.csv'];
end
dlmwrite(outputfile,outputdata,'delimiter',';','precision','%.8e')
