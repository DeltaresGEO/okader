function [output] = correctcost(input, prijsindex, max_kwelscherm, max_stabwand)

output = input;

% correcties voor kwelscherm
L_kwelscherm = input{1,12};
if L_kwelscherm > max_kwelscherm
    MR_kwelscherm = input{1,10};
    EUR_kwelscherm = MR_kwelscherm*(121.44*L_kwelscherm+44.112)*2.413*prijsindex/1000;
    EUR_kwelscherm_new = MR_kwelscherm*(121.44*max_kwelscherm+44.112)*2.413*prijsindex/1000;
    EUR_kwelscherm_diff = EUR_kwelscherm_new - EUR_kwelscherm;
    output{1,11} = input{1,11} + EUR_kwelscherm_diff;
    output{1,12} = max_kwelscherm;
end

% correcties voor stabwand
L_stabwand = input{1,15};
if L_stabwand > max_stabwand
    MR_stabwand = input{1,13};
    
    if L_stabwand <= 20  % damwand
        EUR_stabwand = MR_stabwand*(6.618*L_stabwand^2+69.3055*L_stabwand+1027.34)*2.413*prijsindex/1000;
    else % diepwand
        EUR_stabwand = MR_stabwand*(237.41*L_stabwand^1.2151)*2.413*prijsindex/1000;
    end
    
    if max_stabwand <= 20  % damwand
        EUR_stabwand_new = MR_stabwand*(6.618*max_stabwand^2+69.3055*max_stabwand+1027.34)*2.413*prijsindex/1000;
    else % diepwand
        EUR_stabwand_new = MR_stabwand*(237.41*max_stabwand^1.2151)*2.413*prijsindex/1000;
    end
    EUR_stabwand_diff = EUR_stabwand_new - EUR_stabwand;
    output{1,14} = input{1,14} + EUR_stabwand_diff;
    output{1,15} = max_stabwand;
end

% correcties voor kistdam
L_kistdam = input{1,18};
if L_kistdam > max_stabwand
    MR_kistdam = input{1,16};
    B_kistdam = input{1,19};
    
    % verschillende breedtes van de kistdam
    if B_kistdam == 4
        EURkm_kistdam = (397.55*L_kistdam+32.156)*2.690*prijsindex/1000;
        EURkm_kistdam_new = (397.55*max_stabwand+32.156)*2.690*prijsindex/1000;
    elseif B_kistdam == 6
        EURkm_kistdam = (397.55*L_kistdam+281.45)*2.690*prijsindex/1000;
        EURkm_kistdam_new = (397.55*max_stabwand+281.45)*2.690*prijsindex/1000;
    elseif B_kistdam == 8
        EURkm_kistdam = (397.55*L_kistdam+530.75)*2.690*prijsindex/1000;
        EURkm_kistdam_new = (397.55*max_stabwand+530.75)*2.690*prijsindex/1000;
    end
        
    EUR_kistdam = MR_kistdam*EURkm_kistdam;
    EUR_kistdam_new = MR_kistdam*EURkm_kistdam_new;
    EUR_kistdam_diff = EUR_kistdam_new - EUR_kistdam;
    
    output{1,17} = input{1,17} + EUR_kistdam_diff;
    output{1,18} = max_stabwand;
    
    % correcties voor cc oplossing (factor op basis van kistdam)
    factor_cc = EURkm_kistdam_new/EURkm_kistdam;
    output{1,21} = input{1,21}*factor_cc;
end

% totaalkosten herberekenen
output{1,7} = sum(cell2mat([output(1,9),output(1,11),output(1,14),output(1,17),output(1,21),output(1,22)]));
              


