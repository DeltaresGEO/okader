function dext = dextbepaling(Pf_data,Pf_eis,dext_data)
%
%
% Ontwikkeld door HKV en Deltares in opdracht van RWS-WVL, maart 2017

xdata_temp = Pf_data{2};
ydata_temp = Pf_data{1};
if length(find(isnan(ydata_temp))) ~= length(ydata_temp)
    f1 = find(~isnan(ydata_temp) == 1);
    xdata = xdata_temp(f1);
    ydata = ydata_temp(f1);
else
    xdata = xdata_temp;
    ydata = ydata_temp;
end
% aanpassing als length(xdata) == 1 (15 maart 2017 door David)
if length(xdata) == 1 && length(xdata_temp) == 2
    xdata = xdata_temp;
    ydata = ydata_temp;
    ydata(2) = ydata(1)*0.1;
end

a = 1;
while 1
    
    if a==length(dext_data)
        dext = dext_data(a);
        break;
    end
    
    Pf = 10^interp1(xdata,log10(ydata),dext_data(a),'linear','extrap');
    if Pf <= 0 % togevoegd om log10(0) te voorkomen (15-11-2018)
        Pf = interp1(xdata,ydata,dext_data(a),'linear','extrap');
    end
    if Pf <= 0
        Pf = 0;
    end
    if Pf < Pf_eis
        dext = dext_data(a);
        break;
    elseif isnan(Pf)
        dext = NaN;
        break;
    elseif Pf_eis > max(ydata)
        dext = interp1(log10(ydata),xdata,log10(Pf_eis),'linear','extrap');
        %if dext <= 0 % togevoegd om log10(0) te voorkomen (15-11-2018)
        %    dext = interp1(ydata,xdata,Pf_eis,'linear','extrap');
        %end
        if dext <= 0
            dext = 0;
        end
        break;
    else
        a = a+1;
    end
end

end
