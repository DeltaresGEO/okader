function Pf_data = faalkansberekenen_v1(fc_data,vak_vnk,str,HYD,link_to_figures,T,t0,mechanism,met_figuren,afkap,nn)
% script berekent de faalkansen voor piping en macrostabiliteit voor range van dijkbreedtes
%
% Ontwikkeld door HKV en Deltares in opdracht van RWS-WVL, maart 2017


if strcmp(mechanism,'macrostab')
    H_bs = HYD.H_bs_s;
elseif strcmp(mechanism,'piping')
    H_bs = HYD.H_bs_p;
end

%%
Pf_data = cell(1,2);
% nn = 1000;
% Check vak
ff1 = find(fc_data.vak == vak_vnk);
if ~isempty(ff1)
    db_temp = fc_data.d(ff1);
    mu_temp = fc_data.mu(ff1);
    sig_temp = fc_data.sig(ff1);
    kh_temp = fc_data.kh(ff1);
    bu_t_temp = fc_data.bu_t(ff1);
    ws_min_temp = fc_data.ws_min(ff1);
    ws_max_temp = fc_data.ws_max(ff1);
    ext_temp = fc_data.ext(ff1);
    [db,ia] = unique(db_temp);
    mu = mu_temp(ia);
    sig = sig_temp(ia);
    kh = kh_temp(ia);
    bu_t = bu_t_temp(ia);
    ws_min = ws_min_temp(ia);
    ws_max = ws_max_temp(ia);
    ext = ext_temp(ia);
    for i = 1:length(db)
        % Max-min waarden definieren
        if isempty(HYD.h_pdf)
            return;
        end
        xmin = max([min(HYD.h_pdf(:,1)) ws_min(i)-5]); %-5 is de aanpasing gebracht in het oude script
        xmax = min([max(HYD.h_pdf(:,1)) ws_max(i)]);
        % range van ws
        ws_data = linspace(xmin,xmax,nn); %xmin-5 is niet toegepast i.v.m. afkappen
        % ws pdf
        ws_pdf = 10.^interp1(HYD.h_pdf(:,1),log10(HYD.h_pdf(:,2)),ws_data,'linear','extrap');
        ws_pdf(ws_pdf<0) = 0;
        if ~isnan(mu(i)) && ~isnan(sig(i))
            % fr cdf
            fr_cdf = 0.5+0.5*erf((ws_data-mu(i))/(sig(i)*sqrt(2)));
            ff1 = find(isnan(fr_cdf),1);
            if ~isempty(ff1)
                ff2 = find(~isnan(fr_cdf),1);
                xx = ws_data(ff2); yy = fr_cdf(ff2);
                for k = 1:length(ff1)
                    fr_cdf(ff1(k)) = interp1(xx,yy,ws_data(ff1(k)),'linear','extrap');
                end
                fr_cdf(fr_cdf>1) = 1; fr_cdf(fr_cdf<0) = 0;
            end
        else
            fr_cdf = ext(i).*ones(1,length(ws_data));
        end
        
        % kans op 0 zetten voor waterstanden lager dan bewezen sterke (bs)
        if afkap == 2
            fr_cdf(ws_data<H_bs) = 0;
        elseif afkap == 3
            x2 = kh(i);
            x1 = bu_t(i);
            if ~isnan(x1) && ~isnan(x2)
                fr_cdf(ws_data<x1) = 0;
                fr_cdf(ws_data>x2) = 0;
            elseif ~isnan(x1) && isnan(x2)
                fr_cdf(ws_data<x1) = 0;
            elseif isnan(x1) && ~isnan(x2)
                fr_cdf(ws_data>x2) = 0;
            end
        end
        % product van FC en pdf waterstand
        product = fr_cdf.*ws_pdf;
        % faalkans (integratie)
        Pf = trapz(ws_data,product);
        % resultaat (faalkans vs. versterkingsdimensie)
        Pf_data{1}(i) = Pf;
        Pf_data{2}(i) = db(i);
    
        % Figuur
        if met_figuren == 1 || (met_figuren == 2 && i==1) || (met_figuren == 3 && i==1 && T==t0) || (met_figuren == 4 && i==1 && strcmp(str,'ontwerp'))
            plot_pf_wb(ws_data,ws_pdf,fr_cdf,product,HYD,Pf,mechanism,vak_vnk,db,i,T,link_to_figures,str)
        end
    end
    % Check consistentie
    temp = diff(Pf_data{1});
    f1 = find(temp>0,1);
    if ~isempty(f1)
        return;
    end
    f1 = find(temp == 0,1);
    if length(Pf_data{1})-length(f1) == 1
        for j = 1:length(Pf_data{1})-1
            Pf_data{1}(j+1) = Pf_data{1}(j)*0.1;
        end
    end
else
    Pf_data{1} = [nan nan nan];
    Pf_data{2} = [0 25 50];
end
end