function [x_temp,y_temp] = searchFC(vc_data,dext_data,fc_data,dext,vak)
%
%
% Ontwikkeld door HKV en Deltares in opdracht van RWS-WVL, maart 2017

try
    f1 = find(vc_data == vak);
    f2 = find(dext_data == dext);
    f3 = intersect(f1,f2);
    x_temp = fc_data{f3}(:,1);
    y_temp = fc_data{f3}(:,2);
catch
    x_temp = ones(10,1).*NaN;
    y_temp = x_temp;
end
end