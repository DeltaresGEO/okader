function [output] = interp3cost(dH,dS,dP,VakID,cost_basis,prijsindex,zichtjaar)
% 3D interpolatie van KOSWAT database op basis van versterkingsdimensies
%
% Ontwikkeld door HKV en Deltares in opdracht van RWS-WVL, maart 2017

% vullen van uitvoer
output      = cell(1,29);
output{1,1} = VakID;
output{1,2} = zichtjaar;
output{1,3} = dH;
output{1,4} = dS;
output{1,5} = dP;

vak_select = find(cost_basis.num(:,1)==VakID);

if ~isempty(vak_select)
    
    cost_basis_vak.num = cost_basis.num(vak_select,:);
    cost_basis_vak.txt = cost_basis.txt(vak_select+1,:);
    cost_basis_vak.raw = cost_basis.raw(vak_select+1,:);

    hbn_range = unique(cost_basis_vak.num(:,8));
    macro_range = unique(cost_basis_vak.num(:,9));
    pip_range = unique(cost_basis_vak.num(:,10));

    dH = min(dH,max(hbn_range));    % nodig om extrapolatie buiten het bereik 
    dS = min(dS,max(macro_range));  % van de kostendatabase te voorkomen
    dP = min(dP,max(pip_range));

    n = length(hbn_range);
    m = length(macro_range);
    p = length(pip_range);

    % vullen van uitvoer
    output{1,6} = cost_basis_vak.num(1,7); % vaklengte
    
    for i = 1:n
        for j = 1:m
            for k = 1:p
                %S1 = find(cost_basis_vak.num(:,8)==hbn_range(i));
                %R2 = find(cost_basis_vak.num(:,9)==macro_range(j));
                %S2 = intersect(S1,R2);
                %R3 = find(cost_basis_vak.num(:,10)==pip_range(k));
                %S3 = intersect(S2,R3);
                
                S3 = find(cost_basis_vak.num(:,8)==hbn_range(i) & ...
                          cost_basis_vak.num(:,9)==macro_range(j) & ...
                          cost_basis_vak.num(:,10)==pip_range(k));                
                
                outputtypes     = {'KT' 'LG' 'KG' 'LKS' 'KKS' 'WLKS' 'LS' 'KS' 'WLS' 'LKD' 'KKD' 'WLKD' 'WBKD' 'LCC' 'KCC' 'KI'};
                outputcolumns   = [11:26];
%                 outputtypes     = {'KT' 'LG' 'KG' 'LKS' 'KKS' 'LS' 'KS' 'LKD' 'KKD' 'LCC' 'KCC' 'KI' 'LIP' 'KIP'};
%                 outputcolumns   = [11:24];
                for o = 1:length(outputtypes)
                    cost_matrix(o).name = outputtypes(o);
                    cost_matrix(o).data(j,i,k) = cost_basis_vak.num(S3,outputcolumns(o));
                end
            end
        end
    end
    
    % 
    for o = 1:length(outputtypes)
        if dH>max(hbn_range) && dS<=max(macro_range) && dP<=max(pip_range)
            cost_int(:,1) = hbn_range;
            cost_int(:,2) = interp3(hbn_range,macro_range,pip_range,cost_matrix(o).data,hbn_range,dS,dP);
            cost_reinforce = interp1(cost_int(:,1),cost_int(:,2),dH,'linear','extrap');

        elseif dH<=max(hbn_range) && dS>max(macro_range) && dP<=max(pip_range)
            cost_int(:,1) = macro_range;
            cost_int(:,2) = interp3(hbn_range,macro_range,pip_range,cost_matrix(o).data,dH,macro_range,dP);
            cost_reinforce = interp1(cost_int(:,1),cost_int(:,2),dS,'linear','extrap');

        elseif dH<=max(hbn_range) && dS<=max(macro_range) && dP>max(pip_range)
            cost_int(:,1) = pip_range;
            cost_int(:,2) = interp3(hbn_range,macro_range,pip_range,cost_matrix(o).data,dH,dS,pip_range);
            cost_reinforce = interp1(cost_int(:,1),cost_int(:,2),dP,'linear','extrap');

        elseif dH>max(hbn_range) && dS>max(macro_range) && dP<=max(pip_range)
            cost_int = interp3(hbn_range,macro_range,pip_range,cost_matrix(o).data,hbn_range,macro_range,dP);
            for i = 1:length(macro_range)
                cost_extrap1(i,1) = macro_range(i);
                cost_extrap1(i,2) = interp1(hbn_range,cost_int(i,:),dH,'linear','extrap');
            end
            cost_reinforce = interp1(cost_extrap1(:,1),cost_extrap1(:,2),dS,'linear','extrap');

        elseif dH<=max(hbn_range) && dS>max(macro_range) && dP>max(pip_range)
            cost_int = interp3(hbn_range,macro_range,pip_range,cost_matrix(o).data,dH,macro_range,pip_range);
            for i = 1:length(macro_range)
                for j = 1:length(pip_range)
                    cost_int2(i,j) = cost_int(i,1,j);
                end
            end
            cost_int = cost_int2;

            for i = 1:length(macro_range)
                cost_extrap1(i,1) = macro_range(i);
                cost_extrap1(i,2) = interp1(pip_range,cost_int(i,:),dP,'linear','extrap');
            end
            cost_reinforce = interp1(cost_extrap1(:,1),cost_extrap1(:,2),dS,'linear','extrap');

        elseif dH>max(hbn_range) && dS<=max(macro_range) && dP>max(pip_range)
            cost_int = interp3(hbn_range,macro_range,pip_range,cost_matrix(o).data,hbn_range,dS,pip_range);
            for i = 1:length(hbn_range)
                for j = 1:length(pip_range)
                    cost_int2(i,j) = cost_int(1,i,j);
                end
            end
            cost_int = cost_int2;
            for i = 1:length(hbn_range)
                cost_extrap1(i,1) = hbn_range(i);
                cost_extrap1(i,2) = interp1(pip_range,cost_int(i,:),dP,'linear','extrap');
            end
            cost_reinforce = interp1(cost_extrap1(:,1),cost_extrap1(:,2),dH,'linear','extrap');

        else
%             cost_reinforce = interp3(hbn_range,macro_range,pip_range,cost_matrix(o).data,dH,dS,dP);
            if (length(macro_range) == 1) && (length(pip_range) == 1)
                cost_reinforce = interp1(hbn_range,cost_matrix(o).data,dH);
            else
                cost_reinforce = interp3(hbn_range,macro_range,pip_range,cost_matrix(o).data,dH,dS,dP);
            end
        end
        
%         % lengtes en kosten mogen niet kleiner dan 0 zijn
%         cost_reinforce = max(cost_reinforce,0);
%         % lengtes mogen niet groter zijn dan vaklengte
%         if ismember(outputtypes(o),{'LG' 'LKS' 'LS'  'LKD' 'LCC'})
%             cost_reinforce = min(cost_reinforce,cost_basis.num(vak_select(1),7));
%         end
        
        % Als het een kostenveld is (startend met een 'K') dan indexeren!
        if strcmp(outputtypes{o}(1),'K')
            cost_reinforce = cost_reinforce*prijsindex;
        end
        output{1,o+6} = cost_reinforce;
    end
    
    output(1,o+7) = cost_basis_vak.raw(1,2); % dijkring
    output{1,o+8} =  cost_basis_vak.raw{1,3}; % dijkringnaam
    output{1,o+9} =  cost_basis_vak.raw{1,4}; % traject
    output{1,o+10} =  cost_basis_vak.raw(1,5); % beheerder
    output{1,o+11} =  cost_basis_vak.raw(1,6); % categorie
    output{1,o+12} =  0; % lengte innovatieve pipingmaatregel
    output{1,o+13} =  0; % kosten innovatieve pipingmaatregel
    
else
    disp('Vak niet aanwezig in kostendatabase')
end



