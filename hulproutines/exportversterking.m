function exportversterking(invoer,VAR,uitvoernaam)
% Schrijft de cumulatieve dijkversterkingsdimensies per vak en per jaar naar een *.csv file. 
%
% VAR           = structure met data die tijdens berekening wordt gevuld
% uitvoernaam   = basis van uitvoerbestand. Wordt aangevuld met faalmechanisme
%
% Ontwikkeld door HKV en Deltares in opdracht van RWS-WVL, maart 2017

%% afmetingen matrix
[n,m] = size(VAR.dS_corr);   % m=aantal jaren, n=aantal vakken

%% headers toevoegen
export{1,1} = 'VakID';
for j = 1:m
    export{j+1,1} = ['versterkingsdimensies in ',num2str(VAR.T(j))];
end

%% dS stabiliteit
for i = 2:n+1
    export{1,i} = VAR.VNKvakken(i-1);
    for j = 1:m
        export{j+1,i} = VAR.dS_corr(i-1,j);
    end
end
cell2csv([uitvoernaam,'stabiliteit_vakniveau.csv'],export,';',2010,'.');        

%% Pf_2 piping
for i = 2:n+1
    export{1,i} = VAR.VNKvakken(i-1);
    for j = 1:m
        export{j+1,i} = VAR.dP_corr(i-1,j);
    end
end
cell2csv([uitvoernaam,'piping_vakniveau.csv'],export,';',2010,'.');        

%% dH hoogte
for i = 2:n+1
    export{1,i} = VAR.VNKvakken(i-1);
    for j = 1:m
        export{j+1,i} = VAR.dH_corr(i-1,j);
    end
end
cell2csv([uitvoernaam,'hoogte_vakniveau.csv'],export,';',2010,'.');        

end