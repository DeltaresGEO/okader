function Pf = faalkansbepaling2(Pf_data,dext)
% faalkans bepalen op basis van dijkdimensies en faalkans als functie van
% dijkdimensies
%
% Ontwikkeld door HKV en Deltares in opdracht van RWS-WVL, maart 2017

xdata_temp = Pf_data{2};
ydata_temp = Pf_data{1};
if length(find(isnan(ydata_temp))) ~= length(ydata_temp)
    f1 = find(~isnan(ydata_temp) == 1);
    xdata = xdata_temp(f1);
    ydata = ydata_temp(f1);
else
    xdata = xdata_temp;
    ydata = ydata_temp;
end
if dext <= min(xdata)
    Pf = ydata(1);
elseif dext > max(xdata)
    Pf = 10^interp1(xdata,log10(ydata),dext,'linear','extrap');
    if Pf <= 0 % toegevoegd i.v.m veel vakken met pf == 0 (15-11-2018)
        Pf = interp1(xdata,ydata,dext,'linear','extrap');
    end
    if Pf <= 0
        Pf = 0;
    end
else
    Pf = 10^interp1(xdata,log10(ydata),dext,'linear','extrap');
    if Pf <= 0 % toegevoegd i.v.m veel vakken met pf == 0 (15-11-2018)
        Pf = interp1(xdata,ydata,dext,'linear','extrap');
    end
    if Pf <= 0
        Pf = 0;
    end
end
end