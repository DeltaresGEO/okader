function exportkansen(invoer,VAR,uitvoernaam,vaklengte)
% Schrijft de faalkansen op doorsnedeniveau per vak en per jaar naar een *.csv file. 
% Afhankelijk van keuze in invoer (variabele "exp_faalkans_type") worden ook kansen per normtraject uitgevoerd
%
% Oprollen naar normtraject gebeurt voor hoogte obv volledige
% afhankelijheid, voor piping/macrostab obv volledige onafhankelijkheid.
% Oprollen van mechanismen per traject gebeurt obv volledige onafhankelijkheid
%
% invoer.exp_faalkans_type = 0    -> geen export
% invoer.exp_faalkans_type = 1    -> csv doorsnedekans per vak en jaar
% invoer.exp_faalkans_type = 2    -> csv trajectkans per traject en jaar
% invoer.exp_faalkans_type = 3    -> beide
%
% Ontwikkeld door HKV en Deltares in opdracht van RWS-WVL, maart 2017

Pf_1_t = VAR.Pf_1_t;
Pf_2_t = VAR.Pf_2_t;
Pf_3_t = VAR.Pf_3_t;
normkans = VAR.norm.normkans;
normtraject = VAR.norm.normtraject;
VNKvakken = VAR.VNKvakken;
vak_data= VAR.norm.vak_data;
a_pip = VAR.norm.a_pip;
a_mac = VAR.norm.a_mac;
b_pip = VAR.norm.b_pip;
b_mac = VAR.norm.b_mac;
vaklengte = cell2mat(vaklengte(2:end,1:2));
% t0 = VAR.T(1);

[n,m] = size(Pf_1_t);   % m=aantal jaren, n=aantal vakken

% headers toevoegen
export{1,1} = 'VakID';
export{m+2,1} = 'Faalkanseis';
for j = 2:m+1
%     export{j,1} = ['Faalkans in ',num2str(j-2+t0)];
    export{j,1} = ['Faalkans in ',num2str(VAR.T(j-1))];
end
export_basis = export;

%% Pf_1 stabiliteit
% kansen toevoegen voor doorgerekende dijkvakken (doorsnede niveau)
for i = 2:n+1
    export{1,i} = VNKvakken(i-1);
    for j = 2:m+1
        export{j,i} = Pf_1_t(i-1,j-1);
    end
    export{m+2,i} = VAR.Pf_eis_1_v(i-1,j-1);
end
faalkansen(1).dsnkans_1 = export;
cell2csv([uitvoernaam,'stabiliteit_doorsnedeniveau.csv'],export,';',2010,'.');        

% kansen berekenen op vakniveau
exp_vak = export_basis;
for i = 2:n+1
    exp_vak{1,i} = VNKvakken(i-1);
    L = vaklengte(vaklengte(:,1)==VNKvakken(i-1),2);
    N = round(1+(a_mac(vak_data == VNKvakken(i-1))*L/b_mac(vak_data == VNKvakken(i-1))));
    clear Pf_vak
    if isempty(N)
       exp_vak{j,i} = 0;
    elseif L<b_mac(vak_data == VNKvakken(i-1))+1 || L>b_mac(vak_data == VNKvakken(i-1))-1 % vakkans is doorsnedekans 
        for j = 2:m+1
            exp_vak{j,i} = Pf_1_t(i-1,j-1);
        end        
    else
        for j = 2:m+1
            Pf_vak(1:N) = Pf_1_t(i-1,j-1);
            exp_vak{j,i} = CombinKansen(Pf_vak(1:N));
        end
    end
end
faalkansen(1).vakkans_1 = exp_vak;

% maak lijst met normtrajecten
nrm_trj = unique(normtraject(ismember(vak_data,VNKvakken)));

if invoer.exp_faalkans_traject == 1
    % voor ieder traject een csv bestand uitvoeren
    for j = 1:length(nrm_trj)
        IndexC  = strfind(normtraject,nrm_trj{j,1});
        Index   = find(not(cellfun('isempty', IndexC)));
        vak_trj = vak_data(Index);
        exp_trj(:,1) = export(:,1);
        x = 0;
        for i = 2:n+1
            if ismember(VNKvakken(i-1),vak_trj')
               x = x+1;
               exp_trj(:,1+x) = exp_vak(:,i); % vak kans
            end
        end
        if x>0        
            % combineren van mechanismekans per vak naar mechanismekans per
            % traject (volledige onafhankelijkheid)
            for k = 2:m+1
                Pf_combin = CombinKansen(cell2mat(exp_trj(k,2:1+x)));
                exp_trj(k,2+x) = num2cell(Pf_combin);
            end
            exp_trj(m+2,2+x) = num2cell(9.99);        
        else
            exp_trj(:,2+x) = num2cell(0);
        end
        exp_trj{1,2+x} = 'TrajectCombin';
        cell2csv([uitvoernaam,'stabiliteit_',nrm_trj{j,1},'.csv'],exp_trj,';',2010,'.');
        faalkansen(j).exp_trj_1 = exp_trj;
        clear ind IndexC Index vak_trj exp_trj 
    end
end


%% Pf_2 piping
% kansen toevoegen voor doorgerekende dijkvakken
export = export_basis;
for i = 2:n+1
    export{1,i} = VNKvakken(i-1);
    for j = 2:m+1
        export{j,i} = Pf_2_t(i-1,j-1);
    end
    export{m+2,i} = VAR.Pf_eis_2_v(i-1,j-1);
end
faalkansen(1).dsnkans_2 = export;
cell2csv([uitvoernaam,'piping_doorsnedeniveau.csv'],export,';',2010,'.');        


% kansen berekenen op vakniveau
exp_vak = export_basis;
for i = 2:n+1
    exp_vak{1,i} = VNKvakken(i-1);
    L = vaklengte(vaklengte(:,1)==VNKvakken(i-1),2);
    N = round(1+(a_pip(vak_data == VNKvakken(i-1))*L/b_pip(vak_data == VNKvakken(i-1))));
    clear Pf_vak
    if isempty(N)
       exp_vak{j,i} = 0;
    elseif L<b_pip(vak_data == VNKvakken(i-1))+1 || L>b_pip(vak_data == VNKvakken(i-1))-1 % vakkans is doorsnedekans 
        for j = 2:m+1
            exp_vak{j,i} = Pf_2_t(i-1,j-1);
        end        
    else
        for j = 2:m+1
            Pf_vak(1:N) = Pf_2_t(i-1,j-1);
            exp_vak{j,i} = CombinKansen(Pf_vak(1:N));
        end
    end
end
faalkansen(1).vakkans_2 = exp_vak;

% voor ieder traject een csv bestand uitvoeren
if invoer.exp_faalkans_traject == 1
    for j = 1:length(nrm_trj)
        IndexC  = strfind(normtraject,nrm_trj{j,1});
        Index   = find(not(cellfun('isempty', IndexC)));
        vak_trj = vak_data(Index);
        exp_trj(:,1) = export(:,1);
        x = 0;
        for i = 2:n+1
            if ismember(VNKvakken(i-1),vak_trj')
               x = x+1;
               exp_trj(:,1+x) = exp_vak(:,i); % vak kans
           end
        end
        if x>0
            % combineren van mechanismekans per vak naar mechanismekans per
            % traject (volledige onafhankelijkheid)
            for k = 2:m+1
                Pf_combin = CombinKansen(cell2mat(exp_trj(k,2:1+x)));
                exp_trj(k,2+x) = num2cell(Pf_combin);
            end
            exp_trj(m+2,2+x) = num2cell(9.99);        
        else
            exp_trj(:,2+x) = num2cell(0);
        end
        exp_trj{1,2+x} = 'TrajectCombin';
        cell2csv([uitvoernaam,'piping_',nrm_trj{j,1},'.csv'],exp_trj,';',2010,'.');
        faalkansen(j).exp_trj_2 = exp_trj;
        clear ind IndexC Index vak_trj exp_trj 
    end
end

%% Pf_3 hoogte
% kansen toevoegen voor doorgerekende dijkvakken
export = export_basis;
for i = 2:n+1
    export{1,i} = VNKvakken(i-1);
    for j = 2:m+1
        export{j,i} = Pf_3_t(i-1,j-1);
    end
    export{m+2,i} = VAR.Pf_eis_3_v(i-1,j-1);
end
faalkansen(1).vakkans_3 = export;
faalkansen(1).dsnkans_3 = export;
cell2csv([uitvoernaam,'hoogte_doorsnedeniveau.csv'],export,';',2010,'.');        

% voor ieder traject een csv bestand uitvoeren
if invoer.exp_faalkans_traject == 1
    for j = 1:length(nrm_trj)
        IndexC  = strfind(normtraject,nrm_trj{j,1});
        Index   = find(not(cellfun('isempty', IndexC)));
        vak_trj = vak_data(Index);
        exp_trj(:,1) = export(:,1);
        x = 0;
        for i = 2:n+1
            if ismember(VNKvakken(i-1),vak_trj')
               x = x+1;
               exp_trj(:,1+x) = export(:,i);
            end
        end
        if x>0
            % combineren van mechanismekans per vak naar mechanismekans per traject
            % met volledige afhankelijkheid (max van kansen)
            for k = 2:m+1
                Pf_combin = max(cell2mat(exp_trj(k,2:1+x)));
                exp_trj(k,2+x) = num2cell(Pf_combin);
            end
            exp_trj{m+2,2+x} = 'NaN';        
        else
        exp_trj(:,2+x) = num2cell(0);
        end
        exp_trj{1,2+x} = 'TrajectCombin';
        cell2csv([uitvoernaam,'hoogte_',nrm_trj{j,1},'.csv'],exp_trj,';',2010,'.');
        faalkansen(j).exp_trj_3 = exp_trj;
        clear ind IndexC Index vak_trj exp_trj 
    end
end

%% Mechanismen combineren
% voor ieder traject een csv bestand uitvoeren
if invoer.exp_faalkans_traject == 1
    for j = 1:length(nrm_trj)
        IndexC  = strfind(normtraject,nrm_trj{j,1});
        Index   = find(not(cellfun('isempty', IndexC)));
        vak_trj = vak_data(Index);
        exp_trj(:,1) = faalkansen(1).exp_trj_3(:,1);
        if size(faalkansen(j).exp_trj_1,2)>2 % minstens ��n vak
            x = size(faalkansen(j).exp_trj_1,2)-2; % aantal vakken in traject
            % combineren van mechanismekans naar trajectkans
            for k = 2:m+1 % jaren
                Pf_combin = CombinKansen([faalkansen(j).exp_trj_1{k,2+x} faalkansen(j).exp_trj_2{k,2+x} faalkansen(j).exp_trj_3{k,2+x}]);
                exp_trj(k,2) = num2cell(Pf_combin);
            end
        else
        exp_trj(:,2) = num2cell(0);
        end
        exp_trj{1,2} = 'TrajectCombin';           % header
        exp_trj{m+2,2} = unique(normkans(Index)); % trajectnorm
        cell2csv([uitvoernaam,'combinatie_',nrm_trj{j,1},'.csv'],exp_trj,';',2010,'.');
        faalkansen(j).exp_trj_combin = exp_trj;
        clear ind IndexC Index vak_trj exp_trj 
    end
end

end