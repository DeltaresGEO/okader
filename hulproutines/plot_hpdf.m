function plot_hpdf(T,t,HYD,VNKvakken,v,link_to_figures,titlestr,filestr)
% Plotten van figuur met lokale waterstanden (kansdichtheid en
% overschrijdingsfrequentielijn) in een bepaald zichtjaar.
%
% Ontwikkeld door HKV en Deltares in opdracht van RWS-WVL, maart 2017

h_pdf = HYD.h_pdf;
H_freq_vak_t = HYD.H_freq_vak_t;

%kansdichtheidsfunctie waterstand
s = figure('Visible','off');
plot(h_pdf(:,1),h_pdf(:,2),'-b');
legend({['t = ' num2str(T(t))]},'Location','NorthEastOutside','FontSize',8);
title([titlestr ', vak ' num2str(VNKvakken(v)) ', t = ' num2str(T(t))]);
xlabel('Waterstand [m+NAP]');
ylabel('kansdichtheid');
saveas(s,[link_to_figures '\' filestr '_pdf_' num2str(VNKvakken(v)) '_t_' num2str(T(t)) '.png']);
close(s);

%waterstandsfrequentielijn
s = figure('Visible','off');
semilogx(H_freq_vak_t(:,2),H_freq_vak_t(:,1),'b-')
ylabel('lokale waterstand (m+NAP)')
xlabel('overschrijdingsfrequentie (1/jaar)')
xlims = get(gca, 'XLim');
xlim([xlims(1) min(1,xlims(2))])
set(gca,'Xdir','reverse')
grid on
title(['waterstandsfrequentielijn vak ' num2str(VNKvakken(v)) ', t = ' num2str(T(t))]);
saveas(s,[link_to_figures '\' filestr '_freq_' num2str(VNKvakken(v)) '_t_' num2str(T(t)) '.png']);
close(s);

end
