function [HYD] = maak_h_pdf(HYD,invoer,jaar,zichtjaar,VakID,sc_qh,toeslag,logfile)
% Bepaal in zichtjaar (t) voor vak (v) de kansverdeling lokale waterstand
%
% Default optie (invoer.belastingtype,'QTQH'):
% -CDF afvoer interpoleren naar zichtjaar
% -QdH relatie van maatregelen optellen bij QH relatie referentie
% -QH relatie incl. maatregelen interpoleren naar zichtjaar
% -Bodemdaling en onzekerheidstoeslag toevoegen
% -QH relatie en CDF van afvoer samenvoegen tot CDF van lokale waterstand
% -CDF naar PDF van lokale waterstand
% -PDF van lokale waterstand discretiseren naar 1cm stapgrootte
% -Grenswaarde waterstand voor bewezen sterkte berekenen
%
% Alternatieve optie (invoer.belastingtype,'HT'):
% -waterstandsfrequentielijn loglineair interpoleren naar zichtjaar
% -Bodemdaling en onzekerheidstoeslag toevoegen
% -waterstandsfrequentielijn naar PDF van lokale waterstand
% -PDF van lokale waterstand discretiseren naar 1cm stapgrootte
% -Grenswaarde waterstand voor bewezen sterkte berekenen
%
% Ontwikkeld door HKV en Deltares in opdracht van RWS-WVL, maart 2017

% if zichtjaar > 2069 && zichtjaar < 2072
%     % debug
%     disp('stop')
% end

try
    if strcmp(invoer.belastingtype,'QTQH') % default

        %% gegevens over QT en QH scenarios
        QTscendata = HYD.QTscendata;
        QHscendata = HYD.QHscendata;
        db_bodemdaling = HYD.db_bodemdaling;

        %% jaarlijkse cdf interpoleren naar zichtjaar   
        % bepaal vigerende afvoerscenario
        for i=1:length(QTscendata)
            jaren(i) = QTscendata(i).Tstart;
        end
        sc = find(jaren<=jaar,1,'last');

        % bepaal jaren waartussen geinterpoleerd moet worden
        ind1=find(QTscendata(sc).zichtjaar<=zichtjaar,1,'last');  % indices van vorige zichtjaar
        ind2=find(QTscendata(sc).zichtjaar>zichtjaar,1,'first'); % indices van volgende zichtjaar
        if isempty(ind1) && ~isempty(ind2)
            % geen QT op kleiner zichtjaar, neem twee volgende zichtjaren
            ind1=ind2;
            ind2=find(QTscendata(sc).zichtjaar>zichtjaar,2,'first'); % indices van volgende zichtjaar
            ind2=ind2(2);
        elseif isempty(ind2) && ~isempty(ind1)
            % geen QT op groter zichtjaar, neem twee eerdere zichtjaren
            ind2=ind1;
            ind1=find(QTscendata(sc).zichtjaar<=zichtjaar,2,'last'); % indices van volgende zichtjaar
            ind1=ind1(1);
        end

        t1 = QTscendata(sc).zichtjaar(1,ind1);
        t2 = QTscendata(sc).zichtjaar(1,ind2);
        Q1 = QTscendata(sc).Q_cdf{1,ind1};
        Q2 = QTscendata(sc).Q_cdf{1,ind2};
        % controleren of afvoeren gelijk zijn
        if ~Q1==Q2
            disp('error: afvoerniveaus in afvoerstatistiek zijn niet gelijk')
            fileID = fopen(logfile,'a');
            fprintf(fileID,'%s\n',['ERROR: afvoerniveaus in afvoerstatistiek vak ' num2str(VakID) ' zijn niet gelijk']);
            fclose(fileID);
        end

        Q_cdf_t(:,1) = Q1(:,1);
        for i = 1:length(Q1)
            Q_cdf_t(i,2) = interp1([t1 t2],[Q1(i,2) Q2(i,2)],zichtjaar,'linear','extrap'); % interpoleren van de onderschrijdingskansen:
        end

        %% QH referentie en QdH maatregelen optellen voor alle zichtjaren
        for zj = 1:size(QHscendata(1).data2,2) % voor alle zichjaren
%             for sc=sc_qh(1):sc_qh(end) % voor alle opgegeven QH relaties
            for sc = sc_qh % voor alle opgegeven QH relaties
                if sc==1 % referentie (QH)
                    temp=QHscendata(sc).data2{1,zj};
                    QHtemp=temp(temp(:,1)==VakID,:); % alleen dit vak
                    QHtemp = hpdf_addQ(QHtemp,sc); % waterstand bij laatste afvoerniveau extrapoleren 
                else % maatregelen (QdH)
                    temp = QHscendata(sc).data2{1,zj};
                    temp=temp(temp(:,1)==VakID,:); % alleen dit vak
                    temp = hpdf_addQ(temp,sc);      % effect bij laatste afvoerniveau doortrekken              
                    if isempty(temp)
                        disp(['Warning: geen QdH waarde ' num2str(vakken(v)) ', QdH ID ' num2str(sc)])
                        fileID = fopen(logfile,'a');
                        fprintf(fileID,'%s\n',['Warning: geen QdH waarde vak ' num2str(VakID) ', QdH ID ' num2str(sc)]);
                        fclose(fileID);
                    else
                        QHtemp(:,3)=QHtemp(:,3)+temp(:,3); % dh van maatregel erbij tellen
                    end
                end
            end
            QH_zj.zichtjaar(zj)=QHscendata(sc).zichtjaar(zj);
            QH_zj.data{1,zj}=QHtemp;
        end

        %% QH relatie incl. maatregelen interpoleren naar zichtjaar
        % bepaal jaren waartussen geinterpoleerd moet worden
        ind1=find(QH_zj.zichtjaar<=zichtjaar,1,'last');  % indices van vorige zichtjaar
        ind2=find(QH_zj.zichtjaar>zichtjaar,1,'first'); % indices van volgende zichtjaar
        if isempty(ind1) && ~isempty(ind2)
            % geen QH op kleiner zichtjaar, neem twee volgende zichtjaren
            ind1=ind2;
            ind2=find(QH_zj.zichtjaar>zichtjaar,2,'first'); % indices van volgende zichtjaar
            ind2=ind2(2);
        elseif isempty(ind2) && ~isempty(ind1)
            % geen QH op groter zichtjaar, neem twee eerdere zichtjaren
            ind2=ind1;
            ind1=find(QH_zj.zichtjaar<=zichtjaar,2,'last'); % indices van volgende zichtjaar
            ind1=ind1(1);
        end
        t1 = QH_zj.zichtjaar(1,ind1);
        t2 = QH_zj.zichtjaar(1,ind2);
        tmp1 = QH_zj.data{1,ind1};
        tmp2 = QH_zj.data{1,ind2};
        Q1 = tmp1(tmp1(:,1)==VakID,2);
        Q2 = tmp2(tmp2(:,1)==VakID,2);
        H1 = tmp1(tmp1(:,1)==VakID,3);
        H2 = tmp2(tmp2(:,1)==VakID,3);
        if ~Q1==Q2
            disp('error: afvoerniveaus in QH zijn niet gelijk')
            fileID = fopen(logfile,'a');
            fprintf(fileID,'%s\n',['ERROR: afvoerniveaus in QH relatie vak ' num2str(VakID) ' zijn niet gelijk']);
            fclose(fileID);
        end
        QH_vak_t(:,1) = Q1;
        for i = 1:length(Q1)
            QH_vak_t(i,2) = interp1([t1 t2],[H1(i) H2(i)],zichtjaar,'linear','extrap'); % interpoleren van de onderschrijdingskansen:
        end

        %% bodemdaling en toeslagstoeslag toevoegen
        ind                 = find(db_bodemdaling(:,1)==VakID);
        if isempty(ind)
            bd_snelheid         = 0;            % [m per jaar]
        else
            bd_snelheid         = db_bodemdaling(ind(1),2);            % [m per jaar]
        end
        bodemdaling         = bd_snelheid*(zichtjaar-invoer.refjaar_bodemdaling);     % [m]
        QH_vak_t(:,2)       = QH_vak_t(:,2)+bodemdaling+toeslag;

        %% afvoer cdf en QH relatie combineren tot waterstand cdf
        h_int = interp1(QH_vak_t(:,1),QH_vak_t(:,2),Q_cdf_t(:,1),'linear','extrap') ;
%        if issorted(h_int) % ALS monotoon stijgend
%            H_cdf_vak_t(:,1) = h_int; % waterstanden die horen bij Q1-Q9 en afvoeren daartussen
%            H_cdf_vak_t(:,2) = Q_cdf_t(:,2); % onderschrijdingskans van Q1-Q9 en afvoeren daartussen
%        else % ALS niet monotoon stijgend
            % CDF >> PDF afvoer met voorwaartse numerieke afgeleide
            n = length(Q_cdf_t(:,1));
            Q_pdf_t(:,1) = Q_cdf_t(:,1);
            for i = 1:n-1
                Q_pdf_t(i,2) = ((Q_cdf_t(i+1,2))-(Q_cdf_t(i,2)))/(Q_cdf_t(i+1,1)-Q_cdf_t(i,1));
            end
            % Loglineair extrapoleren voor laatste punt
            if Q_pdf_t(n-1,2)<=0 % als laatste waarde 0 is kan niet loglineair worden geextrapoleerd
                Q_pdf_t(n,2) = 0;
            else
                Q_pdf_t(n,2) = 10^interp1(Q_pdf_t(n-2:n-1,1),log10(Q_pdf_t(n-2:n-1,2)),Q_pdf_t(n,1),'linear','extrap');
            end
            
            % indicatorfunctie
            h_int_s = unique(sort(h_int));
%            h_int_s = (min(h_int):mean(abs(diff(h_int))):max(h_int))';
            IND = false(length(h_int_s),length(h_int));
            pov = zeros(length(h_int_s),1);
            for hi = 1:length(h_int_s)
                IND(hi,:) = h_int>h_int_s(hi); % alleen deel wat niet wordt overschreden
                Q_pdf_t_2(:,1) = Q_pdf_t(:,1);
                Q_pdf_t_2(:,2) = IND(hi,:)'.*Q_pdf_t(:,2); 
                pov(hi) = trapz(Q_pdf_t_2(:,1),Q_pdf_t_2(:,2)); %overschrijdingskans
                clear Q_pdf_t_2
            end
            H_cdf_vak_t = h_int_s; % waterstanden die horen bij Q1-Q9 en afvoeren daartussen
            H_cdf_vak_t(:,2) = 1-pov; % onderschrijdingskans van Q1-Q9 en afvoeren daartussen
%        end

        %% CDF >> PDF waterstand met voorwaartse numerieke afgeleide
        n = length(H_cdf_vak_t(:,1));
%         H_pdf_vak_t(:,1) = H_cdf_vak_t(:,1);
        H_pdf_vak_t = H_cdf_vak_t(:,1);
        for i = 1:n-1
            H_pdf_vak_t(i,2) = ((H_cdf_vak_t(i+1,2))-(H_cdf_vak_t(i,2)))/(H_cdf_vak_t(i+1,1)-H_cdf_vak_t(i,1));
        end

        % uitschieters verwijderen
%         ind = find(H_pdf_vak_t(2:end-1,2)>invoer.n_hpdf_filter*H_pdf_vak_t(1:end-2,2) & H_pdf_vak_t(2:end-1,2)>invoer.n_hpdf_filter*H_pdf_vak_t(3:end,2))+1;
%         if ~isempty(ind)
%             for a = 1:length(ind)
%                 nieuw=mean([H_pdf_vak_t(ind(a)-1,2),H_pdf_vak_t(ind(a)+1,2)]);
%                 diff = H_pdf_vak_t(ind(a),2) - nieuw;
%                 H_pdf_vak_t(ind(a),2)=nieuw;
%                 H_pdf_vak_t(ind(a)-1,2)=H_pdf_vak_t(ind(a)-1,2)+0.5*diff;
%                 H_pdf_vak_t(ind(a)+1,2)=H_pdf_vak_t(ind(a)+1,2)+0.5*diff;
%             end
%         end
        
        % nan waarde vervangen door 0 (komt door horizontale cdf)
        H_pdf_vak_t(isnan(H_pdf_vak_t(:,2)),2)=0.000000000001;
        
        % Loglineair extrapoleren voor laatste punt
        if H_pdf_vak_t(n-1,2)<=0 % als laatste waarde 0 is kan niet loglineair worden geextrapoleerd
            H_pdf_vak_t(n,2) = 0;
        else
            H_pdf_vak_t(n,2) = 10^interp1(H_pdf_vak_t(n-2:n-1,1),log10(H_pdf_vak_t(n-2:n-1,2)),H_pdf_vak_t(n,1),'linear','extrap');
        end
        H_pdf_vak_t(H_pdf_vak_t(:,2)<0,2) = 0;

        % omrekenen naar cdf
        n = length(H_pdf_vak_t(:,1));
        H_cdf_vak_t2 = H_pdf_vak_t;
        for i = 1:n-1
            H_cdf_vak_t2(i,2) = 1-trapz(H_pdf_vak_t(i:end,1),H_pdf_vak_t(i:end,2));
        end
        H_cdf_vak_t2(i+1,2) = interp1(H_cdf_vak_t2(i-1:i,1),H_cdf_vak_t2(i-1:i,2),H_cdf_vak_t2(i+1,1),'linear','extrap');
                
        %% waterstand voor bewezen sterkte berekenen
        index = find(H_cdf_vak_t2(:,2)<1 & H_cdf_vak_t2(:,2)>=0);
%         H_freq_vak_t(:,1) = h_int(index);
        H_freq_vak_t(:,1) = H_cdf_vak_t2(index,1);
        H_freq_vak_t(:,2) = -log(H_cdf_vak_t2(index,2));
        [~, ia, ~] = unique(H_freq_vak_t(:,2));
        H_freq_vak_t = H_freq_vak_t(ia,:);
        HYD.H_bs_h = interp1(H_freq_vak_t(:,2),H_freq_vak_t(:,1),invoer.db_freq_bs_h,'linear','extrap');
        HYD.H_bs_s = interp1(H_freq_vak_t(:,2),H_freq_vak_t(:,1),invoer.db_freq_bs_s,'linear','extrap');
        HYD.H_bs_p = interp1(H_freq_vak_t(:,2),H_freq_vak_t(:,1),invoer.db_freq_bs_p,'linear','extrap');

        HYD.h_pdf = H_pdf_vak_t;
        HYD.H_freq_vak_t = H_freq_vak_t;
        HYD.Q_cdf_t = Q_cdf_t;

    elseif strcmp(invoer.belastingtype,'HT')
        % Let op: functionaliteit nog niet goed getest!
        % -waterstandsfrequentielijn loglineair interpoleren naar zichtjaar
        % -Bodemdaling en onzekerheidstoeslag toevoegen
        % -waterstandsfrequentielijn naar PDF van lokale waterstand
        % -PDF van lokale waterstand discretiseren naar 1cm stapgrootte
        % -Grenswaarde waterstand voor bewezen sterkte berekenen

        HTscendata = HYD.HTscendata;

        %% waterstandsfrequentielijn interpoleren naar zichtjaar
        % bepaal vigerende HTscenario
        for i=1:length(HTscendata)
            jaren(i) = HTscendata(i).Tstart;
        end
        sc = find(jaren<=jaar,1,'last');

        % bepaal jaren waartussen geinterpoleerd moet worden
        ind1=find(HTscendata(sc).zichtjaar<=zichtjaar,1,'last');  % indices van vorige zichtjaar
        ind2=find(HTscendata(sc).zichtjaar>zichtjaar,1,'first'); % indices van volgende zichtjaar
        if isempty(ind1) && ~isempty(ind2)
            % geen HT op kleiner zichtjaar, neem twee volgende zichtjaren
            ind1=ind2;
            ind2=find(HTscendata(sc).zichtjaar>zichtjaar,2,'first'); % indices van volgende zichtjaar
            ind2=ind2(2);
        elseif isempty(ind2) && ~isempty(ind1)
            % geen HT op groter zichtjaar, neem twee eerdere zichtjaren
            ind2=ind1;
            ind1=find(HTscendata(sc).zichtjaar<=zichtjaar,2,'last'); % indices van volgende zichtjaar
            ind1=ind1(1);
        end

        t1 = HTscendata(sc).zichtjaar(1,ind1);
        t2 = HTscendata(sc).zichtjaar(1,ind2);
        H1 = HTscendata(sc).data1{1,ind1}(HTscendata(sc).data1{1,ind1}(:,1)==VakID,2:end);
        H2 = HTscendata(sc).data1{1,ind2}(HTscendata(sc).data1{1,ind2}(:,1)==VakID,2:end);
        f1 = HTscendata(sc).data1{1,ind1}(1,2:end);
        f2 = HTscendata(sc).data1{1,ind2}(1,2:end);

        % controleren of frequenties gelijk zijn
        if ~f1==f2
            disp('error: frequentieniveaus in waterstandstatistiek (HT relaties) zijn niet gelijk')
            fileID = fopen(logfile,'a');
            fprintf(fileID,'%s\n',['ERROR: frequentieniveaus in waterstandstatistiek vak ' num2str(VakID) ' zijn niet gelijk']);
            fclose(fileID);
        end

        % interpolatie uitvoeren
        H_freq_t(:,1) = f1(1,:);
        for i = 1:length(f1)
            H_freq_t(i,2) = interp1([t1 t2],[H1(i) H2(i)],zichtjaar,'linear','extrap'); % interpoleren van de waterstanden:
        end

        %% bodemdaling en toeslagstoeslag toevoegen
        ind                 = find(HYD.db_bodemdaling(:,1)==VakID);
        if isempty(ind)
            bd_snelheid         = 0;            % [m per jaar]
        else
            bd_snelheid         = HYD.db_bodemdaling(ind(1),2);            % [m per jaar]
        end
        bodemdaling         = bd_snelheid*(zichtjaar-invoer.refjaar_bodemdaling);     % [m]
        H_freq_t(:,2)       = H_freq_t(:,2)+bodemdaling+toeslag;


        %% discretisatie frequentielijn
    %     %% waterstands pdf interpoleren tot kleine stapjes (0.01 m)
        wats = min(H_freq_t(:,2))-invoer.Hdisc_low:invoer.Hdisc_step:max(H_freq_t(:,2))+invoer.Hdisc_high;
        H_freq_t_interp(:,1) = wats;
        H_freq_t_interp(:,2) = 10.^interp1(H_freq_t(:,2),log10(H_freq_t(:,1)),wats,'linear','extrap');

        %% frequentielijn >> PDF waterstand met voorwaartse numerieke afgeleide
        n = length(H_freq_t_interp(:,2));
        H_pdf_vak_t(:,1) = H_freq_t_interp(:,1); % waterstanden
        H_cdf_vak_t(:,1) = exp(-H_freq_t_interp(:,2)); % ond.schr.kans = exp(-ov.schr.freq.)
        H_cdf_vak_t(:,2) = H_pdf_vak_t(:,1) ; % waterstanden
        for i = 1:n-1 % kansen
            H_pdf_vak_t(i,2) = ((H_cdf_vak_t(i+1,1))-(H_cdf_vak_t(i,1)))/(H_cdf_vak_t(i+1,2)-H_cdf_vak_t(i,2));
        end

        % Loglineair extrapoleren voor laatste punt
        if H_pdf_vak_t(n-1,2)<=0 % als laatste waarde 0 is kan niet loglineair worden geextrapoleerd
            H_pdf_vak_t(n,2) = 0;
        else
            H_pdf_vak_t(n,2) = 10^interp1(H_pdf_vak_t(n-2:n-1,1),log10(H_pdf_vak_t(n-2:n-1,2)),H_pdf_vak_t(n,1),'linear','extrap');
        end

        %% waterstand voor bewezen sterkte berekenen
        HYD.H_bs_h = interp1(H_freq_t_interp(:,2),H_freq_t_interp(:,1),invoer.db_freq_bs_h,'linear','extrap');
        HYD.H_bs_s = interp1(H_freq_t_interp(:,2),H_freq_t_interp(:,1),invoer.db_freq_bs_s,'linear','extrap');
        HYD.H_bs_p = interp1(H_freq_t_interp(:,2),H_freq_t_interp(:,1),invoer.db_freq_bs_p,'linear','extrap');

        HYD.h_pdf = H_pdf_vak_t;
        HYD.H_freq_vak_t = H_freq_t_interp;

    end
catch
    fileID = fopen(logfile,'a');
    fprintf(fileID,'%s\n',['ERROR in hydraulische belasting vak ' num2str(VakID)]);
    fclose(fileID);    
    HYD.h_pdf = [];
    HYD.H_freq_vak_t = [];
    HYD.Q_cdf_t = [];
end
end

