function exp4figs = save_export(VNKvakken,T,VAR,invoer,vaklengte,uitvoerdir)
% belangrijke data opslaan in een structure, die gebruikt kan worden voor
% post-processingscripts

exp4figs.VNKvakken      =VNKvakken;
exp4figs.T              =T;
exp4figs.T_opgave       =invoer.tijdopgave;
exp4figs.Pf_1_t         =VAR.Pf_1_t;
exp4figs.Pf_2_t         =VAR.Pf_2_t;
exp4figs.Pf_3_t         =VAR.Pf_3_t;
exp4figs.Pf_eis_1       =VAR.Pf_eis_1_v;
exp4figs.Pf_eis_2       =VAR.Pf_eis_2_v;
exp4figs.Pf_eis_3       =VAR.Pf_eis_3_v;
exp4figs.kosten_dijk_cw =VAR.kosten_dijk_cw;
exp4figs.kosten_dijk_ncw=VAR.kosten_dijk_ncw;
exp4figs.kosten_dijk    =VAR.kosten_dijk;
exp4figs.normtraject    =VAR.norm.normtraject;
exp4figs.normkans       =VAR.norm.normkans;
exp4figs.Pf_eis_1_data  =VAR.norm.Pf_eis_1_data;
exp4figs.Pf_eis_2_data  =VAR.norm.Pf_eis_2_data;
exp4figs.Pf_eis_3_data  =VAR.norm.Pf_eis_3_data;
% exp4figs.RMinzet        =RMinzet;
% exp4figs.RMid           =RMid;
exp4figs.vak_data       = VAR.norm.vak_data;

% exp4figs.dP             = min(invoer.pip_factor.*VAR.dP,VAR.afkapgrens*ones(1,length(T)));
% exp4figs.dS             = invoer.mac_factor.*VAR.dS;
% exp4figs.dH             = VAR.dH;
% exp4figs.dP_opg         = min(invoer.pip_factor.*VAR.dP_opg,VAR.afkapgrens*ones(1,length(T)));
% exp4figs.dS_opg         = invoer.mac_factor.*VAR.dS_opg;
% exp4figs.dH_opg         = VAR.dH_opg;
% alleen gecorrigeerde opgaves uitvoeren
exp4figs.dP             = VAR.dP_corr;
exp4figs.dS             = VAR.dS_corr;
exp4figs.dH             = VAR.dH_corr;
exp4figs.dP_opg         = VAR.dP_opg_corr;
exp4figs.dS_opg         = VAR.dS_opg_corr;
exp4figs.dH_opg         = VAR.dH_opg_corr;

exp4figs.vaklengte      = vaklengte;
exp4figs.ws_data        = VAR.ws_data;
exp4figs.akj_ht         = VAR.akj_ht;
exp4figs.akj_mac        = VAR.akj_mac;
exp4figs.akj_pip        = VAR.akj_pip;
% exp4figs.QdHjaren       = HYD.QdHjaren;
save([uitvoerdir 'exp4figs.mat'],'exp4figs')

end
