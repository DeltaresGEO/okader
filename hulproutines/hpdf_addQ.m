function QH = hpdf_addQ(QH,sc)
% afvoerniveau toevoegen om correcte extrapolatie van QdH te krijgen.

if sc==1 % referentie (QH)
    QH(end+1,1)     = QH(end,1);
    QH(end,2)       = QH(end-1,2)+10;
    QH(end,3)       = interp1(QH(1:end-1,2),QH(1:end-1,3),QH(end,2),'linear','extrap');
else % maatregelen (QdH)
    QH(end+1,1)     = QH(end,1);
    QH(end,2)       = QH(end-1,2)+10;
    QH(end,3)       = QH(end-1,3); % gelijk houden: parallel effect
end
