function [VAR] = bepaal_opgave_1(TMP,VAR,HYD,STERKTE,invoer,T,t,VNKvakken,v,link_to_figures,logfile,faalmechanisme)
% Deze functie bepaal_opgave_1.m bepaalt welke ontwerp-belastingsituatie
% gebruikt wordt voor het berekenen van de opgave (welke maatregelen, welk
% zichtjaar)
% 
% in functie bepaal_opgave_2.m wordt daadwerkelijk opgave bepaald aan de
% hand van opgegeven ontwerp-belastingsituatie
%
% daarnaast wordt de forcering van het dijkversterkingsschema aangepast als
% aan voorwaarden wordt voldaan.
%
% Ontwikkeld door HKV en Deltares in opdracht van RWS-WVL, maart 2017

if strcmp(TMP.flag,'nee') % niet versterken
    if t == 1
        VAR.dH(v,t)  = 0; % hoogte
        VAR.dS(v,t)  = 0; % stabiliteit
        VAR.dP(v,t)  = 0; % piping
    else
        VAR.dH(v,t)  = VAR.dH(v,t-1); % hoogte
        VAR.dS(v,t)  = VAR.dS(v,t-1); % stabiliteit
        VAR.dP(v,t)  = VAR.dP(v,t-1); % piping
    end
    VAR.kosten_dijk(v,t) = 0; % niet verdisconteerde kosten per vak
elseif strcmp(TMP.flag,'ja') % wel versterken
	% ontwerpbelasting bepalen
    ontw_duur = VAR.ontwerpduur(VAR.ontwerpduur(:,1)==VNKvakken(v),2);
    if invoer.winst_rvm == 1 % rekening houdend met rivierverruiming
        
        % versterkingsdimensie bepalen voor 3 varianten (a,b,c). Op
        % variant a (dimensies om aan einde ontwerplevensduur te voldoen) 
        i = 1; % initialisatie
        zichtjaar_a = T(t)+ontw_duur;
        sc_qh_a = find(HYD.QdHjaren<=zichtjaar_a); % index referentie en welke maatregelen worden meegenomen in belasting
        [dh(i),dberm(i),dkwel(i),opg_data(i).h_pdf,opg_data(i).H_freq_vak_t] = bepaal_opgave_2(HYD,T,t,zichtjaar_a,VNKvakken,v,sc_qh_a,TMP,invoer,STERKTE,link_to_figures,T(1),VAR,logfile,faalmechanisme);
        
        if zichtjaar_a>invoer.t_voldoen_norm
            % variant b (dimensies om ook in 2050 te voldoen)
            i=i+1;
            zichtjaar_b = invoer.t_voldoen_norm;
            sc_qh_b = find(HYD.QdHjaren<=zichtjaar_b); % index referentie en welke maatregelen worden meegenomen in belasting
            [dh(i),dberm(i),dkwel(i),opg_data(i).h_pdf,opg_data(i).H_freq_vak_t] = bepaal_opgave_2(HYD,T,t,zichtjaar_b,VNKvakken,v,sc_qh_b,TMP,invoer,STERKTE,link_to_figures,T(1),VAR,logfile,faalmechanisme);
        end
        
        % variant c (dimensies om na 2050 ieder jaar net v��r een rivierverruiming te voldoen)
        sc_qh = find(HYD.QdHjaren<=zichtjaar_a & HYD.QdHjaren>invoer.t_voldoen_norm); 
        for j=1:length(sc_qh) % alle maatregelen tot tussen 2050 en T+t_ontw
            i=i+1;
            zichtjaar_c = HYD.QdHjaren(sc_qh(j));
            sc_qh_c = find(HYD.QdHjaren<zichtjaar_c); % index referentie en welke maatregelen worden meegenomen in belasting
            [dh(i),dberm(i),dkwel(i),opg_data(i).h_pdf,opg_data(i).H_freq_vak_t] = bepaal_opgave_2(HYD,T,t,zichtjaar_c,VNKvakken,v,sc_qh_c,TMP,invoer,STERKTE,link_to_figures,T(1),VAR,logfile,faalmechanisme);
        end
        
        % variant met grootste versterking bepalen zodat dijk in alle
        % gevallen voldoet
        [dhm,imax] = max(dh);
        dsm = max(dberm);
        dpm = max(dkwel);
        
        % waterstanden van variant met maatgevende belasting opslaan 
        h_pdf = opg_data(imax).h_pdf;
        H_freq_vak_t = opg_data(imax).H_freq_vak_t;
        
    else %if strcmp(winst_rvm,'tijd')
        zichtjaar_wb = T(t)+ontw_duur;
        sc_qh = find(HYD.QdHjaren<T(t)); % index referentie en alleen maatregelen die al zijn uitgevoerd
        [dhm,dsm,dpm,h_pdf,H_freq_vak_t] = bepaal_opgave_2(HYD,T,t,zichtjaar_wb,VNKvakken,v,sc_qh,TMP,invoer,STERKTE,link_to_figures,T(1),VAR,logfile,faalmechanisme);
    end

    % dimensies mogen niet kleiner zijn dan in vorige tijdstap
    if t==1
        VAR.dH(v,t)  = dhm; % hoogte
        VAR.dS(v,t)  = dsm; % dijkbasis stabiliteit
        VAR.dP(v,t)  = dpm; % dijkbasis piping
    else
        VAR.dH(v,t)  = max(dhm,VAR.dH(v,t-1)); % hoogte
        VAR.dS(v,t)  = max(dsm,VAR.dS(v,t-1)); % dijkbasis stabiliteit
        VAR.dP(v,t)  = max(dpm,VAR.dP(v,t-1)); % dijkbasis piping
    end
    
    % belasting updaten met belastingen incl. maatregelen
    HYD.h_pdf=h_pdf;
    HYD.H_freq_vak_t=H_freq_vak_t;
    
    % plotten van figuur met frequentielijn ontwerpwaterstand
    if invoer.plot_pdf_wb == 1 || invoer.plot_pdf_wb == 3 
        plot_hpdf(T,t,HYD,VNKvakken,v,link_to_figures,'Ontwerpwaterstand','wb_ontwerp')
    end

    % updaten Dijkversterkingschema (faalkans mag onder voorwaarden tot 2050 tijdelijk hoger zijn dan norm)
    VAR.DVschema = update_DVschema(VAR.DVschema,HYD,T(t),ontw_duur,VNKvakken(v),invoer);                    
                    
end

% opgave corrigeren met factor en maximum
if size(VAR.corr_dP,2)>3 % informatie deklaag+L opgenomen
    data = VAR.corr_dP(VAR.corr_dP(:,1)==VNKvakken(v),4:6);
    if data(2)>4 && data(1)>0.5*VAR.dP(v,t) 
        pipfac=0.2;
    elseif data(2)>4
        pipfac=0.75;
    elseif data(1)>0.5*VAR.dP(v,t)
        pipfac=0.75;
    else
        pipfac=1;
    end
else
    pipfac = VAR.corr_dP(VAR.corr_dP(:,1)==VNKvakken(v),3);
end
macfac = VAR.corr_dS(VAR.corr_dS(:,1)==VNKvakken(v),3);
htfac = VAR.corr_dH(VAR.corr_dH(:,1)==VNKvakken(v),3);
pipmax = VAR.corr_dP(VAR.corr_dP(:,1)==VNKvakken(v),2);
macmax = VAR.corr_dS(VAR.corr_dS(:,1)==VNKvakken(v),2);
htmax = VAR.corr_dH(VAR.corr_dH(:,1)==VNKvakken(v),2);
VAR.dH_corr(v,t) = max(0,min(htfac.*VAR.dH(v,t),htmax,'includenan'),'includenan');
VAR.dS_corr(v,t) = max(0,min(macfac.*VAR.dS(v,t),macmax,'includenan'),'includenan');
VAR.dP_corr(v,t) = max(0,min(pipfac.*VAR.dP(v,t),pipmax,'includenan'),'includenan');
VAR.htfac(v,t) = htfac;
VAR.macfac(v,t) = macfac;
VAR.pipfac(v,t) = pipfac;

end
