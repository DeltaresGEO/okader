function plot_profiel_v1(VAR,T,t1,VNKvakken,v33,profieldata,link_to_figures)
%
%
% Ontwikkeld door HKV en Deltares in opdracht van RWS-WVL, maart 2017

try
    % dH,dS,dP,t,vak,link_to_prof_data,link_to_figures
    % Zover is het aantal RP minimaal 3 en maximaal 6 (met buitenberm) 
    % Dummy data
    %close all; clear; clc;
    real_prof_ok = 0;
    dP_data = VAR.dP_cost;%[30 80]; %30 %80
    dH_data = VAR.dH_cost;%[0.5 1];
    dS_data = VAR.dS_cost;%[5 20]; %5 %20
    t = T(t1);
    met_sloot = 0;
    incl_buitenwaarts = 1;
    incl_binnenwaarts = 1;
    const = VAR.check_const(v33);
    %voorbeeld geometrie met 5 rp: 41003022
    %voorbeeld geometrie met 6 rp: ????
    %voorbeeld geometrie met 4 rp: 48001052
    %voorbeeld geometrie met 3 rp: 41003020 of 48001018
    vak_data = VNKvakken(v33);
    for i = 1:length(vak_data)%[132 133 134 135 138 158 188 274 296]%1:37%38:length(vak_data)
        overslaan  = 0;
        for j = 1:length(dH_data)
            for k = 1:length(dS_data)
                for l = 1:length(dP_data)
    % Data read
    vak = vak_data(i);
    dP = dP_data(l);
    if isnan(dP)
        dP = 0;
    end
    dH = dH_data(j);
    if isnan(dH)
        dH = 0;
    end
    dS = dS_data(k);
    if isnan(dS)
        dS = 0;
    end
    vak_str = num2str(vak);
    if length(vak_str) >= 8
        DR = vak_str(1:2);
    else
        DR = vak_str(1);
    end
    index = find(profieldata(1).dijkringen==str2double(DR));
    temp1 = profieldata(index).P;
    x_data_p_temp = temp1(temp1(:,2) == vak,4);
    y_data_p_temp = temp1(temp1(:,2) == vak,5);
    temp1 = profieldata(index).RP;
    x_data_rp_temp = temp1(temp1(:,2) == vak,4);
    y_data_rp_temp = temp1(temp1(:,2) == vak,5);
    x_data_rp1 = x_data_rp_temp(x_data_rp_temp > -100000);
    y_data_rp1 = y_data_rp_temp(x_data_rp_temp > -100000);
    [x_data_rp,ib] = sort(x_data_rp1);
    y_data_rp = y_data_rp1(ib);
    % controleer x_data_p en y_data_p
    [x_data_p,ia] = sort(x_data_p_temp);
    y_data_p = y_data_p_temp(ia);
    [x_data_p,ia] = unique(x_data_p);
    y_data_p = y_data_p(ia);
    x_data_p = x_data_p(x_data_p>-100000);
    y_data_p = y_data_p(x_data_p>-100000);
    x_data_p_ori = x_data_p;
    y_data_p_ori = y_data_p;

    if vak == 16003014
        x1 = x_data_rp(end)+(x_data_rp(end)-x_data_rp(end-1));
        dx = x1-x_data_rp(end-1);
        if dx < 3
            x1 = x1+(3-dx);
        end
        y1 = y_data_rp(end-1);
        x2 = x1+(x_data_rp(end-1)-x_data_rp(1));
        y2 = y_data_rp(1);
        x_data_p = [x_data_rp(1)-80 ; x_data_rp ; x1 ; x2 ; x2+80]; 
        y_data_p = [y_data_rp(1) ; y_data_rp ; y1 ; y2 ; y2];
    elseif vak == 41006001
        x1 = x_data_rp(end)+(x_data_rp(end)-x_data_rp(end-1));
        dx = x1-x_data_rp(end-1);
        if dx < 3
            x1 = x1+(3-dx);
        end
        y1 = y_data_rp(end-1);
        x2 = x1+(x_data_rp(end-1)-x_data_rp(1));
        y2 = y_data_rp(1);
        x_data_p = [x_data_rp(1)-40 ; x_data_rp ; x1 ; x2 ; x2+40]; 
        y_data_p = [y_data_rp(1) ; y_data_rp ; y1 ; y2 ; y2];
    elseif vak == 43003030
        x1 = x_data_rp(end)+(x_data_rp(end)-x_data_rp(end-1));
        dx = x1-x_data_rp(end-1);
        if dx < 3
            x1 = x1+(3-dx);
        end
        y1 = y_data_rp(end-1);
        x2 = x1+(x_data_rp(end-1)-x_data_rp(1));
        y2 = y_data_rp(1);
        x_data_p = [x_data_rp(1)-80 ; x_data_rp ; x1 ; x2 ; x2+80]; 
        y_data_p = [y_data_rp(1) ; y_data_rp ; y1 ; y2 ; y2];
    elseif vak == 43003031
        x1 = x_data_rp(end)+(x_data_rp(end)-x_data_rp(end-1));
        dx = x1-x_data_rp(end-1);
        if dx < 3
            x1 = x1+(3-dx);
        end
        y1 = y_data_rp(end-1);
        x2 = x1+(x_data_rp(end-1)-x_data_rp(1));
        y2 = y_data_rp(1);
        x_data_p = [x_data_rp(1)-80 ; x_data_rp ; x1 ; x2 ; x2+80]; 
        y_data_p = [y_data_rp(1) ; y_data_rp ; y1 ; y2 ; y2];
    elseif vak == 43001041
        x_data_rp = x_data_rp(end-2:end);
        y_data_rp = y_data_rp(end-2:end);
        x_data_p = x_data_p(2:end);
        y_data_p = y_data_p(2:end);
    end

    if (~isempty(x_data_p) && ~isempty(y_data_p)) && (length(x_data_p) ~= length(x_data_rp))
    % controleer y_data_p(end) en y_data_rp(end)
    while 1
        dfg = abs(y_data_rp(end)-y_data_p(end))/y_data_rp(end)*100;
        if dfg > 5
            break;
        else
            x_data_p = x_data_p(1:end-1);
            y_data_p = y_data_p(1:end-1);
        end
    end
    % Data voor initiele profiel
    xmin = min(x_data_p); xmax = max(x_data_p);
    xxd = linspace(xmin,xmax,1000);
    y_data_p = interp1(x_data_p,y_data_p,xxd','linear','extrap');
    x_data_p = xxd';
    xx = x_data_p(x_data_p>x_data_rp(end));
    yy = y_data_p(x_data_p>x_data_rp(end));
    if vak == 38001027 || vak == 38001028 || vak == 38001029 || vak == 38001030 ...
            || vak == 38001033 || vak == 38003014
        df = x_data_p(x_data_p>x_data_rp(end));
        dfg = (df(1)+x_data_rp(end))/2;
        temp1 = [x_data_rp ; dfg];
        x_data_rp = temp1;
        temp1 = [y_data_rp ; y_data_rp(end)];
        y_data_rp = temp1;
    end
    if length(x_data_rp) <= 2
        ff1 = find(x_data_p>x_data_rp(1));
        ff2 = find(x_data_p<=x_data_rp(end));
        ff3 = intersect(ff1,ff2);
        yg = y_data_p(ff3);
        xg = x_data_p(ff3);
        dy = abs(y_data_rp(end)-yg);
        x1 = xg(dy == min(dy));
        if length(x1) > 1
            x1 = x1(1);
        end
        y1 = yg(xg == x1);
        ff1 = find(x_data_p_ori>x1);
        xg = x_data_p_ori(ff1);
        yg = y_data_p_ori(ff1);
        [yg1,ia] = unique(yg);
        xg1 = xg(ia);
        hj = length(xg1);
        df = 0;
        while 1
            x3 = interp1(yg1,xg1,y1,'nearest');
            if isnan(x3)
                dfmin = abs(y1-min(yg1));
                dfmax = abs(y1-max(yg1));
                if dfmin <= dfmax
                    x3 = xg1(1);
                else
                    x3 = xg1(end);
                end
            end
            y3t = interp1(xg,yg,x3,'linear');
            ghj = find(yg == y3t);
            if length(ghj) > 1
                y3t = y3t(1);
            end
            if x3-x1>3 || df == hj
                y3 = y3t;
                break;
            else
                xg1 = xg1(xg1~=x3);
                yg1 = yg1(xg1~=x3);
                df = df+1;
            end
        end
        x2 = (x1+x3)/2;
        y2 = max([y1 y3]);
        x_data_rp = [x_data_rp(1) ; x1 ; x2];
        y_data_rp = [y_data_rp(1) ; y1 ; y2];
        gfgf = 1;
    else
        gfgf = 0;
    end

    if length(x_data_rp) == 3
        hkb = x_data_rp(end)-x_data_rp(end-1); % halve kruin breedte [m]
        hkh = y_data_rp(end)-y_data_rp(end-1); % ophooging op kruin [m]
        hd_bu = y_data_rp(end-1)-y_data_rp(1); % dijkhoogte t.o.v. buitenteen [m]
        hd_bi = y_data_rp(end)-y_data_p(end)-hkh; % dijkhoogte t.o.v. binnenteen [m], aangenomen
        he_bu = abs(hd_bu)/abs(x_data_rp(end-1)-x_data_rp(1)); % hellingbuitentalud v:h [-]
        y_bik = y_data_rp(end)-hkh;
        dy = abs(yy-y_bik);
        x_bik = xx(dy == min(dy));
        if length(x_bik) > 1
            x_bik = x_bik(end);
        end
        kb = x_bik-x_data_rp(end-1);
        fg1 = find(x_data_p>x_data_rp(end));
        fg2 = find(x_data_p<(x_bik+x_data_p(end))*1/2);
        fg3 = intersect(fg1,fg2);
        xx1 = x_data_p(fg3);
        yy1 = y_data_p(fg3);
        y_bit = min(y_data_p);%y_data_p(end);
        dy = abs(yy1-y_bit);
        x_bit = xx1(dy == min(dy));
        if length(x_bit) > 1
            x_bit = x_bit(1);
        end
        di = sqrt((x_bit-xx1).^2+(y_bit-yy1).^2);
        dmin = min(di);
        if length(dmin) > 1
            dmin = dmin(end);
        end
        y_bit = yy1(di == dmin);
        x_bit = xx1(di == dmin);
        hd_bi = y_data_rp(end)-y_bit-hkh;
        he_bi = abs(hd_bi)/abs(x_bit-x_bik); % hellingbinnentalud v:h [-]
    elseif length(x_data_rp) == 4
        hkb = x_data_rp(end)-x_data_rp(end-1); % halve kruin breedte [m]
        hkh = y_data_rp(end)-y_data_rp(end-1); % ophooging op kruin [m]
        hd_bu = y_data_rp(end-1)-y_data_rp(2); % dijkhoogte t.o.v. buitenteen [m]
        hd_bi = y_data_rp(end)-y_data_p(end)-hkh; % dijkhoogte t.o.v. binnenteen [m], aangenomen
        he_bu = abs(hd_bu)/abs(x_data_rp(end-1)-x_data_rp(end-2)); % hellingbuitentalud v:h [-]
        y_bik = y_data_rp(end)-hkh;
        dy = abs(yy-y_bik);
        x_bik = xx(dy == min(dy));
        if length(x_bik) > 1
            x_bik = x_bik(end);
        end
        kb = x_bik-x_data_rp(end-1);
        fg1 = find(x_data_p>x_data_rp(end));
        fg2 = find(x_data_p<(x_bik+x_data_p(end))*1/2);
        fg3 = intersect(fg1,fg2);
        xx1 = x_data_p(fg3);
        yy1 = y_data_p(fg3);
        y_bit = min(y_data_p);%y_data_p(end);
        dy = abs(yy1-y_bit);
        x_bit = xx1(dy == min(dy));
        if length(x_bit) > 1
            x_bit = x_bit(1);
        end
        di = sqrt((x_bit-xx1).^2+(y_bit-yy1).^2);
        dmin = min(di);
        if length(dmin) > 1
            dmin = dmin(end);
        end
        y_bit = yy1(di == dmin);
        x_bit = xx1(di == dmin);
        hd_bi = y_data_rp(end)-y_bit-hkh;
        he_bi = abs(hd_bi)/abs(x_bit-x_bik); % hellingbinnentalud v:h [-]
    elseif length(x_data_rp) == 5
        hkb = x_data_rp(end)-x_data_rp(end-1); % halve kruin breedte [m]
        hkh = y_data_rp(end)-y_data_rp(end-1); % ophooging op kruin [m]
        hd_bu = y_data_rp(end-1)-y_data_rp(3); % dijkhoogte t.o.v. buitenteen [m]
        hd_bi = y_data_rp(end)-y_data_p(end)-hkh; % dijkhoogte t.o.v. binnenteen [m], aangenomen
        he_bu = abs(hd_bu)/abs(x_data_rp(end-1)-x_data_rp(end-2)); % hellingbuitentalud v:h [-]
        %he_bi = he_bu; % hellingbinnentalud v:h [-]
        y_bik = y_data_rp(end)-hkh;
        dy = abs(yy-y_bik);
        x_bik = xx(dy == min(dy));
        if length(x_bik) > 1
            x_bik = x_bik(end);
        end
        kb = x_bik-x_data_rp(end-1);
        fg1 = find(x_data_p>x_data_rp(end));
        fg2 = find(x_data_p<(x_bik+x_data_p(end))*1/2);
        fg3 = intersect(fg1,fg2);
        xx1 = x_data_p(fg3);
        yy1 = y_data_p(fg3);
        y_bit = min(y_data_p);%y_data_p(end);
        dy = abs(yy1-y_bit);
        x_bit = xx1(dy == min(dy));
        if length(x_bit) > 1
            x_bit = x_bit(1);
        end
        di = sqrt((x_bit-xx1).^2+(y_bit-yy1).^2);
        dmin = min(di);
        if length(dmin) > 1
            dmin = dmin(end);
        end
        y_bit = yy1(di == dmin);
        x_bit = xx1(di == dmin);
        hd_bi = y_data_rp(end)-y_bit-hkh;
        he_bi = abs(hd_bi)/abs(x_bit-x_bik); % hellingbinnentalud v:h [-]
    else
        if length(x_data_rp) <= 2
            %display(['niet mogelijk om profiel te maken, lengte van x_data_rp is <= 2, vak ' num2str(vak) ...
            %    ', dH = ' num2str(dH) ', dS = ' num2str(dS) ', dP = ' num2str(dP)]);
            overslaan = 1;
        elseif length(x_data_rp) > 5
            %display(['niet mogelijk om profiel te maken, lengte van x_data_rp is > 5, vak ' num2str(vak) ...
            %    ', dH = ' num2str(dH) ', dS = ' num2str(dS) ', dP = ' num2str(dP)]);
            overslaan = 2;
        end
    end
    else
        overslaan = 3;
    end
    if overslaan == 0
    % Aanvullende
    if length(x_data_rp) == 3
        dx_links = x_data_rp(1)-x_data_p(1);
        xref = x_data_p(1);
        yref = y_data_rp(1);
        xref1 = x_data_p(end);
        yref1 = y_data_p(end);
    elseif length(x_data_rp) == 4
        dx_links = x_data_rp(2)-x_data_p(1);
        xref = x_data_p(1);
        yref = y_data_rp(2);
        xref1 = x_data_p(end);
        yref1 = y_data_p(end);
    elseif length(x_data_rp) == 5
        dx_links = x_data_rp(3)-x_data_p(1);
        xref = x_data_p(1);
        yref = y_data_rp(3);
        xref1 = x_data_p(end);
        yref1 = y_data_p(end);
    else
        %display(['Fout bij het inlezen van data_rp (regel 171), lengte van x_data_rp is > 5, vak ' num2str(vak)]);
        %return;
    end
    dx_rechts = 5; % extensie x vanaf binnenteen [m]
    be_di = 1; % bermdikte voor piping [m], aangenomen
    be_he = 1/1; % hellingberm v:h [-], aangenomen
    if met_sloot == 1
        bi_te_s = 10; % afstand tussen binnenteen en sloot [m]
        sd = 1; % slot diepte t.o.v. binnenteen [m]
        he_sl = 1/1; % v:h [-]
        sb = 1; % slootbreedte [m]
    else
        bi_te_s = 0; % afstand tussen binnenteen en sloot [m]
        sd = 0; % slot diepte t.o.v. binnenteen [m]
        he_sl =1/1; % v:h [-]
        sb = 0; % slootbreedte [m]
    end

    %% Creating data voor initiele profiel
    if length(x_data_rp) == 3
        if incl_buitenwaarts == 0
            xrefb = xref;
            yrefb = yref;
        else
            xrefb = x_data_p(x_data_p < xref+dx_links)';
            yrefb = y_data_p(x_data_p < xref+dx_links)';
        end
        if incl_binnenwaarts == 1
            xrefb1 = x_data_p(x_data_p > x_data_rp(end-1)+kb)';
            yrefb1 = y_data_p(x_data_p > x_data_rp(end-1)+kb)';
        end
        if incl_binnenwaarts == 0
            x_prof_ini = [...
                xrefb ... %begin x
                xref+dx_links ... % t/m buitenteen
                xref+dx_links+hd_bu/he_bu ... % t/m buitenkruin
                xref+dx_links+hd_bu/he_bu+hkb ... % t/m halve kruin
                xref+dx_links+hd_bu/he_bu+kb ... % t/m binnenkruin
                xref+dx_links+hd_bu/he_bu+kb+hd_bi/he_bi ... % t/m binnenteen
                xref1+bi_te_s ... % t/m slootlinks
                xref1+bi_te_s+sd/he_sl ...
                xref1+bi_te_s+sd/he_sl+sb ...
                xref1+bi_te_s+sd/he_sl+sb+sd/he_sl ... % t/m slootrechts
                xref1+bi_te_s+sd/he_sl+sb+sd/he_sl+dx_rechts...
                ];
            if x_prof_ini(end)<xref1
                x_prof_ini(end) = xref1;
            end
            x_sloot_links = xref1+bi_te_s;
            y_prof_ini = [...
                yrefb ... %begin y
                yref ... % t/m buitenteen
                yref+hd_bu ... % t/m buitenkruin
                yref+hd_bu+hkh ... % t/m halve kruin
                yref+hd_bu ... % t/m binnenkruin
                yref+hd_bu-hd_bi ... % t/m binnenteen
                yref+hd_bu-hd_bi ... % t/m slootlinks 
                yref+hd_bu-hd_bi-sd ...
                yref+hd_bu-hd_bi-sd ...
                yref+hd_bu-hd_bi ... % t/m slootrechts
                yref+hd_bu-hd_bi...
                ];
        else
            x_prof_ini = [...
                xrefb ... %begin x
                xref+dx_links ... % t/m buitenteen
                xref+dx_links+hd_bu/he_bu ... % t/m buitenkruin
                xref+dx_links+hd_bu/he_bu+hkb ... % t/m halve kruin
                xref+dx_links+hd_bu/he_bu+kb ... % t/m binnenkruin
                xrefb1 ... % t/m binnenteen
                xref1+bi_te_s ... % t/m slootlinks
                xref1+bi_te_s+sd/he_sl ...
                xref1+bi_te_s+sd/he_sl+sb ...
                xref1+bi_te_s+sd/he_sl+sb+sd/he_sl ... % t/m slootrechts
                xref1+bi_te_s+sd/he_sl+sb+sd/he_sl+dx_rechts...
                ];
            x_sloot_links = xref1+bi_te_s;
            y_prof_ini = [...
                yrefb ... %begin y
                yref ... % t/m buitenteen
                yref+hd_bu ... % t/m buitenkruin
                yref+hd_bu+hkh ... % t/m halve kruin
                yref+hd_bu ... % t/m binnenkruin
                yrefb1 ...
                yref1 ... % t/m slootlinks
                yref1-sd ...
                yref1-sd ...
                yref1 ... % t/m slootrechts
                yref1...
                ];
        end

    elseif length(x_data_rp) == 4
        if incl_buitenwaarts == 0
            xrefb = xref;
            yrefb = yref;
        else
            xrefb = x_data_p(x_data_p < xref+dx_links)';
            yrefb = y_data_p(x_data_p < xref+dx_links)';
        end
        if incl_binnenwaarts == 1
            xrefb1 = x_data_p(x_data_p > x_data_rp(end-1)+kb)';
            yrefb1 = y_data_p(x_data_p > x_data_rp(end-1)+kb)';
        end
        if incl_binnenwaarts == 0
            x_prof_ini = [...
                xrefb ... %begin x
                xref+dx_links ... % t/m buitenteen
                xref+dx_links+hd_bu/he_bu ... % t/m buitenkruin
                xref+dx_links+hd_bu/he_bu+hkb ... % t/m halve kruin
                xref+dx_links+hd_bu/he_bu+kb ... % t/m binnenkruin
                xref+dx_links+hd_bu/he_bu+kb+hd_bi/he_bi ... % t/m binnenteen
                xref1+bi_te_s ... % t/m slootlinks
                xref1+bi_te_s+sd/he_sl ...
                xref1+bi_te_s+sd/he_sl+sb ...
                xref1+bi_te_s+sd/he_sl+sb+sd/he_sl ... % t/m slootrechts
                xref1+bi_te_s+sd/he_sl+sb+sd/he_sl+dx_rechts...
                ];
            if x_prof_ini(end)<xref1
                x_prof_ini(end) = xref1;
            end
            x_sloot_links = xref1+bi_te_s;
            y_prof_ini = [...
                yrefb ... %begin y
                yref ... % t/m buitenteen
                yref+hd_bu ... % t/m buitenkruin
                yref+hd_bu+hkh ... % t/m halve kruin
                yref+hd_bu ... % t/m binnenkruin
                yref+hd_bu-hd_bi ... % t/m binnenteen
                yref+hd_bu-hd_bi ... % t/m slootlinks 
                yref+hd_bu-hd_bi-sd ...
                yref+hd_bu-hd_bi-sd ...
                yref+hd_bu-hd_bi ... % t/m slootrechts
                yref+hd_bu-hd_bi...
                ];
        else
            x_prof_ini = [...
                xrefb ... %begin x
                xref+dx_links ... % t/m buitenteen
                xref+dx_links+hd_bu/he_bu ... % t/m buitenkruin
                xref+dx_links+hd_bu/he_bu+hkb ... % t/m halve kruin
                xref+dx_links+hd_bu/he_bu+kb ... % t/m binnenkruin
                xrefb1 ... % t/m binnenteen
                xref1+bi_te_s ... % t/m slootlinks
                xref1+bi_te_s+sd/he_sl ...
                xref1+bi_te_s+sd/he_sl+sb ...
                xref1+bi_te_s+sd/he_sl+sb+sd/he_sl ... % t/m slootrechts
                xref1+bi_te_s+sd/he_sl+sb+sd/he_sl+dx_rechts...
                ];
            x_sloot_links = xref1+bi_te_s;
            y_prof_ini = [...
                yrefb ... %begin y
                yref ... % t/m buitenteen
                yref+hd_bu ... % t/m buitenkruin
                yref+hd_bu+hkh ... % t/m halve kruin
                yref+hd_bu ... % t/m binnenkruin
                yrefb1 ...
                yref1 ... % t/m slootlinks
                yref1-sd ...
                yref1-sd ...
                yref1 ... % t/m slootrechts
                yref1...
                ];
        end

    elseif length(x_data_rp) == 5
        if incl_buitenwaarts == 0
            xrefb = xref;
            yrefb = yref;
        else
            xrefb = x_data_p(x_data_p < xref+dx_links)';
            yrefb = y_data_p(x_data_p < xref+dx_links)';
        end
        if incl_binnenwaarts == 1
            xrefb1 = x_data_p(x_data_p > x_data_rp(end-1)+kb)';
            yrefb1 = y_data_p(x_data_p > x_data_rp(end-1)+kb)';
        end
        if incl_binnenwaarts == 0
            x_prof_ini = [...
                xrefb ... %begin x
                xref+dx_links ... % t/m buitenteen
                xref+dx_links+hd_bu/he_bu ... % t/m buitenkruin
                xref+dx_links+hd_bu/he_bu+hkb ... % t/m halve kruin
                xref+dx_links+hd_bu/he_bu+kb ... % t/m binnenkruin
                xref+dx_links+hd_bu/he_bu+kb+hd_bi/he_bi ... % t/m binnenteen
                xref1+bi_te_s ... % t/m slootlinks
                xref1+bi_te_s+sd/he_sl ...
                xref1+bi_te_s+sd/he_sl+sb ...
                xref1+bi_te_s+sd/he_sl+sb+sd/he_sl ... % t/m slootrechts
                xref1+bi_te_s+sd/he_sl+sb+sd/he_sl+dx_rechts...
                ];
            if x_prof_ini(end)<xref1
                x_prof_ini(end) = xref1;
            end
            x_sloot_links = xref1+bi_te_s;
            y_prof_ini = [...
                yrefb ... % begin y
                yref ... % t/m buitenteen
                yref+hd_bu ... % t/m buitenkruin
                yref+hd_bu+hkh ... % t/m halve kruin
                yref+hd_bu ... % t/m binnenkruin
                yref+hd_bu-hd_bi ... % t/m binnenteen
                yref+hd_bu-hd_bi ... % t/m slootlinks 
                yref+hd_bu-hd_bi-sd ...
                yref+hd_bu-hd_bi-sd ...
                yref+hd_bu-hd_bi ... % t/m slootrechts
                yref+hd_bu-hd_bi...
                ];
        else
            x_prof_ini = [...
                xrefb ... %begin x
                xref+dx_links ... % t/m buitenteen
                xref+dx_links+hd_bu/he_bu ... % t/m buitenkruin
                xref+dx_links+hd_bu/he_bu+hkb ... % t/m halve kruin
                xref+dx_links+hd_bu/he_bu+kb ... % t/m binnenkruin
                xrefb1 ... % t/m binnenteen
                xref1+bi_te_s ... % t/m slootlinks
                xref1+bi_te_s+sd/he_sl ...
                xref1+bi_te_s+sd/he_sl+sb ...
                xref1+bi_te_s+sd/he_sl+sb+sd/he_sl ... % t/m slootrechts
                xref1+bi_te_s+sd/he_sl+sb+sd/he_sl+dx_rechts...
                ];
            x_sloot_links = xref1+bi_te_s;
            y_prof_ini = [...
                yrefb ... % begin y
                yref ... % t/m buitenteen
                yref+hd_bu ... % t/m buitenkruin
                yref+hd_bu+hkh ... % t/m halve kruin
                yref+hd_bu ... % t/m binnenkruin
                yrefb1 ... % t/m binnenteen
                yref1 ... % t/m slootlinks 
                yref1-sd ...
                yref1-sd ...
                yref1 ... % t/m slootrechts
                yref1...
                ];
        end

    else
        %display(['Fout bij het maken van prof_ini, lengte van x_data_rp is > 5 bij vak ' num2str(vak)]);
        %return;
    end

    %% Creating profiel t.g.v. dH, dP en dS
    if length(x_data_rp) == 3
        x_bik_nu = xref+dx_links+(hd_bu+dH)/he_bu+kb;
        y_bik_nu = yref+(hd_bu+dH);
        d_x_bik = x_bik_nu-x_bik;
        dx = dH/he_bi;
        dx_tot = d_x_bik+dx;
        dS_nett = dS-dx_tot;
        if dS_nett<0
            dS_nett = 0;
        end
        dP_nett = dP-dS_nett-dx_tot;
        if dP_nett<0
            dP_nett = 0;
        end
        x_bit_nu1 = x_bit+dx_tot+dS_nett;
        x_bit_nu2 = x_bit+dx_tot+dS_nett+dP_nett;
        %x_rest = x_data_p(x_data_p > x_bit_nu1);
        %y_rest = y_data_p(x_data_p > x_bit_nu1);
        x_rest1 = x_prof_ini(x_prof_ini > x_bit_nu1);
        y_rest1 = y_prof_ini(x_prof_ini > x_bit_nu1);
        if x_sloot_links <= x_bit_nu1
            sd = 0;
        end
        if x_sloot_links <= x_bit_nu2
            sd = 0;
        end
        if xref1 < x_bit_nu2
            xref1 = x_bit_nu2;
        end
        if incl_binnenwaarts == 0
            if sd == 0
                dsf = x_prof_ini(end)-x_bit_nu1;
                if dsf<dP_nett
                    dsf = dP_nett;
                end
                x_prof_t_dHdSdP = [...
                    xrefb ...
                    xref+dx_links... % t/m buitenteen
                    xref+dx_links+(hd_bu+dH)/he_bu ... % t/m buitenkruin
                    xref+dx_links+(hd_bu+dH)/he_bu+hkb ... % t/m halve kruin
                    x_bik_nu ... % t/m binnenkruin
                    x_bit_nu1 ... % t/m binnenteen
                    x_bit_nu1+dsf ... % eindpunt
                    ];
                y_prof_t_dHdSdP = [...
                    yrefb ...
                    yref ... % t/m buitenteen
                    yref+(hd_bu+dH) ... % t/m buitenkruin
                    yref+(hd_bu+dH)+hkh ... % t/m halve kruin
                    y_bik_nu ... % t/m binnenkruin
                    y_bit ... % t/m binnenteen
                    y_bit... % eindpunt
                    ];
            else
                dsf = x_sloot_links-x_bit_nu1;
                x_prof_t_dHdSdP = [...
                    xrefb ...
                    xref+dx_links... % t/m buitenteen
                    xref+dx_links+(hd_bu+dH)/he_bu ... % t/m buitenkruin
                    xref+dx_links+(hd_bu+dH)/he_bu+hkb ... % t/m halve kruin
                    x_bik_nu ... % t/m binnenkruin
                    x_bit_nu1 ... % t/m binnenteen
                    x_bit_nu1+dsf ... % t/m slootlinks
                    x_bit_nu1+dsf+sd/he_sl ...
                    x_bit_nu1+dsf+sd/he_sl+sb ...
                    x_bit_nu1+dsf+sd/he_sl+sb+sd/he_sl ... % t/m slootrechts
                    x_bit_nu1+dsf+sd/he_sl+sb+sd/he_sl+dx_rechts...
                    ];
                y_prof_t_dHdSdP = [...
                    yrefb ...
                    yref ... % t/m buitenteen
                    yref+(hd_bu+dH) ... % t/m buitenkruin
                    yref+(hd_bu+dH)+hkh ... % t/m halve kruin
                    y_bik_nu ... % t/m binnenkruin
                    y_bit ... % t/m binnenteen
                    y_bit ... % t/m slootlinks 
                    y_bit-sd ...
                    y_bit-sd ...
                    y_bit ... % t/m slootrechts
                    y_bit...
                    ];
            end
            xx = x_prof_t_dHdSdP(x_prof_t_dHdSdP>x_data_rp(end-1));
            yy = y_prof_t_dHdSdP(x_prof_t_dHdSdP>x_data_rp(end-1));
            if xx(end) > x_bit_nu1
                y_bit_nu1 = interp1(xx,yy,x_bit_nu1,'linear','extrap');
            else
                y_bit_nu1 = yy(end);
            end
            %Test
            %y_bit_nu1 = y_bit;
        else
            if x_data_p(end) > x_bit_nu1
                y_bit_nu1 = interp1(x_data_p,y_data_p,x_bit_nu1,'linear','extrap');
            else
                y_bit_nu1 = y_data_p(end);
            end
            %Test
            %y_bit_nu1 = y_bit;
            if sd == 0
                x_prof_t_dHdSdP = [...
                    xrefb ...
                    xref+dx_links ... % t/m buitenteen
                    xref+dx_links+(hd_bu+dH)/he_bu ... % t/m buitenkruin
                    xref+dx_links+(hd_bu+dH)/he_bu+hkb ... % t/m halve kruin
                    x_bik_nu ... % t/m binnenkruin
                    x_bit_nu1 ... % t/m binnenteen
                    xref1 ...
                    xref1+bi_te_s+sd/he_sl+sb+sd/he_sl+dx_rechts...
                    ];
                if x_prof_t_dHdSdP(end) < x_bit_nu2
                    x_prof_t_dHdSdP(end) = x_bit_nu2;
                end
                y_prof_t_dHdSdP = [...
                    yrefb ...
                    yref ... % t/m buitenteen
                    yref+(hd_bu+dH) ... % t/m buitenkruin
                    yref+(hd_bu+dH)+hkh ... % t/m halve kruin
                    yref+(hd_bu+dH) ... % t/m binnenkruin
                    y_bit_nu1 ... % t/m binnenteen
                    yref1 ...
                    yref1...
                    ];
            else
                x_prof_t_dHdSdP = [...
                    xrefb ...
                    xref+dx_links... % t/m buitenteen
                    xref+dx_links+(hd_bu+dH)/he_bu ... % t/m buitenkruin
                    xref+dx_links+(hd_bu+dH)/he_bu+hkb ... % t/m halve kruin
                    x_bik_nu ... % t/m binnenkruin
                    x_bit_nu1 ... % t/m binnenteen
                    x_rest1... % t/m eindpunt
                    ];
                y_prof_t_dHdSdP = [...
                    yrefb ...
                    yref ... % t/m buitenteen
                    yref+(hd_bu+dH) ... % t/m buitenkruin
                    yref+(hd_bu+dH)+hkh ... % t/m halve kruin
                    yref+(hd_bu+dH) ... % t/m binnenkruin
                    y_bit_nu1 ... % t/m binnenteen
                    y_rest1... % t/m eindpunt 
                    ];
            end
        end

    elseif length(x_data_rp) == 4
        x_bik_nu = xref+dx_links+(hd_bu+dH)/he_bu+kb;
        y_bik_nu = yref+(hd_bu+dH);
        d_x_bik = x_bik_nu-x_bik;
        dx = dH/he_bi;
        dx_tot = d_x_bik+dx;
        dS_nett = dS-dx_tot;
        if dS_nett<0
            dS_nett = 0;
        end
        dP_nett = dP-dS_nett-dx_tot;
        if dP_nett<0
            dP_nett = 0;
        end
        x_bit_nu1 = x_bit+dx_tot+dS_nett;
        x_bit_nu2 = x_bit+dx_tot+dS_nett+dP_nett;
        %x_rest = x_data_p(x_data_p > x_bit_nu1);
        %y_rest = y_data_p(x_data_p > x_bit_nu1);
        x_rest1 = x_prof_ini(x_prof_ini > x_bit_nu1);
        y_rest1 = y_prof_ini(x_prof_ini > x_bit_nu1);
        if x_sloot_links <= x_bit_nu1
            sd = 0;
        end
        if x_sloot_links <= x_bit_nu2
            sd = 0;
        end
        if xref1 < x_bit_nu2
            xref1 = x_bit_nu2;
        end
        if incl_binnenwaarts == 0
            if sd == 0
                dsf = x_prof_ini(end)-x_bit_nu1;
                if dsf<dP_nett
                    dsf = dP_nett;
                end
                x_prof_t_dHdSdP = [...
                    xrefb ...
                    xref+dx_links... % t/m buitenteen
                    xref+dx_links+(hd_bu+dH)/he_bu ... % t/m buitenkruin
                    xref+dx_links+(hd_bu+dH)/he_bu+hkb ... % t/m halve kruin
                    x_bik_nu ... % t/m binnenkruin
                    x_bit_nu1 ... % t/m binnenteen
                    x_bit_nu1+dsf ... % eindpunt
                    ];
                y_prof_t_dHdSdP = [...
                    yrefb ...
                    yref ... % t/m buitenteen
                    yref+(hd_bu+dH) ... % t/m buitenkruin
                    yref+(hd_bu+dH)+hkh ... % t/m halve kruin
                    y_bik_nu ... % t/m binnenkruin
                    y_bit ... % t/m binnenteen
                    y_bit... % eindpunt
                    ];
            else
                dsf = x_sloot_links-x_bit_nu1;
                x_prof_t_dHdSdP = [...
                    xrefb ...
                    xref+dx_links... % t/m buitenteen
                    xref+dx_links+(hd_bu+dH)/he_bu ... % t/m buitenkruin
                    xref+dx_links+(hd_bu+dH)/he_bu+hkb ... % t/m halve kruin
                    x_bik_nu ... % t/m binnenkruin
                    x_bit_nu1 ... % t/m binnenteen
                    x_bit_nu1+dsf ... % t/m slootlinks
                    x_bit_nu1+dsf+sd/he_sl ...
                    x_bit_nu1+dsf+sd/he_sl+sb ...
                    x_bit_nu1+dsf+sd/he_sl+sb+sd/he_sl ... % t/m slootrechts
                    x_bit_nu1+dsf+sd/he_sl+sb+sd/he_sl+dx_rechts...
                    ];
                y_prof_t_dHdSdP = [...
                    yrefb ...
                    yref ... % t/m buitenteen
                    yref+(hd_bu+dH) ... % t/m buitenkruin
                    yref+(hd_bu+dH)+hkh ... % t/m halve kruin
                    y_bik_nu ... % t/m binnenkruin
                    y_bit ... % t/m binnenteen
                    y_bit ... % t/m slootlinks 
                    y_bit-sd ...
                    y_bit-sd ...
                    y_bit ... % t/m slootrechts
                    y_bit...
                    ];
            end
            xx = x_prof_t_dHdSdP(x_prof_t_dHdSdP>x_data_rp(end-1));
            yy = y_prof_t_dHdSdP(x_prof_t_dHdSdP>x_data_rp(end-1));
            if xx(end) > x_bit_nu1
                y_bit_nu1 = interp1(xx,yy,x_bit_nu1,'linear','extrap');
            else
                y_bit_nu1 = yy(end);
            end
            %Test
            %y_bit_nu1 = y_bit;
        else
            if x_data_p(end) > x_bit_nu1
                y_bit_nu1 = interp1(x_data_p,y_data_p,x_bit_nu1,'linear','extrap');
            else
                y_bit_nu1 = y_data_p(end);
            end
            %Test
            %y_bit_nu1 = y_bit;
            if sd == 0
                x_prof_t_dHdSdP = [...
                    xrefb ...
                    xref+dx_links ... % t/m buitenteen
                    xref+dx_links+(hd_bu+dH)/he_bu ... % t/m buitenkruin
                    xref+dx_links+(hd_bu+dH)/he_bu+hkb ... % t/m halve kruin
                    x_bik_nu ... % t/m binnenkruin
                    x_bit_nu1 ... % t/m binnenteen
                    xref1 ...
                    xref1+bi_te_s+sd/he_sl+sb+sd/he_sl+dx_rechts...
                    ];
                if x_prof_t_dHdSdP(end) < x_bit_nu2
                    x_prof_t_dHdSdP(end) = x_bit_nu2;
                end
                y_prof_t_dHdSdP = [...
                    yrefb ...
                    yref ... % t/m buitenteen
                    yref+(hd_bu+dH) ... % t/m buitenkruin
                    yref+(hd_bu+dH)+hkh ... % t/m halve kruin
                    yref+(hd_bu+dH) ... % t/m binnenkruin
                    y_bit_nu1 ... % t/m binnenteen
                    yref1 ...
                    yref1...
                    ];
            else
                x_prof_t_dHdSdP = [...
                    xrefb ...
                    xref+dx_links... % t/m buitenteen
                    xref+dx_links+(hd_bu+dH)/he_bu ... % t/m buitenkruin
                    xref+dx_links+(hd_bu+dH)/he_bu+hkb ... % t/m halve kruin
                    x_bik_nu ... % t/m binnenkruin
                    x_bit_nu1 ... % t/m binnenteen
                    x_rest1... % t/m eindpunt
                    ];
                y_prof_t_dHdSdP = [...
                    yrefb ...
                    yref ... % t/m buitenteen
                    yref+(hd_bu+dH) ... % t/m buitenkruin
                    yref+(hd_bu+dH)+hkh ... % t/m halve kruin
                    yref+(hd_bu+dH) ... % t/m binnenkruin
                    y_bit_nu1 ... % t/m binnenteen
                    y_rest1... % t/m eindpunt 
                    ];
            end
        end

    elseif length(x_data_rp) == 5
        x_bik_nu = xref+dx_links+(hd_bu+dH)/he_bu+kb;
        y_bik_nu = yref+(hd_bu+dH);
        d_x_bik = x_bik_nu-x_bik;
        dx = dH/he_bi;
        dx_tot = d_x_bik+dx;
        dS_nett = dS-dx_tot;
        if dS_nett<0
            dS_nett = 0;
        end
        dP_nett = dP-dS_nett-dx_tot;
        if dP_nett<0
            dP_nett = 0;
        end
        x_bit_nu1 = x_bit+dx_tot+dS_nett;
        x_bit_nu2 = x_bit+dx_tot+dS_nett+dP_nett;
        %x_rest = x_data_p(x_data_p > x_bit_nu1);
        %y_rest = y_data_p(x_data_p > x_bit_nu1);
        x_rest1 = x_prof_ini(x_prof_ini > x_bit_nu1);
        y_rest1 = y_prof_ini(x_prof_ini > x_bit_nu1);
        if x_sloot_links <= x_bit_nu1
            sd = 0;
        end
        if x_sloot_links <= x_bit_nu2
            sd = 0;
        end
        if xref1 < x_bit_nu2
            xref1 = x_bit_nu2;
        end
        if incl_binnenwaarts == 0
            if sd == 0
                dsf = x_prof_ini(end)-x_bit_nu1;
                if dsf<dP_nett
                    dsf = dP_nett;
                end
                x_prof_t_dHdSdP = [...
                    xrefb ...
                    xref+dx_links... % t/m buitenteen
                    xref+dx_links+(hd_bu+dH)/he_bu ... % t/m buitenkruin
                    xref+dx_links+(hd_bu+dH)/he_bu+hkb ... % t/m halve kruin
                    x_bik_nu ... % t/m binnenkruin
                    x_bit_nu1 ... % t/m binnenteen
                    x_bit_nu1+dsf ... % eindpunt
                    ];
                y_prof_t_dHdSdP = [...
                    yrefb ...
                    yref ... % t/m buitenteen
                    yref+(hd_bu+dH) ... % t/m buitenkruin
                    yref+(hd_bu+dH)+hkh ... % t/m halve kruin
                    y_bik_nu ... % t/m binnenkruin
                    y_bit ... % t/m binnenteen
                    y_bit... % eindpunt
                    ];
            else
                dsf = x_sloot_links-x_bit_nu1;
                x_prof_t_dHdSdP = [...
                    xrefb ...
                    xref+dx_links... % t/m buitenteen
                    xref+dx_links+(hd_bu+dH)/he_bu ... % t/m buitenkruin
                    xref+dx_links+(hd_bu+dH)/he_bu+hkb ... % t/m halve kruin
                    x_bik_nu ... % t/m binnenkruin
                    x_bit_nu1 ... % t/m binnenteen
                    x_bit_nu1+dsf ... % t/m slootlinks
                    x_bit_nu1+dsf+sd/he_sl ...
                    x_bit_nu1+dsf+sd/he_sl+sb ...
                    x_bit_nu1+dsf+sd/he_sl+sb+sd/he_sl ... % t/m slootrechts
                    x_bit_nu1+dsf+sd/he_sl+sb+sd/he_sl+dx_rechts...
                    ];
                y_prof_t_dHdSdP = [...
                    yrefb ...
                    yref ... % t/m buitenteen
                    yref+(hd_bu+dH) ... % t/m buitenkruin
                    yref+(hd_bu+dH)+hkh ... % t/m halve kruin
                    y_bik_nu ... % t/m binnenkruin
                    y_bit ... % t/m binnenteen
                    y_bit ... % t/m slootlinks 
                    y_bit-sd ...
                    y_bit-sd ...
                    y_bit ... % t/m slootrechts
                    y_bit...
                    ];
            end
            xx = x_prof_t_dHdSdP(x_prof_t_dHdSdP>x_data_rp(end-1));
            yy = y_prof_t_dHdSdP(x_prof_t_dHdSdP>x_data_rp(end-1));
            if xx(end) > x_bit_nu1
                y_bit_nu1 = interp1(xx,yy,x_bit_nu1,'linear','extrap');
            else
                y_bit_nu1 = yy(end);
            end
            %Test
            %y_bit_nu1 = y_bit;
        else
            if x_data_p(end) > x_bit_nu1
                y_bit_nu1 = interp1(x_data_p,y_data_p,x_bit_nu1,'linear','extrap');
            else
                y_bit_nu1 = y_data_p(end);
            end
            %Test
            %y_bit_nu1 = y_bit;
            if sd == 0
                x_prof_t_dHdSdP = [...
                    xrefb ...
                    xref+dx_links ... % t/m buitenteen
                    xref+dx_links+(hd_bu+dH)/he_bu ... % t/m buitenkruin
                    xref+dx_links+(hd_bu+dH)/he_bu+hkb ... % t/m halve kruin
                    x_bik_nu ... % t/m binnenkruin
                    x_bit_nu1 ... % t/m binnenteen
                    xref1 ...
                    xref1+bi_te_s+sd/he_sl+sb+sd/he_sl+dx_rechts...
                    ];
                if x_prof_t_dHdSdP(end) < x_bit_nu2
                    x_prof_t_dHdSdP(end) = x_bit_nu2;
                end
                y_prof_t_dHdSdP = [...
                    yrefb ...
                    yref ... % t/m buitenteen
                    yref+(hd_bu+dH) ... % t/m buitenkruin
                    yref+(hd_bu+dH)+hkh ... % t/m halve kruin
                    yref+(hd_bu+dH) ... % t/m binnenkruin
                    y_bit_nu1 ... % t/m binnenteen
                    yref1 ...
                    yref1...
                    ];
            else
                x_prof_t_dHdSdP = [...
                    xrefb ...
                    xref+dx_links... % t/m buitenteen
                    xref+dx_links+(hd_bu+dH)/he_bu ... % t/m buitenkruin
                    xref+dx_links+(hd_bu+dH)/he_bu+hkb ... % t/m halve kruin
                    x_bik_nu ... % t/m binnenkruin
                    x_bit_nu1 ... % t/m binnenteen
                    x_rest1... % t/m eindpunt
                    ];
                y_prof_t_dHdSdP = [...
                    yrefb ...
                    yref ... % t/m buitenteen
                    yref+(hd_bu+dH) ... % t/m buitenkruin
                    yref+(hd_bu+dH)+hkh ... % t/m halve kruin
                    yref+(hd_bu+dH) ... % t/m binnenkruin
                    y_bit_nu1 ... % t/m binnenteen
                    y_rest1... % t/m eindpunt 
                    ];
            end
        end
    else
        %display(['Fout bij het maken van dHdSdP profiel, lengte van x_data_rp is < 3 of > 5 bij vak ' num2str(vak)]);
        %return;
    end

    % Update prof_ini
    if x_prof_ini(end) < x_prof_t_dHdSdP(end)
        x_prof_ini_upd = [x_prof_ini x_prof_t_dHdSdP(end)];
        y_prof_ini_upd = [y_prof_ini y_prof_t_dHdSdP(end)];
    else
        x_prof_ini_upd = x_prof_ini;
        y_prof_ini_upd = y_prof_ini;
    end
    % Check dHdSdP profiel
    x1 = min(x_prof_ini_upd);
    x2 = max(x_prof_ini_upd);
    xt = linspace(x1,x2,1000);
    x_prof_ini_upd1 = xt;
    [x_prof_ini_upd_u,ia] = unique(x_prof_ini_upd);
    y_prof_ini_upd_u = y_prof_ini_upd(ia);
    y_prof_ini_upd1 = interp1(x_prof_ini_upd_u,y_prof_ini_upd_u,xt,'linear','extrap');
    x_prof_t_dHdSdP1 = xt;
    [x_prof_t_dHdSdP_u,ia] = unique(x_prof_t_dHdSdP);
    y_prof_t_dHdSdP_u = y_prof_t_dHdSdP(ia);
    y_prof_t_dHdSdP1 = interp1(x_prof_t_dHdSdP_u,y_prof_t_dHdSdP_u,xt,'linear','extrap');
    dy = y_prof_t_dHdSdP1-y_prof_ini_upd1;
    y_prof_t_dHdSdP1(dy<0) = y_prof_ini_upd1(dy<0);
    % Updata x_prof_t_dHdSdP en y_prof_t_dHdSdP
    x_prof_t_dHdSdP1(x_prof_t_dHdSdP1>x_bit_nu1) = x_prof_ini_upd1(x_prof_t_dHdSdP1>x_bit_nu1);
    y_prof_t_dHdSdP1(x_prof_t_dHdSdP1>x_bit_nu1) = y_prof_ini_upd1(x_prof_t_dHdSdP1>x_bit_nu1);

    % Improving profile with piping berm
    y_bit_nu2 = y_bit_nu1;
    xx = x_prof_t_dHdSdP1(x_prof_t_dHdSdP1>x_data_rp(end-1));
    yy = y_prof_t_dHdSdP1(x_prof_t_dHdSdP1>x_data_rp(end-1));
    xxr = linspace(min(xx),max(xx),1000);
    yyr = interp1(xx,yy,xxr,'linear','extrap');
    yrt = interp1(xxr,yyr,x_bit_nu2,'linear','extrap'); % om te voorkomen van y_bit_nu2 te hoog is
    if yrt < y_bit_nu2 % y_bit_nu1
        y_bit_nu3 = yrt;
    else
        y_bit_nu3 = y_bit_nu2;%yrt;%y_bit_nu1;
    end
    x_bit_nu3 = x_bit_nu2;
    y_be_inst = y_bit_nu3+be_di;
    if y_be_inst > y_bik_nu
        y_be_inst = (y_bik_nu+y_bit_nu3)/2;
        be_di = y_be_inst-y_bit_nu3;
    end
    x_be_uitst = x_bit_nu3-be_di/be_he;
    y_be_uitst = y_bit_nu3+be_di;
    dy = abs(y_be_uitst-yyr);
    if y_bit_nu3 < y_bit_nu1
        while 1
            x_be_inst = xxr(dy == min(dy));
            if length(x_be_inst) > 1
                x_be_inst = x_be_inst(1);
            end
            if x_be_inst <= x_bit_nu1;%x_bit_nu2 %&& dxx > 0.5
                break;
            else
                dy = dy(dy~=min(dy));
            end
        end
    else
        while 1
            x_be_inst = xxr(dy == min(dy));
            if length(x_be_inst) > 1
                x_be_inst = x_be_inst(1);
            end
            %for test: x_be_inst <= x_bit i.p.v. x_be_inst <= x_bit_nu1
            if x_be_inst <= x_bit_nu1 %x_be_inst <= x_bit_nu1 %&& dxx > 0.5
                break;
            else
                dy = dy(dy~=min(dy));
            end
        end
    end
    x_pip_berm = [x_be_inst x_be_uitst x_bit_nu3];
    y_pip_berm = [y_be_inst y_be_uitst y_bit_nu3];
    if xxr(end)> x_bit_nu3
        ytt = interp1(xxr,yyr,x_bit_nu3,'linear','extrap');
    else
        ytt = yyr(end);
    end
    if y_pip_berm(end) < ytt
        y_pip_berm(end) = ytt;
    end
    
    % Merging profile
    x_prof_t_dHdSdP_merged = [x_prof_t_dHdSdP1(x_prof_t_dHdSdP1< x_pip_berm(1)) ...
        x_pip_berm x_prof_t_dHdSdP1(x_prof_t_dHdSdP1> x_pip_berm(end))];
    y_prof_t_dHdSdP_merged = [y_prof_t_dHdSdP1(x_prof_t_dHdSdP1< x_pip_berm(1)) ...
        y_pip_berm y_prof_t_dHdSdP1(x_prof_t_dHdSdP1> x_pip_berm(end))];

    % Update rp
    x_data_rp_def = [x_data_rp ; x_bik ; x_bit];
    y_data_rp_def = [y_data_rp ; y_bik ; y_bit];
    x_data_rp_def_extra = [x_bit_nu1 ; x_bit_nu2 ; x_bit_nu3];
    y_data_rp_def_extra = [y_bit_nu1 ; yrt ; y_bit_nu3];

    %Final adjustment
    xmin = min([min(x_prof_ini_upd1) min(x_prof_t_dHdSdP_merged)]);
    xmax = max([max(x_prof_ini_upd1) max(x_prof_t_dHdSdP_merged)]);
    xx = linspace(xmin,xmax,1000);
    [x_prof_ini_upd2,ia] = unique(x_prof_ini_upd1);
    y_prof_ini_upd2 = y_prof_ini_upd1(ia);
    yy_ini = interp1(x_prof_ini_upd2,y_prof_ini_upd2,xx,'linear','extrap');
    y_prof_ini_upd3 = yy_ini;
    x_prof_ini_upd3 = xx;
    [x_prof_t_dHdSdP_merged1,ia] = unique(x_prof_t_dHdSdP_merged);
    y_prof_t_dHdSdP_merged1 = y_prof_t_dHdSdP_merged(ia); 
    yy_merged = interp1(x_prof_t_dHdSdP_merged1,y_prof_t_dHdSdP_merged1,xx,'linear','extrap');
    dyy = yy_merged-yy_ini;
    if real_prof_ok == 1
        x_prof_t_dHdSdP_merged2 = xx;
        y_prof_t_dHdSdP_merged2 = yy_merged;
        y_prof_t_dHdSdP_merged2(dyy<=0) = yy_ini(dyy<=0);
    else
        x_prof_t_dHdSdP_merged2 = x_prof_t_dHdSdP_merged;
        y_prof_t_dHdSdP_merged2 = y_prof_t_dHdSdP_merged;
    end

    %% Composing polylines
    y_border_min = min([min(y_prof_ini_upd3) min(y_prof_t_dHdSdP_merged2)])-10;
    y_border_max = max([max(y_prof_ini_upd3) max(y_prof_t_dHdSdP_merged2)])+10;
    y_min = y_border_min+5;
    x_border_min = min([min(x_prof_ini_upd3) min(x_prof_t_dHdSdP_merged2)]);
    x_border_max = max([max(x_prof_ini_upd3) max(x_prof_t_dHdSdP_merged2)]);
    x_min = x_border_min;
    x_max = x_border_max;
    y_max = interp1(x_prof_t_dHdSdP_merged2,y_prof_t_dHdSdP_merged2,x_max,'linear','extrap');

    % Voor initieel profiel
    if real_prof_ok == 1
        if x_prof_ini_upd1(end) < x_max
            x_prof_ini_def = [x_prof_ini_upd3 x_max];
            y_prof_ini_def = [y_prof_ini_upd3 y_max];
        else
            x_prof_ini_def = x_prof_ini_upd3;
            y_prof_ini_def = y_prof_ini_upd3;
        end
    else
        if x_prof_ini_upd1(end) < x_max
            x_prof_ini_def = [x_prof_ini_upd3(x_prof_ini_upd3<=x_bik) x_prof_ini_upd3(x_prof_ini_upd3>=x_bit) x_max];
            y_prof_ini_def = [y_prof_ini_upd3(x_prof_ini_upd3<=x_bik) y_prof_ini_upd3(x_prof_ini_upd3>=x_bit) y_max];
        else
            x_prof_ini_def = [x_prof_ini_upd3(x_prof_ini_upd3<=x_bik) x_prof_ini_upd3(x_prof_ini_upd3>=x_bit)];
            y_prof_ini_def = [y_prof_ini_upd3(x_prof_ini_upd3<=x_bik) y_prof_ini_upd3(x_prof_ini_upd3>=x_bit)];
        end
    end

    % Voor dHdSdP profiel
    x_prof_t_dHdSdP_def = [...
        x_prof_t_dHdSdP1 ...
        x_max ...
        x_min ...
        x_min...
        ];
    y_prof_t_dHdSdP_def = [...
        y_prof_t_dHdSdP1 ...
        y_min ...
        y_min ...
        yref...
        ];

    % Voor de pipingberm
    x_pip_berm_def = [...
        x_pip_berm ...
        x_max ...
        x_min ...
        x_min...
        ];
    y_pip_berm_def = [...
        y_pip_berm ...
        y_min ...
        y_min ...
        yref...
        ];

    % Voor merged profiel
    x_prof_t_dHdSdP_merged_def = [...
        x_prof_t_dHdSdP_merged2 ...
        x_max ...
        x_min ...
        x_prof_t_dHdSdP_merged2(1)...
        ];
    y_prof_t_dHdSdP_merged_def = [...
        y_prof_t_dHdSdP_merged2 ...
        y_min ...
        y_min ...
        y_prof_t_dHdSdP_merged2(1)...
        ];

    % Plot profielen
    s = figure('visible','off');
    set(0,'CurrentFigure',s);
    fill(x_prof_t_dHdSdP_merged_def,y_prof_t_dHdSdP_merged_def,[0 1 0],'LineStyle','-','EdgeColor',[0 0 0],'Linewidth',2);
    hold on;
    plot(x_prof_ini_def,y_prof_ini_def,'--r','LineWidth',0.5);
    plot(x_data_rp_def,y_data_rp_def,'or');
    %plot(x_data_rp_def_extra,y_data_rp_def_extra,'sb');
    if const == 1
        yh = interp1(x_data_p,y_data_p,x_data_rp(end-1),'linear','extrap');
        plot([x_data_rp(end-1) x_data_rp(end-1)],[yh y_min],'-b','LineWidth',1);
    end
    xlim([x_border_min x_border_max]);
    ylim([y_min y_border_max]);
    xlabel('Horizontale afstand [m]');
    ylabel('Verticale afstand [m+NAP]');
    if const == 0
    legend({['Versterkte dijk (dH = ' num2str(dH,'%.2f') ' m, dS = ' num2str(dS,'%.2f') ' m, dP = ' num2str(dP,'%.2f') ' m)'],...
        'initieel','rp'},...
        'Location','SouthOutside','Orientation','Horizontal','FontSize',6);
    else
        legend({['Versterkte dijk (dH = ' num2str(dH,'%.2f') ' m, dS = ' num2str(dS,'%.2f') ' m, dP = ' num2str(dP,'%.2f') ' m)'],...
        'initieel','rp','const_maatregel'},...
        'Location','SouthOutside','Orientation','Horizontal','FontSize',6);
    end
    legend('boxoff');
    if gfgf == 0
        title(['Dijkprofiel vak ' num2str(vak) ', versterking in ' num2str(t)]);
    else
        title(['Dijkprofiel* vak ' num2str(vak) ', versterking in ' num2str(t)]);
    end
    %set(gca,'DataAspectRatio',[1 2 1]);
    % saveas(s,[link_to_figures '\dijkprofiel_vak_' num2str(vak) '_' num2str(t) ...
    %     '_dH' strrep(num2str(dH,'%.2f'),'.',',') 'm_dS' strrep(num2str(dS,'%.2f'),'.',',') ...
    %     'm_dP' strrep(num2str(dP,'%.2f'),'.',',') 'm_binnewaarts' num2str(incl_binnenwaarts,'%.0f') '.png']);
    saveas(s,[link_to_figures '\dijkprofiel_vak_' num2str(vak) '_' num2str(t) ...
        '_dH' strrep(num2str(dH,'%.2f'),'.',',') 'm_dS' strrep(num2str(dS,'%.2f'),'.',',') ...
        'm_dP' strrep(num2str(dP,'%.2f'),'.',',') 'm.png']);
    close(s);
    elseif overslaan == 1
    elseif overslaan == 2
    elseif overslaan == 3
        %display(['Profielvoor vak ' num2str(vak) ' is niet gemaakt vanwege gebrek aan profieldata...(' num2str(i) '/' num2str(length(vak_data)) ')']);
    end
                end
            end
        end
        %display(['Profielanalyse voor vak ' num2str(vak) ' is gedaan...(' num2str(i) '/' num2str(length(vak_data)) ')']);
    end
catch
    disp(['Error bij plotten visualisatie dijk ' num2str(vak_data)])
end 
end