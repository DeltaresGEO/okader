function profieldata = read_profielen(link_to_prof_data)
% leest profielendatabase met profielinfo uit VNK uit. Deze data wordt
% gebruikt voor het visualiseren van de versterkte dijk
%
% Ontwikkeld door HKV en Deltares in opdracht van RWS-WVL, maart 2017

[~,sheets] = xlsfinfo(link_to_prof_data); % tabbladen excel inlezen
temp = regexp(sheets,'_','split');
for i=1:size(temp,2) % in alle tabbladen zoeken naar getal voor '_'
    [x,status] = str2num(temp{1,i}{1,1});
    if status
        dijkring(i)=x;
    end
end
dijkring = unique(dijkring); % unieke dijkringnummers maken

% dataset vullen met ruwe profieldata (P) en rekenprofiel (RP)
profieldata.dijkringen=dijkring;
profieldata(length(dijkring)).dijkring=dijkring(end);
profieldata(length(dijkring)).P=[];
profieldata(length(dijkring)).RP=[];
for i=1:length(dijkring)
    try
        profieldata(i).dijkring=dijkring(i);
        sheet = [num2str(dijkring(i)) '_P'];
        profieldata(i).P = xlsread(link_to_prof_data,sheet);
        sheet = [num2str(dijkring(i)) '_RP'];
        profieldata(i).RP = xlsread(link_to_prof_data,sheet);
    catch
        
    end
end

end