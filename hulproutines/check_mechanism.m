function M = check_mechanism(fm)
%
% Ontwikkeld door HKV en Deltares in opdracht van RWS-WVL, maart 2017

ff = fieldnames(fm);
m = zeros(1,length(ff));
for i = 1:length(ff)
    if getfield(fm,char(ff(i))) == 1
        m(i) = 1;
    end
end
M = find(m == 1);
end