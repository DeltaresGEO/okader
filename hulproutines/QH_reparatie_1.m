function QHdata = QH_reparatie_1(QHdata,diff,logfile)
% repareren voor afnemende waterstanden met toenemende afvoer
%
% Ontwikkeld door HKV en Deltares in opdracht van RWS-WVL, maart 2017

vakken = unique(QHdata(:,1));
for v=1:length(vakken)
    QHvak = QHdata(QHdata(:,1)==vakken(v),:);
    rep = 0;
    for i=2:size(QHvak,1)
        if QHvak(i,3)>QHvak(i-1,3)
            continue
        else
            if i<size(QHvak,1) % middelen tussen vorige en volgende
                QHvak(i,3) = mean([QHvak(i-1,3);QHvak(i+1,3)]);
            else % hoogste afvoer, diff cm bij waterstand optellen
                QHvak(i,3) = QHvak(i-1,3) + diff;
            end
            rep = 1;

        end
    end
    if rep==1
        disp(['Warning: QH relatie vak ' num2str(vakken(v)) ' gecorrigeerd (reparatie 1)'])
        fileID = fopen(logfile,'a');
        fprintf(fileID,'%s\n',['Warning: QH relatie vak ' num2str(vakken(v)) ' gecorrigeerd (reparatie 1)']);
        fclose(fileID);
    end
    QHdata(QHdata(:,1)==vakken(v),:) = QHvak;
end
