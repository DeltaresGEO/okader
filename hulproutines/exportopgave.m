function exportopgave(invoer,VAR,uitvoernaam)
% Schrijft de benodigde dijkversterking om tot een bepaald zichtjaar te voldoen
% aan de eisen (de zgn. opgave) naar een *.csv file. De jaren die opgeslagen worden,
% worden in de invoer (variabele "tijdopgave") opgegeven. 
% 
% invoer        = structure met invoer
% VAR           = structure met data die tijdens berekening wordt gevuld
% uitvoernaam   = basis van uitvoerbestand. Wordt aangevuld met faalmechanisme
%
% Ontwikkeld door HKV en Deltares in opdracht van RWS-WVL, maart 2017
    
Tind =  ismember(VAR.T,invoer.tijdopgave);
opg_jaren = VAR.T(Tind);
ind = find(Tind);

m = length(opg_jaren);   % m=aantal jaren
n = size(VAR.dH_opg_corr,1);   % n=aantal vakken

%% headers toevoegen
export{1,1} = 'VakID';
for j = 1:m
    export{j+1,1} = ['opgave tot ',num2str(opg_jaren(j))];
end
for i = 2:n+1
    export{1,i} = VAR.VNKvakken(i-1);
end

%% dS stabiliteit
for i = 2:n+1
    for j = 1:m
        export{j+1,i} = VAR.dS_opg_corr(i-1,ind(j));
    end
end
cell2csv([uitvoernaam,'stabiliteit_vakniveau.csv'],export,';',2010,'.');        

%% Pf_2 piping
for i = 2:n+1
    for j = 1:m
        export{j+1,i} = VAR.dP_opg_corr(i-1,ind(j));
    end
end
cell2csv([uitvoernaam,'piping_vakniveau.csv'],export,';',2010,'.');        

%% dH hoogte
for i = 2:n+1
    for j = 1:m
        export{j+1,i} = VAR.dH_opg_corr(i-1,ind(j));
    end
end
cell2csv([uitvoernaam,'hoogte_vakniveau.csv'],export,';',2010,'.');        
    
end