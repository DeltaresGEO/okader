function [VAR,TMP] = initialisatie(VAR,T,VNKvakken)
% Opzetten van matrices in de verzamelvariabelen �VAR� en �TMP�
%
% Ontwikkeld door HKV en Deltares in opdracht van RWS-WVL, maart 2017

% matrices vullen met 0 obv vakken en tijd
VAR.T       = T; % tijd
VAR.VNKvakken = VNKvakken;
dH          = zeros(length(VNKvakken),length(T)); % hoogte
VAR.dH      = dH; % hoogte
VAR.dH_opg  = dH; % hoogte
VAR.dH_corr  = dH; % hoogte
VAR.dH_opg_corr = dH; % hoogte
VAR.dP      = dH; % piping
VAR.dP_opg  = dH; % piping
VAR.dP_corr  = dH; % piping
VAR.dP_opg_corr  = dH; % piping
VAR.dS      = dH; % stabiliteit
VAR.dS_opg  = dH; % stabiliteit
VAR.dS_corr  = dH; % stabiliteit
VAR.dS_opg_corr  = dH; % stabiliteit
VAR.Pf_1_t  = dH; % Pf_1 (macro) in de tijd
VAR.Pf_2_t  = dH; % Pf_2 (piping) in de tijd
VAR.Pf_3_t  = dH; % Pf_3 (hoogte) in de tijd
VAR.Pf_eis_1_v = zeros(length(VNKvakken),length(T));
VAR.Pf_eis_2_v = zeros(length(VNKvakken),length(T));
VAR.Pf_eis_3_v = zeros(length(VNKvakken),length(T));
VAR.kosten_dijk = dH;
VAR.kosten_dijk_cw = dH;
VAR.kosten_dijk_ncw = zeros(length(VNKvakken),1);
VAR.check_const = zeros(length(VNKvakken),1);
% VAR.afkapgrens = ones(length(VNKvakken),1)*999;
VAR.ws_data(length(VNKvakken)).vak = 0;
VAR.ws_data(length(VNKvakken)).jaar = 0;
VAR.ws_data(length(VNKvakken)).h_pdf = 0;
VAR.htfac = dH;
VAR.macfac = dH;
VAR.pipfac = dH;

% overige initialisatie
VAR.cost_out = {'VNK vak','Jaartal','Kruinverhoging (m)','Toename Dijkbasis Stabiliteit (m)',...
    'Toename Dijkbasis Piping (m)','Vaklengte (km)','Totaalkosten (M�)','Grondmaatregel Lengte (km)',...
    'Grondmaatregel Totaalkosten (M�)','Pipingmaatregel Lengte (km)','Pipingmaatregel Totaalkosten (M�)',...
    'Stabiliteitswand Lengte (km)','Stabiliteitswand Totaalkosten (M�)','Kistdam Lengte (km)',...
    'Kistdam Totaalkosten (M�)','CC Lengte','CC Totaalkosten','Infrastructuur Totaalkosten',...
    'Dijkring','Dijkringnaam','Dijkringtraject','Beheerder','Categorie',...
    'Lengte innovatieve pipingopl [km]','Kosten innovatieve pipingopl [ME]'};

VAR.cost_out_new = {'Dijkvak','Dijkring','Dijkringnaam','Dijkringtraject','Beheerder','Categorie',...
    'Jaartal','Kruinverhoging (m)','Toename Dijkbasis Stabiliteit (m)','Toename Dijkbasis Piping (m)',...
    'Vaklengte (km)','Totaalkosten (M�)','Grondmaatregel TrajectLengte (km)','Grondmaatregel Totaalkosten (M�)',...
    'Pipingmaatregel TrajectLengte (km)','Pipingmaatregel Totaalkosten (M�)', 'Kwelschermlengte (m)',...
    'Stabiliteitswand TrajectLengte (km)','Stabiliteitswand Totaalkosten (M�)', 'Stabiliteitswandlengte (m)',...
    'Kistdam TrajectLengte (km)','Kistdam Totaalkosten (M�)', 'Kistdamlengte (m)', 'Kistdambreedte (m)',...
    'CC TrajectLengte (km)','CC Totaalkosten (M�)','Infrastructuur Totaalkosten (M�)',...
    'Lengte berm afgekapt [km]','Kosten berm afgekapt [M�]'};

TMP.err         = 0;    %initialisatie
VAR.errorvakken = [];    %initialisatie
TMP.row         = 0;    %initialisatie


end