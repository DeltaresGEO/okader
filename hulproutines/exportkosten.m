function exportkosten(invoer,VAR,kosten,uitvoernaam)
% Schrijft de dijkversterkingskosten per vak en per jaar naar een *.csv file. 
% Afhankelijk van keuze in invoer (variabele "exp_kosten_traject") worden ook kosten per normtraject uitgevoerd
% 
% invoer        = structure met invoer
% VAR           = structure met data die tijdens berekening wordt gevuld
% kosten        = kostenmatrix die moet worden opgeslagen. Kies
% "VAR.kosten_dijk" of "VAR.kosten_dijk_cw"
% uitvoernaam   = basis van uitvoerbestand. Wordt aangevuld met faalmechanisme
%
% Ontwikkeld door HKV en Deltares in opdracht van RWS-WVL, maart 2017

[n,m] = size(kosten);   % m=aantal jaren, n=aantal vakken

% headers toevoegen
export{1,1} = 'VakID';
export{m+1,1} = 'Restwaarde';
export{m+2,1} = 'Totaal';
% t0 = VAR.T(1);
for j = 2:m
%     export{j,1} = ['Kosten in ',num2str(j-2+t0)];
    export{j,1} = ['Kosten in ',num2str(VAR.T(j-1))];
end

% kosten toevoegen voor doorgerekende dijkvakken
for i = 2:n+1
    export{1,i} = VAR.VNKvakken(i-1);
    export{m+2,i} = 0;
    for j = 2:m+1
        export{j,i} = kosten(i-1,j-1);
        export{m+2,i} = export{j,i} + export{m+2,i};
    end
end
cell2csv([uitvoernaam,'vakniveau.csv'],export,';',2010,'.');  

if invoer.exp_kosten_traject == 1
    % maak lijst met normtrajecten
    nrm_trj = unique(VAR.norm.normtraject(ismember(VAR.norm.vak_data,VAR.VNKvakken)));

    % voor ieder traject een csv bestand uitvoeren
    for j = 1:length(nrm_trj)
        IndexC  = strfind(VAR.norm.normtraject,nrm_trj{j,1});
        Index   = find(not(cellfun('isempty', IndexC)));
        vak_trj = VAR.norm.vak_data(Index);
        exp_trj(:,1) = export(:,1);
        x = 0;
        for i = 2:n+1
            if ismember(VAR.VNKvakken(i-1),vak_trj')
               x = x+1;
               exp_trj(:,1+x) = export(:,i);
            end
        end
        if x>0
            exp_trj(:,2+x) = num2cell(sum(cell2mat(exp_trj(:,2:1+x)),2));
        else
            exp_trj(:,2+x) = num2cell(0);
        end
        exp_trj{1,2+x} = 'Totaal';
        cell2csv([uitvoernaam,nrm_trj{j,1},'.csv'],exp_trj,';',2010,'.');
        clear ind IndexC Index vak_trj exp_trj 
    end
end

% cumulatieve kosten (zonder restwaarde)
cumkosten = cumsum(sum(kosten(:,1:end-1),1));
xlswrite([uitvoernaam,'cumulatief.xlsx'],[VAR.T;cumkosten]');        

end % function
