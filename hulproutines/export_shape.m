function export_shape(VAR,exp4figs,dbf,uitvoernaam)
% script maakt een xlsx bestand aan in hetzelfde format als de dbf van de
% dijkvakkenshape. Hierin staan de volgende kolommen:
% - norm (afkeurgrens)
% - opgave hoogte in jaar T
% - opgave piping in jaar T
% - opgave macrostabiliteit in jaar T
% - kosten
% - jaar ontstaan opgave
%
% Ontwikkeld door HKV en Deltares in opdracht van RWS-WVL, maart 2017

%% inlezen dbf van basisshape
[~,~,raw_tmp] = xlsread(dbf);
% kolom met vakids in shape
col_id = find(ismember(raw_tmp(1,:),'VakId'));
vakken_shp = 0;
% overige kolommen verwijderen
raw = raw_tmp(:,col_id);

%% definitie variabelen
var_str{1} = 'norm';
var_str{2} = 'kosten';
var_str{3} = 'kostenkm';
var_str{4} = 'joo_ht';
var_str{5} = 'joo_mac';
var_str{6} = 'joo_pip';
n1 = length(var_str); % aantal var. onafhankelijk van opgavejaren
m = length(exp4figs.T_opgave); % aantal opgavejaren
for i = 1:m
    var_str{n1+(i-1)*3+1} = ['opht' num2str(exp4figs.T_opgave(i))];
    var_str{n1+(i-1)*3+2} = ['oppi' num2str(exp4figs.T_opgave(i))];
    var_str{n1+(i-1)*3+3} = ['opma' num2str(exp4figs.T_opgave(i))];
end
n2 = length(var_str)-n1; % aantal var. afhankelijk van opgavejaren
n3 = n1+n2; % totaal aantal var.
% headers maken
for j=1:n3
    raw{1,1+j}=var_str{j};    
end

% loop over alle vakken
for i=2:size(raw,1) 
    vakken_shp(i-1,1) = str2num(raw{i,1});
    vak = vakken_shp(i-1,1);
    ind = find(ismember(exp4figs.VNKvakken,vak)); % vak zoeken in exp4figs
    if isempty(ind) % vak in shape is niet doorgerekend
        % alle variabelen onafhankelijk van jaar
        raw{i,1+1}=-999;    % norm
        raw{i,1+2}=-999;    % kosten
        raw{i,1+3}=-999;    % kosten/km
        raw{i,1+4}=-999;    % jaar opgave ht
        raw{i,1+5}=-999;    % jaar opgave mac
        raw{i,1+6}=-999;    % jaar opgave pip
        % loop over alle variabelen afhankelijk van jaar
        for j=n1+1:n3
            raw{i,1+j}=-999;    
        end
    else
        % alle variabelen onafhankelijk van jaar
        % norm
        raw{i,1+1}=1/VAR.norm.normkans(find(VAR.norm.vak_data==vak),1);    % norm
        % kosten ncw hele dijkvak [mln euro]
        kost = exp4figs.kosten_dijk_ncw(ind,1);
        raw{i,1+2} = kost;
        % kosten/km ncw
        ind5 = find(ismember(cell2mat(exp4figs.vaklengte(2:end,1)),vak),1);
        vaklengte = cell2mat(exp4figs.vaklengte(1+ind5,2));
        if ~isempty(vaklengte)
            raw{i,1+3} = 1000*kost/vaklengte;
        else
            raw{i,1+3} = -999;
        end
        % jaar ontstaan opgave
        raw{i,1+4} = exp4figs.akj_ht(ind,2);
        raw{i,1+5} = exp4figs.akj_mac(ind,2);
        raw{i,1+6} = exp4figs.akj_pip(ind,2);
            
        % loop over alle variabelen afhankelijk van jaar        
        for t=1:m
            % index opgavejaar
            ind_t = find(exp4figs.T==exp4figs.T_opgave(t)); 
            if isempty(ind_t) % geen berekening uitgevoerd in opgavejaar
                raw{i,1+n1+(t-1)*(n2/m)+1} = -999; % hoogte
                raw{i,1+n1+(t-1)*(n2/m)+2} = -999; % piping
                raw{i,1+n1+(t-1)*(n2/m)+3} = -999; % macrostab
            else
                % hoogte
                if exp4figs.Pf_3_t(ind,1)==-1 || isnan(exp4figs.Pf_3_t(ind,1))
                    raw{i,1+n1+(t-1)*(n2/m)+1} = -999;
                else
                    raw{i,1+n1+(t-1)*(n2/m)+1} = exp4figs.dH_opg(ind,ind_t); % hoogte
                end

                % piping
                if exp4figs.Pf_2_t(ind,1)==-1 || isnan(exp4figs.Pf_2_t(ind,1))
                    raw{i,1+n1+(t-1)*(n2/m)+2} = -999;
                else
                    raw{i,1+n1+(t-1)*(n2/m)+2} = exp4figs.dP_opg(ind,ind_t); % hoogte
                end

                % macrostab
                if exp4figs.Pf_1_t(ind,1)==-1 || isnan(exp4figs.Pf_1_t(ind,1))
                    raw{i,1+n1+(t-1)*(n2/m)+3} = -999;
                else
                    raw{i,1+n1+(t-1)*(n2/m)+3} = exp4figs.dS_opg(ind,ind_t); % hoogte
                end
            end
        end
    end
end
xlswrite(uitvoernaam,raw)

end
