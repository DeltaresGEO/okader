function DVschema = update_DVschema(DVschema,HYD,jaar,ontw_duur,VakID,invoer)
% Toestaan van tijdelijk hogere faalkans in bepaalde gevallen door 
% aanpassing van forcering dijkversterkingen. 
% 
% Voorwaarden: (1) afvoerscenario moet tot 2050 constant zijn, (2)
% versterking moet voor 2050 plaatsvinden
% Dan wordt jaar waarv��r niet versterkt mag worden, aangepast naar einde
% ontwerplevensduur (met een max. van 2050). 
%
% Ontwikkeld door HKV en Deltares in opdracht van RWS-WVL, maart 2017

if strcmp(invoer.belastingtype,'QTQH') % default
    % bepaal aantal afvoerscenarios tot 2050 (jaar_voldoen_norm)
    for i=1:length(HYD.QTscendata)
        jaren(i) = HYD.QTscendata(i).Tstart;
    end
    % n = length(jaren(jaren<=2050));
    n = length(jaren(jaren<invoer.t_voldoen_norm));

    if n<=1 && jaar<invoer.t_voldoen_norm % max. 1 afvoerscenarios tot 2050 EN versterking voor 2050
        update = min(jaar+ontw_duur,invoer.t_voldoen_norm);
        DVschema(DVschema==VakID,4)=update;
        DVschema(DVschema==VakID,2)=0; % aanpassing pr3192
    else % niet updaten

    end
elseif strcmp(invoer.belastingtype,'HT') % nog niet getest
    % bepaal aantal HTscenarios tot 2050 (jaar_voldoen_norm)
    for i=1:length(HYD.HTscendata)
        jaren(i) = HYD.HTscendata(i).Tstart;
    end
    % n = length(jaren(jaren<=2050));
    n = length(jaren(jaren<invoer.t_voldoen_norm));

    if n<=1 && jaar<invoer.t_voldoen_norm % max. 1 HTscenarios tot 2050 EN versterking voor 2050
        update = min(jaar+ontw_duur,invoer.t_voldoen_norm);
        DVschema(DVschema==VakID,4)=update;
    else % niet updaten

    end    
end
