function Pf_data = faalkansberekenen(STERKTE,id,HYD,vak_vnk,str,link_to_figures,T,t0,mechanism,met_figuren,invoer)
% script berekent de faalkansen voor piping en macrostabiliteit voor range van dijkbreedtes
%
% Ontwikkeld door HKV en Deltares in opdracht van RWS-WVL, maart 2017


vc_data = STERKTE.vc_data12{1,id};
dext_data = STERKTE.dext_data12{1,id};
fc_data = STERKTE.fc_data12{1,id};
xdata_wb = HYD.h_pdf(:,1);
ydata_wb = HYD.h_pdf(:,2);
H_bs = HYD.H_bs;

Pf_data = cell(1,2);
dext_sel = dext_data(vc_data == vak_vnk);
if ~isempty(dext_sel)     
    dext_sorted = sort(dext_sel);
else
    dext_sorted = 0:1:3;
end
for i = 1:length(dext_sorted)
    % searchFC zoekt de FC van het juiste vak en juiste dext
    [x_temp,y_temp] = searchFC(vc_data,dext_data,fc_data,dext_sorted(i),vak_vnk);
    % editFC doet extrapolaties etc in FC's
    [pf_s,xdata_wb_new,ydata_wb_new] = editFC(x_temp,y_temp,xdata_wb,ydata_wb,invoer.fkbr_nn);
    product = pf_s.*ydata_wb_new;
    % invloed bewezen sterkte
    product(xdata_wb_new<H_bs)=0;
    Pf = trapz(xdata_wb_new,product);
    Pf_data{1}(i) = Pf;
    Pf_data{2}(i) = dext_sorted(i);

    if met_figuren == 1 || (met_figuren == 2 && i==1) || (met_figuren == 3 && i==1 && T==t0) || (met_figuren == 4 && i==1 && strcmp(str,'ontwerp'))
    	plot_pf_wb(xdata_wb_new,ydata_wb_new,pf_s,product,HYD,Pf,mechanism,vak_vnk,dext_sorted,i,T,link_to_figures,str)
    end
end
temp = diff(Pf_data{1});
f1 = find(temp>0, 1);
if ~isempty(f1)
    return;
end

end