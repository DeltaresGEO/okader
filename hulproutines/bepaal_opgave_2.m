function [dh,dberm,dkwel,h_pdf,H_freq_vak_t] = bepaal_opgave_2(HYD,T,t,zichtjaar_wb,VNKvakken,v,sc_qh,TMP,invoer,STERKTE,link_to_figures,t0,VAR,logfile,faalmechanisme)
% in functie bepaal_opgave_2.m wordt opgave berekende aan de
% hand van opgegeven ontwerp-belastingsituatie. 
% - belasting berekenen
% - faalkansen voor verschillende dimensies berekenen
% - benodigde dimensies berekenen
% - correctie van opgaves met maximum en factor
% Ontwikkeld door HKV en Deltares in opdracht van RWS-WVL, maart 2017,
% aangepast augustus 2017

jaar = T(t);
M=TMP.M;

% ontwerpbelasting bepalen
[HYD] = maak_h_pdf(HYD,invoer,jaar,zichtjaar_wb,VNKvakken(v),sc_qh,TMP.toeslag,logfile);

% % updaten FC met waterstand t+ontwerp_duur
if STERKTE.fc_file == 1
    if faalmechanisme.stbi == 1
        Pf_mstab_data  = faalkansberekenen_v1(STERKTE.fc_data_stbi,VNKvakken(v),'ontwerp',HYD,link_to_figures,zichtjaar_wb,T(1),'macrostab',invoer.plot_pf_s,invoer.db_afkap_s,invoer.fkbr_nn);
    else
        Pf_mstab_data = [];
    end
    if faalmechanisme.stph == 1
        Pf_pip_data = faalkansberekenen_v1(STERKTE.fc_data_stph,VNKvakken(v),'ontwerp',HYD,link_to_figures,zichtjaar_wb,T(1),'piping',invoer.plot_pf_p,invoer.db_afkap_p,invoer.fkbr_nn);
    else
        Pf_pip_data = [];
    end
else
    Pf_mstab_data = faalkansberekenen(STERKTE,1,HYD,VNKvakken(v),'ontwerp',link_to_figures,zichtjaar_wb,T(1),'macrostab',invoer.plot_pf_s);
    Pf_pip_data = faalkansberekenen(STERKTE,2,HYD,VNKvakken(v),'ontwerp',link_to_figures,zichtjaar_wb,T(1),'piping',invoer.plot_pf_p);
end
if faalmechanisme.ht == 1
    Pf_hoogte_data = faalkansberekenen_hoogte(STERKTE,HYD,VNKvakken(v),'ontwerp',link_to_figures,zichtjaar_wb,T(1),'hoogte',invoer);
else
    Pf_hoogte_data = [];
end
% zoeken FC zodanig dat op t+ontwerp_duur voldoet
for m_ind=1:length(M)
    m  = M(m_ind);
    if m == 1 % macrostab
        dberm = dextbepaling(Pf_mstab_data,VAR.Pf_eis_1_v(v,t),0:invoer.dS_stap:invoer.dS_max);
    elseif m == 2 % piping
        dkwel = dextbepaling(Pf_pip_data,VAR.Pf_eis_2_v(v,t),0:invoer.dP_stap:invoer.dP_max);
    else % hoogte
        dh = dextbepaling(Pf_hoogte_data,VAR.Pf_eis_3_v(v,t),0:invoer.dH_stap:invoer.dH_max);
    end
end
if ~exist('dberm','var')
    dberm = nan;
end
if ~exist('dkwel','var')
    dkwel = nan;
end
if ~exist('dh','var')
    dh = nan;
end

% % opgave corrigeren
% dberm = min(dberm*VAR.corr_dS(VAR.corr_dS(:,1)==VNKvakken(v),3),VAR.corr_dS(VAR.corr_dS(:,1)==VNKvakken(v),2));
% dkwel = min(dkwel*VAR.corr_dP(VAR.corr_dS(:,1)==VNKvakken(v),3),VAR.corr_dS(VAR.corr_dP(:,1)==VNKvakken(v),2));
% dh = min(dh*VAR.corr_dH(VAR.corr_dS(:,1)==VNKvakken(v),3),VAR.corr_dH(VAR.corr_dS(:,1)==VNKvakken(v),2));
% % 
% % opgave corrigeren met factor en maximum
% if size(VAR.corr_dP,2)>3 % informatie deklaag+L opgenomen
%     data = VAR.corr_dP(VAR.corr_dP(:,1)==VNKvakken(v),4:6);
%     if data(2)>4 && data(1)>0.5*VAR.dP(v,t) 
%         pipfac=0.2;
%     elseif data(2)>4
%         pipfac=0.75;
%     elseif data(1)>0.5*VAR.dP(v,t)
%         pipfac=0.75;
%     else
%         pipfac=1;
%     end
% else
%     pipfac = VAR.corr_dP(VAR.corr_dP(:,1)==VNKvakken(v),3);
% end
% macfac = VAR.corr_dS(VAR.corr_dS(:,1)==VNKvakken(v),3);
% htfac = VAR.corr_dH(VAR.corr_dH(:,1)==VNKvakken(v),3);
% pipmax = VAR.corr_dP(VAR.corr_dP(:,1)==VNKvakken(v),2);
% macmax = VAR.corr_dS(VAR.corr_dS(:,1)==VNKvakken(v),2);
% htmax = VAR.corr_dH(VAR.corr_dH(:,1)==VNKvakken(v),2);
% % VAR.dH_corr(v,t) = max(0,min(htfac.*VAR.dH(v,t),htmax));
% % VAR.dS_corr(v,t) = max(0,min(macfac.*VAR.dS(v,t),macmax));
% % VAR.dP_corr(v,t) = max(0,min(pipfac.*VAR.dP(v,t),pipmax));
% dh = max(0,min(htfac.*dh,htmax));
% dberm = max(0,min(macfac.*dberm,macmax));
% dkwel = max(0,min(pipfac.*dkwel,pipmax));
% % VAR.htfac(v,t) = htfac;
% % VAR.macfac(v,t) = macfac;
% % VAR.pipfac(v,t) = pipfac;

h_pdf = HYD.h_pdf;
H_freq_vak_t = HYD.H_freq_vak_t;
end
