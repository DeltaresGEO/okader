function flag = check_versterken(VNKvakken,v,VAR,T,t,TMP,faalmechanisme)
% Bepaal in zichtjaar (t) of vak (v) versterkt moet worden. versterking uitvoeren als:
% -Faalkans voor ��n van de mechanismen groter is dan de faalkanseis. 
% Uitzondering1: �niet versterken v��r jaar X� geforceerd in DVschema. 
% Uitzondering2: accepteren tijdelijk hogere faalkans v��r 2050 (zie uitleg bij� ). 
% -Versterking van dit vak wordt in dit jaar geforceerd in DVschema
%
% Ontwikkeld door HKV en Deltares in opdracht van RWS-WVL, maart 2017


vakid = VNKvakken(v);
jaar = T(t);

% check of vak in HWBP programma zit
ind_hwbp = find(vakid==VAR.DVschema(:,1));
if isempty(ind_hwbp)
    disp('Error: Vak niet gevonden in DVschema csv bestand')
end
% kolom 2=switch of kolom 3 of 4 gebruikt moet worden
% kolom 3=geforceerd versterken in jaartal
% kolom 4=niet versterken v��r jaartal
hwbp_check = VAR.DVschema(ind_hwbp,2); % 0=versterking niet uitvoeren v��r bepaald jaar, 1=versterking uitvoeren in bepaald jaar
if (hwbp_check==1 && VAR.DVschema(ind_hwbp,3)>jaar) || (hwbp_check==0 && VAR.DVschema(ind_hwbp,4)>jaar) % vak mag nog niet versterkt worden
    flag='nee';
else
    % toetsen of Pf<Pf,eis voor alle m
    if faalmechanisme.stbi == 1 && faalmechanisme.stph == 1 && faalmechanisme.ht == 1
        if TMP.Pf_1>VAR.Pf_eis_1_v(v,t) || TMP.Pf_2>VAR.Pf_eis_2_v(v,t) || TMP.Pf_3>VAR.Pf_eis_3_v(v,t) || (hwbp_check==1 && VAR.DVschema(ind_hwbp,3)==jaar)
            flag='ja';
        elseif TMP.Pf_1<=VAR.Pf_eis_1_v(v,t) && TMP.Pf_2<=VAR.Pf_eis_2_v(v,t) && TMP.Pf_3<=VAR.Pf_eis_3_v(v,t) % voldoet op alle faalmechanismen
            flag='nee';
        end
    elseif faalmechanisme.stbi == 1 && faalmechanisme.stph == 1 && faalmechanisme.ht == 0
        if TMP.Pf_1>VAR.Pf_eis_1_v(v,t) || TMP.Pf_2>VAR.Pf_eis_2_v(v,t) || (hwbp_check==1 && VAR.DVschema(ind_hwbp,3)==jaar)
            flag='ja';
        elseif TMP.Pf_1<=VAR.Pf_eis_1_v(v,t) && TMP.Pf_2<=VAR.Pf_eis_2_v(v,t) % voldoet op alle faalmechanismen
            flag='nee';
        end
    elseif faalmechanisme.stbi == 1 && faalmechanisme.stph == 0 && faalmechanisme.ht == 1
        if TMP.Pf_1>VAR.Pf_eis_1_v(v,t) || TMP.Pf_3>VAR.Pf_eis_3_v(v,t) || (hwbp_check==1 && VAR.DVschema(ind_hwbp,3)==jaar)
            flag='ja';
        elseif TMP.Pf_1<=VAR.Pf_eis_1_v(v,t) && TMP.Pf_3<=VAR.Pf_eis_3_v(v,t) % voldoet op alle faalmechanismen
            flag='nee';
        end
    elseif faalmechanisme.stbi == 0 && faalmechanisme.stph == 1 && faalmechanisme.ht == 1
        if TMP.Pf_2>VAR.Pf_eis_2_v(v,t) || TMP.Pf_3>VAR.Pf_eis_3_v(v,t) || (hwbp_check==1 && VAR.DVschema(ind_hwbp,3)==jaar)
            flag='ja';
        elseif TMP.Pf_2<=VAR.Pf_eis_2_v(v,t) && TMP.Pf_3<=VAR.Pf_eis_3_v(v,t) % voldoet op alle faalmechanismen
            flag='nee';
        end
    elseif faalmechanisme.stbi == 1 && faalmechanisme.stph == 0 && faalmechanisme.ht == 0
        if TMP.Pf_1>VAR.Pf_eis_1_v(v,t) || (hwbp_check==1 && VAR.DVschema(ind_hwbp,3)==jaar)
            flag='ja';
        elseif TMP.Pf_1<=VAR.Pf_eis_1_v(v,t) % voldoet op alle faalmechanismen
            flag='nee';
        end
    elseif faalmechanisme.stbi == 0 && faalmechanisme.stph == 1 && faalmechanisme.ht == 0
        if TMP.Pf_2>VAR.Pf_eis_2_v(v,t) || (hwbp_check==1 && VAR.DVschema(ind_hwbp,3)==jaar)
            flag='ja';
        elseif TMP.Pf_2<=VAR.Pf_eis_2_v(v,t) % voldoet op alle faalmechanismen
            flag='nee';
        end
    elseif faalmechanisme.stbi == 0 && faalmechanisme.stph == 0 && faalmechanisme.ht == 1
        if TMP.Pf_3>VAR.Pf_eis_3_v(v,t) || (hwbp_check==1 && VAR.DVschema(ind_hwbp,3)==jaar)
            flag='ja';
        elseif TMP.Pf_3<=VAR.Pf_eis_3_v(v,t) % voldoet op alle faalmechanismen
            flag='nee';
        end
    end
end
