function export_resultaten(invoer,VAR,uitvoerdir,vaklengte)
% exporteert berekeningsresultaten (kosten, opgaves, faalkansen,
% verzamelbestand)
%
% Ontwikkeld door HKV en Deltares in opdracht van RWS-WVL, maart 2017

%cell2mat(vaklengte(2:end,1:2))
%% export resultaten kosten 
invoer.exp_kosten_traject = invoer.exp_traject;
% kosten per vak en jaar in csv
if invoer.exp_kosten_type == 2 || invoer.exp_kosten_type == 3
    exportkosten(invoer,VAR,VAR.kosten_dijk,[uitvoerdir,'kosten_dijk_'])
    exportkosten(invoer,VAR,VAR.kosten_dijk_cw,[uitvoerdir,'kosten_dijk_cw_'])
end
% uitvoer met uitsplitsingen van KOSWAT
if invoer.exp_kosten_type == 1 || invoer.exp_kosten_type == 3
    Tabel = cell2table(VAR.cost_out_new);
    writetable(Tabel,[uitvoerdir,'kosten_uitsplitsing_KOSWAT.csv'],'Delimiter',';')
end
%% export resultaten faalkansen
invoer.exp_faalkans_traject = invoer.exp_traject;
exportkansen(invoer,VAR,[uitvoerdir,'faalkans_'],vaklengte)

%% export resultaten opgaves

if invoer.exp_opgave_type == 1
    exportopgave(invoer,VAR,[uitvoerdir,'opgave_'])
end
if invoer.exp_versterking_type == 1
    exportversterking(invoer,VAR,[uitvoerdir,'versterking_'])
end

%% export jaar ontstaan opgave
VAR = export_afkeurjaren(VAR,[uitvoerdir,'jaar_ontstaan_opgave.csv']);

%% export resultaten voor figuren in een structure
exp4figs = save_export(VAR.VNKvakken,VAR.T,VAR,invoer,vaklengte,uitvoerdir);

%% export resultatenshape
if invoer.exp_shape == 1
    uitvoernaam = [uitvoerdir,'resultatenshape.xlsx'];
    export_shape(VAR,exp4figs,invoer.dbf_shape,uitvoernaam)
end

end
