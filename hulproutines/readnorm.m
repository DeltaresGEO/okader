function [VAR] = readnorm(VAR,file)
% inlezen faalkanseisen (norm, faalkansruimte, lengte-effect)
%
% Ontwikkeld door HKV en Deltares in opdracht van RWS-WVL, maart 2017

fid = fopen(file);
temp1 = textscan(fid,'%s','delimiter','\n','HeaderLines',1);
vak = zeros(length(temp1{1}),1);
p_eis_mstab = vak;
p_eis_pip = vak;
p_eis_hoogte = vak;
trjlengte = vak;
normkans = vak;
a_pip = vak;
a_mac = vak;
b_pip = vak;
b_mac = vak;
for i = 1:length(temp1{1})
    temp2 = regexp(char(temp1{1}(i)),';','split');
    vak(i) = str2double(strrep(temp2(2),',','.'));
    normtraject{i,1} = strrep(temp2{1,3},',','.');
    p_eis_mstab(i) = str2double(strrep(temp2(17),',','.'));
    p_eis_pip(i) = str2double(strrep(temp2(16),',','.'));
    p_eis_hoogte(i) = str2double(strrep(temp2(15),',','.'));
    trjlengte(i) = str2double(strrep(temp2(5),',',''));
    normkans(i) = 1./str2double(strrep(temp2(6),',',''));
    a_pip(i) = str2double(strrep(temp2(11),',',''));
    a_mac(i) = str2double(strrep(temp2(12),',',''));
    b_pip(i) = str2double(strrep(temp2(13),',',''));
    b_mac(i) = str2double(strrep(temp2(14),',',''));
end
fclose(fid);

VAR.norm.vak_data = vak;
VAR.norm.Pf_eis_1_data = p_eis_mstab;
VAR.norm.Pf_eis_2_data = p_eis_pip;
VAR.norm.Pf_eis_3_data = p_eis_hoogte;
VAR.norm.trjlengte = trjlengte;
VAR.norm.normkans = normkans;
VAR.norm.normtraject = normtraject;
VAR.norm.a_pip = a_pip;
VAR.norm.a_mac = a_mac;
VAR.norm.b_pip = b_pip;
VAR.norm.b_mac = b_mac;

end