function fc_data = read_fc(fn)
% Inlezen van fragility curves van piping en macrostabiliteit
%
% Ontwikkeld door HKV en Deltares in opdracht van RWS-WVL, maart 2017

fc_data = struct('vak',[],'d',[],'mu',[],'sig',[],'kh',[],'bu_t',[],'ws_min',[],'ws_max',[]);
fid = fopen(fn);
temp = textscan(fid,'%f%f%f%f%f%f%f%f%f%f%f%f','delimiter',';','headerlines',1);
fc_data.vak = temp{1};
fc_data.d = temp{2};
fc_data.mu = temp{3};
fc_data.sig = temp{4};
fc_data.kh = temp{5};
fc_data.bu_t = temp{6};
fc_data.ws_min = temp{7};
fc_data.ws_max = temp{8};
fc_data.ext = temp{9};
fclose(fid);
end