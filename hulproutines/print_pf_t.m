function print_pf_t(T,t0,VAR,v,vaknaam,link_to_figures,VNKvakken,HYD)
% Plotten van faalkansverlopen per vak
%
% Ontwikkeld door HKV en Deltares in opdracht van RWS-WVL, maart 2017

Pf_eis_1 = VAR.Pf_eis_1_v(v,1);
Pf_eis_2 = VAR.Pf_eis_2_v(v,1);
Pf_eis_3 = VAR.Pf_eis_3_v(v,1);
Pf_1_t = VAR.Pf_1_t;
Pf_2_t = VAR.Pf_2_t;
Pf_3_t = VAR.Pf_3_t;
check_const = VAR.check_const;
kosten_dijk = VAR.kosten_dijk;
QdHjaren = HYD.QdHjaren;
 
s = figure('visible','off');
% plot faalkansen
plot(T,Pf_1_t(v,:),'-r',T,Pf_2_t(v,:),'-b',T,Pf_3_t(v,:),'-g','LineWidth',1.4);
hold on;
% plot faalkanseisen
plot(T,ones(1,length(T)).*Pf_eis_1,'--r',T,ones(1,length(T)).*Pf_eis_2,'--b',...
    T,ones(1,length(T)).*Pf_eis_3,'--g');
grid on;
% plot(t0+find(kosten_dijk(v,:)>0),max([Pf_1_t(v,:) Pf_2_t(v,:) Pf_3_t(v,:)])*ones(1,length(find(kosten_dijk(v,:)>0))),'Marker','*','LineStyle','none','Color','k')
plot(T(kosten_dijk(v,:)>0),max([Pf_1_t(v,:) Pf_2_t(v,:) Pf_3_t(v,:)])*ones(1,length(find(kosten_dijk(v,:)>0))),'Marker','*','LineStyle','none','Color','k')
% inzet van rvm markeren
if length(QdHjaren)==1 % geen rvm
    plot(0,max([Pf_1_t(v,:) Pf_2_t(v,:) Pf_3_t(v,:)]),'Marker','v','LineStyle','none','Color','k')                    
else
    plot(QdHjaren(2:end),max([Pf_1_t(v,:) Pf_2_t(v,:) Pf_3_t(v,:)]),'Marker','v','LineStyle','none','Color','k')
end
% xlim([t0 t0+length(T)])
xlim([T(1) T(end)])
title(['Verloop van faalkans, vak ' vaknaam],'interpreter','none');
xlabel('Tijd [jaar]');
ylabel('Faalkans [per jaar]');
set(gca,'YScale','log');
if check_const(v) == 1 % constructieve maatregel piping
    t_constr = min(find(Pf_2_t(v,:) == 0));
    plot([T(t_constr) T(t_constr)],[get(gca,'Ylim')],'k--','LineWidth',1.4)
%     legend({'Faalkans Macro Stabiliteit','Faalkans Piping','Faalkans Hoogte','Faalkanseis Macro Stabiliteit','Faalkanseis Piping','Faalkanseis Hoogte','Dijkversterking','Inzet RVM','Constructieve Maatregel Piping'},...
%         'Location','NorthEastOutside','FontSize',8);
    legend({'Faalkans Macrostabiliteit','Faalkans Piping','Faalkans Hoogte','Faalkanseis Macro Stabiliteit','Faalkanseis Piping','Faalkanseis Hoogte','Dijkversterking','Rivierverruiming','Constructieve Maatregel Piping'},...
        'Location','NorthEastOutside','FontSize',8);
else
%     legend({'Faalkans Macro Stabiliteit','Faalkans Piping','Faalkans Hoogte','Faalkanseis Macro Stabiliteit','Faalkanseis Piping','Faalkanseis Hoogte','Dijkversterking','Inzet RVM'},...
%         'Location','NorthEastOutside','FontSize',8);
    legend({'Faalkans Macrostabiliteit','Faalkans Piping','Faalkans Hoogte','Faalkanseis Macro Stabiliteit','Faalkanseis Piping','Faalkanseis Hoogte','Dijkversterking','Rivierverruiming'},...
        'Location','NorthEastOutside','FontSize',8);
end
%     saveas(s,[link_to_figures '\pf met t_' num2str(VNKvakken(v)) ...
%         '_db_' num2str(dberm_stap) '_dk_' num2str(dkwel_stap) '_dh_' num2str(dh_stap) '.tiff']);
%             saveas(s,[link_to_figures '\pf met t_' num2str(VNKvakken(v)) '.tiff']);
print([link_to_figures '\pf met t_' num2str(VNKvakken(v))],'-dpng');
close(s);
