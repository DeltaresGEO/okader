function fc_data = readFChoogte(filename)
% Inlezen van fragility curves van overslag
%
% Ontwikkeld door HKV en Deltares in opdracht van RWS-WVL, maart 2017

fc_data = cell(1,3);
temp1 = dlmread(filename);
xdata_wb = temp1(1,2:end);
vak_data = temp1(2:end,1);
fc_hoogte_data = temp1(2:end,2:end);
fc_data{1} = xdata_wb;
fc_data{2} = vak_data;
fc_data{3} = fc_hoogte_data;
end