function VAR = export_afkeurjaren(VAR,uitvoernaam)
% Schrijft het jaar waarin de faalkans voor het eerst boven de faalkanseis 
% uitkomt naar een *.csv file. 
% 
% invoer        = structure met invoer
% VAR           = structure met data die tijdens berekening wordt gevuld
% uitvoernaam   = basis van uitvoerbestand. Wordt aangevuld met faalmechanisme
%
% Ontwikkeld door HKV en Deltares in opdracht van RWS-WVL, maart 2017

n = size(VAR.Pf_1_t,1);   % n=aantal vakken

%% headers toevoegen
export{1,1} = 'VakID';
export{1,2} = 'jaar_ht';
export{1,3} = 'jaar_mac';
export{1,4} = 'jaar_pip';
for i = 2:n+1
    export{i,1} = VAR.VNKvakken(i-1);
end

%% per vak de matrix vullen
for i = 2:n+1
    ind_vak = find(VAR.jnvv(:,1)==VAR.VNKvakken(i-1),1);
    jnvv = VAR.jnvv(ind_vak,4); % jaren 'niet versterken voor'
    if jnvv>VAR.T(end) % alleen als niet versterkt wordt in rekenperiode
        jaar_ht = VAR.T(VAR.Pf_3_t(i-1,:)>=VAR.Pf_eis_3_v(i-1,:)); % HT
        jaar_mac = VAR.T(VAR.Pf_1_t(i-1,:)>=VAR.Pf_eis_1_v(i-1,:)); % MAC
        jaar_pip = VAR.T(VAR.Pf_2_t(i-1,:)>=VAR.Pf_eis_2_v(i-1,:)); % PIP
        if ~isempty(jaar_ht)
            export{i,2} = jaar_ht(1); % HT
        else
            export{i,2} = 9999; % HT
        end
        if ~isempty(jaar_mac)
            export{i,3} = jaar_mac(1); % HT
        else
            export{i,3} = 9999; % HT
        end
        if ~isempty(jaar_pip)
            export{i,4} = jaar_pip(1); % HT
        else
            export{i,4} = 9999; % HT
        end   
        
    else
        export{i,2} = -999;
        export{i,3} = -999;
        export{i,4} = -999;
    end    
end
VAR.akj_ht = cell2mat(export(2:end,[1 2]));
VAR.akj_mac = cell2mat(export(2:end,[1 3]));
VAR.akj_pip = cell2mat(export(2:end,[1 4]));
cell2csv(uitvoernaam,export,';',2010,'.');        

end