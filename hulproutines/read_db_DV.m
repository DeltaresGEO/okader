function [data,check] = read_db_DV(T,VNKvakken,invoerfile)

% inlezen dijkversterkingen
I = csvimport(invoerfile,'delimiter',';');
data = cell2mat(I(2:end,2:end));
% laatste regel kopieren omdat csvimport laatste regel niet leest
data = vertcat(data,data(end,:))';

% controle op gelijke vakken en jaren als in invoer
% try
%     vakken = str2num(cell2mat(I(1,2:end)'));
% catch
%     vakken = str2num(cell2mat(I(1,2:end)));
% end
vakken = str2num(char(I(1,2:end)'));
jaar_1 = str2double(I{2,1}(end-3:end));
jaar_2 = str2double(I{3,1}(end-3:end));
jaar_stap = jaar_2-jaar_1;
jaar_end = str2double(I{end,1}(end-3:end))+1; % +1 omdat csvimport laatste regel niet leest
jaren = jaar_1:jaar_stap:jaar_end;

if isequal(jaren,T) && isequal(vakken,VNKvakken)
    check = 0;
else
    check = 1;
end

end