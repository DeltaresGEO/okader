function d3 = controle_deel3(fid1,invoer,d3)
% db_onz
% db_bodm
% dh_int
% extrapolatie
% scenarios QT
% scenarios QH

fprintf(fid1,'%s\n','Deel 3. Controleren parameters "Hydraulische belastingen" ');

%% db_onz
try
    if exist(invoer.db_onz_toeslag,'file') > 0
        fprintf(fid1,'%s\n',['- Onzekerheidstoeslag databases: file ' invoer.db_onz_toeslag ' is gevonden.']);
        %check de inhoud?
    else
        fprintf(fid1,'%s\n',['- Onzekerheidstoeslag databases: file ' invoer.db_onz_toeslag ' is niet gevonden. Controleer de filenaam.***']);
        d3 = d3+1;
    end
catch
    fprintf(fid1,'%s\n','- Onzekerheidstoeslag databases file is niet aangegeven.***');
    d3 = d3+1;
end

%% db_bodm
try
    if exist(invoer.db_bodemdaling,'file') > 0
        fprintf(fid1,'%s\n',['- Bodemdaling databases: file ' invoer.db_bodemdaling 'is gevonden.']);
        %check de inhoud?
    else
        fprintf(fid1,'%s\n',['- Bodemdaling databases: file ' invoer.db_bodemdaling 'is niet gevonden. Controleer de filenaam.***']);
        d3 = d3+1;
    end
catch
    fprintf(fid1,'%s\n','- Bodemdaling databases file is niet aangegeven.***');
    d3 = d3+1;
end

%% dh_int
try
    %check de inhoud?
    fprintf(fid1,'%s\n',['- Integrale waterstand verhoging/verlaging is ' num2str(invoer.dh_integraal) ' m (- voor verlaging en + voor verhoging).']);
catch
    fprintf(fid1,'%s\n','- Integrale waterstand verhoging/verlaging is niet aangegeven.***');
    d3 = d3+1;
end

%% extrapolatie
try
    if invoer.extrapolatie < 1 || invoer.extrapolatie > 2
        fprintf(fid1,'%s\n',['- Extrapolatie methode voor de werklijn is ' num2str(invoer.extrapolatie) ' is buiten de range. De mogelijke opties zijn 1 = loglinear of 2 = parallel loglinear. ***']);
        d3 = d3+1;
    else
        if invoer.extrapolatie == 1
            ss = 'loglinear';
        else
            ss = 'parallel loglinear';
        end
        fprintf(fid1,'%s\n',['- Extrapolatie methode voor de werklijn is ' num2str(invoer.extrapolatie) '(' ss ').']);
    end
catch
    fprintf(fid1,'%s\n','- Extrapolatie methode voor de werklijn is niet aangegeven.***');
    d3 = d3+1;
end
fprintf(fid1,'%s\n','');

%% scenarios QT
fprintf(fid1,'%s\n','Controleren scenarios voor QT');
if ~isempty(invoer.QT_n)
    if invoer.QT_n < 1
        fprintf(fid1,'%s\n','QT scenarios zijn niet gedefinieerd.***');
        d3 = d3+1;
    else
        fprintf(fid1,'%s\n',['- Aantal scenarios = ' num2str(invoer.QT_n) '.']);
        for i = 1:invoer.QT_n
            % sce_id
            fprintf(fid1,'%s\n',['- Scenario id ' num2str(i) ' is gevonden.']);
            % t
            fprintf(fid1,'%s\n',['- Begintijd voor scenario ' num2str(i) ' is gevonden: ' num2str(invoer.QTscenarios{i,2}) '.']);
            % QT_files
%             for j = 1:3
            for j = 3:length(invoer.QTscenarios(i,:))
                [~, status] = str2num(invoer.QTscenarios{i,j}(end-7:end-4));
                if exist(invoer.QTscenarios{i,j},'file') && status == 1
                    fprintf(fid1,'%s\n',['- QT_file ' num2str(j-2) ' voor scenario ' num2str(i) ' is: ' invoer.QTscenarios{i,j}]);
                else
                    fprintf(fid1,'%s\n',['- File ' invoer.QTscenarios{i,j} ' voor QT_file ' num2str(j-2) ', scenario ' num2str(i) ' is niet gevonden of laatste karakters zijn geen jaartal. Controleer de filenaam.***']);
                    d3 = d3+1;
                end
            end
            
        end
    end
else
    fprintf(fid1,'%s\n','QT scenarios zijn niet gedefinieerd.***');
    d3 = d3 +1;
end
fprintf(fid1,'%s\n','');

%% scenarios QH
fprintf(fid1,'%s\n','Controleren scenarios voor QH');
if ~isempty(invoer.QH_n)
    if invoer.QH_n < 1
        fprintf(fid1,'%s\n','QH scenarios zijn niet gedefinieerd.***');
        d3 = d3 +1;
    else
        fprintf(fid1,'%s\n',['- Aantal scenarios = ' num2str(invoer.QH_n) '.']);
        for i = 1:invoer.QH_n
            % sce_id
            fprintf(fid1,'%s\n',['- Scenario id ' num2str(i) ' is gevonden.']);
            % t
            fprintf(fid1,'%s\n',['- Begintijd voor scenario ' num2str(i) ' is gevonden: ' num2str(invoer.QHscenarios{i,2}) '.']);
            % QH_files
%             for j = 1:3
            for j = 3:length(invoer.QHscenarios(i,:))
                [~, status] = str2num(invoer.QHscenarios{i,j}(end-7:end-4));
                if exist(invoer.QHscenarios{i,j},'file') && status == 1
                    fprintf(fid1,'%s\n',['- QH_file ' num2str(j-2) ' voor scenario ' num2str(i) ' is: ' invoer.QHscenarios{i,j}]);
                else
                    fprintf(fid1,'%s\n',['- File ' invoer.QHscenarios{i,j} ' voor QH_file ' num2str(j-2) ', scenario ' num2str(i) ' is niet gevonden of laatste karakters zijn geen jaartal. Controleer de filenaam.***']);
                    d3 = d3 +1;
                end
            end
        end
    end
else
    fprintf(fid1,'%s\n','QH scenarios zijn niet gedefinieerd.***');
    d3 = d3 +1;
end
fprintf(fid1,'%s\n','');


