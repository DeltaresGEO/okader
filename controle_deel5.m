function d5 = controle_deel5(fid1,invoer,d5)

fprintf(fid1,'%s\n','Deel 5. Controleren parameters "Berekening Instellingen" ');

%pip_factor
try % Verzoek van HKV om te verwijderen d.d. 11 juli 2018
    if invoer.pip_factor < 0 || invoer.pip_factor > 1
        fprintf(fid1,'%s\n',['- STPH bermfactor is ' num2str(invoer.pip_factor) ' moet minimaal 0 en maximaal 1 zijn.***']);
        d5 = d5+1;
    else
        fprintf(fid1,'%s\n',['- STPH bermfactor is ' num2str(invoer.pip_factor) '.']);
    end
catch
    % Verzoek van HKV om te verwijderen d.d. 11 juli 2018
    %fprintf(fid1,'%s\n','- STPH bermfactor is niet aangegeven.***');
    %d5 = d5+1;
end
%mac_factor
try % Verzoek van HKV om te verwijderen d.d. 11 juli 2018
    if invoer.mac_factor < 0 || invoer.mac_factor > 1
        fprintf(fid1,'%s\n',['- STBI bermfactor is ' num2str(invoer.mac_factor) ' moet minimaal 0 en maximaal 1 zijn.***']);
        d5 = d5+1;
    else
        fprintf(fid1,'%s\n',['- STBI bermfactor is ' num2str(invoer.mac_factor) '.']);
    end
catch
    % Verzoek van HKV om te verwijderen d.d. 11 juli 2018
    %fprintf(fid1,'%s\n','- STBI bermfactor is niet aangegeven.***');
    %d5 = d5+1;
end
%dS_stap
try
    if invoer.dS_stap < 0
        fprintf(fid1,'%s\n',['- Stapontwikkeling voor STBI berm is ' num2str(invoer.dS_stap) ' is lager dan 0. De waarde moet hoger zijn dan 0.***']);
        d5 = d5+1;
    else
        fprintf(fid1,'%s\n',['- Stapontwikkeling voor STBI berm is ' num2str(invoer.dS_stap) '.']);
    end
catch
    fprintf(fid1,'%s\n','- Stapontwikkeling voor STBI berm is niet aangegeven.***');
    d5 = d5+1;
end
%dP_stap
try
    if invoer.dP_stap < 0
        fprintf(fid1,'%s\n',['- Stapontwikkeling voor STPH berm is ' num2str(invoer.dP_stap) ' is lager dan 0. De waarde moet hoger zijn dan 0.***']);
        d5 = d5+1;
    else
        fprintf(fid1,'%s\n',['- Stapontwikkeling voor STPH berm is ' num2str(invoer.dP_stap) '.']);
    end
catch
    fprintf(fid1,'%s\n','- Stapontwikkeling voor STPH berm is niet aangegeven.***');
    d5 = d5+1;
end
%dH_stap
try
    if invoer.dH_stap < 0
        fprintf(fid1,'%s\n',['- Stapontwikkeling voor kruinhoogte is ' num2str(invoer.dH_stap) ' is lager dan 0. De waarde moet hoger zijn dan 0.***']);
        d5 = d5+1;
    else
        fprintf(fid1,'%s\n',['- Stapontwikkeling voor kruinhoogte is ' num2str(invoer.dH_stap) '.']);
    end
catch
    fprintf(fid1,'%s\n','- Stapontwikkeling voor kruinhoogte is niet aangegeven.***');
    d5 = d5+1;
end

%dH_max
try
    fprintf(fid1,'%s\n',['- dH_max is ' num2str(invoer.dH_max) '.']);
catch
    fprintf(fid1,'%s\n','- dH_max is niet aangegeven.***');
    d5 = d5+1;
end

%dS_max
try
    fprintf(fid1,'%s\n',['- dS_max is ' num2str(invoer.dS_max) '.']);
catch
    fprintf(fid1,'%s\n','- dS_max is niet aangegeven.***');
    d5 = d5+1;
end

%dP_max
try
    fprintf(fid1,'%s\n',['- dP_max is ' num2str(invoer.dP_max) '.']);
catch
    fprintf(fid1,'%s\n','- dP_max is niet aangegeven.***');
    d5 = d5+1;
end

% Qdisc_step
try
    fprintf(fid1,'%s\n',['- Qdisc_step is ' num2str(invoer.Qdisc_step) '.']);
catch
    fprintf(fid1,'%s\n','- (deel) afvoer discretisatie Qdisc_step is niet aangegeven.***');
    d5 = d5+1;
end
% Qdisc_high
try
    fprintf(fid1,'%s\n',['- Qdisc_high is ' num2str(invoer.Qdisc_high) '.']);
catch
    fprintf(fid1,'%s\n','- (deel) afvoer discretisatie Qdisc_high is niet aangegeven.***');
    d5 = d5+1;
end
try % Verzoek van HKV om te verwijderen d.d. 11 juli 2018
    fprintf(fid1,'%s\n',['- Qdisc_min is ' num2str(invoer.Qdisc_min) '.']);
    fprintf(fid1,'%s\n',['- Qdisc_low is ' num2str(invoer.Qdisc_low) '.']);
    fprintf(fid1,'%s\n',['- Qdisc_high is ' num2str(invoer.Qdisc_high) '.']);
catch
    % Verzoek van HKV om te verwijderen d.d. 11 juli 2018
    %fprintf(fid1,'%s\n','- (deel) afvoer discretisatie Qdisc is niet aangegeven.***');
    %d5 = d5+1;
end

% refjaar_bodemdaling       
try
    fprintf(fid1,'%s\n',['- refjaar_bodemdaling is ' num2str(invoer.refjaar_bodemdaling) '.']);
catch
    fprintf(fid1,'%s\n','- refjaar_bodemdaling is niet aangegeven.***');
    d5 = d5+1;
end

% t_voldoen_norm       
try
    fprintf(fid1,'%s\n',['- t_voldoen_norm is ' num2str(invoer.t_voldoen_norm) '.']);
catch
    fprintf(fid1,'%s\n','- t_voldoen_norm is niet aangegeven.***');
    d5 = d5+1;
end

% cost_breakeven_dP       
try % Verzoek van HKV om te verwijderen d.d. 11 juli 2018
    fprintf(fid1,'%s\n',['- cost_breakeven_dP is ' num2str(invoer.cost_breakeven_dP) '.']);
catch
    % Verzoek van HKV om te verwijderen d.d. 11 juli 2018
    %fprintf(fid1,'%s\n','- cost_breakeven_dP is niet aangegeven.***');
    %d5 = d5+1;
end
fprintf(fid1,'%s\n','');
