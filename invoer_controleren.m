% function [a,b,c,d] = invoer_controleren(invoer,fnd,werk_dir)
function [d1,d2,d3,d4,d5,d6] = invoer_controleren(invoer,fnd,werk_dir)
% Controles op de invoerfile
% Deel 1: Algemeen
% Deel 2: Dijksterkte
% Deel 3: Hydraulische belasting
% Deel 4: Dijkversterking
% Deel 5: Berekening instelling
% Deel 6: uitvoer en visualisatie

%% logfile voor invoer
% a = 0; b = 0; c = 0; d = 0;% a voor invoer, b = uitvoer, c voor instelling, d voor scenarios
d1 = 0; d2 = 0; d3 = 0; d4 = 0; d5 = 0; d6 = 0;
%fid1 = fopen([werk_dir 'check_' strrep(fnd,'.txt','.ctr') ],'wt');
fid1 = fopen([werk_dir strrep(fnd,'.txt','.ctr') ],'wt');

%% Beginstuk
fprintf(fid1,'%s\n','#### Tool Kostenreductie Dijkversterking door Rivierverruiming ####');
fprintf(fid1,'%s\n','#### versie 2020.01 ####');
fprintf(fid1,'%s\n','#### HKV en Deltares in opdracht van RWS-WVL, 2017-2020 ####');
fprintf(fid1,'%s\n',' ');
fprintf(fid1,'%s\n',['#### Controleren invoerfile: ' werk_dir fnd ' ####']);
fprintf(fid1,'%s\n','(eventuele fouten zijn met *** aangegeven)');
fprintf(fid1,'%s\n','Ga naar "Samenvatting" om het aantal fouten te bekijken)');
fprintf(fid1,'%s\n','');

%% Deel 1: Algemeen
d1 = controle_deel1(fid1,invoer,d1);

%% Deel 2: Dijksterkte
d2 = controle_deel2(fid1,invoer,d2);

%% Deel 3: Hydraulische belasting
d3 = controle_deel3(fid1,invoer,d3);

%% Deel 4: Dijkversterking
d4 = controle_deel4(fid1,invoer,d4);

%% Deel 5: Berekening instelling
d5 = controle_deel5(fid1,invoer,d5);

%% Deel 6: uitvoer en visualisatie
d6 = controle_deel6(fid1,invoer,werk_dir,d6);

%% fouten samenvatting
fprintf(fid1,'%s\n','Samenvatting:');
fprintf(fid1,'%s\n',['- Invoerfouten "Algemeen": ' num2str(d1)]);
fprintf(fid1,'%s\n',['- Invoerfouten "Dijksterkte": ' num2str(d2)]);
fprintf(fid1,'%s\n',['- Invoerfouten "Hydraulische belasting": ' num2str(d3)]);
fprintf(fid1,'%s\n',['- Invoerfouten "Dijkversterking": ' num2str(d4)]);
fprintf(fid1,'%s\n',['- Invoerfouten "Berekening instelling": ' num2str(d5)]);
fprintf(fid1,'%s\n',['- Invoerfouten "Uitvoer en visualisatie": ' num2str(d6)]);
fprintf(fid1,'%s\n','');
fclose(fid1);

end