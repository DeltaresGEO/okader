%% Hoofdscript
clear; close all; clc;
try
    a = hoofdgui();
catch
    delete(gcf);
    clear; close all; clc;
    return;
end
try
    try
        temp = textscan(fopen('D:\loc_dir.dir'),'%s','delimiter','\n');
    catch
        try 
            temp = textscan(fopen('C:\loc_dir.dir'),'%s','delimiter','\n');
        catch
            temp = textscan(fopen([prefdir '\loc_dir.dir']),'%s','delimiter','\n'); %02-09-2019
        end
    end
    dir_next = char(temp{1});
    if exist(dir_next,'dir') > 0
        dir_start = dir_next;
    else
        dir_start = '';
    end
catch
    dir_start = '';
end
if a == 1 %enkele berekening
    try
        [fn,directory,invoer] = runnen_gui(dir_start);
        % dir opslaan
        dir_next = directory;
        try
            dlmwrite('D:\loc_dir.dir',dir_next,'');
        catch
            try
                dlmwrite('C:\loc_dir.dir',dir_next,'');
            catch
                dlmwrite([prefdir '\loc_dir.dir'],dir_next,''); %02-09-2019
            end
        end
    catch
        delete(gcf);
        clear; close all; clc;
        return;
    end
else % batch berekening
    try
        [fn,directory,invoer] = runnen(dir_start);
        if isnumeric(fn)
            delete(gcf);
            clear; close all; clc;
            return;
        end
        % dir opslaan
        dir_next = directory;
        try
            dlmwrite('D:\loc_dir.dir',dir_next,'');
        catch
            try
                dlmwrite('C:\loc_dir.dir',dir_next,'');
            catch
                dlmwrite([prefdir '\loc_dir.dir'],dir_next,''); %02-09-2019
            end
        end
    catch
        delete(gcf);
        clear; close all; clc;
        return;
    end
end
analyse(fn,directory,invoer);