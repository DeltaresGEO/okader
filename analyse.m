function analyse(fn,werk_dir,invoer)
if iscellstr(fn)
    n = length(fn);
    fnm = fn;
else
    n = 1;
    fnm = cellstr(fn);
end
for i = 1:n
    if isempty(invoer)
        %Invoer inlezen
        % system('taskkill /F /IM EXCEL.EXE');
        invoer = invoer_inlezen(char(fnm(i)),werk_dir);
        [d1,d2,d3,d4,d5,d6] = invoer_controleren(invoer,char(fnm(i)),werk_dir);
        dtot = d1+d2+d3+d4+d5+d6;
    else
        dtot = 0;
    end
    if dtot == 0
        %Analyse
        if invoer.analysetype == 1
            stp = rekenhart1(invoer,i,n,char(fnm(i)));
        elseif invoer.analysetype == 2
            %nog te beschrijven
        elseif invoer.analysetype == 3
            %nog te beschrijven
        end
        if stp == 1
            stg = 0;
            break;
        else
            stg = 0;
            close all;
            invoer = [];
        end
    else
        stp = 1;
        stg = 1;        
        stg_str = ['      Fouten in ' char(fnm(i)) ' gevonden. Berekening is gestopt!'];
        break;
    end
end
try
    if stp == 0 && stg == 0
        uiwait(msgbox('          Berekening voltooid'));
    elseif stp == 1 && stg == 0
        uiwait(warndlg('         Berekening is gestopt!','***Warning***'));
    elseif stp == 1 && stg == 1
        uiwait(warndlg(stg_str));
    else
        uiwait(warndlg('         Berekening is gestopt!','***Warning***'));
    end
catch
    uiwait(warndlg('         Berekening is gestopt!','***Warning***'));
end

% terug naar begin GUI na uiwait
begin_GUI;
end