function d6 = controle_deel6(fid1,invoer,werk_dir,d6)
%uitvoer_map
%tijdopgave
%pf_vs_t
%dims_vs_t
% plot faalkans STBI/STPH/HT
%plot_pdf_wb
%vis_dijk

fprintf(fid1,'%s\n','Deel 6. Controleren parameters "Uitvoer en visualisatie" ');

%uitvoer_map
%mm = exist([werk_dir invoer.uitvoer_map],'dir');
mm = exist(invoer.uitvoer_map,'dir');
if strcmp(invoer.uitvoer_map,'- kies de link naar de uitvoermap via de knop naast deze balk -')
    fprintf(fid1,'%s\n','- Uitvoermap is onbekend.');
    d6 = d6+1;
else
    try
        if mm ~= 7
            mkdir(invoer.uitvoer_map);
            fprintf(fid1,'%s\n',['- Uitvoermap: map ' invoer.uitvoer_map ' is net gemaakt.']);
        else
            fprintf(fid1,'%s\n',['- Uitvoermap: map ' invoer.uitvoer_map ' is al gemaakt.']);
        end
    catch
        fprintf(fid1,'%s\n','- Uitvoermap kan niet worden gemaakt. Controleer de link naar de uitvoermap.');
        d6 = d6+1;
    end
end

%tijdopgave
fft = strfind(invoer.tijdopgave,',');
[~, status] = str2num(invoer.tijdopgave);
try
    if isempty(invoer.tijdopgave)
        fprintf(fid1,'%s\n','- Tijdopgave is niet aangegeven.***');
        d6 = d6+1;
    else
        if isempty(fft) && status==0 % geen comma's een geen enkel getal
            fprintf(fid1,'%s\n','- Tijdopgave is niet correct aangegeven. Voorbeeld van tijdopgave: 2015,2020,2050,2075,...,2100.***');
            d6 = d6+1;
        elseif ~isempty(fft) && status==0
            fprintf(fid1,'%s\n','- Tijdopgave is niet correct aangegeven. Voorbeeld van tijdopgave: 2015,2020,2050,2075,...,2100.***');
            d6 = d6+1;
        else
            %check de inhoud?
            fprintf(fid1,'%s\n',['- Tijdopgave is ' invoer.tijdopgave ' jaar.']);
        end
    end
catch
    fprintf(fid1,'%s\n','- Tijdopgave is niet aangegeven.***');
    d6 = d6+1;
end

%pf_vs_t
try
    if invoer.plot_pf_vs_t > 1
        fprintf(fid1,'%s\n','- Visualisatie pf vs t is niet correct. Dit is niet ok! De keuze is 1 (aan) of 0 (uit).***');
        d6 = d6+1;
    else
        if invoer.plot_pf_vs_t == 0
            fprintf(fid1,'%s\n',['- Visualisatie pf vs t is ' num2str(0) '.']);
        else
            fprintf(fid1,'%s\n',['- Visualisatie pf vs t is ' num2str(invoer.plot_pf_vs_t) '.']);
        end
    end
catch
    fprintf(fid1,'%s\n','- Visualisatie pf vs t is niet aangegeven.***');
    d6 = d6+1;
end

%dims_vs_t
try
    if invoer.plot_dims_vs_t > 2 
        fprintf(fid1,'%s\n','- Visualisatie dimensies vs t is niet correct. Dit is niet ok! De keuze is 1 (aan) of 0 (uit).***');
        d6 = d6+1;
    else
        if invoer.plot_dims_vs_t == 2
            fprintf(fid1,'%s\n',['- Visualisatie dimensies vs t is ' num2str(0) '.']);
        else
            fprintf(fid1,'%s\n',['- Visualisatie dimensies vs t is ' num2str(invoer.plot_dims_vs_t) '.']);
        end
    end
catch
    fprintf(fid1,'%s\n','- Visualisatie dimensies vs t is niet aangegeven.***');
    d6 = d6+1;
end

% plot faalkans STBI
try
    if invoer.plot_pf_s > 5
%         fprintf(fid1,'%s\n',['- Visualisatie faalkans STBI is ' num2str(invoer.plot_pf_s) '. Dit is niet ok! De keuze is 1 (figuren faalkansberekenen) of 2 (voor iedere versterkingsstap) of 3 (alleen zonder versterkingen en T=t0) of 9 (nee).***']);
        fprintf(fid1,'%s\n','- Visualisatie faalkans STBI is niet correct. Dit is niet ok! De keuze is 1, 2, 3, 4 of 0.***');
        d6 = d6+1;
    else
        if invoer.plot_pf_s == 5
            fprintf(fid1,'%s\n',['- Visualisatie faalkans STBI is ' num2str(0) '.']);
        else
            fprintf(fid1,'%s\n',['- Visualisatie faalkans STBI is ' num2str(invoer.plot_pf_s) '.']);
        end
    end
catch
    fprintf(fid1,'%s\n','- Visualisatie faalkans STBI is niet aangegeven.***');
    d6 = d6+1;
end
% plot faalkans STPH
try
    if invoer.plot_pf_p > 5
%         fprintf(fid1,'%s\n',['- Visualisatie faalkans STPH is ' num2str(invoer.plot_pf_p) '. Dit is niet ok! De keuze is 1 (figuren faalkansberekenen) of 2 (voor iedere versterkingsstap) of 3 (alleen zonder versterkingen en T=t0) of 9 (nee).***']);
        fprintf(fid1,'%s\n','- Visualisatie faalkans STPH is niet correct. Dit is niet ok! De keuze is 1, 2, 3, 4 of 0.***');
        d6 = d6+1;
    else
        if invoer.plot_pf_p == 5
            fprintf(fid1,'%s\n',['- Visualisatie faalkans STPH is ' num2str(0) '.']);
        else
            fprintf(fid1,'%s\n',['- Visualisatie faalkans STPH is ' num2str(invoer.plot_pf_p) '.']);
        end
    end
catch
    fprintf(fid1,'%s\n','- Visualisatie faalkans STPH is niet aangegeven.***');
    d6 = d6+1;
end
% plot faalkans HT
try
    if invoer.plot_pf_h > 5
%         fprintf(fid1,'%s\n',['- Visualisatie faalkans HT is ' num2str(invoer.plot_pf_h) '. Dit is niet ok! De keuze is 1 (figuren faalkansberekenen) of 2 (voor iedere versterkingsstap) of 3 (alleen zonder versterkingen en T=t0) of 9 (nee).***']);
        fprintf(fid1,'%s\n','- Visualisatie faalkans HT is niet correct. Dit is niet ok! De keuze is 1, 2, 3, 4 of 0.***');
        d6 = d6+1;
    else
        if invoer.plot_pf_h == 5
            fprintf(fid1,'%s\n',['- Visualisatie faalkans HT is ' num2str(0) '.']);
        else
            fprintf(fid1,'%s\n',['- Visualisatie faalkans HT is ' num2str(invoer.plot_pf_h) '.']);
        end
    end
catch
    fprintf(fid1,'%s\n','- Visualisatie faalkans HT is niet aangegeven.***');
    d6 = d6+1;
end

%plot_pdf_wb
try
    if invoer.plot_pdf_wb > 4
%         fprintf(fid1,'%s\n',['- Visualisatie waterstand pdf is ' num2str(invoer.plot_pdf_wb) '. Dit is niet ok! De keuze is 1 (ieder jaar), 2 (alleen in opgavejaar), 3 (in opgavejaar en bij ontwerp) of 9 (niet).***']);
        fprintf(fid1,'%s\n','- Visualisatie waterstand pdf is niet correct. Dit is niet ok! De keuze is 1,2,3 of 0.***');
        d6 = d6+1;
    else
        if invoer.plot_pdf_wb == 4
            fprintf(fid1,'%s\n',['- Visualisatie waterstand pdf is ' num2str(0) '.']);
        else
            fprintf(fid1,'%s\n',['- Visualisatie waterstand pdf is ' num2str(invoer.plot_pdf_wb) '.']);
        end
    end
catch
    fprintf(fid1,'%s\n','- Visualisatie waterstand pdf is niet aangegeven.***');
    d6 = d6+1;
end

% plot_dijk;
try
    if invoer.plot_dijk > 2
%         fprintf(fid1,'%s\n',['- Visualisatie waterstand pdf is ' num2str(invoer.plot_pdf_wb) '. Dit is niet ok! De keuze is 1 (ieder jaar), 2 (alleen in opgavejaar), 3 (in opgavejaar en bij ontwerp) of 9 (niet).***']);
        fprintf(fid1,'%s\n','- Visualisatie van dijkversterking is niet correct. Dit is niet ok! De keuze is 1 (ja) of 0 (nee).***');
        d6 = d6+1;
    else
        if invoer.plot_dijk == 2
            fprintf(fid1,'%s\n',['- Visualisatie van dijkversterking is ' num2str(0) '.']);
        else
            fprintf(fid1,'%s\n',['- Visualisatie van dijkversterking is ' num2str(invoer.plot_dijk) '.']);
        end
    end
catch
    fprintf(fid1,'%s\n','- Visualisatie van dijkversterking.***');
    d6 = d6+1;
end

%exp_traject
try
    if  invoer.exp_traject > 2
        fprintf(fid1,'%s\n','- Variabele exp_traject is niet correct. Dit is niet ok! De keuze is 1 (ja) of 0 (nee).***');
        d6 = d6+1;
    else
        if invoer.exp_traject == 2
            fprintf(fid1,'%s\n',['- Variabele exp_traject is ' num2str(0) '.']);
        else
            fprintf(fid1,'%s\n',['- Variabele exp_traject is ' num2str(invoer.exp_traject) '.']);
        end
    end
catch
    fprintf(fid1,'%s\n','- Variabele exp_traject is niet aangeveven in de invoerfile.***');
    d6 = d6+1;
end

%exp_shape
try
    if  invoer.exp_shape > 2
        fprintf(fid1,'%s\n','- Variabele exp_shape is niet correct. Dit is niet ok! De keuze is 1 (ja) of 0 (nee).***');
        d6 = d6+1;
    else
        if invoer.exp_shape == 2
            fprintf(fid1,'%s\n',['- Variabele exp_shape is ' num2str(0) '.']);
        else
            fprintf(fid1,'%s\n',['- Variabele exp_shape is ' num2str(invoer.exp_shape) '.']);
        end
    end
catch
    fprintf(fid1,'%s\n','- Variabele exp_shape is niet aangeveven in de invoerfile.***');
    d6 = d6+1;
end

%exp_shape dbf
if invoer.exp_shape == 1
    try
        if invoer.exp_shape == 1 && ~exist(invoer.dbf_shape,'file')
            fprintf(fid1,'%s\n',['- dbf dijkvakkenshape: file ' invoer.dbf_shape ' is niet gevonden. Controleer de filenaam.***']);
            d6 = d6+1;
        elseif invoer.exp_shape == 1 && exist(invoer.dbf_shape,'file') && strcmp(invoer.dbf_shape(end-3:end),'.dbf') == 0
            fprintf(fid1,'%s\n',['- dbf dijkvakkenshape: file ' invoer.dbf_shape ' is geen dbf. Controleer de extensie.***']);
            d6 = d6+1;      
        elseif invoer.exp_shape == 1 && exist(invoer.dbf_shape,'file') && strcmp(invoer.dbf_shape(end-3:end),'.dbf') == 1
            fprintf(fid1,'%s\n',['- dbf dijkvakkenshape: file ' invoer.dbf_shape ' is gevonden.']);
        else
            fprintf(fid1,'%s\n','- controleer dbf dijkvakkenshape.***');
            d6 = d6+1;  
        end
    catch
        fprintf(fid1,'%s\n','- dbf dijkvakkenshape is niet aangegeven.***');
        d6 = d6+1;
    end
else
    fprintf(fid1,'%s\n','- dbf dijkvakkenshape wordt niet gebruikt.');
end 
fprintf(fid1,'%s\n','');
