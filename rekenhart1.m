function stp = rekenhart1(invoer,fi,ntt,fnm)
% Script voor het berekenen van de faalkansen en dijkversterkingskosten bij 
% inzet van rivierverruimende maatregelen, gebaseerd op de afvoerstatistiek
% bij Lobith/Borgharen, afvoerwaterstandsrelaties, fragility curves en KOSWAT. 
% 
% Joost Pol (HKV), Gerbert Pleijter (HKV), Raymond van der Meij (Deltares) 
% en David Nugroho (Deltares)
% 
% versie 2.01, 31 maart 2017
%
% Ontwikkeld door HKV en Deltares in opdracht van RWS-WVL, maart 2017

%clearvars;close all;
tic; % timer
%diary on % opslaan van command window (gedeactiveerd i.v.m. een
%niet foutmelding voor het overschrijven van gegevens)
addpath hulproutines

%% Dit deel % verwijderen om rekenhart1.m zonder GUI te runnen
% %%% begin
% fi = 1;
% ntt = 1;
% % fnm = 'invoer_maas_v6.txt';
% % fnm = 'invoer_wa_dp_test6.txt';
% % fnm = '4_invoer_wa_dp_s031b_opgred.txt';
% % fnm = '5_invoer_wa_dp_s031b_opgfnm = '7_invoer_dp_s031b_vl_dl_min5.txt';
% % fnm = '6_invoer_wa_dp_s031b_ip.txt';
% % fnm = '7_invoer_wa_dp_s031b_FCvoorland.txt';
% % fnm = '8_invoer_wa_dp_s031b_deklaag.txt';
% % fnm = '1_invoer_dp_s031b.txt';
% % fnm = '2_invoer_dp_s031b_vl.txt';
% % fnm = '3_invoer_dp_s031b_vl_dl.txt';
% % fnm = '3_invoer_dp_s031b_vl_dl_test.txt';
% % fnm = '4_invoer_dp_s031b_vl_dl_ip.txt';
% % fnm = '6_invoer_dp_s031b_vl_min5.txt';
% % fnm = '7_invoer_dp_s031b_vl_dl_min5.txt';
% % fnm = '8_invoer_dp_s031b_vl_dl_ip_min5.txt';
% % fnm = '5a_invoer_dp_s031b_min5.txt';
% % fnm = 'test_MK.txt';
% % fnm = '9_invoer_dp_s031b_ip.txt';
% % fnm = '10_invoer_dp_s031b_ip_min5.txt';
% % fnm = 'invoer_bm_PB_G_dijkenW.txt';
% % fnm = '001_invoer_dp_s031b_2test_bs100.txt';
% % fnm = '002_invoer_dp_s031b_vl.txt';
% % fnm = '002_invoer_dp_s031b_vl2test.txt';
% % fnm = '002_invoer_dp_s031b_vl2test_bs100.txt';
% % fnm = '003_invoer_dp_s031b_vl_ip.txt';
% % fnm = '003_invoer_dp_s031b_vl_ip_test.txt';
% % fnm = '004_invoer_dp_s031b_overslag.txt';
% % fnm = '005_invoer_vksu_s031b.txt';
% % fnm = '006_invoer_vksu_s031b_vl.txt';
% % fnm = '007_invoer_vksu_s031b_vl_ip.txt';
% % fnm = '008_invoer_vksu_s031b_overslag.txt';
% 
% % fnm = '101_invoer_bm_ref_s031b.txt';
% % fnm = '102_invoer_bm_ref_s031b_vl.txt';
% % fnm = 'invoer_maas_ref_s031b.txt';
% % fnm = 'invoer_rijn_ref_s031b.txt';
% % fnm = 'invoer_nrl_ref_s031b.txt';
% 
% % werk_dir = 'd:\pr3514\Werkmap_deel1\oplossen_bugs\scripts\aanpassingen_tbv_ronde2\'; % locatie waar matlab script staat
% % werk_dir = 'd:\pr3514\Werkmap_deel1\oplossen_bugs\scripts\aanpassingen_tbv_ronde3\aanpassing_voorlanden_2018_2\'; % locatie waar matlab script staat
% werk_dir = 'd:\pr3514\Werkmap_deel1\oplossen_bugs\scripts\'; % locatie waar matlab script staat
% disp('invoer inlezen')
% invoer = invoer_inlezen(fnm,werk_dir);
% disp('invoer controleren')
% [fa,fb,fc,fd,fe,ff] = invoer_controleren(invoer,fnm,werk_dir);
% if fa+fb+fc+fd+fe+ff ~= 0
%    return;
% end
% %%% einde

%% waitbar
pr = 0;
hw = waitbar(pr,['Berekening  "' strrep(fnm,'_','\_') '" voorbereiden (' num2str(fi) '/' num2str(ntt) ')'],...
    'Name','Voorbereiden... (niet afbreken!)');
set(findall(hw),'Units','normalized');
set(hw,'Position', [0.25 0.4 0.6 0.08]);
stp = 0;

%% gecodeerde parameters
STERKTE.fc_file = 1; % 0 = .mat bestand met volledige curves, 1 = .csv bestand parameters, 
%%% Begin geavanceerde optie
% keuze om waterstandsfrequentielijn te gebruiken ipv afvoer+QH (Let op:
% deze functie is nog niet uitgebreid getest!)
invoer.belastingtype  = 'QTQH'; % 'QTQH' (default) of 'HT'
% invoer.belastingtype  = 'HT'; % 'HT' is geavanceerde optie waarvoor
% onderstaande invoer nodig is. Hieronder is een voorbeeld gegeven voor het
% opgeven van twee scenario's. hfreq_maas_ID1 start in 2015, en vanaf 2040
% wordt overgestapt op hfreq_maas_ID2.
invoer.HTscenarios = cell(2,5); % grootte cell array aangeven 2=aantal scenarios, 5=2+aantal zichtjaren
invoer.HTscenarios{1,1} = 1;    % ID1
invoer.HTscenarios{2,1} = 2;    % ID2
invoer.HTscenarios{1,2} = 2015; % startjaar 1
invoer.HTscenarios{2,2} = 2040; % startjaar 2
invoer.HTscenarios(1,3:end) = {'d:\HKV\pr3514.10_Kostenreductie_Tool\Matlab\testomgeving\databases\HT\hfreq_maas_ID1_2015.csv','d:\HKV\pr3514.10_Kostenreductie_Tool\Matlab\testomgeving\databases\HT\hfreq_maas_ID1_2050.csv','d:\HKV\pr3514.10_Kostenreductie_Tool\Matlab\testomgeving\databases\HT\hfreq_maas_ID1_2100.csv'};
invoer.HTscenarios(2,3:end) = {'d:\HKV\pr3514.10_Kostenreductie_Tool\Matlab\testomgeving\databases\HT\hfreq_maas_ID2_2015.csv','d:\HKV\pr3514.10_Kostenreductie_Tool\Matlab\testomgeving\databases\HT\hfreq_maas_ID2_2050.csv','d:\HKV\pr3514.10_Kostenreductie_Tool\Matlab\testomgeving\databases\HT\hfreq_maas_ID2_2100.csv'};
invoer.Hdisc_step = 0.01;       % discretisatiestap waterstandsfrequentielijn
invoer.Hdisc_low = 0.5;         % laagste waterstand Freq.lijn verlagen met dit getal
invoer.Hdisc_high = 2;          % hoogste waterstand Freq.lijn verhogen met dit getal
%%% Einde geavanceerde optie
%
% invoer.smooth_hcdf  = 0;
invoer.rep_1 = 1;   % reparatie QH relatie per zichtjaar uitvoeren? 0 = nee, 1=ja
invoer.rep_2 = 1;   % reparatie QH relaties in tijd uitvoeren? 0 = nee, 1=ja
invoer.rep_incr = 0.001;
invoer.fkbr_nn = 1000;
% invoer.cost_max_dS = 50;
invoer.exp_kosten_type = 3; % 0 = geen, 1=KOSWAT tabel, 2=per vak en jaar in csv, 3=beide 
invoer.exp_opgave_type = 1; % 0 = geen, 1=per vak en jaar in csv 
invoer.exp_versterking_type = 1; % 0 = geen, 1=per vak en jaar in csv 
% invoer.n_hpdf_filter = 999; % filter om pieken uit hpdf te halen
% opgavejaren
invoer.tijdopgave   = str2double(regexp(invoer.tijdopgave,',','split'));    % [jaar] jaren waarin opgave (dH, dS, dP) wordt bepaald
waitbar(5,hw);

%% uitbreiding om opgave hard op te leggen (later in GUI verwerken)
%invoer.opl_DV = 0; % 1=optie hard opleggen gebruiken, dan worden onderstaande resultaten ingelezen
%invoer.db_DV_dH = 'd:\pr3514\databases_maas\resultaat_maas_ref_s031b\uitvoer\versterking_hoogte_vakniveau.csv';
%invoer.db_DV_dS = 'd:\pr3514\databases_maas\resultaat_maas_ref_s031b\uitvoer\versterking_stabiliteit_vakniveau.csv';
%invoer.db_DV_dP = 'd:\pr3514\databases_maas\resultaat_maas_ref_s031b\uitvoer\versterking_piping_vakniveau.csv';

%% uitbreiding om opgaves per vak te corrigeren(later in GUI verwerken, vervangt maxpipberm en pipfac/macfac)
% % invoer.db_corr_dH = 'd:\pr3514\Werkmap_deel1\oplossen_bugs\scripts\databases\correctie_dH9.csv';
% % invoer.db_corr_dS = 'd:\pr3514\Werkmap_deel1\oplossen_bugs\scripts\databases\correctie_dS7.csv';
% % invoer.db_corr_dP = 'd:\pr3514\Werkmap_deel1\oplossen_bugs\scripts\databases\correctie_dP8.csv';
% % invoer.db_corr_dP = 'd:\pr3514\Werkmap_deel1\oplossen_bugs\scripts\databases\correctie_dP8_deklaag.csv';
% invoer.db_corr_dH = 'd:\pr3514\Werkmap_deel1\oplossen_bugs\scripts\databases_rijn_incl_nrl\correctie_dH.csv';
% invoer.db_corr_dS = 'd:\pr3514\Werkmap_deel1\oplossen_bugs\scripts\databases_rijn_incl_nrl\correctie_dS.csv';
% invoer.db_corr_dP = 'd:\pr3514\Werkmap_deel1\oplossen_bugs\scripts\databases_rijn_incl_nrl\correctie_dP.csv';
% % invoer.db_corr_dP = 'd:\pr3514\Werkmap_deel1\oplossen_bugs\scripts\databases\correctie_dP_deklaag.csv';
% % invoer.db_corr_dP = 'd:\pr3514\Werkmap_deel1\oplossen_bugs\scripts\databases\correctie_dP_deklaag_maxcreep.csv';
% invoer.db_pipcost = 'd:\pr3514\Werkmap_deel1\oplossen_bugs\scripts\databases_rijn_incl_nrl\pipcost.csv';
% % invoer.db_pipcost = 'd:\pr3514\Werkmap_deel1\oplossen_bugs\scripts\databases\pipcost_dummyronde1.csv';
   
%% bestanden laden
VNKvakken       = unique(textread(invoer.db_vakken)); % lijst met VNK vakken die in analyse worden meegenomen
VAR.ontwerpduur = dlmread(invoer.db_ontwerp_duur,';',1,0);
if invoer.indexeren == 1
    VAR.CBSindex    = dlmread(invoer.db_CBSindex,';',1,0);
    VAR.prijsindex  = VAR.CBSindex(VAR.CBSindex(:,1)==invoer.prijspeil,3);
else
    VAR.prijsindex  = 1;
end    
vaklengte       = csvimport([invoer.db_vaklengte],'delimiter',';');
[VAR]           = readnorm(VAR,invoer.db_norm);
%maxpipberm      = csvimport([invoer.db_maxpipberm],'delimiter',';');
%maxpipberm      = cell2mat(maxpipberm(2:end,:));
onzekerheid     = dlmread(invoer.db_onz_toeslag,';',1,0);
[VAR.cost_basis.num,VAR.cost_basis.txt,VAR.cost_basis.raw]= xlsread(invoer.db_KOSWAT); % KOSWAT database inlezen
% VAR.nieuwe_kostendatabase_gebruiken = 1; % voor testen (0 voor oude berekening, 1 voor nieuwe berekening)
% if VAR.nieuwe_kostendatabase_gebruiken == 1
[mi,ni] = size(VAR.cost_basis.num);
if ni < 26 % extra kolom in oude kostendatabase toevoegen
    num_data = [VAR.cost_basis.num(:,1:15) ...
        zeros(mi,1) ...
        VAR.cost_basis.num(:,16:17) ...
        zeros(mi,1) ...
        VAR.cost_basis.num(:,18:19) ...
        zeros(mi,1) ...
        zeros(mi,1) ...
        VAR.cost_basis.num(:,20:22)];
    str_data = [VAR.cost_basis.txt(:,1:15) ...
        [cellstr('Kwelscherm Lengte [m]') ; num2cell(zeros(mi,1))] ...
        VAR.cost_basis.txt(:,16:17) ...
        [cellstr('Stabiliteitswand Lengte [m]') ; num2cell(zeros(mi,1))] ...
        VAR.cost_basis.txt(:,18:19) ...
        [cellstr('Kistdam Lengte [m]') ; num2cell(zeros(mi,1))] ...
        [cellstr('Kistdam Breedte [m]') ; num2cell(zeros(mi,1))] ...
        VAR.cost_basis.txt(:,20:22)];
    raw_data = [VAR.cost_basis.raw(:,1:15) ...
        [cellstr('Kwelscherm Lengte [m]') ; num2cell(zeros(mi,1))] ...
        VAR.cost_basis.raw(:,16:17) ...
        [cellstr('Stabiliteitswand Lengte [m]') ; num2cell(zeros(mi,1))] ...
        VAR.cost_basis.raw(:,18:19) ...
        [cellstr('Kistdam Lengte [m]') ; num2cell(zeros(mi,1))] ...
        [cellstr('Kistdam Breedte [m]') ; num2cell(zeros(mi,1))] ...
        VAR.cost_basis.raw(:,20:22)];
    VAR.cost_basis.num = num_data;
    VAR.cost_basis.txt = str_data;
    VAR.cost_basis.raw = raw_data;
    clear num_data str_data raw_data;
end
% end
%if invoer.constlength == 0 %terugzetten naar het format van de oude kostendatabase
    %num_data = [VAR.cost_basis.num(:,1:15) VAR.cost_basis.num(:,17:18) VAR.cost_basis.num(:,20:21) VAR.cost_basis.num(:,24:26)];
    %str_data = [VAR.cost_basis.txt(:,1:15) VAR.cost_basis.txt(:,17:18) VAR.cost_basis.txt(:,20:21) VAR.cost_basis.txt(:,24:26)];
    %raw_data = [VAR.cost_basis.raw(:,1:15) VAR.cost_basis.raw(:,17:18) VAR.cost_basis.raw(:,20:21) VAR.cost_basis.raw(:,24:26)];
    %VAR.cost_basis.num = num_data;
    %VAR.cost_basis.txt = str_data;
    %VAR.cost_basis.raw = raw_data;
    %clear num_data str_data raw_data;
%end
if invoer.constlength == 1
    temp = dlmread(invoer.db_constlength,';',1,0);
    VAR.db_constlength.vak = temp(:,1);
    VAR.db_constlength.max_kwelscherm = temp(:,2);
    VAR.db_constlength.max_stabwand = temp(:,3);
    clear temp;
end

VAR.DVschema    = dlmread(invoer.db_DVschema,';',1,0); 
VAR.jnvv        = VAR.DVschema; % gebruiken voor bepalen afkeurjaar
link_to_prof_data = [invoer.db_vakken(1:strfind(invoer.db_vakken,'databases')+8) '\dijkprofielen.xlsx'];
if invoer.plot_dijk == 1
    profieldata = read_profielen(link_to_prof_data);
%     profieldata = read_profielen(invoer.db_profieldata);
end
VAR.corr_dH = dlmread(invoer.db_corr_dH,';',1,0);
VAR.corr_dS = dlmread(invoer.db_corr_dS,';',1,0);
VAR.corr_dP = dlmread(invoer.db_corr_dP,';',1,0);
VAR.pipcost = dlmread(invoer.db_pipcost,';',1,0);
waitbar(10,hw);

%% Indentificeren en inlezen FC gegevens (vak, d-berm, d-kwel)
% Lijst van faalmechanismen (in goede volgorde) -> kan worden uitgebreid
% stbi = macrostabiliteit = 1, stph = piping = 2, ht = hoogte = 3
faalmechanisme.stbi = invoer.faalmech_s; %1 = dit faalmechanisme wordt meegenomen
faalmechanisme.stph = invoer.faalmech_p; %1 = dit faalmechanisme wordt meegenomen
faalmechanisme.ht = invoer.faalmech_h; %1 = dit faalmechanisme wordt meegenomen

% Macrostab -- 1 en Piping -- 2
if STERKTE.fc_file == 1 % default
    if faalmechanisme.stbi == 1
        STERKTE.fc_data_stbi = read_fc(invoer.db_FC_s);
    end
    if faalmechanisme.stph == 1
        STERKTE.fc_data_stph = read_fc(invoer.db_FC_p);
    end
    STERKTE.vc_data12   = {[] []}; % alleen gebruikt bij STERKTE.fc_file == 0
    STERKTE.dext_data12 = {[] []}; % alleen gebruikt bij STERKTE.fc_file == 0
    STERKTE.fc_data12   = {[] []}; % alleen gebruikt bij STERKTE.fc_file == 0
else % functionaliteit om oude format Fragility Curves in .mat file in te lezen
    load(invoer.db_FC_s);
    load(invoer.db_FC_p);
    STERKTE.vc_data12   = {vc_mstab_data vc_pip_data};
    STERKTE.dext_data12 = {d_berm_data d_kwel_data};
    STERKTE.fc_data12   = {fc_data_mstab fc_data_pip};
    STERKTE.fc_data_stbi = []; % alleen gebruikt bij STERKTE.fc_file == 1
    STERKTE.fc_data_stph = []; % alleen gebruikt bij STERKTE.fc_file == 1
end
% Overloop-overslag -- 3
if faalmechanisme.ht == 1
    STERKTE.fc_data3    = readFChoogte(invoer.db_FC_h);
end
waitbar(25,hw);
        
%% mappen aanmaken
link_to_figures = [invoer.uitvoer_map '\figures\'];
if ~exist(link_to_figures,'dir')
    mkdir(link_to_figures)
end
uitvoerdir      = [invoer.uitvoer_map '\uitvoer\'];
if ~exist(uitvoerdir,'dir')
    mkdir(uitvoerdir)
end

logfile = [uitvoerdir 'logfile_berekening.txt'];
invoer.logfile = logfile;
% if exist([uitvoerdir 'diary.txt'],'file')
%     delete([uitvoerdir 'diary.txt'])
% end
waitbar(50,hw);

%% initialisatie
T           = invoer.t_begin:invoer.t_stap:invoer.t_eind; %Door te rekenen tijdsreeks
[VAR,TMP]   = initialisatie(VAR,T,VNKvakken);
% controleren welke faalmechanismen zijn meegenomen in de analyse
TMP.M = check_mechanism(faalmechanisme); %1 = stbi, 2 = stph, 3 = ht
waitbar(75,hw);

%% opgave hard opleggen
if invoer.opl_DV == 1 % opgelegde dijkversterking
    [VAR.DV_dH,error1] = read_db_DV(T,VNKvakken,invoer.db_DV_dH);    
    [VAR.DV_dS,error2] = read_db_DV(T,VNKvakken,invoer.db_DV_dS);    
    [VAR.DV_dP,error3] = read_db_DV(T,VNKvakken,invoer.db_DV_dP);
    if error1==1 || error2==1 || error3==1 % jaren of vakken in invoer komen niet overeen met opgelegde opgave
        invoer.opl_DV = 0;
    end
end
try 
    set(hw,'Position', [0.25 0.4 0.4 0.08]);
catch
    stp = 1;
    return;
end

%% Preprocessing afvoerstatistiek en QH relaties
[HYD] = maak_h_pdf_pre(invoer,invoer.logfile);
waitbar(100,hw);

%% Per vak, jaar en faalmechanisme toetsen of kering aan norm voldoet
% loop over jaren
aa = 1; % initialisatie
for v=1:length(VNKvakken)
% for v=1:10
    try
        % Bepalen faalkanseisen
        try
            VAR.Pf_eis_1_v(v,:) = VAR.norm.Pf_eis_1_data(VAR.norm.vak_data == VNKvakken(v));
            VAR.Pf_eis_2_v(v,:) = VAR.norm.Pf_eis_2_data(VAR.norm.vak_data == VNKvakken(v));
            VAR.Pf_eis_3_v(v,:) = VAR.norm.Pf_eis_3_data(VAR.norm.vak_data == VNKvakken(v));
        catch
            fileID = fopen(logfile,'a');
            fprintf(fileID,'%s\n',['ERROR in faalkanseisen vak ' num2str(VNKvakken(v))]);
            fclose(fileID);    
        end
        
%         % maximum aan pipingberm (vanuit creepfactor)
%         try
%             VAR.afkapgrens(v)=maxpipberm(maxpipberm(:,1)==VNKvakken(v),2);
%         catch
%             fileID = fopen(logfile,'a');
%             fprintf(fileID,'%s\n',['ERROR in maximale pipingberm vak ' num2str(VNKvakken(v))]);
%             fclose(fileID);    
%         end
        
        %% loop over jaren
        for t=1:length(T) 
            display(['Kosten berekenen, vak ' num2str(VNKvakken(v)) ', t=' num2str(T(t)) ...
                '/' num2str(T(end))]);

            % waterstandstatistiek voor dit vak en dit jaar
            TMP.toeslag     = invoer.dh_integraal + onzekerheid(onzekerheid(:,1)==VNKvakken(v),2);
            TMP.sc_qh       = find(HYD.QdHjaren<=T(t)); % index referentie en welke maatregelen worden meegenomen in belasting
%             [HYD]           = maak_h_pdf(HYD,invoer,T(t),T(t),VNKvakken(v),TMP.sc_qh,TMP.toeslag,invoer.freq_bs,logfile);
            [HYD]           = maak_h_pdf(HYD,invoer,T(t),T(t),VNKvakken(v),TMP.sc_qh,TMP.toeslag,logfile);
            
            % waterstandsdata om op te slaan in uitvoer
            if t==1
                VAR.ws_data(v).vak = VNKvakken(v);
                VAR.ws_data(v).jaar = T(t);
                VAR.ws_data(v).h_pdf = HYD.h_pdf;
            end

            % waterstands pdf plotten
            if invoer.plot_pdf_wb == 1 || ( (invoer.plot_pdf_wb == 2 || invoer.plot_pdf_wb == 3) && ismember(T(t),invoer.tijdopgave)) 
                plot_hpdf(T,t,HYD,VNKvakken,v,link_to_figures,'Waterstand','wb')
            end

            % Berekenen faalkansen als functie van dext (dimensies van
            % dijkversterking) voor de belasting in dit jaar
            try
                if STERKTE.fc_file == 1 % FC piping/macro op basis van mu/sigma
                    if faalmechanisme.stbi == 1
                        Pf_mstab_data  = faalkansberekenen_v1(STERKTE.fc_data_stbi,VNKvakken(v),'normal',HYD,link_to_figures,T(t),T(1),'macrostab',invoer.plot_pf_s,invoer.db_afkap_s,invoer.fkbr_nn);
                    else
                        Pf_mstab_data = [];
                    end
                    if faalmechanisme.stph == 1 
                        Pf_pip_data = faalkansberekenen_v1(STERKTE.fc_data_stph,VNKvakken(v),'normal',HYD,link_to_figures,T(t),T(1),'piping',invoer.plot_pf_p,invoer.db_afkap_p,invoer.fkbr_nn);
                    else
                        Pf_pip_data = [];
                    end
                else % FC piping/macro op basis van hele curve
                    Pf_mstab_data   = faalkansberekenen(STERKTE,1,HYD,VNKvakken(v),'normal',link_to_figures,T(t),T(1),'macrostab',invoer.plot_pf_s);
                    Pf_pip_data     = faalkansberekenen(STERKTE,2,HYD,VNKvakken(v),'normal',link_to_figures,T(t),T(1),'piping',invoer.plot_pf_p);
                end
                if faalmechanisme.ht == 1 
                    Pf_hoogte_data  = faalkansberekenen_hoogte(STERKTE,HYD,VNKvakken(v),'normal',link_to_figures,T(t),T(1),'hoogte',invoer);
                else
                    Pf_hoogte_data = [];
                end
            catch
                fileID = fopen(logfile,'a');
                fprintf(fileID,'%s\n',['ERROR in berekenen faalkansen vak ' num2str(VNKvakken(v))]);
                fclose(fileID);    
            end
            
            % ALS jaar=opgavejaar, wordt versterking om tot opgavejaar te 
            % voldoen berekend 
            if ismember(T(t),invoer.tijdopgave)
                invoer_tmp = invoer; 
                invoer_tmp.plot_pf_s = 0; invoer_tmp.plot_pf_p = 0; invoer_tmp.plot_pf_h = 0; % geen figuren maken
                [VAR.dH_opg(v,t),VAR.dS_opg(v,t),VAR.dP_opg(v,t),HYD.h_pdf,HYD.H_freq_vak_t] = bepaal_opgave_2(HYD,T,t,T(t),VNKvakken,v,TMP.sc_qh,TMP,invoer_tmp,STERKTE,link_to_figures,T(1),VAR,logfile,faalmechanisme);
                % opgave corrigeren met factor en maximum
                if size(VAR.corr_dP,2)>3 % informatie deklaag+L opgenomen
                    data = VAR.corr_dP(VAR.corr_dP(:,1)==VNKvakken(v),4:6);
                    if data(2)>4 && data(1)>0.5*VAR.dP_opg(v,t) 
                        pipfac=0.2;
                    elseif data(2)>4
                        pipfac=0.75;
                    elseif data(1)>0.5*VAR.dP_opg(v,t)
                        pipfac=0.75;
                    else
                        pipfac=1;
                    end
                else
                    pipfac = VAR.corr_dP(VAR.corr_dP(:,1)==VNKvakken(v),3);
                end
                macfac = VAR.corr_dS(VAR.corr_dS(:,1)==VNKvakken(v),3);
                htfac = VAR.corr_dH(VAR.corr_dH(:,1)==VNKvakken(v),3);
                pipmax = VAR.corr_dP(VAR.corr_dP(:,1)==VNKvakken(v),2);
                macmax = VAR.corr_dS(VAR.corr_dS(:,1)==VNKvakken(v),2);
                htmax = VAR.corr_dH(VAR.corr_dH(:,1)==VNKvakken(v),2);
                VAR.dH_opg_corr(v,t) = max(0,min(htfac.*VAR.dH_opg(v,t),htmax,'includenan'),'includenan');
                VAR.dS_opg_corr(v,t) = max(0,min(macfac.*VAR.dS_opg(v,t),macmax,'includenan'),'includenan');
                VAR.dP_opg_corr(v,t) = max(0,min(pipfac.*VAR.dP_opg(v,t),pipmax,'includenan'),'includenan');
%                 VAR.htfac(v,t) = htfac;
%                 VAR.macfac(v,t) = macfac;
%                 VAR.pipfac(v,t) = pipfac;

            end
            clear invoer_tmp
            
            % Bepaal faalkans aan de hand van FC die geldt in jaar t
            TMP = faalkansbepaling1(TMP.M,t,Pf_mstab_data,Pf_pip_data,Pf_hoogte_data,v,VAR,TMP);
%             if ~(isreal(TMP.Pf_1) && isreal(TMP.Pf_2) && isreal(TMP.Pf_3))
%                 fileID = fopen(logfile,'a');
%                 fprintf(fileID,'%s\n',['Warning: imaginaire faalkansen vak ' num2str(VNKvakken(v))]);
%                 fclose(fileID);    
%             elseif TMP.Pf_1>1 || TMP.Pf_2>1 || TMP.Pf_3>1 || TMP.Pf_1<0 || TMP.Pf_2<0 || TMP.Pf_3<0
%                 fileID = fopen(logfile,'a');
%                 fprintf(fileID,'%s\n',['ERROR faalkansen >1 of <0 in vak ' num2str(VNKvakken(v))]);
%                 fclose(fileID);    
%             end

            % faalkansen op doorsnede niveau opslaan, als NaN op 0 zetten voor verdere berekening
            if faalmechanisme.stbi == 1
                VAR.Pf_1_t(v,t) = TMP.Pf_1;
                TMP.Pf_1(isnan(TMP.Pf_1))=0;
                if ~isreal(TMP.Pf_1)
                    fileID = fopen(logfile,'a');
                    fprintf(fileID,'%s\n',['Warning: imaginaire faalkansen MAC vak ' num2str(VNKvakken(v))]);
                    fclose(fileID);    
                elseif TMP.Pf_1>1 || TMP.Pf_1<0
                    fileID = fopen(logfile,'a');
                    fprintf(fileID,'%s\n',['ERROR faalkansen MAC >1 of <0 in vak ' num2str(VNKvakken(v))]);
                    fclose(fileID);    
                end                
            else
                VAR.Pf_1_t(v,t) = nan;
            end
            if faalmechanisme.stph == 1
                VAR.Pf_2_t(v,t) = TMP.Pf_2;
                TMP.Pf_2(isnan(TMP.Pf_2))=0;
                if ~isreal(TMP.Pf_2)
                    fileID = fopen(logfile,'a');
                    fprintf(fileID,'%s\n',['Warning: imaginaire faalkansen PIP vak ' num2str(VNKvakken(v))]);
                    fclose(fileID);    
                elseif TMP.Pf_2>1 || TMP.Pf_2<0
                    fileID = fopen(logfile,'a');
                    fprintf(fileID,'%s\n',['ERROR faalkansen PIP >1 of <0 in vak ' num2str(VNKvakken(v))]);
                    fclose(fileID);    
                end                
            else
                VAR.Pf_2_t(v,t) = nan;
            end
            if faalmechanisme.ht == 1
                VAR.Pf_3_t(v,t) = TMP.Pf_3;
                TMP.Pf_3(isnan(TMP.Pf_3))=0;
                if ~isreal(TMP.Pf_3)
                    fileID = fopen(logfile,'a');
                    fprintf(fileID,'%s\n',['Warning: imaginaire faalkansen HT vak ' num2str(VNKvakken(v))]);
                    fclose(fileID);    
                elseif TMP.Pf_3>1 || TMP.Pf_3<0
                    fileID = fopen(logfile,'a');
                    fprintf(fileID,'%s\n',['ERROR faalkansen HT >1 of <0 in vak ' num2str(VNKvakken(v))]);
                    fclose(fileID);    
                end
            else
                VAR.Pf_3_t(v,t) = nan;
            end
            
            % opgave berekenen
            if invoer.opl_DV == 1
%                 [VAR] = bepaal_opgave_geforceerd(TMP,VAR,HYD,STERKTE,invoer,T,t,VNKvakken,v,link_to_figures,logfile,faalmechanisme);
                VAR.dH(v,t)  = VAR.DV_dH(v,t); % hoogte
                VAR.dS(v,t)  = VAR.DV_dS(v,t); % stabiliteit
                VAR.dP(v,t)  = VAR.DV_dP(v,t); % piping
                % wordt er versterkt?
                if t==1 && ( VAR.dH(v,t)>0 || VAR.dS(v,t)>0 || VAR.dP(v,t)>0 )
                    TMP.flag     = 'ja';
                elseif t==1
                    TMP.flag     = 'nee';
                elseif VAR.dH(v,t)>VAR.dH(v,t-1) || VAR.dS(v,t)>VAR.dS(v,t-1) || VAR.dP(v,t)>VAR.dP(v,t-1)
                    TMP.flag     = 'ja';
                else
                    TMP.flag     = 'nee';
                end
%                 % factoren op 1 zetten, die zijn al verwerkt in geforceerde opgave
%                 invoer.mac_factor = 1;
%                 invoer.pip_factor = 1;
            else
                % check of vak versterkt moet worden
                TMP.flag = check_versterken(VNKvakken,v,VAR,T,t,TMP,faalmechanisme);
            	% opgave berekenen
                [VAR] = bepaal_opgave_1(TMP,VAR,HYD,STERKTE,invoer,T,t,VNKvakken,v,link_to_figures,logfile,faalmechanisme);
            end
            
            % Check VAR.dH_coor, VAR.dS_coor en VAR.dP_coor (27-02-2019)
            if isnan(VAR.dH_corr(v,t))
                if isnan(VAR.Pf_3_t(v,t))
                    if t == 1
                        VAR.dH_corr(v,t) = 0;
                    else
                        VAR.dH_corr(v,t) = VAR.dH_corr(v,t-1);
                    end
                end
            end
            if isnan(VAR.dP_corr(v,t))
                if isnan(VAR.Pf_2_t(v,t))
                    if t == 1
                        VAR.dP_corr(v,t) = 0;
                    else
                        VAR.dP_corr(v,t) = VAR.dP_corr(v,t-1);
                    end
                end
            end
            if isnan(VAR.dS_corr(v,t))
                if isnan(VAR.Pf_1_t(v,t))
                    if t == 1
                        VAR.dS_corr(v,t) = 0;
                    else
                        VAR.dS_corr(v,t) = VAR.dS_corr(v,t-1);
                    end
                end
            end
            
            % berekenen kosten mbv KOSWAT data
            if strcmp(TMP.flag,'ja') % wel versterken
                TMP.row = TMP.row+1;
                [VAR] = kostenberekening(VNKvakken,v,T,t,VAR,TMP,invoer);
                
            end
            
            % contante waarde van kosten dijkversterking
            VAR.kosten_dijk_cw(v,t) = VAR.kosten_dijk(v,t)/(1+invoer.discontovoet/100)^(T(t)-invoer.basisjaar);
            
            % toevoegen restwaarde dijkversterkingen :> goed naar
            % kijken bij variabele klimaatscenarios
            if t==length(T) 
                [VAR] = add_restwaarde(VAR,T,t,v,invoer);
            end
            
            % Visualisatie
            if invoer.plot_dijk == 1 && strcmp(TMP.flag,'ja')
%                 plot_profiel_v1(VAR,T,t,VNKvakken,v,link_to_prof_data,link_to_figures);
                plot_profiel_v1(VAR,T,t,VNKvakken,v,profieldata,link_to_figures);
            end
            
            pr = aa/(length(VNKvakken)*length(T));
            try
                set(hw,'Name','Rekenen... (klik op kruis rechtsboven om berekening af te breken)');
                waitbar(pr,hw,[ 'Berekening "' strrep(fnm,'_','\_') '" (' num2str(fi) '/' num2str(ntt) '), voortgang ' num2str(pr*100,'%.1f') ' %']);
                aa = aa+1;
            catch
                %warndlg('         Berekening is gestopt!','***Warning***');
                stp = 1;
                %close(hw);
                return;
            end
        end

        % kosten dijkvak gesommeerd over de tijd
        VAR.kosten_dijk_ncw(v) = sum(VAR.kosten_dijk_cw(v,:),'omitnan');

        % Plot faalkansverloop (t vs. pf)
        if invoer.plot_pf_vs_t==1 
            vaknaam = vaklengte(find(cell2mat(vaklengte(2:end,1))==VNKvakken(v))+1,3);
            vaknaam = vaknaam{1,1};
            print_pf_t(T,T(1),VAR,v,vaknaam,link_to_figures,VNKvakken,HYD)
        end
        % Plot verloop versterkingen (t vs. dH, dS, dP)
        if invoer.plot_dims_vs_t == 1 
            printdHdSdP(invoer,TMP.M,T,VAR,v,vaknaam,link_to_figures,VNKvakken)
        end
        
%         % opgave (in opgavejaren) corrigeren met factor en maximum
% %         pipfac = VAR.corr_dP(VAR.corr_dP(:,1)==VNKvakken(v),3);
%         if size(VAR.corr_dP,2)>3 % informatie deklaag+L opgenomen
%             data = VAR.corr_dP(VAR.corr_dP(:,1)==VNKvakken(v),4:6);
%             if data(2)>4 && data(1)>0.5*VAR.dP(v,t) 
%                 pipfac=0.2;
%             elseif data(2)>4
%                 pipfac=0.75;
%             elseif data(1)>0.5*VAR.dP(v,t)
%                 pipfac=0.75;
%             else
%                 pipfac=1;
%             end
%         else
%             pipfac = VAR.corr_dP(VAR.corr_dP(:,1)==VNKvakken(v),3);
%         end
%         macfac = VAR.corr_dS(VAR.corr_dS(:,1)==VNKvakken(v),3);
%         htfac = VAR.corr_dH(VAR.corr_dH(:,1)==VNKvakken(v),3);
%         pipmax = VAR.corr_dP(VAR.corr_dP(:,1)==VNKvakken(v),2);
%         macmax = VAR.corr_dS(VAR.corr_dS(:,1)==VNKvakken(v),2);
%         htmax = VAR.corr_dH(VAR.corr_dH(:,1)==VNKvakken(v),2);
%         VAR.dH_opg_corr(v,:) = max(0,min(htfac.*VAR.dH_opg(v,:),htmax));
%         VAR.dS_opg_corr(v,:) = max(0,min(macfac.*VAR.dS_opg(v,:),macmax));
%         VAR.dP_opg_corr(v,:) = max(0,min(pipfac.*VAR.dP_opg(v,:),pipmax));

    catch
       TMP.err=TMP.err+1;
       VAR.errorvakken(TMP.err)=VNKvakken(v);
       disp(['ERROR in vak ' num2str(VNKvakken(v)) ', vak overgeslagen'])
       fileID = fopen([uitvoerdir 'logfile_berekening.txt'],'a');
       fprintf(fileID,'%s\n',['ERROR in vak ' num2str(VNKvakken(v)) ', vak overgeslagen']);
       fclose(fileID);
    end
end

%% export resultaten 
disp('export resultaten')
try
    matfile = [uitvoerdir 'results.mat'];
    save(matfile, 'invoer', 'VAR', 'uitvoerdir', 'vaklengte');
catch
        
end

try
    % system('taskkill /F /IM EXCEL.EXE');
    export_resultaten(invoer,VAR,uitvoerdir,vaklengte);
catch
        
end

%% kopieren invoerfile naar uitvoermap
src = [invoer.prog_dir invoer.fn];
dest = [uitvoerdir invoer.fn];
copyfile(src,dest);

%% logfile
fileID = fopen(logfile,'a');
fprintf(fileID,'%s\n',['aantal vakken afgebroken: ' num2str(length(VAR.errorvakken))]);
fprintf(fileID,'%s\n',['aantal vakken geslaagd: ' num2str(length(VNKvakken)-length(VAR.errorvakken))]);
text = ['Totale contante waarde dijkversterkingskosten = ' num2str(sum(VAR.kosten_dijk_ncw,'omitnan'),'%.1f') ' miljoen euro'];
disp(text);
fprintf(fileID,'%s\n',text);
fprintf(fileID,'%s\n',['berekening klaar: ' datestr(now)]);
fprintf(fileID,'%s\n',['rekentijd ' num2str(toc) ' seconden']);
fprintf(fileID,'%s\n',['invoerfile: ' fnm]);
fclose(fileID);

toc
disp('berekening klaar')
%diary([uitvoerdir 'diary.txt'])
delete(hw)
