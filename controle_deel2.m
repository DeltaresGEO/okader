function [d2] = controle_deel2(fid1,invoer,d2)
% mechanismen aan/uit
% fragility curve
% afkappen curve

fprintf(fid1,'%s\n','Deel 2. Controleren parameters "Dijksterkte" ');

%% mechanismen aan/uit
%faalmech_h
try
    if invoer.faalmech_h < 0 || invoer.faalmech_h > 1
        fprintf(fid1,'%s\n',['- HT (aan/uit) instelling is ' num2str(invoer.faalmech_h) '. Dit is niet ok! De instelling dient 1 (aan) of 0 (uit) te zijn.***']);
        d2 = d2+1;
    else
        if invoer.faalmech_h == 1
            ss = 'aan';
            %check de inhoud?
            fprintf(fid1,'%s\n',['- HT (aan/uit) instelling is ' num2str(invoer.faalmech_h) '. HT staat nu ' ss '.']);
        else
            ss = 'uit';
            %check de inhoud?
            fprintf(fid1,'%s\n',['- HT (aan/uit) instelling is ' num2str(0) '. HT staat nu ' ss '.']);
        end
    end
catch
    fprintf(fid1,'%s/n','- HT (aan/uit) instelling is niet aangegeven.***');
    d2 = d2+1;
end
%faalmech_s
try
    if invoer.faalmech_s < 0 || invoer.faalmech_s > 1
        fprintf(fid1,'%s\n',['- STBI (aan/uit) instelling is ' num2str(invoer.faalmech_s) '. Dit is niet ok! De instelling dient 1 (aan) of 0 (uit) te zijn.***']);
        d2 = d2+1;
    else
        if invoer.faalmech_s == 1
            ss = 'aan';
            %check de inhoud?
            fprintf(fid1,'%s\n',['- STBI (aan/uit) instelling is ' num2str(invoer.faalmech_s) '. STBI staat nu ' ss '.']);
        else
            ss = 'uit';
            %check de inhoud?
            fprintf(fid1,'%s\n',['- STBI (aan/uit) instelling is ' num2str(0) '. STBI staat nu ' ss '.']);
        end
    end
catch
    fprintf(fid1,'%s/n','- STBI (aan/uit) instelling is niet aangegeven.***');
        d2 = d2+1;
end
%faalmech_p
try
    if invoer.faalmech_p < 0 || invoer.faalmech_p > 1
        fprintf(fid1,'%s\n',['- STPH (aan/uit) instelling is ' num2str(invoer.faalmech_p) '. Dit is niet ok! De instelling dient 1 (aan) of 0 (uit) te zijn.***']);
        d2 = d2+1;
    else
        if invoer.faalmech_p == 1
            ss = 'aan';
            %check de inhoud?
            fprintf(fid1,'%s\n',['- STPH (aan/uit) instelling is ' num2str(invoer.faalmech_p) '. STPH staat nu ' ss '.']);
        else
            ss = 'uit';
            %check de inhoud?
            fprintf(fid1,'%s\n',['- STPH (aan/uit) instelling is ' num2str(0) '. STPH staat nu ' ss '.']);
        end
    end
catch
    fprintf(fid1,'%s/n','- STPH (aan/uit) instelling is niet aangegeven.***');
        d2 = d2+1;
end

%% fragility curve
%db_FC_h
if invoer.faalmech_h == 1
try
    if exist(invoer.db_FC_h,'file') > 0
        fprintf(fid1,'%s\n',['- HT fragility curven: file ' invoer.db_FC_h ' is gevonden.']);
        %check de inhoud?
    elseif ~invoer.faalmech_h == 1
        % mechanisme uit
    else
        fprintf(fid1,'%s\n',['- HT fragility curven: file ' invoer.db_FC_h ' is niet gevonden. Controleer de filenaam.***']);
        d2 = d2+1;
    end
catch
    fprintf(fid1,'%s\n','- HT fragility curven file is niet aangegeven.***');
    d2 = d2+1;
end
else
    fprintf(fid1,'%s\n','- Fragility curve file HT is niet gecheckt. Faalmechanisme HT wordt niet in de berekening meegenomen');
end
%db_FC_s
if invoer.faalmech_s == 1
try
    if exist(invoer.db_FC_s,'file') > 0
        fprintf(fid1,'%s\n',['- STBI fragility curven: file ' invoer.db_FC_s ' is gevonden.']);
        %check de inhoud?
    elseif ~invoer.faalmech_s == 1
        % mechanisme uit
    else
        fprintf(fid1,'%s\n',['- STBI fragility curven: file ' invoer.db_FC_s ' is niet gevonden. Controleer de mapnaam.***']);
        d2 = d2+1;
    end
catch
    fprintf(fid1,'%s\n','- STBI fragility curven file is niet aangegeven.***');
    d2 = d2+1;
end
else
    fprintf(fid1,'%s\n','- Fragility curve file STBI is niet gecheckt. Faalmechanisme STBI wordt niet in de berekening meegenomen');
end
%db_FC_p
if invoer.faalmech_p == 1
try
    if exist(invoer.db_FC_p,'file') > 0
        fprintf(fid1,'%s\n',['- STPH fragility curven: file ' invoer.db_FC_p ' is gevonden.']);
        %check de inhoud?
    elseif ~invoer.faalmech_p == 1
        % mechanisme uit
    else
        fprintf(fid1,'%s\n',['- STPH fragility curven: file ' invoer.db_FC_p ' is niet gevonden. Controleer de mapnaam.***']);
        d2 = d2+1;
    end
catch
    fprintf(fid1,'%s\n','- STPH fragility curven file is niet aangegeven.***');
    d2 = d2+1;
end
else
    fprintf(fid1,'%s\n','- Fragility curve file STPH is niet gecheckt. Faalmechanisme STPH wordt niet in de berekening meegenomen');
end

%% afkappen
%faalmech_h
try
    if invoer.db_afkap_h < 1 || invoer.db_afkap_h > 2
        fprintf(fid1,'%s\n',['- afkap HT instelling is ' num2str(invoer.db_afkap_h) '. Dit is niet ok! De instelling dient 1 of 2 te zijn.***']);
        d2 = d2+1;
    else
        if invoer.db_afkap_h == 1
            ss = 'geen afkappen';
        elseif invoer.db_afkap_h == 2
            ss = 'obv frequentie';
        end
        %check de inhoud?
        fprintf(fid1,'%s\n',['- afkap HT instelling is ' num2str(invoer.db_afkap_h) ': ' ss '.']);
    end
catch
    fprintf(fid1,'%s\n','- afkap HT instelling is niet aangegeven. ***');
        d2 = d2+1;
end
%faalmech_s
try
    if invoer.db_afkap_s < 1 || invoer.db_afkap_s > 3
        fprintf(fid1,'%s\n',['- afkap STBI instelling is ' num2str(invoer.db_afkap_s) '. Dit is niet ok! De instelling dient 1, 2 of 3 te zijn.***']);
        d2 = d2+1;
    else
        if invoer.db_afkap_s == 1
            ss = 'geen afkappen';
        elseif invoer.db_afkap_s == 2
            ss = 'obv frequentie';
        elseif invoer.db_afkap_s == 3
            ss = 'obv kruinhoogte en buitenteen';
        end
        %check de inhoud?
        fprintf(fid1,'%s\n',['- afkap STBI instelling is ' num2str(invoer.db_afkap_s) ': ' ss '.']);
    end
catch
    fprintf(fid1,'%s\n','- afkap STBI instelling is niet aangegeven.***');
        d2 = d2+1;
end
%faalmech_p
try
    if invoer.db_afkap_p < 1 || invoer.db_afkap_p > 3
        fprintf(fid1,'%s\n',['- afkap STPH instelling is ' num2str(invoer.db_afkap_p) '. Dit is niet ok! De instelling dient 1, 2 of 3 te zijn.***']);
        d2 = d2+1;
    else
        if invoer.db_afkap_p == 1
            ss = 'geen afkappen';
        elseif invoer.db_afkap_p == 2
            ss = 'obv frequentie';
        elseif invoer.db_afkap_p == 3
            ss = 'obv kruinhoogte en buitenteen';
        end
        %check de inhoud?
        fprintf(fid1,'%s\n',['- afkap STPH instelling is ' num2str(invoer.db_afkap_p) ': ' ss '.']);
    end
catch
    fprintf(fid1,'%s\n','- afkap STPH instelling is niet aangegeven.***');
        d2 = d2+1;
end
fprintf(fid1,'%s\n','');
