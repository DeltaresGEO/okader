function invoer = invoer_inlezen(fnd,werk_dir)
%werk_dir = 'D:\Test rvr voor compileren\reprository\compileren\definitief\';
%fnd = 'invoer_maas_v1.txt';
fid = fopen([werk_dir fnd]);
temp = textscan(fid,'%s','delimiter','\n'); 
%ff1 = find(strncmpi(temp{1},'% scenarios',11));
var_str = cell(length(temp{1}),1);
val_str = var_str;
for i = 1:length(temp{1})
    tt = regexp(char(temp{1}(i)),': ','split');
    if length(tt) == 2
        var_str(i) = strtrim(tt(1));
        tt2 = tt(2);
        tt3 = strtrim(regexp(char(tt2),'%','split'));
        val_str(i) = tt3(1);
    elseif length(tt) == 1
        var_str(i) = strtrim(tt(1));
        val_str(i) = cellstr('-');
    else
        var_str(i) = strtrim(tt(1));
        val_str(i) = cellstr('-');
    end
end

%% werk dir en filenaam
invoer.prog_dir = werk_dir; %prog_dir
invoer.fn = fnd;

%% tab1
invoer.analysetype = str2double(val_str(strcmpi(var_str,'analysetype')));
invoer.t_begin = str2double(val_str(strcmpi(var_str,'t_begin')));
invoer.t_eind = str2double(val_str(strcmpi(var_str,'t_eind')));
invoer.t_stap = str2double(val_str(strcmpi(var_str,'t_stap')));
invoer.db_vakken = char(val_str(strcmpi(var_str,'db_vakken')));
invoer.db_vaklengte = char(val_str(strcmpi(var_str,'db_vaklengte')));
invoer.db_KOSWAT = char(val_str(strcmpi(var_str,'db_KOSWAT')));
invoer.constlength = str2double(val_str(strcmpi(var_str,'constlength')));
invoer.db_constlength = char(val_str(strcmpi(var_str,'db_constlength')));
invoer.discontovoet = str2double(val_str(strcmpi(var_str,'discontovoet')));
invoer.basisjaar = str2double(val_str(strcmpi(var_str,'basisjaar')));
invoer.indexeren = str2double(val_str(strcmpi(var_str,'indexeren'))); % (0 = nee, 1 = ja)
invoer.db_CBSindex = char(val_str(strcmpi(var_str,'db_CBSindex')));
invoer.prijspeil = str2double(val_str(strcmpi(var_str,'prijspeil')));

%% tab2
invoer.faalmech_h = str2double(val_str(strcmpi(var_str,'faalmech_h')));
%if invoer.faalmech_h == 0 || invoer.faalmech_h == 9
%    invoer.faalmech_h = 2;
%end
invoer.faalmech_s = str2double(val_str(strcmpi(var_str,'faalmech_s')));
%if invoer.faalmech_s == 0 || invoer.faalmech_s == 9
%    invoer.faalmech_s = 2;
%end
invoer.faalmech_p = str2double(val_str(strcmpi(var_str,'faalmech_p')));
%if invoer.faalmech_p == 0 || invoer.faalmech_p == 9
%    invoer.faalmech_p = 2;
%end
invoer.db_FC_h = char(val_str(strcmpi(var_str,'db_FC_h')));
invoer.db_FC_s = char(val_str(strcmpi(var_str,'db_FC_s')));
invoer.db_FC_p = char(val_str(strcmpi(var_str,'db_FC_p')));
invoer.db_afkap_s = str2double(val_str(strcmpi(var_str,'afkap_s')));
invoer.db_freq_bs_s = str2double(val_str(strcmpi(var_str,'freq_bs_s')));
invoer.db_afkap_p = str2double(val_str(strcmpi(var_str,'afkap_p')));
invoer.db_freq_bs_p = str2double(val_str(strcmpi(var_str,'freq_bs_p')));
invoer.db_afkap_h = str2double(val_str(strcmpi(var_str,'afkap_h')));
invoer.db_freq_bs_h = str2double(val_str(strcmpi(var_str,'freq_bs_h')));

%% tab3
invoer.db_onz_toeslag = char(val_str(strcmpi(var_str,'db_onz_toeslag')));
invoer.db_bodemdaling = char(val_str(strcmpi(var_str,'db_bodemdaling')));
invoer.dh_integraal = str2double(val_str(strcmpi(var_str,'dh_integraal')));
invoer.extrapolatie = str2double(val_str(strcmpi(var_str,'extrapolatie')));
% scenarios QT
frewind(fid);
while 1
    temp = textscan(fid,'%s',1,'delimiter','\n');
    if strcmpi(strtrim(temp{1}),'% scenarios voor QT') || strcmpi(strtrim(temp{1}),'%% scenarios voor QT')
        break;
    end
    if feof(fid) == 1
        break;
    end
end
while 1
    temp = textscan(fid,'%s',1,'delimiter','\n');
    if strfind(char(temp{1}),'ns')
        temp0 = strtrim(regexp(char(temp{1}),'%','split'));
        temp1 = strtrim(regexp(char(temp0(1)),'=','split'));
        ns = str2double(temp1(end));
        break;
    end
    if feof(fid) == 1
        break;
    end
end
invoer.QT_n = ns;
if ns <= 1
    invoer.QTscenarios = cell(1,5);
    invoer.QTscenarios{1,1} = 1;
    ns = 1;
else
    invoer.QTscenarios = cell(ns,5);
end
% for j = 1:ns
%     invoer.QTscenarios{j,1} = j;
% end
for i = 1:ns
    % sce_id
    while 1
        temp = textscan(fid,'%s',1,'delimiter','\n');
        if strfind(char(temp{1}),['sce_id = ' num2str(i)])
            break;
        end
        if feof(fid) == 1
            break;
        end
    end
    invoer.QTscenarios{i,1} = i;
    % t
    while 1
        temp = textscan(fid,'%s',1,'delimiter','\n');
        if strfind(char(temp{1}),'t')
            temp0 = strtrim(regexp(char(temp{1}),'%','split'));
            temp1 = strtrim(regexp(char(temp0(1)),'=','split'));
            t = str2double(temp1(end));
            break;
        end
        if feof(fid) == 1
            break;
        end
    end
    try
        invoer.QTscenarios{i,2} = t;
    catch
        invoer.QTscenarios{i,2} = '';
    end
    % QT_files
    for j = 1:3
        while 1
            temp = textscan(fid,'%s',1,'delimiter','\n');
            if strfind(char(temp{1}),['QT_file_' num2str(j)])
                temp0 = strtrim(regexp(char(temp{1}),'%','split'));
                temp1 = strtrim(regexp(char(temp0(1)),'=','split'));
                f = char(temp1(end));
                break;
            end
            if feof(fid) == 1
                break;
            end
        end
        try
            % als de 3e file niet beschikbaar is is de 3e file = 2e file
            invoer.QTscenarios{i,2+j} = f;
        catch
            invoer.QTscenarios{i,2+j} = '';
        end
    end
end
% scenarios QH
frewind(fid);
while 1
    temp = textscan(fid,'%s',1,'delimiter','\n');
    if strcmpi(strtrim(temp{1}),'% scenarios voor QH') || strcmpi(strtrim(temp{1}),'%% scenarios voor QH')
        break;
    end
    if feof(fid) == 1
        break;
    end
end
while 1
    temp = textscan(fid,'%s',1,'delimiter','\n');
    if strfind(char(temp{1}),'ns')
        temp0 = strtrim(regexp(char(temp{1}),'%','split'));
        temp1 = strtrim(regexp(char(temp0(1)),'=','split'));
        ns = str2double(temp1(end));
        break;
    end
    if feof(fid) == 1
        break;
    end
end
invoer.QH_n = ns;
if ns <= 1
    invoer.QHscenarios = cell(1,5);
    invoer.QHscenarios{1,1} = 1;
    ns = 1;
else
    invoer.QHscenarios = cell(ns,5);
end
% for j = 1:ns
%     invoer.QHscenarios{j,1} = j;
% end
for i = 1:ns
    % sce_id
    while 1
        temp = textscan(fid,'%s',1,'delimiter','\n');
        if strfind(char(temp{1}),['sce_id = ' num2str(i)])
            break;
        end
        if feof(fid) == 1
            break;
        end
    end
    invoer.QHscenarios{i,1} = i;
    % t
    while 1
        temp = textscan(fid,'%s',1,'delimiter','\n');
        if strfind(char(temp{1}),'t')
            temp0 = strtrim(regexp(char(temp{1}),'%','split'));
            temp1 = strtrim(regexp(char(temp0(1)),'=','split'));
            t = str2double(temp1(end));
            break;
        end
        if feof(fid) == 1
            break;
        end
    end
    try
        invoer.QHscenarios{i,2} = t;
    catch
        invoer.QHscenarios{i,2} = '';
    end
    % QH files
     for j = 1:3
        while 1
            temp = textscan(fid,'%s',1,'delimiter','\n');
            if strfind(char(temp{1}),['QH_file_' num2str(j)])
                temp0 = strtrim(regexp(char(temp{1}),'%','split'));
                temp1 = strtrim(regexp(char(temp0(1)),'=','split'));
                f = char(temp1(end));               
                break;            
            end
            if feof(fid) == 1
                break;
            end
        end
        try
            invoer.QHscenarios{i,2+j} = f;
        catch
            invoer.QHscenarios{i,2+j} = '';
        end
     end
end

%% tab4
invoer.db_norm = char(val_str(strcmpi(var_str,'db_norm')));
invoer.db_ontwerp_duur = char(val_str(strcmpi(var_str,'db_ontwerp_duur')));
% Op verzoek van HKV om te verwijderen d.d. 11 juli 2018
try
    invoer.db_maxpipberm = char(val_str(strcmpi(var_str,'db_maxpipberm')));
catch
end
invoer.db_DVschema = char(val_str(strcmpi(var_str,'db_DVschema')));
invoer.winst_rvm = str2double(val_str(strcmpi(var_str,'winst_rvm')));
invoer.db_corr_dH = char(val_str(strcmpi(var_str,'db_corr_dH')));
invoer.db_corr_dS = char(val_str(strcmpi(var_str,'db_corr_dS')));
invoer.db_corr_dP = char(val_str(strcmpi(var_str,'db_corr_dP')));
invoer.db_pipcost = char(val_str(strcmpi(var_str,'db_pipcost')));
invoer.opl_DV = str2double(val_str(strcmpi(var_str,'opl_DV')));
invoer.db_DV_dH = char(val_str(strcmpi(var_str,'db_DV_dH')));
invoer.db_DV_dS = char(val_str(strcmpi(var_str,'db_DV_dS')));
invoer.db_DV_dP = char(val_str(strcmpi(var_str,'db_DV_dP')));
invoer.sce_inst = 1;
% scenarios normen
% frewind(fid);
% while 1
%     temp = textscan(fid,'%s',1,'delimiter','\n');
%     if strcmpi(strtrim(temp{1}),'% scenarios voor norm van dijkversterking')
%         break;
%     end
%     if feof(fid)
%         break;
%     end
% end
% while 1
%     temp = textscan(fid,'%s',1,'delimiter','\n');
%     if strfind(char(temp{1}),'ns')
%         temp1 = strtrim(regexp(char(temp{1}),'=','split'));
%         ns = str2double(temp1(end));
%         break;
%     end
%     if feof(fid)
%         break;
%     end
% end
% if feof(fid)
%     invoer.norm_n = 0;
%     ns = 0;
% else
%     invoer.norm_n = ns;
% end
% if ns <= 1
%     invoer.normen = cell(1,3);
%     invoer.normen{1,1} = 1;
% else
%     invoer.normen = cell(ns,3);
%     for j = 1:ns
%         invoer.normen{j,1} = j;
%     end
% end
% for i = 1:ns
%     % sce_id
%     while 1
%         temp = textscan(fid,'%s',1,'delimiter','\n');
%         if strfind(char(temp{1}),['sce_id = ' num2str(i)])
%             break;       
%         end
%     end
%     invoer.normen{i,1} = i;
%     % t
%     while 1
%         temp = textscan(fid,'%s',1,'delimiter','\n');
%         if strfind(char(temp{1}),'t')
%             temp1 = strtrim(regexp(char(temp{1}),'=','split'));
%             t = str2double(temp1(end));
%             break;
%         end
%     end
%     invoer.normen{i,2} = t;
%     % norm file
%     while 1
%         temp = textscan(fid,'%s',1,'delimiter','\n');
%         if strfind(char(temp{1}),'norm_')
%             temp1 = strtrim(regexp(char(temp{1}),'=','split'));
%             norm_f = char(temp1(end));
%             break;
%         end
%     end
%     invoer.normen{i,3} = norm_f;
% end
% scenarios QT en QH voor dijkversterking
% frewind(fid);
% while 1
%     temp = textscan(fid,'%s',1,'delimiter','\n');
%     if strcmpi(strtrim(temp{1}),'% scenarios QT en QH voor dijkversterking')
%         break;
%     end
%     if feof(fid)
%         break;
%     end
% end
% while 1
%     temp = textscan(fid,'%s',1,'delimiter','\n');
%     if strfind(char(temp{1}),'ns')
%         temp1 = strtrim(regexp(char(temp{1}),'=','split'));
%         ns = str2double(temp1(end));
%         break;
%     end
%     if feof(fid)
%         break;
%     end
% end
% if feof(fid)
%     invoer.verst_n = 0;
%     ns = 0;
% else
%     invoer.verst_n = ns;
% end
% if ns <= 1
%     invoer.verst = cell(1,4);
%     invoer.verst{1,1} = 1;
% else
%     invoer.verst = cell(ns,4);
%     for j = 1:ns
%         invoer.verst{j,1} = j;
%     end
% end
% for i = 1:ns
%     % sce_id
%     while 1
%         temp = textscan(fid,'%s',1,'delimiter','\n');
%         if strfind(char(temp{1}),['sce_id = ' num2str(i)])
%             break;
%         end
%     end
%     invoer.verst{i,1} = i;
%     % t
%     while 1
%         temp = textscan(fid,'%s',1,'delimiter','\n');
%         if strfind(char(temp{1}),'t')
%             temp1 = strtrim(regexp(char(temp{1}),'=','split'));
%             t = str2double(temp1(end));
%             break;
%         end
%     end
%     invoer.verst{i,2} = t;
%     % versterkingsscenarios - QT
%     while 1
%         temp = textscan(fid,'%s',1,'delimiter','\n');
%         if strfind(char(temp{1}),'QT_sce')
%             temp1 = strtrim(regexp(char(temp{1}),'=','split'));
%             qt_sce = str2double(temp1(end));
%             break;
%         end
%     end
%     invoer.verst{i,3} = qt_sce;
%     % versterkingsscenarios - QH
%     while 1
%         temp = textscan(fid,'%s',1,'delimiter','\n');
%         if strfind(char(temp{1}),'QH_sce')
%             temp1 = strtrim(regexp(char(temp{1}),'=','split'));
%             qh_sce = str2double(temp1(end));
%             break;
%         end
%     end
%     invoer.verst{i,4} = qh_sce;
% end
% if invoer.norm_n > 1 || invoer.verst_n > 1
%     invoer.sce_inst = 2;
% elseif invoer.norm_n == 0 && invoer.verst_n == 0
%     invoer.sce_inst = 1;
% else
%     invoer.sce_inst = 1;
% end

%% tab5
try %Verzoek van HKV om te verwijderen d.d. 11 juli 2018
    invoer.pip_factor = str2double(val_str(strcmpi(var_str,'pip_factor')));
catch
end
try %Verzoek van HKV om te verwijderen d.d. 11 juli 2018
    invoer.mac_factor = str2double(val_str(strcmpi(var_str,'mac_factor')));
catch
end
invoer.dS_stap = str2double(val_str(strcmpi(var_str,'dS_stap')));
invoer.dP_stap = str2double(val_str(strcmpi(var_str,'dP_stap')));
invoer.dH_stap = str2double(val_str(strcmpi(var_str,'dH_stap')));
invoer.dS_max = str2double(val_str(strcmpi(var_str,'dS_max')));
invoer.dP_max = str2double(val_str(strcmpi(var_str,'dP_max')));
invoer.dH_max = str2double(val_str(strcmpi(var_str,'dH_max')));
invoer.Qdisc_step = str2double(val_str(strcmpi(var_str,'Qdisc_step')));
invoer.Qdisc_high = str2double(val_str(strcmpi(var_str,'Qdisc_high')));
try %Verzoek van HKV om te verwijderen d.d. 11 juli 2018
    invoer.Qdisc_min = str2double(val_str(strcmpi(var_str,'Qdisc_min')));
    invoer.Qdisc_low = str2double(val_str(strcmpi(var_str,'Qdisc_low')));
catch
end
invoer.refjaar_bodemdaling = str2double(val_str(strcmpi(var_str,'refjaar_bodemdaling')));
invoer.t_voldoen_norm = str2double(val_str(strcmpi(var_str,'t_voldoen_norm')));
try %Verzoek van HKV om te verwijderen d.d. 11 juli 2018
    invoer.cost_breakeven_dP = str2double(val_str(strcmpi(var_str,'cost_breakeven_dP')));
catch
end
%try % extra variabele voor uitgebreide kostenberekening
%    invoer.kost_uitgebreid = str2double(val_str(strcmpi(var_str,'uitgebreide_kostenberekening')));
%catch
%end
%% tab6
invoer.uitvoer_map = char(val_str(strcmpi(var_str,'uitvoer_map')));
invoer.tijdopgave = char(val_str(strcmp(var_str,'tijdopgave')));
invoer.plot_pf_vs_t = str2double(val_str(strcmpi(var_str,'plot_pf_vs_t')));
%if ~isempty(invoer.plot_pf_vs_t)
%    if invoer.plot_pf_vs_t == 0 || invoer.plot_pf_vs_t == 9
%        invoer.plot_pf_vs_t = 2;
%    end
%else
%    invoer.plot_pf_vs_t = 2;
%end
invoer.plot_dims_vs_t = str2double(val_str(strcmpi(var_str,'plot_dims_vs_t')));
if ~isempty(invoer.plot_dims_vs_t)
    if invoer.plot_dims_vs_t == 0 || invoer.plot_dims_vs_t == 9
        invoer.plot_dims_vs_t = 2;
    end
else
    invoer.plot_dims_vs_t = 2;
end
invoer.plot_pf_s = str2double(val_str(strcmpi(var_str,'plot_pf_s')));
if ~isempty(invoer.plot_pf_s)
    if invoer.plot_pf_s == 0 || invoer.plot_pf_s == 9
        invoer.plot_pf_s = 5;
    end
else
    invoer.plot_pf_s = 5;
end
invoer.plot_pf_p = str2double(val_str(strcmpi(var_str,'plot_pf_p')));
if ~isempty(invoer.plot_pf_p)
    if invoer.plot_pf_p == 0 || invoer.plot_pf_p == 9
        invoer.plot_pf_p = 5;
    end
else
    invoer.plot_pf_p = 5;
end
invoer.plot_pf_h = str2double(val_str(strcmpi(var_str,'plot_pf_h')));
if ~isempty(invoer.plot_pf_h)
    if invoer.plot_pf_h == 0 || invoer.plot_pf_h == 9
        invoer.plot_pf_h = 5;
    end
else
    invoer.plot_pf_h = 5;
end
invoer.plot_pdf_wb = str2double(val_str(strcmpi(var_str,'plot_pdf_wb')));
if ~isempty(invoer.plot_pdf_wb)
    if invoer.plot_pdf_wb == 0 || invoer.plot_pdf_wb == 9
        invoer.plot_pdf_wb = 4;
    end
else
    invoer.plot_pdf_wb = 4;
end
invoer.plot_dijk = str2double(val_str(strcmpi(var_str,'plot_dijk')));
if ~isempty(invoer.plot_dijk)
    if invoer.plot_dijk == 0 || invoer.plot_dijk == 9
        invoer.plot_dijk = 2;
    end
else
    invoer.plot_dijk = 2;
end
try
    invoer.exp_traject = str2double(val_str(strcmpi(var_str,'exp_traject')));
    if invoer.exp_traject == 0 || invoer.exp_traject == 9
        invoer.exp_traject = 2;
    end
% invoer.exp_shape = char(val_str(strcmpi(var_str,'exp_shape')));
% if invoer.exp_shape == 0 || invoer.exp_shape == 9
%     invoer.exp_shape = 2;
% end
% invoer.dbf_shape = str2double(val_str(strcmpi(var_str,'dbf_shape')));
    invoer.exp_shape = str2double(val_str(strcmpi(var_str,'exp_shape')));
    if invoer.exp_shape == 0 || invoer.exp_shape == 9
        invoer.exp_shape = 2;
    end
    invoer.dbf_shape = char(val_str(strcmpi(var_str,'dbf_shape')));
catch
    invoer.exp_traject = 2;
    invoer.exp_shape = 2;
    invoer.dbf_shape = '';
end
fclose(fid);
end