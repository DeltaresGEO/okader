function d1 = controle_deel1(fid1,invoer,d1)
% parameters voor analysetype
% parameters Tijd
% parameters dijkvakgegevens
% parameters kosten

fprintf(fid1,'%s\n','Deel 1. Controleren parameters "Algemeen" ');

%% parameters voor analysetype
try
    if invoer.analysetype < 0 || invoer.analysetype > 1
        fprintf(fid1,'%s\n',['- Analysetype is ' num2str(invoer.analysetype) '. Voorlopig is alleen optie 1 mogelijk (standaard).***']);
        d1 = d1+1;
    else
        fprintf(fid1,'%s\n',['- Analysetype is ' num2str(invoer.analysetype) '.']);
    end
catch
    fprintf(fid1,'%s\n','- Analysetype is niet aangegeven. Deze optie is automatisch naar 1 gezet.');
    d1 = d1+1;
end

%% parameters Tijd
%t_begin
try
    if invoer.t_begin < 0
        fprintf(fid1,'%s\n',['- Begintijd is ' num2str(invoer.t_begin) ' jaar. Dit is niet ok! Begintijd dient groter dan 0 te zijn.***']);
        d1 = d1+1;
    else
        %check de inhoud?
        fprintf(fid1,'%s\n',['- Begintijd is ' num2str(invoer.t_begin) ' jaar.']);
    end
catch
    fprintf(fid1,'%s\n','- Begintijd is niet aangegeven.***');
    d1 = d1+1;
end
%t_eind
try
    if invoer.t_eind < 0 || invoer.t_eind < invoer.t_begin
        fprintf(fid1,'%s\n',['- Eindtijd is ' num2str(invoer.t_eind) ' jaar. Dit is niet ok! Eindtijd dient groter te zijn dan 0 en begintijd.***']);
        d1 = d1+1;
    else
        %check de inhoud?
        fprintf(fid1,'%s\n',['- Eindtijd is ' num2str(invoer.t_eind) ' jaar.']);
    end
catch
    fprintf(fid1,'%s\n','- Eindtijd is niet aangegeven.***');
    d1 = d1+1;
end
%tijdstap
try
    if invoer.t_stap < 0
        fprintf(fid1,'%s\n',['- Tijdstap is ' num2str(invoer.t_stap) ' jaar. Dit is niet ok! Tijdstap dient groter dan 0 te zijn.***']);
        d1 = d1+1;
    else
        %check de inhoud?
        fprintf(fid1,'%s\n',['- Tijdstap is ' num2str(invoer.t_stap) ' jaar.']);
    end
catch
    fprintf(fid1,'%s\n','- Tijdstap is niet aangegeven.***');
    d1 = d1+1;
end

%% parameters dijkvakgegevens
%vnk_vakken
try
    if exist(invoer.db_vakken,'file') > 0
        fprintf(fid1,'%s\n',['- VNK vakken databases: file ' invoer.db_vakken ' is gevonden.']);
        %check de inhoud?
    else
        fprintf(fid1,'%s\n',['- VNK vakken databases: file ' invoer.db_vakken ' is niet gevonden. Controleer de filenaam.***']);
        d1 = d1+1;
    end
catch
    fprintf(fid1,'%s\n','- VNK vakken databases file is niet aangegeven.***');
    d1 = d1+1;
end
%db_vaklen
try
    if exist(invoer.db_vaklengte,'file') > 0
        fprintf(fid1,'%s\n',['- Vaklengte databases: file ' invoer.db_vaklengte ' is gevonden.']);
        %check de inhoud?
    else
        fprintf(fid1,'%s\n',['- Vaklengte databases: file ' invoer.db_vaklengte ' is niet gevonden. Controleer de filenaam.***']);
        d1 = d1+1;
    end
catch
    fprintf(fid1,'%s\n','- Vaklengte databases file is niet aangegeven.***');
    d1 = d1+1;
end

%% parameters kosten
%db_KOSWAT
try
    if exist(invoer.db_KOSWAT,'file') > 0
        fprintf(fid1,'%s\n',['- KOSWAT databases: file ' invoer.db_KOSWAT ' is gevonden.']);
        %check de inhoud?
    else
        fprintf(fid1,'%s\n',['- KOSWAT databases: file ' invoer.db_KOSWAT ' is niet gevonden. Controleer de filenaam.***']);
        d1 = d1+1;
    end
catch
    fprintf(fid1,'%s\n','- KOSWAT databases file is niet aangegeven.***');
    d1 = d1+1;
end

% optie met constructielengte
try
    if invoer.constlength == 1
        if exist(invoer.db_constlength,'file') > 0
            fprintf(fid1,'%s\n',['- Er is gekozen voor de kostenberekening met specifieke constructielengte (db_constlength = 1). Constructielengte databases: file ' invoer.db_constlength ' is gevonden.']);
            %check de inhoud?
        else
            fprintf(fid1,'%s\n',['- Er is gekozen voor de kostenberekening met specifieke constructielengte (db_constlength = 1). Maar constructielengte database file ' invoer.db_constlength ' is niet gevonden. Controleer de filenaam.***']);
            d1 = d1+1;
        end
    else
        fprintf(fid1,'%s\n',['- Er is gekozen voor de kostenberekening zonder specifieke invoer voor constructielengte (db_constlength = 0). Constructielengte database file ' invoer.db_constlength ' is niet meer relevant.']);
        invoer.constlength = 0;
    end
catch
    fprintf(fid1,'%s\n','- Het bestand kan niet worden ingelezen of de kostenberekening wordt met specifieke constructielengte uitgevoerd (db_constlength ?).***');
    d1 = d1+1;
end

% optie indexeren prijspeil
try
    if invoer.indexeren == 1
        if exist(invoer.db_CBSindex,'file') > 0
            %check de inhoud?
            try
                CBSindex    = dlmread(invoer.db_CBSindex,';',1,0);
                prijsindex = find(CBSindex(:,1)==invoer.prijspeil);
                if prijsindex > 1
                    fprintf(fid1,'%s\n',['- Er is gekozen voor de kostenberekening met indexeren van het prijspeil (indexeren = 1). Indexeringsdatabasefile ' invoer.db_CBSindex ' is gevonden. Prijspeil is ' num2str(invoer.prijspeil) '.']);    
                else
                    fprintf(fid1,'%s\n',['- Er is gekozen voor de kostenberekening met indexeren van het prijspeil (indexeren = 1). Maar het jaartal ' num2str(invoer.prijspeil) ' is niet gevonden in de indexeringsdatabasefile ' invoer.db_CBSindex '. Controleer het bestand.***']);    
                    d1 = d1+1;    
                end
            catch
                fprintf(fid1,'%s\n',['- Er is gekozen voor de kostenberekening met indexeren van het prijspeil (indexeren = 1). Maar Indexeringsdatabasefile ' invoer.db_CBSindex ' is niet correct. Controleer het bestand.***']);    
                d1 = d1+1;
            end
        else
            fprintf(fid1,'%s\n',['- Er is gekozen voor de kostenberekening met indexeren van het prijspeil (indexeren = 1). Maar Indexeringsdatabasefile ' invoer.db_CBSindex ' is niet gevonden. Controleer de filenaam.***']);
            d1 = d1+1;
        end
    else
        fprintf(fid1,'%s\n',['- Er is gekozen voor de kostenberekening zonder indexeren van het prijspeil (indexeren = 0). Indexeringsdatabasefile ' invoer.db_CBSindex ' en prijspeil ' num2str(invoer.prijspeil) ' zijn niet meer relevant.']);
        invoer.indexeren = 0;
    end
catch
    fprintf(fid1,'%s\n','- Indexeringsdatabasefile kan niet worden ingelezen (db_CBSindex ?).***');
    d1 = d1+1;
end

% tweede check of de nieuwe kostendatabase wordt gebruikt bij invoer.constlength == 1 
try
    if invoer.constlength == 1
        [~,~,temp] = xlsread(invoer.db_KOSWAT,'Z1:Z1');
        if isnan(temp{1}) %oude database
            fprintf(fid1,'%s\n',['- Er is gekozen voor de kostenberekening met specifieke constructielengte (db_constlength = 1). Maar de kostendatabase file ' invoer.db_KOSWAT ' is in het oude formaat geschreven. Gebruik de nieuwe kostendatabase.***']);
            d1 = d1+1;
        else
            fprintf(fid1,'%s\n',['- Er is gekozen voor de kostenberekening met specifieke constructielengte (db_constlength = 1). De kostendatabase file ' invoer.db_KOSWAT ' heeft het nieuwe formaat.']);
        end
        %[~,~,temp] = xlsread(invoer.db_KOSWAT); % uitvoer temp --> [~,~,temp]
        %[~,ni] = size(temp);
        %if ni < 26
        %    fprintf(fid1,'%s\n',['- Er is gekozen voor de kostenberekening met specifieke constructielengte (db_constlength = 1). Maar de kostendatabase file ' invoer.db_KOSWAT ' is in het oude formaat geschreven. Gebruik de nieuwe kostendatabase.***']);
        %    d1 = d1+1;
        %else
        %    fprintf(fid1,'%s\n',['- Er is gekozen voor de kostenberekening met specifieke constructielengte (db_constlength = 1). De kostendatabase file ' invoer.db_KOSWAT ' heeft het nieuwe formaat.']);
        %end
    end
catch
    fprintf(fid1,'%s\n',['- Er is gekozen voor de kostenberekening met specifieke constructielengte (db_constlength = 1). Maar de kosten database file ' invoer.db_KOSWAT ' heeft geen geschikt formaat. Gebruik de nieuwe kostendatabase.***']);
    d1 = d1+1;
end
clear temp;

%discontovoet
try
    if invoer.discontovoet < 0 || invoer.discontovoet > 20
        fprintf(fid1,'%s\n',['- Discontovoet is ' num2str(invoer.discontovoet) ' %. Dit is niet ok! Discontovoet dient minimaal 0% en maximaal 20% te zijn.***']);
        d1 = d1+1;
    else
        %check de inhoud?
        fprintf(fid1,'%s\n',['- Discontovoet is ' num2str(invoer.discontovoet) ' %.']);
    end
catch
    fprintf(fid1,'%s\n','- Discontovoet is niet aangegeven.***');
    d1 = d1+1;
end
% basisjaar
try
    if invoer.basisjaar < 0 
        fprintf(fid1,'%s\n',['- Basisjaar is ' num2str(invoer.basisjaar) '. Dit is niet ok! Basisjaar dient groter dan 0 te zijn.***']);
        d1 = d1+1;
    else
        %check de inhoud?
        fprintf(fid1,'%s\n',['- Basisjaar is ' num2str(invoer.basisjaar) '. ']);
    end
catch
    fprintf(fid1,'%s\n','- Basisjaar is niet aangegeven.***');
    d1 = d1+1;
end
fprintf(fid1,'%s\n','');

