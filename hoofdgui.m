function a = hoofdgui()
clear; close all; clc;
dm = dialog('Units','normalized','Position',[0.4 0.4 0.3 0.2],'Name',...
    'OKADER - kies het type van berekening','DeleteFcn','delete(gcf)');
bg = uibuttongroup('Parent',dm,'Visible','on','Units','normalized','Position',[0.02 0.02 0.97 0.96],...
    'SelectionChangedFcn','');
r1 = uicontrol('Parent',bg,'Style','radiobutton','Units','normalized','String','Enkele berekening / invoerfile aanmaken',...
    'Position',[0.025 0.8 0.975 0.1],'HandleVisibility','off','FontSize',10,'FontWeight','Bold');
r2 = uicontrol('Parent',bg,'Style','radiobutton','Units','normalized','String','Batch berekening',...
    'Position',[0.025 0.6 0.975 0.1],'HandleVisibility','off','FontSize',10,'FontWeight','Bold');
btn = uicontrol('Parent',dm,'Units','normalized','Position',[0.3 0.2 0.4 0.2],...
    'String','Doorgaan','FontSize',14,'FontWeight','Bold','Callback',@btn_callback);
uiwait(dm);
    function btn_callback(btn,callbackdata)
        if get(r1,'Value') == 1
            a = 1;
        else
            a = 2;
        end
        delete(gcf);
    end
end
