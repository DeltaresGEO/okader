function d4 = controle_deel4(fid1,invoer,d4)
%norm
%ontwerp_duur
%db_maxpipberm
%db_DVschema

fprintf(fid1,'%s\n','Deel 4. Controleren parameters "Dijkversterking" ');

%norm
try
    if exist(invoer.db_norm,'file') > 0
        fprintf(fid1,'%s\n',['- Normen: file ' invoer.db_norm ' is gevonden.']);
        %check de inhoud?
    else
        fprintf(fid1,'%s\n',['- Normen: file ' invoer.db_norm ' is niet gevonden. Controleer de filenaam.***']);
        d4 = d4+1;
    end
catch
    fprintf(fid1,'%s\n','- Normen file is niet aangegeven.***');
    d4 = d4+1;
end

%ontwerp_duur
try
    if exist(invoer.db_ontwerp_duur,'file') > 0
        fprintf(fid1,'%s\n',['- Ontwerpduur: file ' invoer.db_ontwerp_duur ' is gevonden.']);
        %check de inhoud?
    else
        fprintf(fid1,'%s\n',['- Ontwerpduur: file ' invoer.db_ontwerp_duur ' is niet gevonden. Controleer de filenaam.***']);
        d4 = d4+1;
    end
catch
    fprintf(fid1,'%s\n','- Ontwerpduur is niet aangegeven.***');
    d4 = d4+1;
end

%db_maxpipberm
%try % Op verzoek van HKV om te verwijderen d.d. 11 juli 2018
%    if exist(invoer.db_maxpipberm,'file') > 0
%        fprintf(fid1,'%s\n',['- Maximale pipingberm: file ' invoer.db_maxpipberm ' is gevonden.']);
%        %check de inhoud?
%    else
%        %fprintf(fid1,'%s\n',['- Maximale pipingberm: file ' invoer.db_maxpipberm ' is niet gevonden. Controleer de filenaam.***']);
%        %d4 = d4+1;
%    end
%catch
%    %fprintf(fid1,'%s\n','- Maximale pipingberm file is niet aangegeven.***');
%    %d4 = d4+1;
%end

%db_DVschema
try
    if exist(invoer.db_DVschema,'file') > 0
        fprintf(fid1,'%s\n',['- DVschema: file ' invoer.db_DVschema ' is gevonden.']);
        %check de inhoud?
    else
        fprintf(fid1,'%s\n',['- DVschema: file ' invoer.db_DVschema ' is niet gevonden. Controleer de filenaam.***']);
        d4 = d4+1;
    end
catch
    fprintf(fid1,'%s\n','- DVschema file is niet aangegeven.***');
    d4 = d4+1;
end

%winst_rvm
try
    if invoer.winst_rvm==1
        fprintf(fid1,'%s\n','- variabele "winst_rvm" = 1 (dimensies).');
    elseif invoer.winst_rvm==2
        fprintf(fid1,'%s\n','- variabele "winst_rvm" = 2 (tijd).');
    else
        fprintf(fid1,'%s\n','- variabele "winst_rvm" is niet correct aangegeven. Kies uit 1 (dimensies) of 2 (tijd).***');
        d4 = d4+1;
    end
catch
    fprintf(fid1,'%s\n','- variabele "winst_rvm" is niet aangegeven.***');
    d4 = d4+1;
end

%db_corr_dH
try
    if exist(invoer.db_corr_dH,'file') > 0
        fprintf(fid1,'%s\n',['- db_corr_dH: file ' invoer.db_corr_dH ' is gevonden.']);
        %check de inhoud?
    else
        fprintf(fid1,'%s\n',['- db_corr_dH: file ' invoer.db_corr_dH ' is niet gevonden. Controleer de filenaam.***']);
        d4 = d4+1;
    end
catch
    fprintf(fid1,'%s\n','- db_corr_dH file is niet aangegeven.***');
    d4 = d4+1;
end

%db_corr_dS
try
    if exist(invoer.db_corr_dS,'file') > 0
        fprintf(fid1,'%s\n',['- db_corr_dS: file ' invoer.db_corr_dS ' is gevonden.']);
        %check de inhoud?
    else
        fprintf(fid1,'%s\n',['- db_corr_dS: file ' invoer.db_corr_dS ' is niet gevonden. Controleer de filenaam.***']);
        d4 = d4+1;
    end
catch
    fprintf(fid1,'%s\n','- db_corr_dS file is niet aangegeven.***');
    d4 = d4+1;
end

%db_corr_dP
try
    if exist(invoer.db_corr_dP,'file') > 0
        fprintf(fid1,'%s\n',['- db_corr_dP: file ' invoer.db_corr_dP ' is gevonden.']);
        %check de inhoud?
    else
        fprintf(fid1,'%s\n',['- db_corr_dP: file ' invoer.db_corr_dP ' is niet gevonden. Controleer de filenaam.***']);
        d4 = d4+1;
    end
catch
    fprintf(fid1,'%s\n','- db_corr_dP file is niet aangegeven.***');
    d4 = d4+1;
end

%db_pipcost
try
    if exist(invoer.db_pipcost,'file') > 0
        fprintf(fid1,'%s\n',['- db_pipcost: file ' invoer.db_pipcost ' is gevonden.']);
        %check de inhoud?
    else
        fprintf(fid1,'%s\n',['- db_pipcost: file ' invoer.db_pipcost ' is niet gevonden. Controleer de filenaam.***']);
        d4 = d4+1;
    end
catch
    fprintf(fid1,'%s\n','- db_pipcost file is niet aangegeven.***');
    d4 = d4+1;
end

%opl_DV
try
    if invoer.opl_DV>1 || invoer.opl_DV<0
        fprintf(fid1,'%s\n','- variabele "opl_DV" is niet correct aangegeven. Kies uit 1 (wel opleggen dijkversterkingen) of 0 (niet opleggen dijkversterkingen).***');
        d4 = d4+1;
    elseif invoer.opl_DV == 1
        fprintf(fid1,'%s\n','- variabele "opl_DV" = 1 (wel opleggen dijkversterkingen).');
    elseif invoer.opl_DV == 0
        fprintf(fid1,'%s\n','- variabele "opl_DV" = 0 (niet opleggen dijkversterkingen).');
    end
catch
    fprintf(fid1,'%s\n','- variabele "opl_DV" is niet aangegeven.***');
    d4 = d4+1;
end

%db_DV_dH
try
    if invoer.opl_DV == 1
        try
            if exist(invoer.db_DV_dH,'file') > 0
                fprintf(fid1,'%s\n',['- db_DV_dH: file ' invoer.db_DV_dH ' is gevonden.']);
                %check de inhoud?
            else
                fprintf(fid1,'%s\n',['- db_DV_dH: file ' invoer.db_DV_dH ' is niet gevonden. Controleer de filenaam.***']);
                d4 = d4+1;
            end
        catch
            fprintf(fid1,'%s\n','- db_DV_dH file is niet aangegeven.***');
            d4 = d4+1;
        end
    end
end    

%db_DV_dS
try
    if invoer.opl_DV == 1
        try
            if exist(invoer.db_DV_dS,'file') > 0
                fprintf(fid1,'%s\n',['- db_DV_dS: file ' invoer.db_DV_dS ' is gevonden.']);
                %check de inhoud?
            else
                fprintf(fid1,'%s\n',['- db_DV_dS: file ' invoer.db_DV_dS ' is niet gevonden. Controleer de filenaam.***']);
                d4 = d4+1;
            end
        catch
            fprintf(fid1,'%s\n','- db_DV_dS file is niet aangegeven.***');
            d4 = d4+1;
        end
    end
end

%db_DV_dP
try
    if invoer.opl_DV == 1
        try
            if exist(invoer.db_DV_dP,'file') > 0
                fprintf(fid1,'%s\n',['- db_DV_dP: file ' invoer.db_DV_dP ' is gevonden.']);
                %check de inhoud?
            else
                fprintf(fid1,'%s\n',['- db_DV_dP: file ' invoer.db_DV_dP ' is niet gevonden. Controleer de filenaam.***']);
                d4 = d4+1;
            end
        catch
            fprintf(fid1,'%s\n','- db_DV_dP file is niet aangegeven.***');
            d4 = d4+1;
        end
    end
end

fprintf(fid1,'%s\n','');
