function [fn_sta,werk_dir_sta,invoer] = runnen_gui_feb_2019(dir_start)
%% Hoofdscript
%clear; close all; clc;

%% Parameters
nQT_max = 10;
nQH_max = 10;
nnorm_max = 1;
nverst_max = 1;

%% Hoofdframe
persistent wd_global;
try
    wd_global = dir_start;
catch
    wd_global = '';
end
d = dialog('Units','normalized','Position',[0.2 0.2 0.6 0.7],'Name',...
    'OKADER - enkele berekening / invoerfile aanmaken -','DeleteFcn',@del_func,'Visible','on');
all_handles = struct('id',[]);
all_handles.id = nan(1,216); %altijd checken en zonodig updaten!!!
% total handles_id = 173; check: 03-02-2019
% total handles_id = 211; check: 05-02-2019
% total handles_id = 213; check: 12-02-2019
% total handles_id = 216; check: 13-02-2019

%% Hoofdvraag (3 handles_id, id 1 t/m 3) --> cum = 0 + 3 = 3
% Invoerfile selectie
txt1 = uicontrol('Parent',d,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.025 0.95 0.2 0.03],'String','Kies een bestaande invoerfile :','Callback','','Visible','on',...
    'Enable','on');
all_handles.id(1) = txt1;
lst1 = uicontrol('Parent',d,'Style','edit','Units','normalized',...
    'Position',[0.2 0.95 0.75 0.03],'String','- (selecteer een bestaande invoerfile via de knop naast deze balk) -',...
    'HorizontalAlignment','left','Callback','','FontWeight','bold','Enable','on','Visible','on');
all_handles.id(2) = lst1;
btn1 = uicontrol('Parent',d,'Units','normalized','Position',[0.955 0.95 0.02 0.03],...
    'String','...','Callback',@btn1_callback,'Visible','on','Enable','on');
all_handles.id(3) = btn1;

%% HoofdUItab
tab_all = uitabgroup('Parent',d,'Units','Normalized','Position',[0.025 0.075 0.95 0.86],'Visible','on');
tab1 = uitab(tab_all,'Title','Algemeen');
tab2 = uitab(tab_all,'Title','Dijksterkte');
tab3 = uitab(tab_all,'Title','Hydraulische belasting');
tab4 = uitab(tab_all,'Title','Dijkversterking en normen');
tab5 = uitab(tab_all,'Title','Berekening instellingen');
tab6 = uitab(tab_all,'Title','Uitvoer en visualisatie');

%% UItab1 - tab1 - (30 handles id, id 4 t/m 33) --> cum = 3 + 30 = 33
% Type van analyse
tb1_ana_txt = uicontrol('Parent',tab1,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.01 0.925 0.1 0.04],'String','Analyse','Callback','','FontWeight','bold',...
    'FontSize',10,'FontName','Cambria','Enable','on','Visible','on');
all_handles.id(4) = tb1_ana_txt;

tb1_ana_txt_str = uicontrol('Parent',tab1,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.02 0.875 0.15 0.035],'String','Type van analyse :','Callback','','Enable','on','Visible','on');
all_handles.id(5) = tb1_ana_txt_str;
tb1_ana_popup = uicontrol('Parent',tab1,'Style','popup','Units','normalized','Position',[0.2 0.895 0.2 0.025],...
    'String',{'Standaard, met faalkansverloop'},'Callback',@tb1_ana_popup_callback,'Enable','on','Visible','on');
all_handles.id(6) = tb1_ana_popup;

% Tijd
tb1_tijd_txt = uicontrol('Parent',tab1,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.01 0.8 0.3 0.04],'String','Tijd','Callback','','FontWeight','bold',...
    'FontSize',10,'FontName','Cambria','Enable','on','Visible','on');
all_handles.id(7) = tb1_tijd_txt;

% Begintijd
tb1_tbegin_txt = uicontrol('Parent',tab1,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.02 0.75 0.4 0.035],'String','Begintijd :','Callback','','Enable','on','Visible','on');
all_handles.id(8) = tb1_tbegin_txt;
tb1_tbegin_txt_str = uicontrol('Parent',tab1,'Style','edit','Units','normalized',...
    'Position',[0.2 0.75+0.0075 0.075 0.04],'String','2020','HorizontalAlignment','left','Callback','',...
    'Enable','on','Visible','on');
all_handles.id(9) = tb1_tbegin_txt_str;
tb1_tbegin_txt_unit = uicontrol('Parent',tab1,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.28 0.75 0.4 0.04],'String','jaar','Callback','','Enable','on','Visible','on');
all_handles.id(10) = tb1_tbegin_txt_unit;

% Eindtijd
tb1_teind_txt = uicontrol('Parent',tab1,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.02 0.7 0.4 0.035],'String','Eindtijd :','Callback','','Enable','on','Visible','on');
all_handles.id(11) = tb1_teind_txt;
tb1_teind_txt_str = uicontrol('Parent',tab1,'Style','edit','Units','normalized',...
    'Position',[0.2 0.7+0.0075 0.075 0.04],'String','2100','HorizontalAlignment','left','Callback','',...
    'Enable','on','Visible','on');
all_handles.id(12) = tb1_teind_txt_str;
tb1_teind_txt_unit = uicontrol('Parent',tab1,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.28 0.7 0.4 0.04],'String','jaar','Callback','','Enable','on','Visible','on');
all_handles.id(13) = tb1_teind_txt_unit;

% Tijdstap
tb1_tstap_txt = uicontrol('Parent',tab1,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.02 0.65 0.4 0.035],'String','Tijdstap :','Callback','','Enable','on','Visible','on');
all_handles.id(14) = tb1_tstap_txt;
tb1_tstap_txt_str = uicontrol('Parent',tab1,'Style','edit','Units','normalized',...
    'Position',[0.2 0.65+0.0075 0.075 0.04],'String','1','HorizontalAlignment','left','Callback','',...
    'Enable','on','Visible','on');
all_handles.id(15) = tb1_tstap_txt_str;
tb1_tstap_txt_unit = uicontrol('Parent',tab1,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.28 0.65 0.4 0.04],'String','jaar','Callback','','Enable','on','Visible','on');
all_handles.id(16) = tb1_tstap_txt_unit;

% Dijkvakgegevens
tb1_vak_txt = uicontrol('Parent',tab1,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.01 0.575 0.3 0.04],'String','Dijkvak gegevens','Callback','','FontWeight','bold',...
    'FontSize',10,'FontName','Cambria','Enable','on','Visible','on');
all_handles.id(17) = tb1_vak_txt;

% Dijkvak
tb1_dijkvak_txt = uicontrol('Parent',tab1,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.02 0.525 0.2 0.04],'String','Dijkvak :','Callback','','Enable','on','Visible','on');
all_handles.id(18) = tb1_dijkvak_txt;
tb1_dijkvak_txt_str = uicontrol('Parent',tab1,'Style','edit','Units','normalized',...
    'Position',[0.2 0.525+0.0075 0.77 0.04],'String','- (selecteer een file met dijkvak gegevens via de knop naast deze balk) -',...
    'HorizontalAlignment','left','Callback','','Enable','on','Visible','on');
all_handles.id(19) = tb1_dijkvak_txt_str;
tb1_dijkvak_txt_btn = uicontrol('Parent',tab1,'Units','normalized','Position',[0.975 0.525+0.0075 0.02 0.04],...
    'String','...','Callback',@tb1_dijkvak_txt_btn_callback,'Enable','on','Visible','on');
all_handles.id(20) = tb1_dijkvak_txt_btn;

% Dijkvaklengte
tb1_dijkvaklen_txt = uicontrol('Parent',tab1,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.02 0.475 0.2 0.04],'String','Dijkvaklengte :','Callback','','Enable','on','Visible','on');
all_handles.id(21) = tb1_dijkvaklen_txt;
tb1_dijkvaklen_txt_str = uicontrol('Parent',tab1,'Style','edit','Units','normalized',...
    'Position',[0.2 0.475+0.0075 0.77 0.04],'String','- (selecteer een file met dijkvaklengte gegevens via de knop naast deze balk) -',...
    'HorizontalAlignment','left','Callback','','Enable','on','Visible','on');
all_handles.id(22) = tb1_dijkvaklen_txt_str;
tb1_dijkvaklen_txt_btn = uicontrol('Parent',tab1,'Units','normalized','Position',[0.975 0.475+0.0075 0.02 0.04],...
    'String','...','Callback',@tb1_dijkvaklen_txt_btn_callback,'Enable','on','Visible','on');
all_handles.id(23) = tb1_dijkvaklen_txt_btn;

% Kosten gegevens
tb1_kosten_txt = uicontrol('Parent',tab1,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.01 0.4 0.3 0.04],'String','Kosten gegevens','Callback','','FontWeight','bold',...
    'FontSize',10,'FontName','Cambria','Enable','on','Visible','on');
all_handles.id(24) = tb1_kosten_txt;

% KOSWAT database
tb1_koswat_txt = uicontrol('Parent',tab1,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.02 0.35 0.2 0.04],'String','KOSWAT gegevens :','Callback','','Enable','on','Visible','on');
all_handles.id(25) = tb1_koswat_txt;
tb1_koswat_txt_str = uicontrol('Parent',tab1,'Style','edit','Units','normalized',...
    'Position',[0.2 0.35+0.0075 0.77 0.04],'String','- (selecteer een file met KOSWAT gegevens via de knop naast deze balk) -',...
    'HorizontalAlignment','left','Callback','','Enable','on','Visible','on','Visible','on');
all_handles.id(26) = tb1_koswat_txt_str;
tb1_koswat_txt_btn = uicontrol('Parent',tab1,'Units','normalized','Position',[0.975 0.35+0.0075 0.02 0.04],...
    'String','...','Callback',@tb1_koswat_txt_btn_callback,'Enable','on','Visible','on');
all_handles.id(27) = tb1_koswat_txt_btn;

% tbasis
tb1_basisjaar_txt = uicontrol('Parent',tab1,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.02 0.25 0.2 0.04],'String','Basisjaar :','Callback','','Enable','on','Visible','on');
all_handles.id(28) = tb1_basisjaar_txt;
tb1_basisjaar_txt_str = uicontrol('Parent',tab1,'Style','edit','Units','normalized',...
    'Position',[0.2 0.25+0.0075 0.075 0.04],'String','2025','HorizontalAlignment','left','Callback','','Enable','on','Visible','on');
all_handles.id(29) = tb1_basisjaar_txt_str;
tb1_basisjaar_txt_unit = uicontrol('Parent',tab1,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.28 0.25 0.2 0.04],'String','jaar','Callback','','Enable','on','Visible','on');
all_handles.id(30) = tb1_basisjaar_txt_unit;

% Discontovoet
tb1_disc_txt = uicontrol('Parent',tab1,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.02 0.2 0.2 0.04],'String','Discontovoet :','Callback','','Enable','on','Visible','on');
all_handles.id(31) = tb1_disc_txt;
tb1_disc_txt_str = uicontrol('Parent',tab1,'Style','edit','Units','normalized',...
    'Position',[0.2 0.2+0.0075 0.075 0.04],'String','3','HorizontalAlignment','left','Callback','','Enable','on','Visible','on');
all_handles.id(32) = tb1_disc_txt_str;
tb1_disc_txt_unit = uicontrol('Parent',tab1,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.28 0.2 0.2 0.04],'String','%','Callback','','Enable','on','Visible','on');
all_handles.id(33) = tb1_disc_txt_unit;

%% UItab2 - tab2 - (32 handles id, id 34 t/m 65) --> cum = 33 + 32 = 65
% Fragility curve
tb2_fc_txt = uicontrol('Parent',tab2,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.01 0.925 0.2 0.04],'String','Fragility curve','Callback','','FontWeight','bold',...
    'FontSize',10,'FontName','Cambria','Enable','on','Visible','on');
all_handles.id(34) = tb2_fc_txt;

% Faalmechanisme HT
tb2_ht_txt = uicontrol('Parent',tab2,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.02 0.875 0.2 0.04],'String','Faalmechanisme HT :','Callback','',...
    'Enable','on','SelectionHighlight','on','Visible','on');
all_handles.id(35) = tb2_ht_txt;
tb2_ht_txt_popup = uicontrol('Parent',tab2,'Style','popup','Units','normalized','Position',[0.2 0.905 0.075 0.02],...
    'String',{'Aan';'Uit';},'Callback',@tb2_ht_txt_popup_callback,'Enable','on','Visible','on');
all_handles.id(36) = tb2_ht_txt_popup;

% Faalmechanisme STBI
tb2_stbi_txt = uicontrol('Parent',tab2,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.02 0.825 0.2 0.04],'String','Faalmechanisme STBI :','Callback','','Enable','on','Visible','on');
all_handles.id(37) = tb2_stbi_txt;
tb2_stbi_txt_popup = uicontrol('Parent',tab2,'Style','popup','Units','normalized','Position',[0.2 0.855 0.075 0.02],...
    'String',{'Aan';'Uit';},'Callback',@tb2_stbi_txt_popup_callback,'Enable','on','Visible','on');
all_handles.id(38) = tb2_stbi_txt_popup;

% Faalmechanisme STPH
tb2_stph_txt = uicontrol('Parent',tab2,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.02 0.775 0.2 0.04],'String','Faalmechanisme STPH :','Callback','','Enable','on','Visible','on');
all_handles.id(39) = tb2_stph_txt;
tb2_stph_txt_popup = uicontrol('Parent',tab2,'Style','popup','Units','normalized','Position',[0.2 0.805 0.075 0.02],...
    'String',{'Aan';'Uit';},'Callback',@tb2_stph_txt_popup_callback,'Enable','on','Visible','on');
all_handles.id(40) = tb2_stph_txt_popup;

% Gegevens FC HT
tb2_htdata_txt = uicontrol('Parent',tab2,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.02 0.725 0.2 0.04],'String','FC gegevens HT :','Callback','',...
    'Enable','on','Visible','on');
all_handles.id(41) = tb2_htdata_txt;
tb2_htdata_txt_str = uicontrol('Parent',tab2,'Style','edit','Units','normalized',...
    'Position',[0.2 0.73 0.77 0.04],'String','- (selecteer een file met fragility curve gegevens voor faalmechanisme HT via de knop naast deze balk) -',...
    'HorizontalAlignment','left','Callback','','Enable','on','Visible','on');
all_handles.id(42) = tb2_htdata_txt_str;
tb2_htdata_txt_btn = uicontrol('Parent',tab2,'Units','normalized','Position',[0.975 0.73 0.02 0.04],...
    'String','...','Callback',@tb2_htdata_txt_btn_callback,'Enable','on','Visible','on');
all_handles.id(43) = tb2_htdata_txt_btn;

% Gegevens FC STBI
tb2_stbidata_txt = uicontrol('Parent',tab2,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.02 0.675 0.2 0.04],'String','FC gegevens STBI :','Callback','','Enable','on','Visible','on');
all_handles.id(44) = tb2_stbidata_txt;
tb2_stbidata_txt_str = uicontrol('Parent',tab2,'Style','edit','Units','normalized',...
    'Position',[0.2 0.68 0.77 0.04],'String','- (selecteer een file met fragility curve gegevens voor faalmechanisme STBI via de knop naast deze balk) -',...
    'HorizontalAlignment','left','Callback','','Enable','on','Visible','on');
all_handles.id(45) = tb2_stbidata_txt_str;
tb2_stbidata_txt_btn = uicontrol('Parent',tab2,'Units','normalized','Position',[0.975 0.68 0.02 0.04],...
    'String','...','Callback',@tb2_stbidata_txt_btn_callback,'Enable','on','Visible','on');
all_handles.id(46) = tb2_stbidata_txt_btn;

% Gegevens FC STPH
tb2_stphdata_txt = uicontrol('Parent',tab2,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.02 0.625 0.2 0.04],'String','FC gegevens STPH :','Callback','','Enable','on','Visible','on');
all_handles.id(47) = tb2_stphdata_txt;
tb2_stphdata_txt_str = uicontrol('Parent',tab2,'Style','edit','Units','normalized',...
    'Position',[0.2 0.63 0.77 0.04],'String','- (selecteer een file met fragility curve gegevens voor faalmechanisme STPH via de knop naast deze balk) -',...
    'HorizontalAlignment','left','Callback','','Enable','on','Visible','on');
all_handles.id(48) = tb2_stphdata_txt_str;
tb2_stphdata_txt_btn = uicontrol('Parent',tab2,'Units','normalized','Position',[0.975 0.63 0.02 0.04],...
    'String','...','Callback',@tb2_stphdata_txt_btn_callback,'Enable','on','Visible','on');
all_handles.id(49) = tb2_stphdata_txt_btn;

% Parameters
tb2_param_txt = uicontrol('Parent',tab2,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.01 0.55 0.2 0.04],'String','Parameters','Callback','','FontWeight','bold',...
    'FontSize',10,'FontName','Cambria','Enable','on','Visible','on');
all_handles.id(50) = tb2_param_txt;

% afkappen fragility curve HT
tb2_afkapht_txt = uicontrol('Parent',tab2,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.02 0.5 0.2 0.04],'String','Afkappen fragility curve HT:','Callback','','Enable','on',...
    'Visible','on');
all_handles.id(51) = tb2_afkapht_txt;
tb2_afkapht_popup = uicontrol('Parent',tab2,'Style','popup','Units','normalized','Position',[0.2 0.52 0.175 0.025],...
    'String',{'geen';'frequentie basis'},'Value',2,'Callback',@tb2_afkapht_popup_callback,'Enable','on','Visible','on');
all_handles.id(52) = tb2_afkapht_popup;
tb2_freqht_txt = uicontrol('Parent',tab2,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.425 0.5 0.2 0.04],'String','Frequentie :','Callback','','Enable','on','Visible','on');
all_handles.id(53) = tb2_freqht_txt;
tb2_freqht_txt_str = uicontrol('Parent',tab2,'Style','edit','Units','normalized','Position',[0.5 0.5+0.005 0.075 0.04],'String','1',...
    'HorizontalAlignment','left','Callback','','Enable','on','Visible','on');
all_handles.id(54) = tb2_freqht_txt_str;
tb2_freqht_txt_unit = uicontrol('Parent',tab2,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.575 0.5 0.2 0.04],'String','per jaar','Callback','','Enable','on','Visible','on');
all_handles.id(55) = tb2_freqht_txt_unit;

% afkappen fragility curve STBI
tb2_afkapstbi_txt = uicontrol('Parent',tab2,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.02 0.45 0.2 0.04],'String','Afkappen fragility curve STBI:','Callback','','Enable','on',...
    'Visible','on');
all_handles.id(56) = tb2_afkapstbi_txt;
tb2_afkapstbi_popup = uicontrol('Parent',tab2,'Style','popup','Units','normalized','Position',[0.2 0.47 0.175 0.025],...
    'String',{'geen';'frequentie basis';'buitenteen en kruin'},'Callback',@tb2_afkapstbi_popup_callback,'Enable','on',...
    'Value',2,'Visible','on');
all_handles.id(57) = tb2_afkapstbi_popup;
tb2_freqstbi_txt = uicontrol('Parent',tab2,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.425 0.45 0.2 0.04],'String','Frequentie:','Callback','','Enable','on','Visible','on');
all_handles.id(58) = tb2_freqstbi_txt;
tb2_freqstbi_txt_str = uicontrol('Parent',tab2,'Style','edit','Units','normalized','Position',[0.5 0.45+0.005 0.075 0.04],'String','1',...
    'HorizontalAlignment','left','Callback','','Enable','on','Visible','on');
all_handles.id(59) = tb2_freqstbi_txt_str;
tb2_freqstbi_txt_unit = uicontrol('Parent',tab2,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.575 0.45 0.2 0.04],'String','per jaar','Callback','','Enable','on','Visible','on');
all_handles.id(60) = tb2_freqstbi_txt_unit;

% afkappen fragility curve STPH
tb2_afkapstph_txt = uicontrol('Parent',tab2,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.02 0.4 0.2 0.04],'String','Afkappen fragility curve STPH:','Callback','','Enable','on',...
    'Visible','on');
all_handles.id(61) = tb2_afkapstph_txt;
tb2_afkapstph_popup = uicontrol('Parent',tab2,'Style','popup','Units','normalized','Position',[0.2 0.42 0.175 0.025],...
    'String',{'geen';'frequentie basis';'buitenteen en kruin'},'Callback',@tb2_afkapstph_popup_callback,'Enable','on',...
    'Value',2,'Visible','on');
all_handles.id(62) = tb2_afkapstph_popup;
tb2_freqstph_txt = uicontrol('Parent',tab2,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.425 0.4 0.2 0.04],'String','Frequentie:','Callback','','Enable','on','Visible','on');
all_handles.id(63) = tb2_freqstph_txt;
tb2_freqstph_txt_str = uicontrol('Parent',tab2,'Style','edit','Units','normalized','Position',[0.5 0.4+0.005 0.075 0.04],'String','1',...
    'HorizontalAlignment','left','Callback','','Enable','on','Visible','on');
all_handles.id(64) = tb2_freqstph_txt_str;
tb2_freqstph_txt_unit = uicontrol('Parent',tab2,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.575 0.4 0.2 0.04],'String','per jaar','Callback','','Enable','on','Visible','on');
all_handles.id(65) = tb2_freqstph_txt_unit;

%% UItab3 - tab3 - (16 handles id, id 66 t/m 81) --> cum = 65 + 16 = 81
% Hydraulische belasting
tb3_hyd_txt = uicontrol('Parent',tab3,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.01 0.925 0.5 0.04],'String','Toeslagen en waterstandverhoging/verlaging','Callback','','FontWeight','bold',...
    'FontSize',10,'FontName','Cambria','Enable','on','Visible','on');
all_handles.id(66) = tb3_hyd_txt;

% onzerkheidstoeslag
tb3_onz_txt = uicontrol('Parent',tab3,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.02 0.875 0.4 0.035],'String','Onzekerheidstoeslag :','Callback','','Enable','on',...
    'Visible','on');
all_handles.id(67) = tb3_onz_txt;
tb3_onz_txt_str = uicontrol('Parent',tab3,'Style','edit','Units','normalized',...
    'Position',[0.2 0.875 0.77 0.04],'String','- (selecteer een file met onzekerheidstoeslag gegevens via de knop naast deze balk) -',...
    'HorizontalAlignment','left','Callback','','Enable','on','Visible','on');
all_handles.id(68) = tb3_onz_txt_str;
tb3_onz_txt_btn = uicontrol('Parent',tab3,'Units','normalized','Position',[0.975 0.875 0.02 0.04],...
    'String','...','Callback',@tb3_onz_txt_btn_callback,'Enable','on','Visible','on');
all_handles.id(69) = tb3_onz_txt_btn;

% bodemdaling
tb3_bodm_txt = uicontrol('Parent',tab3,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.02 0.83 0.15 0.035],'String','Bodemdaling :','Callback','','Enable','on','Visible','on');
all_handles.id(70) = tb3_bodm_txt;
tb3_bodm_txt_str = uicontrol('Parent',tab3,'Style','edit','Units','normalized',...
    'Position',[0.2 0.830 0.77 0.04],'String','- (selecteer een file met bodemdaling gegevens via de knop naast deze balk) -',...
    'HorizontalAlignment','left','Callback','','Enable','on','Visible','on');
all_handles.id(71) = tb3_bodm_txt_str;
tb3_bodm_txt_btn = uicontrol('Parent',tab3,'Units','normalized','Position',[0.975 0.830 0.02 0.04],...
    'String','...','Callback',@tb3_bodm_txt_btn_callback,'Enable','on','Visible','on');
all_handles.id(72) = tb3_bodm_txt_btn;

% integrale verhoging/verlaging
tb3_dh_txt = uicontrol('Parent',tab3,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.02 0.78 0.15 0.035],'String','Integrale dh :','Callback','','Enable','on','Visible','on');
all_handles.id(73) = tb3_dh_txt;
tb3_dh_txt_str = uicontrol('Parent',tab3,'Style','edit','Units','normalized',...
    'Position',[0.2 0.78+0.005 0.075 0.04],'String','0','HorizontalAlignment','left','Callback','','Enable','on',...
    'Visible','on');
all_handles.id(74) = tb3_dh_txt_str;
tb3_dh_txt_unit = uicontrol('Parent',tab3,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.28 0.78 0.4 0.04],'String','m (- voor verlaging en + voor verhooging)','Callback','',...
    'Enable','on','Visible','on');
all_handles.id(75) = tb3_dh_txt_unit;

% Extrapolatie
tb3_extrap_txt = uicontrol('Parent',tab3,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.02 0.73 0.2 0.04],'String','Extrapolatie werklijn :','Callback','',...
    'Enable','on','Visible','on');
all_handles.id(76) = tb3_extrap_txt;
tb3_extrap_popup = uicontrol('Parent',tab3,'Style','popup','Units','normalized','Position',[0.2 0.755 0.15 0.025],...
    'String',{'Loglinear';'Parallel loglinear';},'Callback',@tb3_extrap_popup_callback,'Enable','on',...
    'Visible','on');
all_handles.id(77) = tb3_extrap_popup;

% QT scenario's
t18 = uicontrol('Parent',tab3,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.01 0.655 0.3 0.04],'String','QT scenario''s','Callback','','FontWeight','bold',...
    'FontSize',10,'FontName','Cambria','Enable','on','Visible','on');
all_handles.id(78) = t18;
d18 = cell(nQT_max,5);
for i = 1:nQT_max
    d18{i,1} = i;
end
ta18 = uitable('Parent',tab3,'Units','Normalized','Position',[0.02 0.39 0.975 0.25],...
    'ColumnName',{'Id','Begintijd [jaar]','QT file 1','QT file 2','QT file 3'},'RowName',[],...
    'ColumnWidth',{'auto' 100 250 250 250},'Data',d18,'ColumnFormat',{'numeric' 'numeric' 'char' 'char' 'char'},...
    'ColumnEditable',[false true true true true],'CellSelectionCallback',@ta18_select_callback,'Visible','on',...
    'CellEditCallback',@ta18_edit_callback);
all_handles.id(79) = ta18;

% QH scenario's
t19 = uicontrol('Parent',tab3,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.01 0.3 0.3 0.04],'String','QH scenario''s','Callback','','FontWeight','bold',...
    'FontSize',10,'FontName','Cambria','Enable','on','Visible','on');
all_handles.id(80) = t19;
d19 = cell(nQH_max,5);
for i = 1:nQH_max
    d19{i,1} = i;
end
ta19 = uitable('Parent',tab3,'Units','Normalized','Position',[0.02 0.035 0.975 0.25],...
    'ColumnName',{'Id','Begintijd [jaar]','QH file 1','QH file 2','QH file 3'},'RowName',[],...
    'ColumnWidth',{'auto' 100 250 250 250},'Data',d19,'ColumnFormat',{'numeric' 'numeric' 'char' 'char' 'char'},...
    'ColumnEditable',[false true true true true],'CellSelectionCallback',@ta19_select_callback,'Visible','on',...
    'CellEditCallback',@ta19_edit_callback);
all_handles.id(81) = ta19;

%% UItab4 - tab4 - (23 handles id, id 82 t/m 104) --> cum = 81 + 23 = 104
% Scenario instelling
tb4_sceinst_txt = uicontrol('Parent',tab4,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.01 0.925 0.4 0.04],'String','Scenario instelling','Callback','','FontWeight','bold',...
    'FontSize',10,'FontName','Cambria','Enable','on','Visible','on');
all_handles.id(82) = tb4_sceinst_txt;
bg20 = uibuttongroup('Parent',tab4,'Visible','on','Units','Normalized','Position',[0.02 0.875 0.5 0.05],'SelectionChangedFcn',@bg20_callback,'BorderType','none');
all_handles.id(83) = bg20;
bg20a = uicontrol(bg20,'Style','radiobutton','String','Enkel scenario','Value',1,'Units','Normalized','Position',[0.01 0.01 0.5 0.8],'HandleVisibility','on','Enable','on');
all_handles.id(84) = bg20a;
bg20b = uicontrol(bg20,'Style','radiobutton','String','Meerdere scenario''s','Units','Normalized','Position',[0.3 0.01 0.5 0.8],'HandleVisibility','on','Enable','off');
all_handles.id(85) = bg20b;

% Dijkversterking parameters
tb4_param_txt = uicontrol('Parent',tab4,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.01 0.8 0.4 0.04],'String','Parameters','Callback','','FontWeight','bold',...
    'FontSize',10,'FontName','Cambria','Enable','on','Visible','on');
all_handles.id(86) = tb4_param_txt;

% Ontwerpduur
tb4_ont_txt = uicontrol('Parent',tab4,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.02 0.75 0.1 0.035],'String','Ontwerpduur :','Callback','',...
    'Enable','on','Visible','on');
all_handles.id(87) = tb4_ont_txt;
tb4_ont_txt_str = uicontrol('Parent',tab4,'Style','edit','Units','normalized',...
    'Position',[0.2 0.75 0.77 0.04],'String','- (selecteer een file met onzekerheidstoeslag gegevens via de knop naast deze balk) -',...
    'HorizontalAlignment','left','Callback','','Enable','on','Visible','on');
all_handles.id(88) = tb4_ont_txt_str;
tb4_ont_txt_btn = uicontrol('Parent',tab4,'Units','normalized','Position',[0.975 0.75 0.02 0.04],...
    'String','...','Callback',@tb4_ont_txt_btn_callback,'Enable','on','Visible','on');
all_handles.id(89) = tb4_ont_txt_btn;

% maximale pipingberm (op verzoek van HKV om te werwijderen d.d 11 juli 2018)?
% tb4_maxpip_txt = uicontrol('Parent',tab4,'Style','text','Units','normalized','HorizontalAlignment','left',...
%     'Position',[0.02 0.7 0.5 0.035],'String','Maximale pipingberm :','Callback','',...
%     'Enable','on','Visible','on');
% all_handles.id(90) = tb4_maxpip_txt;
% tb4_maxpip_txt_str = uicontrol('Parent',tab4,'Style','edit','Units','normalized',...
%     'Position',[0.2 0.7 0.77 0.04],'String','- (selecteer een file met maximale pipingberm gegevens via de knop naast deze balk) -',...
%     'HorizontalAlignment','left','Callback','','Enable','on','Visible','on');
% all_handles.id(91) = tb4_maxpip_txt_str;
% tb4_maxpip_txt_btn = uicontrol('Parent',tab4,'Units','normalized','Position',[0.975 0.7 0.02 0.04],...
%     'String','...','Callback',@tb4_maxpip_txt_btn_callback,'Enable','on','Visible','on');
% all_handles.id(92) = tb4_maxpip_txt_btn;

% forcering dijkversterking
ty = 0.7;
tb4_hwbp_txt = uicontrol('Parent',tab4,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.02 ty 0.5 0.035],'String','Forcering dijkversterking :','Callback','',...
    'Enable','on','Visible','on');
all_handles.id(93) = tb4_hwbp_txt;
tb4_hwbp_txt_str = uicontrol('Parent',tab4,'Style','edit','Units','normalized',...
    'Position',[0.2 ty 0.77 0.04],'String','- (selecteer een file met forcering dijkversterking gegevens via de knop naast deze balk) -',...
    'HorizontalAlignment','left','Callback','','Enable','on','Visible','on');
all_handles.id(94) = tb4_hwbp_txt_str;
tb4_hwbp_txt_btn = uicontrol('Parent',tab4,'Units','normalized','Position',[0.975 ty 0.02 0.04],...
    'String','...','Callback',@tb4_hwbp_txt_btn_callback,'Enable','on','Visible','on');
all_handles.id(95) = tb4_hwbp_txt_btn;

% winst rivierverruiming
tb4_winst_txt = uicontrol('Parent',tab4,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.02 ty-0.05 0.5 0.035],'String','Winst rivierverruiming :','Callback','',...
    'Enable','on','Visible','on');
all_handles.id(96) = tb4_winst_txt;
tb4_winst_txt_popup = uicontrol('Parent',tab4,'Style','popup','Units','normalized','Position',[0.2 ty-0.035 0.175 0.025],...
    'String',{'dimensie';'tijd'},'Value',1,'Callback',@tb4_winst_popup_callback,'Enable','on',...
    'Visible','on');
all_handles.id(97) = tb4_winst_txt_popup;

% normfile
tb4_norm_txt = uicontrol('Parent',tab4,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.02 ty-0.10 0.5 0.035],'String','Norm :','Callback','',...
    'Enable','on','Visible','on');
all_handles.id(98) = tb4_norm_txt;
tb4_norm_txt_str = uicontrol('Parent',tab4,'Style','edit','Units','normalized',...
    'Position',[0.2 ty-0.10 0.77 0.04],'String','- (selecteer een file met norm gegevens via de knop naast deze balk) -',...
    'HorizontalAlignment','left','Callback','','Enable','on','Visible','on');
all_handles.id(99) = tb4_norm_txt_str;
tb4_norm_txt_btn = uicontrol('Parent',tab4,'Units','normalized','Position',[0.975 ty-0.10 0.02 0.04],...
    'String','...','Callback',@tb4_norm_txt_btn_callback,'Enable','on','Visible','on');
all_handles.id(100) = tb4_norm_txt_btn;

% normen scenario's
txt24 = uicontrol('Parent',tab4,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.01 0.525 0.4 0.04],'String','Normen scenario''s:','Callback','','FontWeight','bold',...
    'FontSize',10,'FontName','Cambria','Enable','off','Visible','off');
all_handles.id(101) = txt24;
d24 = cell(nnorm_max,3);
for i = 1:min(size(d24))
    d24{i,1} = i;
end
ta24 = uitable('Parent',tab4,'Units','Normalized','Position',[0.02 0.3 0.975 0.2],...
    'ColumnName',{'Id','Eindtijd [jaar]','Norm file'},'RowName',[],...
    'ColumnWidth',{'auto' 100 250 250 250},'Data',d24,'ColumnFormat',{'numeric' 'numeric' 'char'},...
    'ColumnEditable',[false true true],'CellSelectionCallback',@ta24_select_callback,'Visible','off',...
    'CellEditCallback',@ta24_edit_callback);
all_handles.id(102) = ta24;

% dijkversterking scenario's
txt25 = uicontrol('Parent',tab4,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.01 0.225 0.4 0.04],'String','Dijkversterking scenario''s:','Callback','','FontWeight','bold',...
    'FontSize',10,'FontName','Cambria','Enable','on','Visible','off');
all_handles.id(103) = txt25;
d25 = cell(nverst_max,4);
for i = 1:min(size(d25))
    d25{i,1} = i;
end
ta25 = uitable('Parent',tab4,'Units','Normalized','Position',[0.02 0.01 0.975 0.2],...
    'ColumnName',{'Id','Eindtijd [jaar]','QT scenario','QH scenario'},'RowName',[],...
    'ColumnWidth',{'auto' 100 250 250 250},'Data',d25,'ColumnFormat',{'numeric' 'numeric' 'numeric' 'numeric'},...
    'ColumnEditable',[false true true true],'CellSelectionCallback',@ta25_select_callback,'Visible','off',...
    'CellEditCallback',@ta25_edit_callback);
all_handles.id(104) = ta25;

%% UItab5 - tab5 - (46 handles id, id 105 t/m 150) --> cum = 104 + 46 = 150
% Geadvanceerde instelling:
tb5_adv_txt = uicontrol('Parent',tab5,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.01 0.925 0.4 0.04],'String','Geadvanceerde parameters','Callback','','FontWeight','bold',...
    'FontSize',10,'FontName','Cambria','Enable','on','Visible','on');
all_handles.id(105) = tb5_adv_txt;

% Stab factor (op verzoek van HKV om te verwijderen d.d. 11 juli 2018)
% tb5_stabfac_txt = uicontrol('Parent',tab5,'Style','text','Units','normalized','HorizontalAlignment','left',...
%     'Position',[0.02 0.875 0.15 0.04],'String','dS versterkingsfactor :','Callback','','Enable','on',...
%     'Visible','on');
% all_handles.id(106) = tb5_stabfac_txt;
% tb5_stabfac_txt_str = uicontrol('Parent',tab5,'Style','edit','Units','normalized','Position',[0.25 0.88 0.075 0.04],'String','1',...
%     'HorizontalAlignment','left','Callback','','Enable','on','Visible','on');
% all_handles.id(107) = tb5_stabfac_txt_str;
% tb5_stabfac_txt_unit = uicontrol('Parent',tab5,'Style','text','Units','normalized','HorizontalAlignment','left',...
%     'Position',[0.33 0.875 0.4 0.04],'String','[-]','Callback','','Enable','on','Visible','on');
% all_handles.id(108) = tb5_stabfac_txt_unit;

% Pipfactor (op verzoek van HKV om te verwijderen d.d. 11 juli 2018)
% tb5_pipfac_txt = uicontrol('Parent',tab5,'Style','text','Units','normalized','HorizontalAlignment','left',...
%     'Position',[0.02 0.825 0.15 0.04],'String','dP versterkingsfactor :','Callback','',...
%     'Enable','on','Visible','on');
% all_handles.id(109) = tb5_pipfac_txt;
% tb5_pipfac_txt_str = uicontrol('Parent',tab5,'Style','edit','Units','normalized','Position',[0.25 0.83 0.075 0.04],...
%     'String','1','HorizontalAlignment','left','Callback','','Enable','on','Visible','on');
% all_handles.id(110) = tb5_pipfac_txt_str;
% tb5_pipfac_txt_unit = uicontrol('Parent',tab5,'Style','text','Units','normalized','HorizontalAlignment','left',...
%     'Position',[0.33 0.825 0.4 0.04],'String','[-]','Callback','','Enable','on','Visible','on');
% all_handles.id(111) = tb5_pipfac_txt_unit;

%dH_stap
tt = 0.875; % 0.775;
tt1 = tt+0.005;
tb5_dhstap_txt = uicontrol('Parent',tab5,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.02 tt 0.2 0.04],'String','dH stapgrootte :','Callback','',...
    'Enable','on','Visible','on');
all_handles.id(112) = tb5_dhstap_txt;
tb5_dhstap_txt_str = uicontrol('Parent',tab5,'Style','edit','Units','normalized','Position',[0.25 tt1 0.075 0.04],'String','0.02',...
    'HorizontalAlignment','left','Callback','','Enable','on','Visible','on');
all_handles.id(113) = tb5_dhstap_txt_str;
tb5_dhstap_txt_unit = uicontrol('Parent',tab5,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.33 tt 0.4 0.04],'String','m','Callback','','Enable','on','Visible','on');
all_handles.id(114) = tb5_dhstap_txt_unit;

%dS_stap
tt = 0.825; % 0.725;
tt1 = tt+0.005;
tb5_dsstap_txt = uicontrol('Parent',tab5,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.02 0.825 0.2 0.04],'String','dS stapgrootte :','Callback','',...
    'Enable','on','Visible','on');
all_handles.id(115) = tb5_dsstap_txt;
tb5_dsstap_txt_str = uicontrol('Parent',tab5,'Style','edit','Units','normalized','Position',[0.25 tt1 0.075 0.04],'String','0.2',...
    'HorizontalAlignment','left','Callback','','Enable','on','Visible','on');
all_handles.id(116) = tb5_dsstap_txt_str;
tb5_dsstap_txt_unit = uicontrol('Parent',tab5,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.33 0.825 0.4 0.04],'String','m','Callback','','Enable','on','Visible','on');
all_handles.id(117) = tb5_dsstap_txt_unit;

%dP_stap
tt = 0.775; % 0.675;
tt1 = tt+0.005;
tb5_dpstap_txt = uicontrol('Parent',tab5,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.02 tt 0.2 0.04],'String','dP stapgrootte :','Callback','',...
    'Enable','on','Visible','on');
all_handles.id(118) = tb5_dpstap_txt;
tb5_dpstap_txt_str = uicontrol('Parent',tab5,'Style','edit','Units','normalized','Position',[0.25 tt1 0.075 0.04],'String','0.2',...
    'HorizontalAlignment','left','Callback','','Enable','on','Visible','on');
all_handles.id(119) = tb5_dpstap_txt_str;
tb5_dpstap_txt_unit = uicontrol('Parent',tab5,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.33 tt 0.4 0.04],'String','m','Callback','','Enable','on');
all_handles.id(120) = tb5_dpstap_txt_unit;

%dH_max
tt = 0.725; % 0.625;
tt1 = tt+0.005;
tb5_dHmax_txt = uicontrol('Parent',tab5,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.02 tt 0.25 0.04],'String','dH max. :','Callback','',...
    'Enable','on','Visible','on');
all_handles.id(121) = tb5_dHmax_txt;
tb5_dHmax_txt_str = uicontrol('Parent',tab5,'Style','edit','Units','normalized','Position',[0.25 tt1 0.075 0.04],'String','10',...
    'HorizontalAlignment','left','Callback','','Enable','on','Visible','on');
all_handles.id(122) = tb5_dHmax_txt_str;
tb5_dHmax_txt_unit = uicontrol('Parent',tab5,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.33 tt 0.4 0.04],'String','m','Callback','','Enable','on');
all_handles.id(123) = tb5_dHmax_txt_unit;

%dS_max
tt = 0.675; % 0.575;
tt1 = tt+0.005;
tb5_dSmax_txt = uicontrol('Parent',tab5,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.02 tt 0.25 0.04],'String','dS max. :','Callback','',...
    'Enable','on','Visible','on');
all_handles.id(124) = tb5_dSmax_txt;
tb5_dSmax_txt_str = uicontrol('Parent',tab5,'Style','edit','Units','normalized','Position',[0.25 tt1 0.075 0.04],'String','100',...
    'HorizontalAlignment','left','Callback','','Enable','on','Visible','on');
all_handles.id(125) = tb5_dSmax_txt_str;
tb5_dSmax_txt_unit = uicontrol('Parent',tab5,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.33 tt 0.4 0.04],'String','m','Callback','','Enable','on');
all_handles.id(126) = tb5_dSmax_txt_unit;

%dP_max
tt = 0.625; % 0.525;
tt1 = tt+0.005;
tb5_dPmax_txt = uicontrol('Parent',tab5,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.02 tt 0.25 0.04],'String','dP max. :','Callback','',...
    'Enable','on','Visible','on');
all_handles.id(127) = tb5_dPmax_txt;
tb5_dPmax_txt_str = uicontrol('Parent',tab5,'Style','edit','Units','normalized','Position',[0.25 tt1 0.075 0.04],'String','500',...
    'HorizontalAlignment','left','Callback','','Enable','on','Visible','on');
all_handles.id(128) = tb5_dPmax_txt_str;
tb5_dPmax_txt_unit = uicontrol('Parent',tab5,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.33 tt 0.4 0.04],'String','m','Callback','','Enable','on');
all_handles.id(129) = tb5_dPmax_txt_unit;

%Qdisc_step
tt = 0.575; % 0.475;
tt1 = tt+0.005;
tb5_Qdiscstep_txt = uicontrol('Parent',tab5,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.02 tt 0.4 0.04],'String','Discretisatiestap afvoeren :','Callback','',...
    'Enable','on','Visible','on');
all_handles.id(130) = tb5_Qdiscstep_txt;
tb5_Qdiscstep_txt_str = uicontrol('Parent',tab5,'Style','edit','Units','normalized','Position',...
    [0.35 tt1+0.005 0.075 0.04],'String','100','HorizontalAlignment','left','Callback','','Enable','on','Visible','on');
all_handles.id(131) = tb5_Qdiscstep_txt_str;
tb5_Qdiscstep_txt_unit = uicontrol('Parent',tab5,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.43 tt 0.4 0.04],'String','m3/s','Callback','','Enable','on');
all_handles.id(132) = tb5_Qdiscstep_txt_unit;

%Qdisc_min (verwijderd op verzoek van HKV d.d. 11 juli 2018)
% tb5_Qdiscmin_txt = uicontrol('Parent',tab5,'Style','text','Units','normalized','HorizontalAlignment','left',...
%     'Position',[0.02 0.425 0.4 0.04],'String','Discretisatie afvoeren (ondergrens) :','Callback','',...
%     'Enable','off','Visible','off');
% all_handles.id(133) = tb5_Qdiscmin_txt;
% tb5_Qdiscmin_txt_str = uicontrol('Parent',tab5,'Style','edit','Units','normalized','Position',[0.35 0.43 0.075 0.04],'String','100',...
%     'HorizontalAlignment','left','Callback','','Enable','off','Visible','off');
% all_handles.id(134) = tb5_Qdiscmin_txt_str;
% tb5_Qdiscmin_txt_unit = uicontrol('Parent',tab5,'Style','text','Units','normalized','HorizontalAlignment','left',...
%     'Position',[0.43 0.425 0.4 0.04],'String','m3/s','Callback','','Enable','off','Visible','off');
% all_handles.id(135) = tb5_Qdiscmin_txt_unit;

%Qdisc_low (verwijderd op verzoek van HKV d.d. 11 juli 2018)
% tb5_Qdisclow_txt = uicontrol('Parent',tab5,'Style','text','Units','normalized','HorizontalAlignment','left',...
%     'Position',[0.02 0.375 0.4 0.04],'String','Discretisatie afvoeren (verlagen laagste waarde in QT file) :','Callback','',...
%     'Enable','off','Visible','off');
% all_handles.id(136) = tb5_Qdisclow_txt;
% tb5_Qdisclow_txt_str = uicontrol('Parent',tab5,'Style','edit','Units','normalized','Position',[0.35 0.38 0.075 0.04],'String','500',...
%     'HorizontalAlignment','left','Callback','','Enable','off','Visible','off');
% all_handles.id(137) = tb5_Qdisclow_txt_str;
% tb5_Qdisclow_txt_unit = uicontrol('Parent',tab5,'Style','text','Units','normalized','HorizontalAlignment','left',...
%     'Position',[0.43 0.375 0.4 0.04],'String','m3/s','Callback','','Enable','off','Visible','off');
% all_handles.id(138) = tb5_Qdisclow_txt_unit;

%Qdisc_high
tt = 0.525; % 0.325;
tt1 = tt+0.005;
tb5_Qdischigh_txt = uicontrol('Parent',tab5,'Style','text','Units','normalized','HorizontalAlignment','left',...
   'Position',[0.02 tt 0.4 0.04],'String','Discretisatie afvoeren (verhogen hoogste waarde in QT file) :','Callback','',...
   'Enable','on','Visible','on');
all_handles.id(139) = tb5_Qdischigh_txt;
tb5_Qdischigh_txt_str = uicontrol('Parent',tab5,'Style','edit','Units','normalized','Position',...
    [0.35 tt1+0.005 0.075 0.04],'String','1000','HorizontalAlignment','left','Callback','','Enable','on','Visible','on');
all_handles.id(140) = tb5_Qdischigh_txt_str;
tb5_Qdischigh_txt_unit = uicontrol('Parent',tab5,'Style','text','Units','normalized','HorizontalAlignment','left',...
   'Position',[0.43 tt 0.4 0.04],'String','m3/s','Callback','','Enable','on');
all_handles.id(141) = tb5_Qdischigh_txt_unit;

%refjaar_bodemdaling
tt = 0.475; % 0.275;
tt1 = tt+0.005;
tb5_refjaar_txt = uicontrol('Parent',tab5,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.02 tt 0.4 0.04],'String','Referentiejaar bodemdaling :','Callback','',...
    'Enable','on','Visible','on');
all_handles.id(142) = tb5_refjaar_txt;
tb5_refjaar_txt_str = uicontrol('Parent',tab5,'Style','edit','Units','normalized','Position',...
    [0.35 tt1+0.005 0.075 0.04],'String','2015','HorizontalAlignment','left','Callback','','Enable','on','Visible','on');
all_handles.id(143) = tb5_refjaar_txt_str;
tb5_refjaar_txt_unit = uicontrol('Parent',tab5,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.43 tt 0.4 0.04],'String','jaar','Callback','','Enable','on');
all_handles.id(144) = tb5_refjaar_txt_unit;

%t_voldoen_norm
tt = 0.425; % 0.225;
tt1 = tt+0.005;
tb5_tvoln_txt = uicontrol('Parent',tab5,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.02 tt 0.4 0.04],'String','Jaar waarna altijd aan norm moet worden voldaan :','Callback','',...
    'Enable','on','Visible','on');
all_handles.id(145) = tb5_tvoln_txt;
tb5_tvoln_txt_str = uicontrol('Parent',tab5,'Style','edit','Units','normalized','Position',...
    [0.35 tt1+0.005 0.075 0.04],'String','2050','HorizontalAlignment','left','Callback','','Enable','on','Visible','on');
all_handles.id(146) = tb5_tvoln_txt_str;
tb5_tvoln_txt_unit = uicontrol('Parent',tab5,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.43 tt 0.4 0.04],'String','jaar','Callback','','Enable','on');
all_handles.id(147) = tb5_tvoln_txt_unit;

%cost_breakeven_dP (verwijderd op verzoek van HKV d.d. 11 juli 2018)
% tb5_costbdP_txt = uicontrol('Parent',tab5,'Style','text','Units','normalized','HorizontalAlignment','left',...
%    'Position',[0.02 0.175 0.4 0.04],'String','Knikpunt waar constructief voordeliger wordt dan pipingberm :','Callback','',...
%    'Enable','on','Visible','on');
% all_handles.id(148) = tb5_costbdP_txt;
% tb5_costbdP_txt_str = uicontrol('Parent',tab5,'Style','edit','Units','normalized','Position',[0.35 0.18 0.075 0.04],'String','100',...
%    'HorizontalAlignment','left','Callback','','Enable','on','Visible','on');
% all_handles.id(149) = tb5_costbdP_txt_str;
% tb5_costbdP_txt_unit = uicontrol('Parent',tab5,'Style','text','Units','normalized','HorizontalAlignment','left',...
%    'Position',[0.43 0.175 0.4 0.04],'String','m','Callback','','Enable','on');
% all_handles.id(150) = tb5_costbdP_txt_unit;

%% UItab6 - tab6 - (30 handles id, id 151 t/m 180) --> cum = 150 + 30 = 180
% Uitvoer
tb6_uitvoer_txt = uicontrol('Parent',tab6,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.01 0.925 0.4 0.04],'String','Uitvoer','Callback','','FontWeight','bold',...
    'FontSize',10,'FontName','Cambria','Enable','on','Visible','on');
all_handles.id(151) = tb6_uitvoer_txt;

% Uitvoermap
tb6_uitvoermap_txt = uicontrol('Parent',tab6,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.02 0.875 0.2 0.04],'String','Link naar uitvoermap :','Callback','','Enable','on',...
    'Visible','on');
all_handles.id(152) = tb6_uitvoermap_txt;
tb6_uitvoermap_txt_str = uicontrol('Parent',tab6,'Style','edit','Units','normalized','Position',...
    [0.2 0.875+0.005 0.77 0.04],'String','- kies de link naar de uitvoermap via de knop naast deze balk -',...
    'HorizontalAlignment','left','Callback','','Enable','on','Visible','on');
all_handles.id(153) = tb6_uitvoermap_txt_str;
tb6_uitvoermap_txt_btn = uicontrol('Parent',tab6,'Units','normalized','Position',[0.975 0.875+0.005 0.02 0.04],...
    'String','...','Callback',@tb6_uitvoermap_txt_btn_callback,'Enable','on','Visible','on');
all_handles.id(154) = tb6_uitvoermap_txt_btn;

% Tijdopgave
tb6_tijdopgave_txt = uicontrol('Parent',tab6,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.02 0.825 0.2 0.04],'String','Tijdopgave :','Callback','','Enable','on',...
    'Visible','on');
all_handles.id(155) = tb6_tijdopgave_txt;
tb6_tijdopgave_txt_str = uicontrol('Parent',tab6,'Style','edit','Units','normalized','Position',...
    [0.2 0.825+0.005 0.3 0.04],'String','- typ de tijdopgave in (voorbeeld: 2015,2050,2075)','HorizontalAlignment','left','Callback','','Enable','on',...
    'Visible','on');
all_handles.id(156) = tb6_tijdopgave_txt_str;
tb6_tijdopgave_txt_unit = uicontrol('Parent',tab6,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.505 0.825 0.2 0.04],'String','jaar','Callback','','Enable','on','Visible','on');
all_handles.id(157) = tb6_tijdopgave_txt_unit;

% Visualisatie
tb6_vis_txt = uicontrol('Parent',tab6,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.01 0.775 0.4 0.04],'String','Visualisatie(s)','Callback','','FontWeight','bold',...
    'FontSize',10,'FontName','Cambria','Enable','on','Visible','on');
all_handles.id(158) = tb6_vis_txt;
% Faalkans vs tijd
tb6_pfvst_txt = uicontrol('Parent',tab6,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.02 0.725 0.5 0.04],'String','Faalkans vs. tijd :','Callback','','Enable','on',...
    'Visible','on');
all_handles.id(159) = tb6_pfvst_txt;
tb6_pfvst_txt_popup = uicontrol('Parent',tab6,'Style','popup','Units','normalized','Position',[0.2 0.75 0.05 0.025],...
    'String',{'ja';'nee'},'Callback',@tb6_pfvst_txt_popup_callback,'Enable','on','Visible','on',...
    'Value',1);
all_handles.id(160) = tb6_pfvst_txt_popup;

% Dimensie vs tijd
tb6_dimvst_txt = uicontrol('Parent',tab6,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.02 0.675 0.5 0.04],'String','Dimensie vs. tijd :','Callback','','Enable','on',...
    'Visible','on');
all_handles.id(161) = tb6_dimvst_txt;
tb6_dimvst_txt_popup = uicontrol('Parent',tab6,'Style','popup','Units','normalized','Position',[0.2 0.7 0.05 0.025],...
    'String',{'ja';'nee'},'Callback',@tb6_dimvst_txt_popup_callback,'Enable','on','Visible','on',...
    'Value',1);
all_handles.id(162) = tb6_dimvst_txt_popup;

% Faalkans HT
tb6_pfht_txt = uicontrol('Parent',tab6,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.02 0.625 0.5 0.04],'String','Faalkans HT :','Callback','','Enable','on',...
    'Visible','on');
all_handles.id(163) = tb6_pfht_txt;
tb6_pfht_txt_popup = uicontrol('Parent',tab6,'Style','popup','Units','normalized','Position',[0.2 0.65 0.3 0.025],...
    'String',{'voor iedere versterkingstap';'alleen zonder versterkingen';'alleen zonder versterkingen en t = begintijd';'bij ontwerp en T=t0';'nee'},...
    'Callback',@tb6_pfht_txt_popup_callback,'Enable','on','Visible','on','Value',5);
all_handles.id(164) = tb6_pfht_txt_popup;

% Faalkans STBI
tb6_pfstbi_txt = uicontrol('Parent',tab6,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.02 0.575 0.5 0.04],'String','Faalkans STBI :','Callback','','Enable','on',...
    'Visible','on');
all_handles.id(165) = tb6_pfstbi_txt;
tb6_pfstbi_txt_popup = uicontrol('Parent',tab6,'Style','popup','Units','normalized','Position',[0.2 0.6 0.3 0.025],...
    'String',{'voor iedere versterkingstap';'alleen zonder versterkingen';'alleen zonder versterkingen en t = begintijd';'bij ontwerp en T=t0';'nee'},...
    'Callback',@tb6_pfstbi_txt_popup_callback,'Enable','on','Visible','on','Value',5);
all_handles.id(166) = tb6_pfstbi_txt_popup;

% Faalkans STPH
tb6_pfstph_txt = uicontrol('Parent',tab6,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.02 0.525 0.5 0.04],'String','Faalkans STPH :','Callback','','Enable','on',...
    'Visible','on');
all_handles.id(167) = tb6_pfstph_txt;
tb6_pfstph_txt_popup = uicontrol('Parent',tab6,'Style','popup','Units','normalized','Position',[0.2 0.55 0.3 0.025],...
    'String',{'voor iedere versterkingstap';'alleen zonder versterkingen';'alleen zonder versterkingen en t = begintijd';'bij ontwerp en T=t0';'nee'},...
    'Callback',@tb6_pfstph_txt_popup_callback,'Enable','on','Visible','on','Value',5);
all_handles.id(168) = tb6_pfstph_txt_popup;

% Frequentie waterstand
tb6_freqws_txt = uicontrol('Parent',tab6,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.02 0.475 0.5 0.04],'String','Frequentie van waterstand :','Callback','','Enable','on',...
    'Visible','on');
all_handles.id(169) = tb6_freqws_txt;
tb6_freqws_txt_popup = uicontrol('Parent',tab6,'Style','popup','Units','normalized','Position',[0.2 0.5 0.3 0.025],...
    'String',{'ieder jaar';'alleen in opgavejaar';'in opgavejaar en bij ontwerp';'nee'},'Callback',@tb6_freqws_txt_popup_callback,...
    'Enable','on','Visible','on','Value',4);
all_handles.id(170) = tb6_freqws_txt_popup;

% Visualisatie dijk
tb6_visdijk_txt = uicontrol('Parent',tab6,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.02 0.425 0.5 0.04],'String','Visualisatie dijkversterking :','Callback','',...
    'Enable','off','Visible','on');
all_handles.id(171) = tb6_visdijk_txt;
tb6_visdijk_txt_popup = uicontrol('Parent',tab6,'Style','popup','Units','normalized','Position',[0.2 0.45 0.3 0.025],...
    'String',{'ja';'nee'},'Callback',@tb6_visdijk_txt_popup_callback,...
    'Enable','off','Visible','on','Value',2);
all_handles.id(172) = tb6_visdijk_txt_popup;

% Exporteren
tb6_export_txt = uicontrol('Parent',tab6,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.01 0.35 0.4 0.04],'String','Exporteren','Callback','','FontWeight','bold',...
    'FontSize',10,'FontName','Cambria','Enable','on','Visible','on');
all_handles.id(173) = tb6_export_txt;

% Exporteren als csv
tb6_exportcsv_txt = uicontrol('Parent',tab6,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.02 0.30 0.5 0.04],'String','Exporteren .csv per normtraject :','Callback','',...
    'Enable','off','Visible','on');
all_handles.id(174) = tb6_exportcsv_txt;
tb6_exportcsv_txt_popup = uicontrol('Parent',tab6,'Style','popup','Units','normalized','Position',[0.275 0.325 0.05 0.025],...
    'String',{'ja';'nee'},'Callback',@tb6_exportcsv_txt_popup_callback,...
    'Enable','off','Visible','on','Value',2);
all_handles.id(175) = tb6_exportcsv_txt_popup;

% Exporteren als dbf
tb6_exportdbf_txt = uicontrol('Parent',tab6,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.02 0.25 0.5 0.04],'String','Exporteren .xlsx in formaat van dijkshape :','Callback','',...
    'Enable','on','Visible','on');
all_handles.id(176) = tb6_exportdbf_txt;
tb6_exportdbf_txt_popup = uicontrol('Parent',tab6,'Style','popup','Units','normalized','Position',[0.275 0.275 0.05 0.025],...
    'String',{'ja';'nee'},'Callback',@tb6_exportdbf_txt_popup_callback,...
    'Enable','on','Visible','on','Value',2);
all_handles.id(177) = tb6_exportdbf_txt_popup;
tb6_exportdbffile_txt = uicontrol('Parent',tab6,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.35 0.25 0.3 0.04],'String','Kies bestaande .dbf file :','Callback','',...
    'Enable','off','Visible','off');
all_handles.id(178) = tb6_exportdbffile_txt;
tb6_exportdbffile_txt_str = uicontrol('Parent',tab6,'Style','edit','Units','normalized','Position',[0.5 0.255+0.005 0.47 0.04],...
    'String','- kies de link naar de bestaande .dbf file via de knop naast deze balk -',...
    'HorizontalAlignment','left','Callback','','Enable','off','Visible','off');
all_handles.id(179) = tb6_exportdbffile_txt_str;
tb6_exportdbffile_txt_btn = uicontrol('Parent',tab6,'Units','normalized','Position',[0.975 0.255+0.005 0.02 0.04],...
    'String','...','Callback',@tb6_exportdbffile_txt_btn_callback,'Enable','off','Visible','off');
all_handles.id(180) = tb6_exportdbffile_txt_btn;

%% Hoofdknoppen - (6 handles id, id 181 t/m 186) --> cum = 180 + 6 = 186
% Startknop
btn_start = uicontrol('Parent',d,'Units','normalized','Position',[0.875 0.0125 0.1 0.05],...
    'String','Start','FontSize',10,'FontWeight','bold','Callback',@btn_start_callback,...
    'Enable','on','Visible','on');
all_handles.id(181) = btn_start;
btn_ops = uicontrol('Parent',d,'Units','normalized','position',[0.775 0.0125 0.1 0.05],...
    'String','Opslaan','FontSize',10,'FontWeight','bold','Callback',@btn_ops_callback,...
    'Enable','on','Visible','on');
all_handles.id(182) = btn_ops;
btn_che = uicontrol('Parent',d,'Units','normalized','position',[0.675 0.0125 0.1 0.05],...
    'String','Controle','FontSize',10,'FontWeight','bold','Callback',@btn_che_callback,...
    'Enable','on','Visible','on');
all_handles.id(183) = btn_che;

% Check invoer text
inv_stat_0 = uicontrol('Parent',d,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.025 0.025 0.1 0.05],'String','Status:','Callback','',...
    'Enable','on','FontWeight','bold','FontSize',10,'Visible','on');
all_handles.id(184) = inv_stat_0;
inv_stat = uicontrol('Parent',d,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.085 0.018 0.57 0.05],'String','Er is geen invoerfile ingelezen. Kies een invoerfile!','Callback','',...
    'Enable','on','FontWeight','bold','FontSize',7,'Visible','on');
all_handles.id(185) = inv_stat;

%dss = ['>>> Aantal invoerfouten: tab 1 (' num2str(20,'%.0f') '), tab 2 (' num2str(20,'%.0f') '), tab 3 (' num2str(20,'%.0f') '), tab 4 (' num2str(20,'%.0f') '), tab 5 (' num2str(20,'%.0f') '), en tab 6 (' num2str(20,'%.0f') ')'];
dss = '';
inv_stat1 = uicontrol('Parent',d,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.085 -0.025 0.57 0.05],'String',dss,'Callback','',...
    'Enable','on','FontWeight','bold','FontSize',7,'Visible','on');
all_handles.id(186) = inv_stat1;

%% Extra invoer tablad 4 - (23 handles id, id 187 t/m 209) --> cum = 186 + 23 = 209
% dH correctie
tb4_db_corr_dH_txt = uicontrol('Parent',tab4,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.02 ty-0.15 0.5 0.035],'String','dH correctie :','Callback','',...
    'Enable','on','Visible','on');
all_handles.id(187) = tb4_db_corr_dH_txt;
tb4_db_corr_dH_txt_str = uicontrol('Parent',tab4,'Style','edit','Units','normalized',...
    'Position',[0.2 ty-0.15 0.77 0.04],'String','- (selecteer een file met dH correctie via de knop naast deze balk) -',...
    'HorizontalAlignment','left','Callback','','Enable','on','Visible','on');
all_handles.id(188) = tb4_db_corr_dH_txt_str;
tb4_db_corr_dH_txt_btn = uicontrol('Parent',tab4,'Units','normalized','Position',[0.975 ty-0.15 0.02 0.04],...
    'String','...','Callback',@tb4_db_corr_dH_txt_btn_callback,'Enable','on','Visible','on');
all_handles.id(189) = tb4_db_corr_dH_txt_btn;

% dS correctie
tb4_db_corr_dS_txt = uicontrol('Parent',tab4,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.02 ty-0.2 0.5 0.035],'String','dS correctie :','Callback','',...
    'Enable','on','Visible','on');
all_handles.id(190) = tb4_db_corr_dS_txt;
tb4_db_corr_dS_txt_str = uicontrol('Parent',tab4,'Style','edit','Units','normalized',...
    'Position',[0.2 ty-0.2 0.77 0.04],'String','- (selecteer een file met dS correctie via de knop naast deze balk) -',...
    'HorizontalAlignment','left','Callback','','Enable','on','Visible','on');
all_handles.id(191) = tb4_db_corr_dS_txt_str;
tb4_db_corr_dS_txt_btn = uicontrol('Parent',tab4,'Units','normalized','Position',[0.975 ty-0.2 0.02 0.04],...
    'String','...','Callback',@tb4_db_corr_dS_txt_btn_callback,'Enable','on','Visible','on');
all_handles.id(192) = tb4_db_corr_dS_txt_btn;

% dP correctie
tb4_db_corr_dP_txt = uicontrol('Parent',tab4,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.02 ty-0.25 0.5 0.035],'String','dP correctie :','Callback','',...
    'Enable','on','Visible','on');
all_handles.id(193) = tb4_db_corr_dP_txt;
tb4_db_corr_dP_txt_str = uicontrol('Parent',tab4,'Style','edit','Units','normalized',...
    'Position',[0.2 ty-0.25 0.77 0.04],'String','- (selecteer een file met dP correctie via de knop naast deze balk) -',...
    'HorizontalAlignment','left','Callback','','Enable','on','Visible','on');
all_handles.id(194) = tb4_db_corr_dP_txt_str;
tb4_db_corr_dP_txt_btn = uicontrol('Parent',tab4,'Units','normalized','Position',[0.975 ty-0.25 0.02 0.04],...
    'String','...','Callback',@tb4_db_corr_dP_txt_btn_callback,'Enable','on','Visible','on');
all_handles.id(195) = tb4_db_corr_dP_txt_btn;

% pip_cost
tb4_db_pipcost_txt = uicontrol('Parent',tab4,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.02 ty-0.30 0.5 0.035],'String','Piping kosten :','Callback','',...
    'Enable','on','Visible','on');
all_handles.id(196) = tb4_db_pipcost_txt;
tb4_db_pipcost_txt_str = uicontrol('Parent',tab4,'Style','edit','Units','normalized',...
    'Position',[0.2 ty-0.30 0.77 0.04],'String','- (selecteer een file met piping kosten via de knop naast deze balk) -',...
    'HorizontalAlignment','left','Callback','','Enable','on','Visible','on');
all_handles.id(197) = tb4_db_pipcost_txt_str;
tb4_db_pipcost_txt_btn = uicontrol('Parent',tab4,'Units','normalized','Position',[0.975 ty-0.30 0.02 0.04],...
    'String','...','Callback',@tb4_db_pipcost_txt_btn_callback,'Enable','on','Visible','on');
all_handles.id(198) = tb4_db_pipcost_txt_btn;

% opl_DV
tb4_opl_DV_txt = uicontrol('Parent',tab4,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.02 ty-0.35 0.5 0.035],'String','Opgelegde dijkversterking? :','Callback','','Enable','on','Visible','on');
all_handles.id(199) = tb4_opl_DV_txt;
tb4_opl_DV_opt_popup = uicontrol('Parent',tab4,'Style','popup','Units','normalized','Position',[0.2 ty-0.35+0.017 0.05 0.025],...
    'String',{'Ja','Nee'},'Callback',@tb4_opl_DV_opt_popup_callback,'Enable','on','Visible','on','Value',2);
all_handles.id(200) = tb4_opl_DV_opt_popup;

% db_DV_dH
tb4_db_DV_dH_txt = uicontrol('Parent',tab4,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.2 ty-0.40 0.5 0.035],'String','Faalmech. HT :','Callback','','Enable','off','Visible','on');
all_handles.id(201) = tb4_db_DV_dH_txt;
tb4_db_DV_dH_txt_str = uicontrol('Parent',tab4,'Style','edit','Units','normalized',...
    'Position',[0.3 ty-0.40 0.67 0.04],'String','- (selecteer een file met opgelegde dijkversterking voor HT via de knop naast deze balk) -',...
    'HorizontalAlignment','left','Callback','','Enable','off','Visible','on');
all_handles.id(202) = tb4_db_DV_dH_txt_str;
tb4_db_DV_dH_txt_btn = uicontrol('Parent',tab4,'Units','normalized','Position',[0.975 ty-0.40 0.02 0.04],...
    'String','...','Callback',@tb4_db_DV_dH_txt_btn_callback,'Enable','off','Visible','on');
all_handles.id(203) = tb4_db_DV_dH_txt_btn;

% db_DV_dS
tb4_db_DV_dS_txt = uicontrol('Parent',tab4,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.2 ty-0.45 0.5 0.035],'String','Faalmech. STBI :','Callback','',...
    'Enable','off','Visible','on');
all_handles.id(204) = tb4_db_DV_dS_txt;
tb4_db_DV_dS_txt_str = uicontrol('Parent',tab4,'Style','edit','Units','normalized',...
    'Position',[0.3 ty-0.45 0.67 0.04],'String','- (selecteer een file met opgelegde dijkversterking voor STBI via de knop naast deze balk) -',...
    'HorizontalAlignment','left','Callback','','Enable','off','Visible','on');
all_handles.id(205) = tb4_db_DV_dS_txt_str;
tb4_db_DV_dS_txt_btn = uicontrol('Parent',tab4,'Units','normalized','Position',[0.975 ty-0.45 0.02 0.04],...
    'String','...','Callback',@tb4_db_DV_dS_txt_btn_callback,'Enable','off','Visible','on');
all_handles.id(206) = tb4_db_DV_dS_txt_btn;

% db_DV_dP
tb4_db_DV_dP_txt = uicontrol('Parent',tab4,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.2 ty-0.5 0.5 0.035],'String','Faalmech. STPH :','Callback','',...
    'Enable','off','Visible','on');
all_handles.id(207) = tb4_db_DV_dP_txt;
tb4_db_DV_dP_txt_str = uicontrol('Parent',tab4,'Style','edit','Units','normalized',...
    'Position',[0.3 ty-0.5 0.67 0.04],'String','- (selecteer een file met opgelegde dijksversterking voor STPH via de knop naast deze balk) -',...
    'HorizontalAlignment','left','Callback','','Enable','off','Visible','on');
all_handles.id(208) = tb4_db_DV_dP_txt_str;
tb4_db_DV_dP_txt_btn = uicontrol('Parent',tab4,'Units','normalized','Position',[0.975 ty-0.5 0.02 0.04],...
    'String','...','Callback',@tb4_db_DV_dP_txt_btn_callback,'Enable','off','Visible','on');
all_handles.id(209) = tb4_db_DV_dP_txt_btn;

%% Extra invoer tablad 5 - (2 handles id, id 210 t/m 211) --> cum = 209 + 2 = 211
% Uitgebreide kostenberekening (wordt niet meer gebruikt, array 207 t/m 208 blijven leeg voor toekomstige gebruik):
% tt = 0.425-0.05;
% tb5_koex_txt = uicontrol('Parent',tab5,'Style','text','Units','normalized','HorizontalAlignment','left',...
%     'Position',[0.02 tt 0.4 0.04],'String','Uitgebreide kostenberekening uitvoeren :','Callback','',...
%     'Enable','on','Visible','on');
% all_handles.id(210) = tb5_koex_txt;
% tb5_koex_text_popup = uicontrol('Parent',tab5,'Style','popup','Units','normalized','Position',[0.35 tt+0.025 0.05 0.025],...
%     'String',{'Ja','Nee'},'Callback',@tb5_koex_text_popup_callback,'Enable','on','Visible','on');
% all_handles.id(211) = tb5_koex_text_popup;

%% Extra invoer tablad 1 - (5 handles id, id 212 t/m 216) --> cum = 211 + 5 = 216
% Maximale kwelschermlengte en stabiliteitswandlengte
tt = 0.30;
tb1_constlength_txt = uicontrol('Parent',tab1,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.325 tt 0.2 0.04],'String','Constructielengte gegevens :','Callback','','Enable','off','Visible','off');
all_handles.id(212) = tb1_constlength_txt;
tb1_constlength_txt_str = uicontrol('Parent',tab1,'Style','edit','Units','normalized',...
    'Position',[0.48 tt+0.0075 0.49 0.04],'String','- (selecteer een file met constructielengte gegevens via de knop naast deze balk) -',...
    'HorizontalAlignment','left','Callback','','Enable','on','Visible','off');
all_handles.id(213) = tb1_constlength_txt_str;
tb1_constlength_txt_btn = uicontrol('Parent',tab1,'Units','normalized','Position',[0.975 tt+0.0075 0.02 0.04],...
    'String','...','Callback',@tb1_constlength_txt_btn_callback,'Enable','off','Visible','off');
all_handles.id(214) = tb1_constlength_txt_btn;

% Optie voor hanmatig invoeren van constructielengte
tb1_constlength_opt_txt = uicontrol('Parent',tab1,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.02 tt 0.2 0.04],'String','Constructielengte invoeren? :','Callback','','Enable','on','Visible','on');
all_handles.id(215) = tb1_constlength_opt_txt;
tb1_constlength_opt_popup = uicontrol('Parent',tab1,'Style','popup','Units','normalized','Position',[0.2 tt+0.02 0.075 0.025],...
    'String',{'Ja','Nee'},'Callback',@tb1_constlength_opt_popup_callback,'Enable','on','Visible','on','Value',2);
all_handles.id(216) = tb1_constlength_opt_popup;

% Optie voor indexeren van prijspeil
tt = 0.15;
tb1_indexeren_opt_txt = uicontrol('Parent',tab1,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.02 tt 0.2 0.04],'String','Prijspeil indexeren? :','Callback','','Enable','on','Visible','on');
all_handles.id(217) = tb1_indexeren_opt_txt;
tb1_indexeren_opt_popup = uicontrol('Parent',tab1,'Style','popup','Units','normalized','Position',[0.2 tt+0.02 0.075 0.025],...
    'String',{'Ja','Nee'},'Callback',@tb1_indexeren_opt_popup_callback,'Enable','on','Visible','on','Value',2);
all_handles.id(218) = tb1_indexeren_opt_popup;

% Bestand indexeren prijspeil
tt = 0.15;
tb1_db_CBSindex_txt = uicontrol('Parent',tab1,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.325 tt 0.2 0.04],'String','Database prijsindex CBS :','Callback','','Enable','off','Visible','off');
all_handles.id(219) = tb1_db_CBSindex_txt;
tb1_db_CBSindex_txt_str = uicontrol('Parent',tab1,'Style','edit','Units','normalized',...
    'Position',[0.48 tt+0.0075 0.49 0.04],'String','- (selecteer een file met de CBS prijsindex gegevens via de knop naast deze balk) -',...
    'HorizontalAlignment','left','Callback','','Enable','off','Visible','off');
all_handles.id(220) = tb1_db_CBSindex_txt_str;
tb1_db_CBSindex_txt_btn = uicontrol('Parent',tab1,'Units','normalized','Position',[0.975 tt+0.0075 0.02 0.04],...
    'String','...','Callback',@tb1_db_CBSindex_txt_btn_callback,'Enable','off','Visible','off');
all_handles.id(221) = tb1_db_CBSindex_txt_btn;

% Optie voor indexeren van prijspeil
tt = 0.10;
tb1_prijspeil_opt_txt = uicontrol('Parent',tab1,'Style','text','Units','normalized','HorizontalAlignment','left',...
    'Position',[0.02 tt 0.2 0.04],'String','Kies prijspeil :','Callback','','Enable','off','Visible','off');
all_handles.id(217) = tb1_prijspeil_opt_txt;
tb1_prijspeil_opt_popup = uicontrol('Parent',tab1,'Style','popup','Units','normalized','Position',[0.2 tt+0.02 0.075 0.025],...
    'String',{'2013'},'Callback','','Enable','off','Visible','off');
all_handles.id(218) = tb1_prijspeil_opt_popup;

%% Visibiliteit van hoofdhandle en andere handles
%set(d,'Visible','off');
%set(tab_all,'Visible','off');
% for i = 1:length([all_handles.id])
%     try
%         set(all_handles.id(i),'Visible','on');
%         set(all_handles.id(i),'Enable','on');
%     catch
%         try
%             set(all_handles.id(i),'Visible','on');
%         catch
%         end
%     end
% end
%set(d,'Visible','on');
%uiwait(d,1); % om lag te voorkomen
%set(tab_all,'Visible','on');
% Wait for d to close before running to completion
uiwait(d);

%% Button functies
    function btn1_callback(btn1,callbackdata)
        [fn1,wd1] = uigetfile('*.txt','Selecteer de invoerfile',wd_global);
        if ischar(fn1) == 1
            wd_global = wd1;
        end
        if ~isnumeric(fn1)
            set(lst1,'String',[wd1 fn1]);
            try
                invoer = invoer_inlezen(fn1,wd1);
                % Optie om de link naar databases te veranderen
                cc = questdlg('        Link naar data aanpassen?',...
                    '- Link naar data -','Ja','Nee (sluiten = Nee)','Ja');
                if ~isempty(cc) && strcmp(cc,'Ja')
                    invoer_laden(invoer);
                    strs = {'Voer hier de oude link in (eindigen met ''\''):','Voer hier de nieuwe link in (eindigen met ''\''):'};
                    dtitle = 'Voer de nieuwe link in';
                    ans1 = {[invoer.prog_dir],''};
                    ans2 = inputdlg(strs,dtitle,[1 100; 1 100],ans1);
                    if ~isempty(ans2)
                        set(inv_stat,'ForegroundColor','b');
                        set(inv_stat,'String','>>> bezig met aanpassen van data link ...');
                        invoer_aanpassen_databases(ans2{1},ans2{2});
                    end
                    set(inv_stat,'String',...
                        ['De link naar databases map in invoerfile ' fn1 ' is geupdatet.']);
                    set(inv_stat1,'String','Druk op "Controle" om de nieuwe invoer te controleren. Vergeet niet om de invoerfile op te slaan');
                    set(inv_stat,'ForegroundColor','k');
                    set(inv_stat1,'ForegroundColor','k');
                    set(btn_start,'Enable','on');
                    set(btn_che,'Enable','on');
                    set(btn_ops,'Enable','on');
                else
                    set(inv_stat,'ForegroundColor','b');
                    set(inv_stat,'String','>>> bezig met inlezen en controleren van gekozen invoerfile ...');
                    [fa,fb,fc,fd,fe,ff] = invoer_controleren(invoer,fn1,wd1);
                    invoer_laden(invoer);
                    set(lst1,'String',[wd1 fn1]);
                    set(inv_stat,'String',...
                        ['invoerfile ' fn1 ' is ingelezen. Controleer check_' strrep(fn1,'.txt','.ctr') ' voor de mogelijke fout(en).']);
                    set(inv_stat,'ForegroundColor','k')
                    set(inv_stat1,'String',...
                        ['>>> Aantal invoerfouten gevonden: tab 1 (' num2str(fa,'%.0f') '), tab 2 (' num2str(fb,'%.0f') ...
                        '), tab 3 (' num2str(fc,'%.0f') '), tab 4 (' num2str(fd,'%.0f') '), tab 5 (' num2str(fe,'%.0f') '), en tab 6 (' num2str(ff,'%.0f') ')']);
                    if fa+fb+fc+fd+fe+ff > 0
                        set(inv_stat1,'ForegroundColor','r');
                        set(btn_start,'Enable','off');
                        set(btn_che,'Enable','on');
                        set(btn_ops,'Enable','on');
                    else
                        set(inv_stat1,'ForegroundColor','k');
                        set(btn_start,'Enable','on');
                        set(btn_che,'Enable','on');
                        set(btn_ops,'Enable','on');
                    end
                end
            catch
                try
                    set(lst1,'String','Invoerfile is niet ingelezen');
                    [~,~,~,~,~,~] = invoer_controleren(invoer,fn1,wd1);
                    set(inv_stat,'String',...
                        ['invoerfile ' fn1 ' kan niet worden ingelezen. Controleer check_' strrep(fn1,'.txt','.ctr') ' voor de mogelijke fout(en).']);
                    set(inv_stat1,'String','');
                    set(inv_stat,'ForegroundColor','r');
                    set(inv_stat1,'ForegroundColor','r');
                    set(btn_start,'Enable','off');
                    set(btn_che,'Enable','on');
                    set(btn_ops,'Enable','on');
                catch
                    set(inv_stat,'String',...
                        ['invoerfile ' fn1 ' kan niet worden ingelezen. De controle file kan niet worden gemaakt.']);
                    set(inv_stat1,'String','Mogelijk ontbreekt een groot aantal invoergegevens in de invoerfile.  Neem contact met HKV of Deltares op.');
                    set(inv_stat,'ForegroundColor','r');
                    set(inv_stat1,'ForegroundColor','r');
                    set(btn_start,'Enable','off');
                    set(btn_che,'Enable','on');
                    set(btn_ops,'Enable','on');
                end
            end
        else
            set(lst1,'String','- niet geselecteerd -');
            set(inv_stat,'String','invoerfile is niet geselecteerd.');
            set(inv_stat1,'String','');
            set(inv_stat,'ForegroundColor','r');
            set(btn_start,'Enable','off');
            set(btn_che,'Enable','on');
            set(btn_ops,'Enable','on');
        end
    end  
    function tb2_htdata_txt_btn_callback(tb2_htdata_txt_btn,callbackdata)
        [fn6,werk_dir6] = uigetfile('*.csv','Selecteer de fragility curve file voor faalmechanisme HT',wd_global);
        if ischar(fn6) == 1
            wd_global = werk_dir6;
        end
        if ~isnumeric(fn6)
            set(tb2_htdata_txt_str,'String',[werk_dir6 fn6]);
        else
            set(tb2_htdata_txt_str,'String','- niet geselecteerd -');
        end
    end
    function tb2_stbidata_txt_btn_callback(tb2_stbidata_txt_btn,callbackdata)
        [fn7,werk_dir7] = uigetfile('*.csv','Selecteer de fragility curve file voor faalmechanisme STBI',wd_global);
        if ischar(fn7) == 1
            wd_global = werk_dir7;
        end
        if ~isnumeric(fn7)
            set(tb2_stbidata_txt_str,'String',[werk_dir7 fn7]);
        else
            set(tb2_stbidata_txt_str,'String','- niet geselecteerd -');
        end
    end
    function tb2_stphdata_txt_btn_callback(tb2_stphdata_txt_btn,callbackdata)
        [fn8,werk_dir8] = uigetfile('*.csv','Selecteer de fragility curve file voor faalmechanisme STPH',wd_global);
        if ischar(fn8) == 1
            wd_global = werk_dir8;
        end
        if ~isnumeric(fn8)
            set(tb2_stphdata_txt_str,'String',[werk_dir8 fn8]);
        else
            set(tb2_stphdata_txt_str,'String','- niet geselecteerd -');
        end
    end
    function tb1_dijkvak_txt_btn_callback(tb1_dijkvak_txt_btn,callbackdata)
        [fn9,werk_dir9] = uigetfile({'*.txt';'*.csv'},'Selecteer de dijkvakken',wd_global);
        if ischar(fn9) == 1
            wd_global = werk_dir9;
        end
        if ~isnumeric(fn9)
            set(tb1_dijkvak_txt_str,'String',[werk_dir9 fn9]);
        else
            set(tb1_dijkvak_txt_str,'String','- niet geselecteerd -');
        end
    end
    function tb1_dijkvaklen_txt_btn_callback(tb1_dijkvaklen_txt_btn,callbackdata)
        [fn10,werk_dir10] = uigetfile('*.csv','Selecteer de lengten van dijkvakken',wd_global);
        if ischar(fn10) == 1
            wd_global = werk_dir10;
        end
        if ~isnumeric(fn10)
            set(tb1_dijkvaklen_txt_str,'String',[werk_dir10 fn10]);
        else
            set(tb1_dijkvaklen_txt_str,'String','- niet geselecteerd -');
        end
    end
    function tb1_koswat_txt_btn_callback(tb1_koswat_txt_btn,callbackdata)
        [fn12,werk_dir12] = uigetfile('*.xlsx','Selecteer de KOSWAT file',wd_global);
        if ischar(fn12) == 1
            wd_global = werk_dir12;
        end
        if ~isnumeric(fn12)
            set(tb1_koswat_txt_str,'String',[werk_dir12 fn12]);
        else
            set(tb1_koswat_txt_str,'String','- niet geselecteerd -');
        end
    end
    function tb1_constlength_txt_btn_callback(tb1_constlength_txt_btn, callbackdata)
        [fn12a,werk_dir12a] = uigetfile('*.csv','Selecteer de constructielengte file',wd_global);
        if ischar(fn12a) == 1
            wd_global = werk_dir12a;
        end
        if ~isnumeric(fn12a)
            set(tb1_constlength_txt_str,'String',[werk_dir12a fn12a]);
        else
            set(tb1_constlength_txt_str,'String','- niet geselecteerd -');
        end
    end
    function tb1_db_CBSindex_txt_btn_callback(tb1_db_CBSindex_txt_btn, callbackdata)
        [fn12b,werk_dir12b] = uigetfile('*.csv','Selecteer de CBS index file',wd_global);
        if ischar(fn12b) == 1
            wd_global = werk_dir12b;
        end
        if ~isnumeric(fn12b)
            set(tb1_db_CBSindex_txt_str,'String',[werk_dir12b fn12b]);
            CBSindex    = dlmread([werk_dir12b fn12b],';',1,0);
            if find(CBSindex(:,1)==2013) > 1
                set(tb1_prijspeil_opt_txt,'Enable','on');
                set(tb1_prijspeil_opt_popup,'Enable','on','String',num2cell(CBSindex(:,1)),'Value',1);
            else
                set(tb1_db_CBSindex_txt_str,'String','- onjuist bestand geselecteerd -');
                set(tb1_prijspeil_opt_txt,'Enable','off');
                set(tb1_prijspeil_opt_popup,'Enable','off','String',{2013},'Value',1);
            end
        else
            set(tb1_db_CBSindex_txt_str,'String','- niet geselecteerd -');
            set(tb1_prijspeil_opt_txt,'Enable','off');
            set(tb1_prijspeil_opt_popup,'Enable','off','String',{'2013'},'Value',1);
        end
    end
    function tb3_onz_txt_btn_callback(tb3_onz_txt_btn,callbackdata)
        [fn15,werk_dir15] = uigetfile('*.csv','Selecteer de file met onzekerheidstoeslag',wd_global);
        if ischar(fn15) == 1
            wd_global = werk_dir15;
        end
        if ~isnumeric(fn15)
            set(tb3_onz_txt_str,'String',[werk_dir15 fn15]);
        else
            set(tb3_onz_txt_str,'String','- niet geselecteerd -');
        end
    end
    function tb3_bodm_txt_btn_callback(tb3_bodm_txt_btn,callbackdata)
        [fn16,werk_dir16] = uigetfile('*.csv','Selecteer de file met bodemdaling gegevens',wd_global);
        if ischar(fn16) == 1
            wd_global = werk_dir16;
        end
        if ~isnumeric(fn16)
            set(tb3_bodm_txt_str,'String',[werk_dir16 fn16]);
        else
            set(tb3_bodm_txt_str,'String','- niet geselecteerd -');
        end
    end
    % Op verzoek van HKv om te verwijderen d.d. 11 juli 2018?
    %function tb4_maxpip_txt_btn_callback(tb4_maxpip_txt_btn,callbackdata)
    %   [fn23,werk_dir23] = uigetfile('*.csv','Selecteer de file met maximale pipingberm gegevens',wd_global);
    %   if ischar(fn23) == 1
    %       wd_global = werk_dir23;
    %   end
    %   if ~isnumeric(fn23)
    %       set(tb4_maxpip_txt_str,'String',[werk_dir23 fn23]);
    %   else
    %       set(tb4_maxpip_txt_str,'String','- niet geselecteerd -');
    %   end
    %end
    function tb4_norm_txt_btn_callback(tb4_norm_txt_btn,callbackdata)
        [fn23a,werk_dir23a] = uigetfile('*.csv','Selecteer de file met norm pipingberm gegevens',wd_global);
        if ischar(fn23a) == 1
            wd_global = werk_dir23a;
        end
        if ~isnumeric(fn23a)
            set(tb4_norm_txt_str,'String',[werk_dir23a fn23a]);
        else
            set(tb4_norm_txt_str,'String','- niet geselecteerd -');
        end
    end
    function tb6_uitvoermap_txt_btn_callback(tb6_uitvoermap_txt_btn,callbackdata)
        mm = uigetdir(wd_global,'Kies de link naar uitvoermap');
        if ~isnumeric(mm)
            set(tb6_uitvoermap_txt_str,'String',mm);
        else
            set(tb6_uitvoermap_txt_str,'String','link naar uitvoermap is niet geselecteerd');
        end
    end
    function tb4_ont_txt_btn_callback(tb4_ont_txt_btn,callbackdata)
        [fn16a,werk_dir16a] = uigetfile('*.csv','Selecteer de file met ontwerpduur gegevens',wd_global);
        if ischar(fn16a) == 1
            wd_global = werk_dir16a;
        end
        if ~isnumeric(fn16a)
            set(tb4_ont_txt_str,'String',[werk_dir16a fn16a]);
        else
            set(tb4_ont_txt_str,'String','- niet geselecteerd -');
        end
    end
    function tb4_hwbp_txt_btn_callback(tb4_hwbp_txt_btn,callbackdata)
        [fn16b,werk_dir16b] = uigetfile('*.csv','Selecteer de file met forcering dijkversterking gegevens',wd_global);
        if ischar(fn16b) == 1
            wd_global = werk_dir16b;
        end
        if ~isnumeric(fn16b)
            set(tb4_hwbp_txt_str,'String',[werk_dir16b fn16b]);
        else
            set(tb4_hwbp_txt_str,'String','- niet geselecteerd -');
        end
    end
    function tb6_exportdbffile_txt_btn_callback(tb6_exportdbffile_txt_btn,callbackdata)
        [fn16c,werk_dir16c] = uigetfile('*.dbf','Selecteer de bestaande .dbf file',wd_global);
        if ischar(fn16c) == 1
            wd_global = werk_dir16c;
        end
        if ~isnumeric(fn16c)
            set(tb6_exportdbffile_txt_str,'String',[werk_dir16c fn16c]);
        else
            set(tb6_exportdbffile_txt_str,'String','- niet geselecteerd -');
        end
    end  
    function tb4_db_corr_dH_txt_btn_callback(tb4_db_corr_dH_txt_btn,callbackdata)
        [fn_ext_t4_1,werk_dir_ext_t4_1] = uigetfile('*.csv','Selecteer de file met dH correctie gegevens',wd_global);
        if ischar(fn_ext_t4_1) == 1
            wd_global = werk_dir_ext_t4_1;
        end
        if ~isnumeric(fn_ext_t4_1)
            set(tb4_db_corr_dH_txt_str,'String',[werk_dir_ext_t4_1 fn_ext_t4_1]);
        else
            set(tb4_db_corr_dH_txt_str,'String','- niet geselecteerd -');
        end
    end
    function tb4_db_corr_dS_txt_btn_callback(tb4_db_corr_dS_txt_btn,callbackdata)
        [fn_ext_t4_2,werk_dir_ext_t4_2] = uigetfile('*.csv','Selecteer de file met dS correctie gegevens',wd_global);
        if ischar(fn_ext_t4_2) == 1
            wd_global = werk_dir_ext_t4_2;
        end
        if ~isnumeric(fn_ext_t4_2)
            set(tb4_db_corr_dS_txt_str,'String',[werk_dir_ext_t4_2 fn_ext_t4_2]);
        else
            set(tb4_db_corr_dS_txt_str,'String','- niet geselecteerd -');
        end
    end
    function tb4_db_corr_dP_txt_btn_callback(tb4_db_corr_dP_txt_btn,callbackdata)
        [fn_ext_t4_3,werk_dir_ext_t4_3] = uigetfile('*.csv','Selecteer de file met dP correctie gegevens',wd_global);
        if ischar(fn_ext_t4_3) == 1
            wd_global = werk_dir_ext_t4_3;
        end
        if ~isnumeric(fn_ext_t4_3)
            set(tb4_db_corr_dP_txt_str,'String',[werk_dir_ext_t4_3 fn_ext_t4_3]);
        else
            set(tb4_db_corr_dP_txt_str,'String','- niet geselecteerd -');
        end
    end
    function tb4_db_pipcost_txt_btn_callback(tb4_db_pipcost_txt_btn,callbackdata)
        [fn_ext_t4_4,werk_dir_ext_t4_4] = uigetfile('*.csv','Selecteer de file met piping kosten gegevens',wd_global);
        if ischar(fn_ext_t4_4) == 1
            wd_global = werk_dir_ext_t4_4;
        end
        if ~isnumeric(fn_ext_t4_4)
            set(tb4_db_pipcost_txt_str,'String',[werk_dir_ext_t4_4 fn_ext_t4_4]);
        else
            set(tb4_db_pipcost_txt_str,'String','- niet geselecteerd -');
        end
    end
    function tb4_db_DV_dH_txt_btn_callback(tb4_db_DV_dH_txt_btn,callbackdata)
        [fn_ext_t4_5,werk_dir_ext_t4_5] = uigetfile('*.csv','Selecteer de file met opgelegde dijkversterking gegevens voor HT',wd_global);
        if ischar(fn_ext_t4_5) == 1
            wd_global = werk_dir_ext_t4_5;
        end
        if ~isnumeric(fn_ext_t4_5)
            set(tb4_db_DV_dH_txt_str,'String',[werk_dir_ext_t4_5 fn_ext_t4_5]);
        else
            set(tb4_db_DV_dH_txt_str,'String','- niet geselecteerd -');
        end
    end
    function tb4_db_DV_dS_txt_btn_callback(tb4_db_DV_dS_txt_btn,callbackdata)
        [fn_ext_t4_6,werk_dir_ext_t4_6] = uigetfile('*.csv','Selecteer de file met opgelegde dijkversterking gegevens voor STBI',wd_global);
        if ischar(fn_ext_t4_6) == 1
            wd_global = werk_dir_ext_t4_6;
        end
        if ~isnumeric(fn_ext_t4_6)
            set(tb4_db_DV_dS_txt_str,'String',[werk_dir_ext_t4_6 fn_ext_t4_6]);
        else
            set(tb4_db_DV_dS_txt_str,'String','- niet geselecteerd -');
        end
    end
    function tb4_db_DV_dP_txt_btn_callback(tb4_db_DV_dP_txt_btn,callbackdata)
        [fn_ext_t4_7,werk_dir_ext_t4_7] = uigetfile('*.csv','Selecteer de file met opgelegde dijkversterking gegevens voor STPH',wd_global);
        if ischar(fn_ext_t4_7) == 1
            wd_global = werk_dir_ext_t4_7;
        end
        if ~isnumeric(fn_ext_t4_7)
            set(tb4_db_DV_dP_txt_str,'String',[werk_dir_ext_t4_7 fn_ext_t4_7]);
        else
            set(tb4_db_DV_dP_txt_str,'String','- niet geselecteerd -');
        end
    end
    function btn_start_callback(btn_start,callbackdata)
        invoer = invoerdata_verzamelen(nQT_max,nQH_max,nnorm_max,nverst_max);
        fn_sta = invoer.fn;
        werk_dir_sta = invoer.prog_dir;
        invoerfile_aanmaken(fn_sta,werk_dir_sta,invoer);
        hcm = questdlg(['Invoerfile ' fn_sta ' wordt overgeschreven en de berekening wordt gestart. Is dit ok?'], ...
            'Let op!','Ja','Nee','Ja');
        if ~isempty(hcm)
        if strcmp('Ja',hcm)
            set(inv_stat,'ForegroundColor','b');
            set(inv_stat,'String','>>> bezig met inlezen en controleren van gekozen invoerfile ...');
            [fa,fb,fc,fd,fe,ff] = invoer_controleren(invoer,fn_sta,werk_dir_sta);
            if fa+fb+fc+fd+fe+ff == 0
                if get(tb2_ht_txt_popup,'Value') == 2 && get(tb2_stbi_txt_popup,'Value') == 2 && get(tb2_stph_txt_popup,'Value') == 2
                    set(inv_stat,'String','De berekening kan niet doorgaan. Minimaal moet 1 faalmechanisme worden meegenomen.');
                    set(inv_stat1,'String','');
                    set(inv_stat,'ForegroundColor','r');
                    set(inv_stat1,'ForegroundColor','r');
                    set(btn_start,'Enable','off');
                else
                    delete(gcf);
                end
            else
                set(inv_stat,'String',['De berekening kan niet doorgaan. Controleer de fouten in check_' strrep(fn_sta,'.txt','.ctr')]);
                set(inv_stat1,'String',...
                    ['>>> Aantal invoerfouten gevonden: tab 1 (' num2str(fa,'%.0f') '), tab 2 (' num2str(fb,'%.0f') ...
                    '), tab 3 (' num2str(fc,'%.0f') '), tab 4 (' num2str(fd,'%.0f') '), tab 5 (' num2str(fe,'%.0f') '), en tab 6 (' num2str(ff,'%.0f') ')']);
                set(inv_stat,'ForegroundColor','r');
                set(inv_stat1,'ForegroundColor','r');
                set(btn_start,'Enable','off');
            end
        end
        end
    end    
    function btn_ops_callback(btn_ops,callbackdata)
        invoer = invoerdata_verzamelen(nQT_max,nQH_max,nnorm_max,nverst_max);
        [fn_ops,werk_dir_ops] = uiputfile('*.txt','Huidige invoerfile opslaan',wd_global);
        if ischar(fn_ops) == 1
            wd_global = werk_dir_ops;
        end
        if ~isnumeric(fn_ops)
            invoerfile_aanmaken(fn_ops,werk_dir_ops,invoer);
            set(inv_stat,'ForegroundColor','b');
            set(inv_stat,'String','>>> bezig met inlezen en controleren van gekozen invoerfile ...');
            [fa,fb,fc,fd,fe,ff] = invoer_controleren(invoer,fn_ops,werk_dir_ops);
            set(lst1,'String',[werk_dir_ops fn_ops]);
            set(inv_stat,'String',['invoerfile ' fn_ops ' is opgeslagen. Controle file "check_' strrep(fn_ops,'.txt','.ctr') '" is gemaakt.']);
            set(inv_stat1,'String',...
                ['>>> Aantal invoerfouten gevonden: tab 1 (' num2str(fa,'%.0f') '), tab 2 (' num2str(fb,'%.0f') ...
                '), tab 3 (' num2str(fc,'%.0f') '), tab 4 (' num2str(fd,'%.0f') '), tab 5 (' num2str(fe,'%.0f') '), en tab 6 (' num2str(ff,'%.0f') ')']);
            if fa+fb+fc+fd+fe+ff > 0
                set(inv_stat1,'ForegroundColor','r');
                set(btn_start,'Enable','off');
            else
                set(inv_stat1,'ForegroundColor','k');
                set(btn_start,'Enable','on');
            end
        end
    end
    function btn_che_callback(btn_che,callbackdata)
        invoer = invoerdata_verzamelen(nQT_max,nQH_max,nnorm_max,nverst_max);
        [fn_che,werk_dir_che] = uiputfile('*.ctr','File ter controle opslaan',wd_global);
        if ischar(fn_che) == 1
            wd_global = werk_dir_che;
        end
        if ~isnumeric(fn_che)
            set(inv_stat,'ForegroundColor','b');
            set(inv_stat,'String','>>> bezig met inlezen en controleren van gekozen invoerfile ...');
            [fa,fb,fc,fd,fe,ff] = invoer_controleren(invoer,fn_che,werk_dir_che);
            set(inv_stat,'String',['controle file ' fn_che ' is gemaakt. Controleer de mogelijke fouten. Vergeet niet de invoerfile op te slaan.']);
            set(inv_stat1,'String',...
                ['>>> Aantal invoerfouten gevonden: tab 1 (' num2str(fa,'%.0f') '), tab 2 (' num2str(fb,'%.0f') ...
                '), tab 3 (' num2str(fc,'%.0f') '), tab 4 (' num2str(fd,'%.0f') '), tab 5 (' num2str(fe,'%.0f') '), en tab 6 (' num2str(ff,'%.0f') ')']);
            if fa+fb+fc+fd+fe+ff > 0
                set(inv_stat1,'ForegroundColor','r');
                set(btn_start,'Enable','off');
            else
                set(inv_stat1,'ForegroundColor','k');
                set(btn_start,'Enable','on');
            end
        end
    end

%% Buttongroup functie
    function del_func(d,callbackdata)
        delete(gcf);
    end
    function bg20_callback(bg20,callbackdata)
        ar = callbackdata.NewValue.String;
        if strcmp(ar,'Meerdere scenario''s')
            if callbackdata.NewValue.Value == 1
                a = 2;
            else
                a = 1;
            end
        elseif strcmp(ar,'Enkel scenario')
            if callbackdata.NewValue.Value == 1
                a = 1;
            else
                a = 2;
            end
        end
        if a == 1
            nnorm_max = 1;
            nverst_max = 1;
        else
            nnorm_max = 10;
            nverst_max = 10;
        end
        d24a = cell(nnorm_max,3);
        for at = 1:nnorm_max
            d24a{at,1} = at;
        end
        d24a(1,:) = d24(1,:);
        d24 = d24a;
        set(ta24,'Data',d24);
        d25a = cell(nverst_max,4);
        for at = 1:nverst_max
            d25a{at,1} = at;
        end
        d25a(1,:) = d25(1,:);
        d25 = d25a;
        set(ta25,'Data',d25);
    end

%% Tabel functies
    function ta18_select_callback(ta18,callbackdata)
        try
            a = callbackdata.Indices(1);
            b = callbackdata.Indices(2);
            %c = [a b]
            if b > 2
                if isempty(d18{a,b})
                    [fn18,werk_dir18] = uigetfile('*.csv',['Selecteer de QT file ' num2str(b-2) ' voor scenario id ' num2str(a)],wd_global);
                    if ischar(fn18) == 1
                        wd_global = werk_dir18;
                    end
                    if ~isnumeric(fn18)
                        d18{a,b} = [werk_dir18 fn18];
                    else
                        d18{a,b} = '';
                    end
                end
            end
            set(ta18,'Data',d18);
        catch
            set(ta18,'Data',d18);
        end
    end
    function ta18_edit_callback(ta18,callbackdata)
        a = callbackdata.Indices(1);
        b = callbackdata.Indices(2);
        if b > 2
            if isempty(d18{a,b})
                [fn18,werk_dir18] = uigetfile('*.csv',['Selecteer de QT file ' num2str(b-2) ' voor scenario id ' num2str(a)],wd_global);
                if ischar(fn18) == 1
                    wd_global = werk_dir18;
                end
                if ~isnumeric(fn18)
                    d18{a,b} = [werk_dir18 fn18];
                else
                    d18{a,b} = '';
                end
            end
            d18{a,b} = callbackdata.NewData;
        else
            if ~isnan(callbackdata.NewData) && callbackdata.NewData >= 0
                d18{a,b} = callbackdata.NewData;
            else
                d18{a,b} = [];
            end
        end
        set(ta18,'Data',d18);
    end
    function ta19_select_callback(ta19,callbackdata)
        try
            a = callbackdata.Indices(1);
            b = callbackdata.Indices(2);
            %c = [a b]
            if b > 2
                if isempty(d19{a,b})
                    [fn19,werk_dir19] = uigetfile('*.csv',['Selecteer de QT file ' num2str(b-2) ' voor scenario id ' num2str(a)],wd_global);
                    if ischar(fn19) == 1
                        wd_global = werk_dir19;
                    end
                    if ~isnumeric(fn19)
                        d19{a,b} = [werk_dir19 fn19];
                    else
                        d19{a,b} = '';
                    end
                end
            end
            set(ta19,'Data',d19);
        catch
            set(ta19,'Data',d19);
        end
    end
    function ta19_edit_callback(ta19,callbackdata)
        a = callbackdata.Indices(1);
        b = callbackdata.Indices(2);
        if b > 2
            if isempty(d19{a,b})
                [fn19,werk_dir19] = uigetfile('*.csv',['Selecteer de QT file ' num2str(b-2) ' voor scenario id ' num2str(a)],wd_global);
                if ischar(fn19) == 1
                    wd_global = werk_dir19;
                end
                if ~isnumeric(fn19)
                    d19{a,b} = [werk_dir19 fn19];
                else
                    d19{a,b} = '';
                end
            end
            d19{a,b} = callbackdata.NewData;
        else
            if ~isnan(callbackdata.NewData) && callbackdata.NewData >= 0
                d19{a,b} = callbackdata.NewData;
            else
                d19{a,b} = [];
            end
        end
        set(ta19,'Data',d19);
    end
    function ta24_select_callback(ta24,callbackdata)
        try
            a = callbackdata.Indices(1);
            b = callbackdata.Indices(2);
            %c = [a b]
            if b > 2
                if isempty(d24{a,b})
                    [fn24,werk_dir24] = uigetfile('*.csv',['Selecteer de norm file voor scenario id ' num2str(a)],wd_global);
                    if ischar(fn24) == 1
                        wd_global = werk_dir24;
                    end
                    if ~isnumeric(fn24)
                        d24{a,b} = [werk_dir24 fn24];
                    else
                        d24{a,b} = '';
                    end
                end
            end
            set(ta24,'Data',d24);
        catch
            set(ta24,'Data',d24);
        end
    end
    function ta24_edit_callback(ta24,callbackdata)
        a = callbackdata.Indices(1);
        b = callbackdata.Indices(2);
        if b > 2
            if isempty(d24{a,b})
                [fn24,werk_dir24] = uigetfile('*.csv',['Selecteer de norm file voor scenario id ' num2str(a)],wd_global);
                if ischar(fn24) == 1
                    wd_global = werk_dir24;
                end
                if ~isnumeric(fn24)
                    d24{a,b} = [werk_dir24 fn24];
                else
                    d24{a,b} = '';
                end
            end
            d24{a,b} = callbackdata.NewData;
        else
            if ~isnan(callbackdata.NewData) && callbackdata.NewData >= 0
                d24{a,b} = callbackdata.NewData;
            else
                d24{a,b} = [];
            end
        end
        set(ta24,'Data',d24);
    end
    function ta25_select_callback(ta25,callbackdata)
        set(ta25,'Data',d25);
    end
    function ta25_edit_callback(ta25,callbackdata)
        a = callbackdata.Indices(1);
        b = callbackdata.Indices(2);
        d25{a,b} = callbackdata.NewData;
        set(ta25,'Data',d25);
    end

%% Popup functies
    function tb1_ana_popup_callback(popup2,callbackdata)
        %voorlopig niets
    end
    function tb1_constlength_opt_popup_callback(tb1_constlength_opt_popup,callbackdata)
        if get(tb1_constlength_opt_popup,'Value') == 1
            set(tb1_constlength_txt,'Enable','on'); set(tb1_constlength_txt,'Visible','on');
            set(tb1_constlength_txt_str,'Enable','on'); set(tb1_constlength_txt_str,'Visible','on');
            set(tb1_constlength_txt_btn,'Enable','on'); set(tb1_constlength_txt_btn,'Visible','on');
        else
            set(tb1_constlength_txt,'Enable','off'); set(tb1_constlength_txt,'Visible','off');
            set(tb1_constlength_txt_str,'Enable','off'); set(tb1_constlength_txt_str,'Visible','off');
            set(tb1_constlength_txt_btn,'Enable','off'); set(tb1_constlength_txt_btn,'Visible','off');
        end
    end
    function tb1_indexeren_opt_popup_callback(tb1_indexeren_opt_popup,callbackdata)
        if get(tb1_indexeren_opt_popup,'Value') == 1
            set(tb1_db_CBSindex_txt,'Enable','on'); set(tb1_db_CBSindex_txt,'Visible','on');
            set(tb1_db_CBSindex_txt_str,'Enable','on'); set(tb1_db_CBSindex_txt_str,'Visible','on');
            set(tb1_db_CBSindex_txt_btn,'Enable','on'); set(tb1_db_CBSindex_txt_btn,'Visible','on');
            set(tb1_prijspeil_opt_txt,'Visible','on');
            set(tb1_prijspeil_opt_popup,'Visible','on');
        else
            set(tb1_db_CBSindex_txt,'Enable','off'); set(tb1_db_CBSindex_txt,'Visible','off');
            set(tb1_db_CBSindex_txt_str,'Enable','off'); set(tb1_db_CBSindex_txt_str,'Visible','off');
            set(tb1_db_CBSindex_txt_btn,'Enable','off'); set(tb1_db_CBSindex_txt_btn,'Visible','off');
            set(tb1_prijspeil_opt_txt,'Visible','off');
            set(tb1_prijspeil_opt_popup,'Visible','off');
        end
    end
    function tb2_ht_txt_popup_callback(tb2_ht_txt_popup,callbackdata)
        if get(tb2_ht_txt_popup,'Value') == 2
            set(tb2_htdata_txt,'Enable','off');
            set(tb2_htdata_txt_str,'Enable','off');
            set(tb2_htdata_txt_btn,'Enable','off');
        else
            set(tb2_htdata_txt,'Enable','on');
            set(tb2_htdata_txt_str,'Enable','on');
            set(tb2_htdata_txt_btn,'Enable','on');
        end
    end
    function tb2_stbi_txt_popup_callback(tb2_stbi_txt_popup,callbackdata)
        if get(tb2_stbi_txt_popup,'Value') == 2
            set(tb2_stbidata_txt,'Enable','off');
            set(tb2_stbidata_txt_str,'Enable','off');
            set(tb2_stbidata_txt_btn,'Enable','off');
        else
            set(tb2_stbidata_txt,'Enable','on');
            set(tb2_stbidata_txt_str,'Enable','on');
            set(tb2_stbidata_txt_btn,'Enable','on');
        end
    end
    function tb2_stph_txt_popup_callback(tb2_stph_txt_popup,callbackdata)
        if get(tb2_stph_txt_popup,'Value') == 2
            set(tb2_stphdata_txt,'Enable','off');
            set(tb2_stphdata_txt_str,'Enable','off');
            set(tb2_stphdata_txt_btn,'Enable','off');
        else
            set(tb2_stphdata_txt,'Enable','on');
            set(tb2_stphdata_txt_str,'Enable','on');
            set(tb2_stphdata_txt_btn,'Enable','on');
        end
    end
    function tb2_afkapht_popup_callback(tb2_afkapht_popup,callbackdata)
        v = get(tb2_afkapht_popup,'Value');
        if v == 2
            set(tb2_freqht_txt,'Visible','on'); set(tb2_freqht_txt,'Enable','on');
            set(tb2_freqht_txt_str,'Visible','on'); set(tb2_freqht_txt_str,'Enable','on');
            set(tb2_freqht_txt_unit,'Visible','on'); set(tb2_freqht_txt_unit,'Enable','on');
        else
            set(tb2_freqht_txt,'Visible','off'); set(tb2_freqht_txt,'Enable','off');
            set(tb2_freqht_txt_str,'Visible','off'); set(tb2_freqht_txt_str,'Enable','off');
            set(tb2_freqht_txt_unit,'Visible','off'); set(tb2_freqht_txt_unit,'Enable','off');
        end
    end
    function tb2_afkapstbi_popup_callback(tb2_afkapstbi_popup,callbackdata)
        v = get(tb2_afkapstbi_popup,'Value');
        if v == 2
            set(tb2_freqstbi_txt,'Visible','on'); set(tb2_freqstbi_txt,'Enable','on');
            set(tb2_freqstbi_txt_str,'Visible','on'); set(tb2_freqstbi_txt_str,'Enable','on');
            set(tb2_freqstbi_txt_unit,'Visible','on'); set(tb2_freqstbi_txt_unit,'Enable','on');
        else
            set(tb2_freqstbi_txt,'Visible','off'); set(tb2_freqstbi_txt,'Enable','off');
            set(tb2_freqstbi_txt_str,'Visible','off'); set(tb2_freqstbi_txt_str,'Enable','off');
            set(tb2_freqstbi_txt_unit,'Visible','off'); set(tb2_freqstbi_txt_unit,'Enable','off');
        end
    end
    function tb2_afkapstph_popup_callback(tb2_afkapstph_popup,callbackdata)
        v = get(tb2_afkapstph_popup,'Value');
        if v == 2
            set(tb2_freqstph_txt,'Visible','on'); set(tb2_freqstph_txt,'Enable','on');
            set(tb2_freqstph_txt_str,'Visible','on'); set(tb2_freqstph_txt_str,'Enable','on');
            set(tb2_freqstph_txt_unit,'Visible','on'); set(tb2_freqstph_txt_unit,'Enable','on');
        else
            set(tb2_freqstph_txt,'Visible','off'); set(tb2_freqstph_txt,'Enable','off');
            set(tb2_freqstph_txt_str,'Visible','off'); set(tb2_freqstph_txt_str,'Enable','off');
            set(tb2_freqstph_txt_unit,'Visible','off'); set(tb2_freqstph_txt_unit,'Enable','off');
        end
    end
    function tb3_extrap_popup_callback(tb3_extrap_popup,callbackdata)
        %voorlopig niets
    end
    function tb4_winst_popup_callback(tb4_winst_popup,callbackdata)
        %voorlopig niets
    end
    function tb4_opl_DV_opt_popup_callback(tb4_opl_DV_opt_popup,callbackdata)
        if get(tb4_opl_DV_opt_popup,'Value') == 1
            set(tb4_db_DV_dH_txt,'Enable','on'); set(tb4_db_DV_dH_txt,'Visible','on');
            set(tb4_db_DV_dH_txt_str,'Enable','on'); set(tb4_db_DV_dH_txt_str,'Visible','on');
            set(tb4_db_DV_dH_txt_btn,'Enable','on'); set(tb4_db_DV_dH_txt_btn,'Visible','on');
            set(tb4_db_DV_dS_txt,'Enable','on'); set(tb4_db_DV_dS_txt,'Visible','on');
            set(tb4_db_DV_dS_txt_str,'Enable','on'); set(tb4_db_DV_dS_txt_str,'Visible','on');
            set(tb4_db_DV_dS_txt_btn,'Enable','on'); set(tb4_db_DV_dS_txt_btn,'Visible','on');
            set(tb4_db_DV_dP_txt,'Enable','on'); set(tb4_db_DV_dP_txt,'Visible','on');
            set(tb4_db_DV_dP_txt_str,'Enable','on'); set(tb4_db_DV_dP_txt_str,'Visible','on');
            set(tb4_db_DV_dP_txt_btn,'Enable','on'); set(tb4_db_DV_dP_txt_btn,'Visible','on');
        else
            set(tb4_db_DV_dH_txt,'Enable','off'); set(tb4_db_DV_dH_txt,'Visible','on');
            set(tb4_db_DV_dH_txt_str,'Enable','off'); set(tb4_db_DV_dH_txt_str,'Visible','on');
            set(tb4_db_DV_dH_txt_btn,'Enable','off'); set(tb4_db_DV_dH_txt_btn,'Visible','on');
            set(tb4_db_DV_dS_txt,'Enable','off'); set(tb4_db_DV_dS_txt,'Visible','on');
            set(tb4_db_DV_dS_txt_str,'Enable','off'); set(tb4_db_DV_dS_txt_str,'Visible','on');
            set(tb4_db_DV_dS_txt_btn,'Enable','off'); set(tb4_db_DV_dS_txt_btn,'Visible','on');
            set(tb4_db_DV_dP_txt,'Enable','off'); set(tb4_db_DV_dP_txt,'Visible','on');
            set(tb4_db_DV_dP_txt_str,'Enable','off'); set(tb4_db_DV_dP_txt_str,'Visible','on');
            set(tb4_db_DV_dP_txt_btn,'Enable','off'); set(tb4_db_DV_dP_txt_btn,'Visible','on');
        end
    end
    function tb6_pfvst_txt_popup_callback(tb6_pfvst_txt_popup,callbackdata)
        %voorlopig niets
    end
    function tb6_dimvst_txt_popup_callback(tb6_dimvst_txt_popup,callbackdata)
        %voorlopig niets
    end
    function tb6_pfht_txt_popup_callback(tb6_pfht_txt_popup,callbackdata)
        %voorlopig niets
    end
    function tb6_pfstbi_txt_popup_callback(tb6_pfstbi_txt_popup,callbackdata)
        %voorlopig niets
    end
    function tb6_pfstph_txt_popup_callback(tb6_pfstph_txt_popup,callbackdata)
        %voorlopig niets
    end
    function tb6_freqws_txt_popup_callback(tb6_freqws_txt_popup,callbackdata)
        %voorlopig niets
    end
    function tb6_visdijk_txt_popup_callback(tb6_visdijk_txt_popup,callbackdata)
        %voorlopig niets
    end
    function tb6_exportcsv_txt_popup_callback(tb6_exportcsv_txt_popup,callback)
        %voorlopig niets
    end
    function tb6_exportdbf_txt_popup_callback(tb6_exportdbf_txt_popup,callback)
        if get(tb6_exportdbf_txt_popup,'Value') == 1
            set(tb6_exportdbffile_txt,'Visible','on');
            set(tb6_exportdbffile_txt_str,'Visible','on');
            set(tb6_exportdbffile_txt_btn,'Visible','on');
            set(tb6_exportdbffile_txt,'Enable','on');
            set(tb6_exportdbffile_txt_str,'Enable','on');
            set(tb6_exportdbffile_txt_btn,'Enable','on');
        else
            set(tb6_exportdbffile_txt,'Visible','off');
            set(tb6_exportdbffile_txt_str,'Visible','off');
            set(tb6_exportdbffile_txt_btn,'Visible','off');
            set(tb6_exportdbffile_txt,'Enable','off');
            set(tb6_exportdbffile_txt_str,'Enable','off');
            set(tb6_exportdbffile_txt_btn,'Enable','off');
        end
    end

%% Overige functies
    function invoer = invoerdata_verzamelen(nQT_max,nQH_max,nnorm_max,nverst_max)
        % voor
        stg1 = get(lst1,'String');
        gj = strfind(stg1,'\');
        if isempty(gj) % aanpassing 02-09-2019
            gj = strfind(stg1,'/');
        end
        try
            invoer.fn = stg1(gj(end)+1:end);
            invoer.prog_dir = stg1(1:gj(end));
        catch
            invoer.fn = '-';
            invoer.prog_dir = '-';
        end
        % tab 1
        try
            invoer.analysetype = get(tb1_ana_popup,'Value');
        catch
            invoer.analysetype = 1;
        end
        invoer.t_begin = str2double(get(tb1_tbegin_txt_str,'String'));
        invoer.t_eind = str2double(get(tb1_teind_txt_str,'String'));
        invoer.t_stap = str2double(get(tb1_tstap_txt_str,'String'));
        invoer.db_vakken = get(tb1_dijkvak_txt_str,'String');
        invoer.db_vaklengte = get(tb1_dijkvaklen_txt_str,'String');
        invoer.db_KOSWAT = get(tb1_koswat_txt_str,'String');
        %extra variabele voor uitgebreide kostenberekening
        temp_i = get(tb1_constlength_opt_popup,'Value'); %optie: ja(1)/nee(0)
        if temp_i == 1
            invoer.constlength = 1;
        else
            invoer.constlength = 0;
        end
        if invoer.constlength == 1 %afhankelijk van invoer.constlength
            invoer.db_constlength = get(tb1_constlength_txt_str,'String');
        else
            invoer.db_constlength = get(tb1_constlength_txt_str,'String');
        end
        invoer.discontovoet = str2double(get(tb1_disc_txt_str,'String'));
        invoer.basisjaar = str2double(get(tb1_basisjaar_txt_str,'String'));
        %extra variabele indexeren prijspeil
        temp_i = get(tb1_indexeren_opt_popup,'Value'); %optie: ja(1)/nee(0)
        if temp_i == 1
            invoer.indexeren = 1;
        else
            invoer.indexeren = 0;
        end
        invoer.db_CBSindex = get(tb1_db_CBSindex_txt_str,'String');
        prijspeillist = get(tb1_prijspeil_opt_popup,'String');
        prijspeilindex = get(tb1_prijspeil_opt_popup,'Value');
        invoer.prijspeil = str2double(cell2mat(prijspeillist(prijspeilindex,1)));
        
        % tab 2
        temp_i = get(tb2_ht_txt_popup,'Value'); %optie: aan(1)/uit(0)
        if temp_i == 1
            invoer.faalmech_h = 1;
        else
            invoer.faalmech_h = 0;
        end
        temp_i = get(tb2_stbi_txt_popup,'Value'); %optie: aan(1)/uit(0)
        if temp_i == 1
            invoer.faalmech_s = 1;
        else
            invoer.faalmech_s = 0;
        end
        temp_i = get(tb2_stph_txt_popup,'Value'); %optie: aan(1)/uit(0)
        if temp_i == 1
            invoer.faalmech_p = 1;
        else
            invoer.faalmech_p = 0;
        end
        invoer.db_FC_h = get(tb2_htdata_txt_str,'String');
        invoer.db_FC_s = get(tb2_stbidata_txt_str,'String');
        invoer.db_FC_p = get(tb2_stphdata_txt_str,'String');
        invoer.db_afkap_h = get(tb2_afkapht_popup,'Value');
        invoer.db_freq_bs_h = str2double(get(tb2_freqht_txt_str,'String'));
        invoer.db_afkap_s = get(tb2_afkapstbi_popup,'Value');
        invoer.db_freq_bs_s = str2double(get(tb2_freqstbi_txt_str,'String'));
        invoer.db_afkap_p = get(tb2_afkapstph_popup,'Value');
        invoer.db_freq_bs_p = str2double(get(tb2_freqstph_txt_str,'String'));
        
        % tab 3
        invoer.db_onz_toeslag = get(tb3_onz_txt_str,'String');
        invoer.db_bodemdaling = get(tb3_bodm_txt_str,'String');
        invoer.dh_integraal = str2double(get(tb3_dh_txt_str,'String'));
        invoer.extrapolatie = get(tb3_extrap_popup,'Value');
        d18_temp = cell(nQT_max,5);
        idx = nan(nQT_max,1);
        a = 1;
        for a1 = 1:nQT_max
            if ~isempty(d18{a1,2})
                d18_temp{a1,1} = a;
                d18_temp(a1,2:end) = d18(a1,2:end);
                idx(a1) = a1;
                a = a+1;
            end
        end
        d18_ok = d18_temp(~isnan(idx),:);
        invoer.QTscenarios = d18_ok;
        if isempty(d18_ok)
            invoer.QT_n = 0;
        else
            [r1,~] = size(d18_ok);
            invoer.QT_n = r1;
        end
        d19_temp = cell(nQH_max,5);
        idx = nan(nQH_max,1);
        a = 1;
        for a1 = 1:nQT_max
            if ~isempty(d19{a1,2})
                d19_temp{a1,1} = a;
                d19_temp(a1,2:end) = d19(a1,2:end);
                idx(a1) = a1;
                a = a+1;
            end
        end
        d19_ok = d19_temp(~isnan(idx),:);
        invoer.QHscenarios = d19_ok;
        if isempty(d19_ok)
            invoer.QH_n = 0;
        else
            [r1,~] = size(d19_ok);
            invoer.QH_n = r1;
        end
        
        % tab 4
        invoer.sce_inst = 1;
        nnorm_max = 1;
        nverst_max = 1;
        %if get(bg20a,'Value') == 1
        %    invoer.sce_inst = 1;
        %    nnorm_max = 1;
        %    nverst_max = 1;
        %else
        %    invoer.sce_inst = 2;
        %    nnorm_max = 10;
        %    nverst_max = 10;
        %end
        invoer.db_norm = get(tb4_norm_txt_str,'String');
        invoer.db_ontwerp_duur = get(tb4_ont_txt_str,'String');
        % Op verzoek van HKV om te verwijderen d.d. 11 juli 2018
        %try
        %    invoer.db_maxpipberm = get(tb4_maxpip_txt_str,'String');
        %catch
        %end
        invoer.db_DVschema = get(tb4_hwbp_txt_str,'String');
        invoer.winst_rvm = get(tb4_winst_txt_popup,'Value');
        invoer.db_corr_dH = get(tb4_db_corr_dH_txt_str,'String');
        invoer.db_corr_dS = get(tb4_db_corr_dS_txt_str,'String');
        invoer.db_corr_dP = get(tb4_db_corr_dP_txt_str,'String');
        invoer.db_pipcost = get(tb4_db_pipcost_txt_str,'String');
        if get(tb4_opl_DV_opt_popup,'Value') == 1
            invoer.opl_DV = 1;
        else
            invoer.opl_DV = 0;
        end
        invoer.db_DV_dH = get(tb4_db_DV_dH_txt_str,'String');
        invoer.db_DV_dS = get(tb4_db_DV_dS_txt_str,'String');
        invoer.db_DV_dP = get(tb4_db_DV_dP_txt_str,'String');
        
        d24_temp = cell(nnorm_max,3);
        idx = nan(nnorm_max,1);
        a = 1;
        for a1 = 1:nnorm_max
            if ~isempty(d24{a1,2})
                d24_temp{a1,1} = a;
                d24_temp(a1,2:end) = d24(a1,2:end);
                idx(a1) = a1;
                a = a+1;
            end
        end
        d24_ok = d24_temp(~isnan(idx),:);
        invoer.normen = d24_ok;
        if isempty(d24_ok)
            invoer.norm_n = 0;
        else
            [r1,~] = size(d24_ok);
            invoer.norm_n = r1;
        end
        d25_temp = cell(nverst_max,4);
        idx = nan(nverst_max,1);
        a = 1;
        for a1 = 1:nverst_max
            if ~isempty(d25{a1,2})
                d25_temp{a1,1} = a;
                d25_temp(a1,2:end) = d25(a1,2:end);
                idx(a1) = a1;
                a = a+1;
            end
        end
        d25_ok = d25_temp(~isnan(idx),:);
        invoer.verst = d25_ok;
        if isempty(d25_ok)
            invoer.verst_n = 0;
        else
            [r1,~] = size(d25_ok);
            invoer.verst_n = r1;
        end
        
        % tab 5
        try %Op verzoek van HKV om te verwijderen d.d. 11 juli 2018
            invoer.mac_factor = str2double(get(tb5_stabfac_txt_str,'String'));
        catch
        end
        try %Op verzoek van HKV om te verwijderen d.d. 11 juli 2018
            invoer.pip_factor = str2double(get(tb5_pipfac_txt_str,'String'));
        catch
        end
        invoer.dH_stap = str2double(get(tb5_dhstap_txt_str,'String'));
        invoer.dS_stap = str2double(get(tb5_dsstap_txt_str,'String'));
        invoer.dP_stap = str2double(get(tb5_dpstap_txt_str,'String'));
        invoer.dH_max = str2double(get(tb5_dHmax_txt_str,'String'));
        invoer.dS_max = str2double(get(tb5_dSmax_txt_str,'String'));
        invoer.dP_max = str2double(get(tb5_dPmax_txt_str,'String'));
        invoer.Qdisc_step = str2double(get(tb5_Qdiscstep_txt_str,'String'));
        try %Op verzoek van HKV om te verwijderen d.d. 11 juli 2018
            invoer.Qdisc_min = str2double(get(tb5_Qdiscmin_txt_str,'String'));
            invoer.Qdisc_low = str2double(get(tb5_Qdisclow_txt_str,'String'));
        catch
        end
        invoer.Qdisc_high = str2double(get(tb5_Qdischigh_txt_str,'String'));
        invoer.refjaar_bodemdaling = str2double(get(tb5_refjaar_txt_str,'String'));
        invoer.t_voldoen_norm = str2double(get(tb5_tvoln_txt_str,'String'));
        try %Op verzoek van HKV om te verwijderen d.d. 11 juli 2018
            invoer.cost_breakeven_dP = str2double(get(tb5_costbdP_txt_str,'String'));
        catch
        end
        %try %extra variabele voor uitgebreide kostenberekening (wordt niet meer gebruikt)
        %    invoer.kost_uitgebreid = get(tb5_koex_text_popup,'Value');
        %catch
        %end
        
        % tab 6
        invoer.uitvoer_map = get(tb6_uitvoermap_txt_str,'String');
        invoer.tijdopgave = get(tb6_tijdopgave_txt_str,'String');
        invoer.plot_pf_vs_t = get(tb6_pfvst_txt_popup,'Value');
        if invoer.plot_pf_vs_t == 2
            invoer.plot_pf_vs_t = 0;
        end
        invoer.plot_dims_vs_t = get(tb6_dimvst_txt_popup,'Value');
        invoer.plot_pf_s = get(tb6_pfstbi_txt_popup,'Value');
        invoer.plot_pf_p = get(tb6_pfstph_txt_popup,'Value');
        invoer.plot_pf_h = get(tb6_pfht_txt_popup,'Value');
        invoer.plot_pdf_wb = get(tb6_freqws_txt_popup,'Value');
        invoer.plot_dijk = get(tb6_visdijk_txt_popup,'Value');
        if invoer.plot_dijk == 1
            invoer.plot_dijk = 2;
        end
        invoer.exp_traject = get(tb6_exportcsv_txt_popup,'Value');
        invoer.exp_shape = get(tb6_exportdbf_txt_popup,'Value');
        invoer.dbf_shape = get(tb6_exportdbffile_txt_str,'String');
    end
    function invoerfile_aanmaken(fn,wd,invoer)
        fid = fopen([wd fn],'wt');
        fprintf(fid,'%%%% Invoerfile voor Tool Kostenreductie Dijkversterking door Rivierverruiming\n');
        fprintf(fid,'%%%% versie 2.01\n');
        fprintf(fid,'%%%% HKV en Deltares in opdracht van RWS-WVL, maart 2017\n\n');
        
        fprintf(fid,'%%%% Tabblad "Algemeen"\n');
        fprintf(fid,'analysetype \t : '); fprintf(fid,'%s\n',num2str(invoer.analysetype));
        fprintf(fid,'t_begin \t : '); fprintf(fid,'%s\n',num2str(invoer.t_begin));
        fprintf(fid,'t_eind \t\t : '); fprintf(fid,'%s\n',num2str(invoer.t_eind));
        fprintf(fid,'t_stap \t\t : '); fprintf(fid,'%s\n',num2str(invoer.t_stap));
        fprintf(fid,'db_vakken \t : '); fprintf(fid,'%s\n',invoer.db_vakken);
        fprintf(fid,'db_vaklengte \t : '); fprintf(fid,'%s\n',invoer.db_vaklengte);
        fprintf(fid,'db_KOSWAT \t : '); fprintf(fid,'%s\n',invoer.db_KOSWAT);
        fprintf(fid,'constlength \t : '); fprintf(fid,'%s\n',num2str(invoer.constlength));
        fprintf(fid,'db_constlength \t : '); fprintf(fid,'%s\n',invoer.db_constlength);
        fprintf(fid,'discontovoet \t : '); fprintf(fid,'%s\n',num2str(invoer.discontovoet));
        fprintf(fid,'basisjaar \t : '); fprintf(fid,'%s\n',num2str(invoer.basisjaar));
        fprintf(fid,'indexeren \t : '); fprintf(fid,'%s\n',num2str(invoer.indexeren));
        fprintf(fid,'db_CBSindex \t : '); fprintf(fid,'%s\n',num2str(invoer.db_CBSindex));
        fprintf(fid,'prijspeil \t : '); fprintf(fid,'%s\n\n',num2str(invoer.prijspeil));
                
        fprintf(fid,'%%%% Tabblad "Dijksterkte"\n');
        if invoer.faalmech_h == 2
            fprintf(fid,'faalmech_h \t : '); fprintf(fid,'%s\n',num2str(0));
        else
            fprintf(fid,'faalmech_h \t : '); fprintf(fid,'%s\n',num2str(invoer.faalmech_h));
        end
        if invoer.faalmech_s == 2
            fprintf(fid,'faalmech_s \t : '); fprintf(fid,'%s\n',num2str(0));
        else
            fprintf(fid,'faalmech_s \t : '); fprintf(fid,'%s\n',num2str(invoer.faalmech_s));
        end
        if invoer.faalmech_p == 2
            fprintf(fid,'faalmech_p \t : '); fprintf(fid,'%s\n',num2str(0));
        else
            fprintf(fid,'faalmech_p \t : '); fprintf(fid,'%s\n',num2str(invoer.faalmech_p));
        end
        fprintf(fid,'db_FC_h \t : '); fprintf(fid,'%s\n',invoer.db_FC_h);
        fprintf(fid,'db_FC_s \t : '); fprintf(fid,'%s\n',invoer.db_FC_s);
        fprintf(fid,'db_FC_p \t : '); fprintf(fid,'%s\n',invoer.db_FC_p);
        fprintf(fid,'afkap_h \t : '); fprintf(fid,'%s\n',num2str(invoer.db_afkap_h));
        fprintf(fid,'freq_bs_h \t : '); fprintf(fid,'%s\n',num2str(invoer.db_freq_bs_h));
        fprintf(fid,'afkap_s \t : '); fprintf(fid,'%s\n',num2str(invoer.db_afkap_s));
        fprintf(fid,'freq_bs_s \t : '); fprintf(fid,'%s\n',num2str(invoer.db_freq_bs_s));
        fprintf(fid,'afkap_p \t : '); fprintf(fid,'%s\n',num2str(invoer.db_afkap_p));
        fprintf(fid,'freq_bs_p \t : '); fprintf(fid,'%s\n\n',num2str(invoer.db_freq_bs_p));
        
        fprintf(fid,'%%%% Tabblad "Hydraulische belasting"\n');
        fprintf(fid,'db_onz_toeslag \t : '); fprintf(fid,'%s\n',num2str(invoer.db_onz_toeslag));
        fprintf(fid,'db_bodemdaling \t : '); fprintf(fid,'%s\n',num2str(invoer.db_bodemdaling));
        fprintf(fid,'dh_integraal \t : '); fprintf(fid,'%s\n',num2str(invoer.dh_integraal));
        fprintf(fid,'extrapolatie \t : '); fprintf(fid,'%s\n\n',num2str(invoer.extrapolatie));
        fprintf(fid,'%% scenarios voor QT\n');
        fprintf(fid,'ns = '); fprintf(fid,'%s\n',num2str(invoer.QT_n));
        if invoer.QT_n == 0
            fprintf(fid,['sce_id = ' num2str(invoer.QT_n) '\n']);
            fprintf(fid,['t = ' num2str(0) '\n']);
            fprintf(fid,'QT_file_1 = \n');
            fprintf(fid,'QT_file_2 = \n');
            fprintf(fid,'QT_file_3 = \n\n');
        else
            for u = 1:invoer.QT_n
                fprintf(fid,['sce_id = ' num2str(u) '\n']);
                fprintf(fid,['t = ' num2str(invoer.QTscenarios{u,2}) '\n']);
                fprintf(fid,'QT_file_1 = '); fprintf(fid,'%s\n',invoer.QTscenarios{u,3});
                fprintf(fid,'QT_file_2 = '); fprintf(fid,'%s\n',invoer.QTscenarios{u,4});
                if u ~= invoer.QT_n
                    fprintf(fid,'QT_file_3 = '); fprintf(fid,'%s\n',invoer.QTscenarios{u,5});
                else
                    fprintf(fid,'QT_file_3 = '); fprintf(fid,'%s\n\n',invoer.QTscenarios{u,5});
                end
            end
        end
        fprintf(fid,'%% scenarios voor QH\n');
        fprintf(fid,['ns = ' num2str(invoer.QH_n) '\n']);
        if invoer.QH_n == 0
            fprintf(fid,['sce_id = ' num2str(invoer.QH_n) '\n']);
            fprintf(fid,['t = ' num2str(0) '\n']);
            fprintf(fid,'QH_file_1 = \n');
            fprintf(fid,'QH_file_2 = \n');
            fprintf(fid,'QH_file_3 = \n\n');
        else
            for u = 1:invoer.QH_n
                fprintf(fid,['sce_id = ' num2str(u) '\n']);
                fprintf(fid,['t = ' num2str(invoer.QHscenarios{u,2}) '\n']);
                fprintf(fid,'QH_file_1 = '); fprintf(fid,'%s\n',invoer.QHscenarios{u,3});
                fprintf(fid,'QH_file_2 = '); fprintf(fid,'%s\n',invoer.QHscenarios{u,4});
                if u ~= invoer.QH_n
                    fprintf(fid,'QH_file_3 = '); fprintf(fid,'%s\n',invoer.QHscenarios{u,5});
                else
                    fprintf(fid,'QH_file_3 = '); fprintf(fid,'%s\n\n',invoer.QHscenarios{u,5});
                end
            end
        end
        
        fprintf(fid,'%%%% Tabblad "Dijkversterking en normen"\n');
        fprintf(fid,'db_norm \t : '); fprintf(fid,'%s\n',invoer.db_norm);
        fprintf(fid,'db_ontwerp_duur  : '); fprintf(fid,'%s\n',invoer.db_ontwerp_duur);
        % Op verzoek van HKV om te verwijderen d.d. 11 juli 2018
        %try
        %    fprintf(fid,'db_maxpipberm \t : '); fprintf(fid,'%s\n',invoer.db_maxpipberm);
        %catch
        %end
        fprintf(fid,'db_DVschema \t : '); fprintf(fid,'%s\n',invoer.db_DVschema);
        fprintf(fid,'winst_rvm \t : '); fprintf(fid,'%s\n',num2str(invoer.winst_rvm));
        fprintf(fid,'db_corr_dH \t : '); fprintf(fid,'%s\n',invoer.db_corr_dH);
        fprintf(fid,'db_corr_dS \t : '); fprintf(fid,'%s\n',invoer.db_corr_dS);
        fprintf(fid,'db_corr_dP \t : '); fprintf(fid,'%s\n',invoer.db_corr_dP);
        fprintf(fid,'db_pipcost \t : '); fprintf(fid,'%s\n',invoer.db_pipcost);
        try
            fprintf(fid,'opl_DV \t\t : '); fprintf(fid,'%s\n',num2str(invoer.opl_DV));
        catch
            fprintf(fid,'opl_DV \t\t : '); fprintf(fid,'%s\n',num2str(0));
        end
        try
            fprintf(fid,'db_DV_dH \t : '); fprintf(fid,'%s\n',invoer.db_DV_dH);
        catch
            fprintf(fid,'db_DV_dH \t : '); fprintf(fid,'%s\n','-');
        end
        try
            fprintf(fid,'db_DV_dS \t : '); fprintf(fid,'%s\n',invoer.db_DV_dS);
        catch
            fprintf(fid,'db_DV_dS \t : '); fprintf(fid,'%s\n','-');
        end
        try
            fprintf(fid,'db_DV_dP \t : '); fprintf(fid,'%s\n\n',invoer.db_DV_dP);
        catch
            fprintf(fid,'db_DV_dP \t : '); fprintf(fid,'%s\n\n','-');
        end
        
        fprintf(fid,'%%%% Tabblad "Berekening instellingen"\n');
        % Op verzoek van HKV om te verwijderen d.d. 11 juli 2018
        %fprintf(fid,'pip_factor \t\t : '); fprintf(fid,'%s\n',num2str(invoer.pip_factor));
        % Op verzoek van HKV om te verwijderen d.d. 11 juli 2018
        %fprintf(fid,'mac_factor \t\t : '); fprintf(fid,'%s\n',num2str(invoer.mac_factor));
        fprintf(fid,'dH_stap \t\t : '); fprintf(fid,'%s\n',num2str(invoer.dH_stap));
        fprintf(fid,'dS_stap \t\t : '); fprintf(fid,'%s\n',num2str(invoer.dS_stap));
        fprintf(fid,'dP_stap \t\t : '); fprintf(fid,'%s\n',num2str(invoer.dP_stap));
        fprintf(fid,'dH_max \t\t\t : '); fprintf(fid,'%s\n',num2str(invoer.dH_max));
        fprintf(fid,'dS_max \t\t\t : '); fprintf(fid,'%s\n',num2str(invoer.dS_max));
        fprintf(fid,'dP_max \t\t\t : '); fprintf(fid,'%s\n',num2str(invoer.dP_max));
        fprintf(fid,'Qdisc_step \t\t : '); fprintf(fid,'%s\n',num2str(invoer.Qdisc_step));
        fprintf(fid,'Qdisc_high \t\t : '); fprintf(fid,'%s\n',num2str(invoer.Qdisc_high));
        %Verzoek van HKV om dit te verwijderen d.d. 11 juli 2018
        %fprintf(fid,'Qdisc_min \t\t : '); fprintf(fid,'%s\n',num2str(invoer.Qdisc_min));
        %fprintf(fid,'Qdisc_low \t\t : '); fprintf(fid,'%s\n',num2str(invoer.Qdisc_low));
        fprintf(fid,'Refjaar_bodemdaling \t : '); fprintf(fid,'%s\n',num2str(invoer.refjaar_bodemdaling));
        fprintf(fid,'t_voldoen_norm \t\t : '); fprintf(fid,'%s\n\n',num2str(invoer.t_voldoen_norm));
        %Verzoek van HKV om dit te verwijderen d.d. 11 juli 2018
        %fprintf(fid,'cost_breakeven_dP \t : '); fprintf(fid,'%s\n\n',num2str(invoer.cost_breakeven_dP));
        
        fprintf(fid,'%%%% Tabblad "Uitvoer en visualisatie"\n');
        fprintf(fid,'uitvoer_map \t : '); fprintf(fid,'%s\n',invoer.uitvoer_map);
        %try
        %if ~exist(invoer.uitvoer_map,'dir')
        %    mkdir(invoer.uitvoer_map);
        %end
        %catch
        %end
        fprintf(fid,'tijdopgave \t : '); fprintf(fid,'%s\n',invoer.tijdopgave);
        if invoer.plot_pf_vs_t == 2
            fprintf(fid,'plot_pf_vs_t \t : '); fprintf(fid,'%s\n',num2str(0));
        else
            fprintf(fid,'plot_pf_vs_t \t : '); fprintf(fid,'%s\n',num2str(invoer.plot_pf_vs_t));
        end
        if invoer.plot_dims_vs_t == 2
            fprintf(fid,'plot_dims_vs_t \t : '); fprintf(fid,'%s\n',num2str(0));
        else
            fprintf(fid,'plot_dims_vs_t \t : '); fprintf(fid,'%s\n',num2str(invoer.plot_dims_vs_t));
        end
        if invoer.plot_pf_s == 5
            fprintf(fid,'plot_pf_s \t : '); fprintf(fid,'%s\n',num2str(0));
        else
            fprintf(fid,'plot_pf_s \t : '); fprintf(fid,'%s\n',num2str(invoer.plot_pf_s));
        end
        if invoer.plot_pf_p == 5
            fprintf(fid,'plot_pf_p \t : '); fprintf(fid,'%s\n',num2str(0));
        else
            fprintf(fid,'plot_pf_p \t : '); fprintf(fid,'%s\n',num2str(invoer.plot_pf_p));
        end
        if invoer.plot_pf_h == 5
            fprintf(fid,'plot_pf_h \t : '); fprintf(fid,'%s\n',num2str(0));
        else
            fprintf(fid,'plot_pf_h \t : '); fprintf(fid,'%s\n',num2str(invoer.plot_pf_h));
        end
        if invoer.plot_pdf_wb == 4
            fprintf(fid,'plot_pdf_wb \t : '); fprintf(fid,'%s\n',num2str(0));
        else
            fprintf(fid,'plot_pdf_wb \t : '); fprintf(fid,'%s\n',num2str(invoer.plot_pdf_wb));
        end
        if invoer.plot_dijk == 2
            fprintf(fid,'plot_dijk \t : '); fprintf(fid,'%s\n',num2str(0));
        else
            fprintf(fid,'plot_dijk \t : '); fprintf(fid,'%s\n',num2str(invoer.plot_dijk));
        end
        if invoer.exp_traject == 2
            fprintf(fid,'exp_traject  \t : '); fprintf(fid,'%s\n',num2str(0));
        else
            fprintf(fid,'exp_traject  \t : '); fprintf(fid,'%s\n',num2str(invoer.exp_traject));
        end
        if invoer.exp_shape == 2
            fprintf(fid,'exp_shape \t : '); fprintf(fid,'%s\n',num2str(0));
        else
            fprintf(fid,'exp_shape \t : '); fprintf(fid,'%s\n',num2str(invoer.exp_shape));
        end
        fprintf(fid,'dbf_shape \t : '); fprintf(fid,'%s\n',invoer.dbf_shape);
        
        %fprintf(fid,'%%%% scenarios voor normen\n');
        %fprintf(fid,['ns = ' num2str(invoer.norm_n) '\n']);
        %if invoer.norm_n == 0
        %    fprintf(fid,['sce_id = ' num2str(invoer.norm_n) '\n']);
        %    fprintf(fid,['t = ' num2str(0) '\n']);
        %    fprintf(fid,'norm_file = \n\n');
        %else
        %    for u = 1:invoer.norm_n
        %        fprintf(fid,['sce_id = ' num2str(u) '\n']);
        %        fprintf(fid,['t = ' num2str(invoer.normen{u,2}) '\n']);
        %        if u ~= invoer.norm_n
        %            fprintf(fid,'norm_file = '); fprintf(fid,'%s\n',invoer.normen{u,3});
        %        else
        %            fprintf(fid,'norm_file = '); fprintf(fid,'%s\n\n',invoer.normen{u,3});
        %        end
        %    end
        %end
        %fprintf(fid,'%%%% scenarios voor versterkingen\n');
        %fprintf(fid,['ns = ' num2str(invoer.verst_n) '\n']);
        %if invoer.verst_n == 0
        %    fprintf(fid,['sce_id = ' num2str(invoer.verst_n) '\n']);
        %    fprintf(fid,['t = ' num2str(0) '\n']);
        %    fprintf(fid,['QT_sce = ' num2str(0) '\n']);
        %    fprintf(fid,['QH_sce = ' num2str(0) '\n']);
        %else
        %    for u = 1:invoer.verst_n
        %        fprintf(fid,['sce_id = ' num2str(u) '\n']);
        %        fprintf(fid,['t = ' num2str(invoer.verst{u,2}) '\n']);
        %        fprintf(fid,['QT_sce = ' num2str(invoer.verst{u,3}) '\n']);
        %        fprintf(fid,['QH_sce = ' num2str(invoer.verst{u,4}) '\n']);
        %    end
        %end
        fclose(fid);
    end
    function invoer_laden(invoer)
        % tab 1
        try
            set(tb1_ana_popup,'Value',invoer.analyse);
        catch
            set(tb1_ana_popup,'Value',1);
        end
        try
            set(tb1_tbegin_txt_str,'String',num2str(invoer.t_begin));
        catch
            set(tb1_tbegin_txt_str,'String','0');
        end
        try
            set(tb1_teind_txt_str,'String',num2str(invoer.t_eind));
        catch
            set(tb1_tbegin_txt_str,'String','0');
        end
        try
            set(tb1_tstap_txt_str,'String',num2str(invoer.t_stap));
        catch
            set(tb1_tstap_txt_str,'String','0');
        end
        try
            set(tb1_dijkvak_txt_str,'String',invoer.db_vakken);
        catch
            set(tb1_dijkvak_txt_str,'String','- niet geselecteerd -')
        end
        try
            set(tb1_dijkvaklen_txt_str,'String',invoer.db_vaklengte);
        catch
            set(tb1_dijkvaklen_txt_str,'String','- niet geselecteerd -')
        end
        try
            set(tb1_koswat_txt_str,'String',invoer.db_KOSWAT);
        catch
            set(tb1_koswat_txt_str,'String','- niet geselecteerd -')
        end
        try
            if invoer.constlength == 1
                set(tb1_constlength_opt_popup,'Value',1);
                set(tb1_constlength_txt_str,'String',invoer.db_constlength);
                set(tb1_constlength_txt,'Enable','on'); set(tb1_constlength_txt,'Visible','on');
                set(tb1_constlength_txt_str,'Enable','on'); set(tb1_constlength_txt_str,'Visible','on');
                set(tb1_constlength_txt_btn,'Enable','on'); set(tb1_constlength_txt_btn,'Visible','on');
            elseif invoer.indexeren == 0
                set(tb1_constlength_opt_popup,'Value',2);
                set(tb1_constlength_txt_str,'String',invoer.db_constlength);
                set(tb1_constlength_txt,'Enable','off'); set(tb1_constlength_txt,'Visible','off');
                set(tb1_constlength_txt_str,'Enable','off'); set(tb1_constlength_txt_str,'Visible','off');
                set(tb1_constlength_txt_btn,'Enable','off'); set(tb1_constlength_txt_btn,'Visible','off');
            end
        catch
            set(tb1_constlength_opt_popup,'Value',2);
            set(tb1_constlength_txt_str,'String',invoer.db_constlength);
            set(tb1_constlength_txt,'Enable','off'); set(tb1_constlength_txt,'Visible','off');
            set(tb1_constlength_txt_str,'Enable','off'); set(tb1_constlength_txt_str,'Visible','off');
            set(tb1_constlength_txt_btn,'Enable','off'); set(tb1_constlength_txt_btn,'Visible','off');
        end        
        try
            if invoer.indexeren == 1
                set(tb1_indexeren_opt_popup,'Value',1);
                set(tb1_db_CBSindex_txt_str,'String',invoer.db_CBSindex);
                set(tb1_db_CBSindex_txt,'Enable','on'); set(tb1_db_CBSindex_txt,'Visible','on');
                set(tb1_db_CBSindex_txt_str,'Enable','on'); set(tb1_db_CBSindex_txt_str,'Visible','on');
                set(tb1_db_CBSindex_txt_btn,'Enable','on'); set(tb1_db_CBSindex_txt_btn,'Visible','on');
                
                CBSindex    = dlmread(invoer.db_CBSindex,';',1,0);
                prijsindex = find(CBSindex(:,1)==invoer.prijspeil);
                if prijsindex > 1
                    set(tb1_prijspeil_opt_txt,'Enable','on'); set(tb1_prijspeil_opt_txt,'Visible','on');
                    set(tb1_prijspeil_opt_popup,'Enable','on'); set(tb1_prijspeil_opt_popup,'Visible','on');
                    set(tb1_prijspeil_opt_popup,'String',num2cell(CBSindex(:,1)),'Value',prijsindex);
                else
                    set(tb1_db_CBSindex_txt_str,'String','- onjuist bestand geselecteerd of gekozen prijspeil niet aanwezig in bestand -');
                    set(tb1_prijspeil_opt_txt,'Enable','off'); set(tb1_prijspeil_opt_txt,'Visible','on');
                    set(tb1_prijspeil_opt_popup,'Enable','off'); set(tb1_prijspeil_opt_popup,'Visible','on');
                    set(tb1_prijspeil_opt_popup,'String',{'2013'},'Value',1);
                end
            elseif invoer.indexeren == 0
                set(tb1_indexeren_opt_popup,'Value',2);
                set(tb1_db_CBSindex_txt_str,'String',invoer.db_CBSindex);
                set(tb1_db_CBSindex_txt,'Enable','off'); set(tb1_db_CBSindex_txt,'Visible','off');
                set(tb1_db_CBSindex_txt_str,'Enable','off'); set(tb1_db_CBSindex_txt_str,'Visible','off');
                set(tb1_db_CBSindex_txt_btn,'Enable','off'); set(tb1_db_CBSindex_txt_btn,'Visible','off');
                set(tb1_prijspeil_opt_txt,'Enable','off'); set(tb1_prijspeil_opt_txt,'Visible','off');
                set(tb1_prijspeil_opt_popup,'Enable','off'); set(tb1_prijspeil_opt_popup,'Visible','off');
            end
        catch
            set(tb1_indexeren_opt_popup,'Value',2);
            set(tb1_db_CBSindex_txt_str,'String',invoer.db_CBSindex);
            set(tb1_db_CBSindex_txt,'Enable','off'); set(tb1_db_CBSindex_txt,'Visible','off');
            set(tb1_db_CBSindex_txt_str,'Enable','off'); set(tb1_db_CBSindex_txt_str,'Visible','off');
            set(tb1_db_CBSindex_txt_btn,'Enable','off'); set(tb1_db_CBSindex_txt_btn,'Visible','off');
            set(tb1_prijspeil_opt_txt,'Enable','off'); set(tb1_prijspeil_opt_txt,'Visible','off');
            set(tb1_prijspeil_opt_popup,'Enable','off'); set(tb1_prijspeil_opt_popup,'Visible','off');
        end
        try
            set(tb1_disc_txt_str,'String',num2str(invoer.discontovoet));
        catch
            set(tb1_disc_txt_str,'String','0');
        end
        try
            set(tb1_basisjaar_txt_str,'String',num2str(invoer.basisjaar));
        catch
            set(tb1_basisjaar_txt_str,'String','0');
        end
        
        % tab2
        try % ht aan/uit met de fc
            if invoer.faalmech_h == 1
                set(tb2_ht_txt_popup,'Value',1);
            elseif invoer.faalmech_h == 0
                set(tb2_ht_txt_popup,'Value',2);
            else
                set(tb2_ht_txt_popup,'Value',1);
            end
        catch
            set(tb2_ht_txt_popup,'Value',1);
        end
        if get(tb2_ht_txt_popup,'Value') == 2
            set(tb2_htdata_txt,'Enable','off');
            set(tb2_htdata_txt_str,'Enable','off');
            set(tb2_htdata_txt_btn,'Enable','off');
        else
            set(tb2_htdata_txt,'Enable','on');
            set(tb2_htdata_txt_str,'Enable','on');
            set(tb2_htdata_txt_btn,'Enable','on');
        end
        
        try % stab aan/uit met de fc
            if invoer.faalmech_s == 1
                set(tb2_stbi_txt_popup,'Value',1);
            elseif invoer.faalmech_s == 0
                set(tb2_stbi_txt_popup,'Value',2);
            else
                set(tb2_stbi_txt_popup,'Value',1);
            end
        catch
            set(tb2_stbi_txt_popup,'Value',1);
        end
        if get(tb2_stbi_txt_popup,'Value') == 2
            set(tb2_stbidata_txt,'Enable','off');
            set(tb2_stbidata_txt_str,'Enable','off');
            set(tb2_stbidata_txt_btn,'Enable','off');
        else
            set(tb2_stbidata_txt,'Enable','on');
            set(tb2_stbidata_txt_str,'Enable','on');
            set(tb2_stbidata_txt_btn,'Enable','on');
        end
        
        try % piping aan/uit met de fc
            if invoer.faalmech_p == 1
                set(tb2_stph_txt_popup,'Value',1);
            elseif invoer.faalmech_p == 0
                set(tb2_stph_txt_popup,'Value',2);
            else
                set(tb2_stph_txt_popup,'Value',1);
            end
        catch
            set(tb2_stph_txt_popup,'Value',1);
        end
        if get(tb2_stph_txt_popup,'Value') == 2
            set(tb2_stphdata_txt,'Enable','off');
            set(tb2_stphdata_txt_str,'Enable','off');
            set(tb2_stphdata_txt_btn,'Enable','off');
        else
            set(tb2_stphdata_txt,'Enable','on');
            set(tb2_stphdata_txt_str,'Enable','on');
            set(tb2_stphdata_txt_btn,'Enable','on');
        end
        
        try
            set(tb2_htdata_txt_str,'String',invoer.db_FC_h);
        catch
            set(tb2_htdata_txt_str,'String',1);
        end
        try
            set(tb2_stbidata_txt_str,'String',invoer.db_FC_s);
        catch
            set(tb2_stbidata_txt_str,'String',1);
        end
        try
            set(tb2_stphdata_txt_str,'String',invoer.db_FC_p);
        catch
            set(tb2_stphdata_txt_str,'String',1);
        end
        try
            set(tb2_afkapht_popup,'Value',invoer.db_afkap_h);
        catch
            set(tb2_afkapht_popup,'Value',1);
        end
        if get(tb2_afkapht_popup,'Value') == 2
            set(tb2_freqht_txt,'Visible','on');
            set(tb2_freqht_txt_str,'Visible','on');
            set(tb2_freqht_txt_unit,'Visible','on');
            set(tb2_freqht_txt_str,'Enable','on');
        else
            set(tb2_freqht_txt,'Visible','off');
            set(tb2_freqht_txt_str,'Visible','off');
            set(tb2_freqht_txt_unit,'Visible','off');
            set(tb2_freqht_txt_str,'Enable','off');
        end
        try
            set(tb2_afkapstbi_popup,'Value',invoer.db_afkap_s);
        catch
            set(tb2_afkapstbi_popup,'Value',1);
        end
        if get(tb2_afkapstbi_popup,'Value') == 2
            set(tb2_freqstbi_txt,'Visible','on');
            set(tb2_freqstbi_txt_str,'Visible','on');
            set(tb2_freqstbi_txt_unit,'Visible','on');
            set(tb2_freqstbi_txt_str,'Enable','on');
        else
            set(tb2_freqstbi_txt,'Visible','off');
            set(tb2_freqstbi_txt_str,'Visible','off');
            set(tb2_freqstbi_txt_unit,'Visible','off');
            set(tb2_freqstbi_txt_str,'Enable','off');
        end
        try
            set(tb2_afkapstph_popup,'Value',invoer.db_afkap_p);
        catch
            set(tb2_afkapstph_popup,'Value',1);
        end
        if get(tb2_afkapstph_popup,'Value') == 2
            set(tb2_freqstph_txt,'Visible','on');
            set(tb2_freqstph_txt_str,'Visible','on');
            set(tb2_freqstph_txt_unit,'Visible','on');
            set(tb2_freqstph_txt_str,'Enable','on');
        else
            set(tb2_freqstph_txt,'Visible','off');
            set(tb2_freqstph_txt_str,'Visible','off');
            set(tb2_freqstph_txt_unit,'Visible','off');
            set(tb2_freqstph_txt_str,'Enable','off');
        end
        try
            set(tb2_freqht_txt_str,'String',num2str(invoer.db_freq_bs_h));
        catch
            set(tb2_freqht_txt_str,'String','1');
        end
        try
            set(tb2_freqstbi_txt_str,'String',num2str(invoer.db_freq_bs_s));
        catch
            set(tb2_freqstbi_txt_str,'String','1');
        end
        try
            set(tb2_freqstph_txt_str,'String',num2str(invoer.db_freq_bs_p));
        catch
            set(tb2_freqstph_txt_str,'String','1');
        end
        
        % tab3
        try
            set(tb3_onz_txt_str,'String',invoer.db_onz_toeslag);
        catch
            set(tb3_onz_txt_str,'String','- niet geselecteerd -');
        end
        try
            set(tb3_bodm_txt_str,'String',invoer.db_bodemdaling);
        catch
            set(tb3_bodm_txt_str,'String','- niet geselecteerd -');
        end
        try
            set(tb3_dh_txt_str,'String',num2str(invoer.dh_integraal));
        catch
            set(tb3_dh_txt_str,'String','0');
        end
        try
            set(tb3_extrap_popup,'Value',invoer.extrapolatie);
        catch
            set(tb3_extrap_popup,'Value',1);
        end
        try
            hj = size(invoer.QTscenarios,1);
            if hj < nQT_max
                cell_extra = cell(nQT_max-hj,5);
                d18 = [invoer.QTscenarios ; cell_extra];
                for o = 1:nQT_max-hj
                    d18{hj+o,1} = hj+o;
                end
            else
                d18 = invoer.QTscenarios;
            end
            set(ta18,'Data',d18)
        catch
        end
        try
            hj = size(invoer.QHscenarios,1);
            if hj < nQH_max
                cell_extra = cell(nQH_max-hj,5);
                d19 = [invoer.QHscenarios ; cell_extra];
                for o = 1:nQH_max-hj
                    d19{hj+o,1} = hj+o;
                end
            else
                d19 = invoer.QHscenarios;
            end
            set(ta19,'Data',d19)
        catch
        end
        
        % tab4
        %if invoer.sce_inst == 1
        %    set(bg20a,'Value',1);
        %else
        %    set(bg20b,'Value',1);
        %end
        try 
            sd = invoer.sce_inst;
            if sd ~= 1
                set(bg20a,'Value',1);
                invoer.sce_inst = 1;
            end
        catch
            set(bg20a,'Value',1);
            invoer.sce_inst = 1;
        end
        try
            set(tb4_norm_txt_str,'String',invoer.db_norm);
        catch
            set(tb4_norm_txt_str,'String','- niet geselecteerd -');
        end
        try
            set(tb4_ont_txt_str,'String',invoer.db_ontwerp_duur)
        catch
            set(tb4_ont_txt_str,'String','- niet geselecteerd -');
        end
        %try %Op verzoek van HKV om te verwijderen d.d. 11 juli 2018
        %    set(tb4_maxpip_txt_str,'String',invoer.db_maxpipberm)
        %catch
        %    %set(tb4_maxpip_txt_str,'String','- niet geselecteerd -');
        %end
        try
            set(tb4_hwbp_txt_str,'String',invoer.db_DVschema)
        catch
            set(tb4_hwbp_txt_str,'String','- niet geselecteerd -');
        end
        try
            set(tb4_winst_txt_popup,'Value',invoer.winst_rvm)
        catch
            set(tb4_winst_txt_popup,'Value',1);
        end
        try
            set(tb4_db_corr_dH_txt_str,'String',invoer.db_corr_dH)
        catch
            set(tb4_db_corr_dH_txt_str,'String','- niet geselecteerd -');
        end
        try
            set(tb4_db_corr_dS_txt_str,'String',invoer.db_corr_dS)
        catch
            set(tb4_db_corr_dS_txt_str,'String','- niet geselecteerd -');
        end
        try
            set(tb4_db_corr_dP_txt_str,'String',invoer.db_corr_dP)
        catch
            set(tb4_db_corr_dP_txt_str,'String','- niet geselecteerd -');
        end
        try
            set(tb4_db_pipcost_txt_str,'String',invoer.db_pipcost)
        catch
            set(tb4_db_pipcost_txt_str,'String','- niet geselecteerd -');
        end
        try
            set(tb4_db_DV_dH_txt_str,'String',invoer.db_DV_dH)
        catch
            set(tb4_db_DV_dH_txt_str,'String','- niet geselecteerd -');
        end
        try
            set(tb4_db_DV_dS_txt_str,'String',invoer.db_DV_dS)
        catch
            set(tb4_db_DV_dS_txt_str,'String','- niet geselecteerd -');
        end
        try
            set(tb4_db_DV_dP_txt_str,'String',invoer.db_DV_dP)
        catch
            set(tb4_db_DV_dP_txt_str,'String','- niet geselecteerd -');
        end
        try
            if invoer.opl_DV == 1
                set(tb4_opl_DV_opt_popup,'Value',1);
                set(tb4_db_DV_dH_txt,'Enable','on'); set(tb4_db_DV_dH_txt,'Visible','on');
                set(tb4_db_DV_dH_txt_str,'Enable','on'); set(tb4_db_DV_dH_txt_str,'Visible','on');
                set(tb4_db_DV_dH_txt_btn,'Enable','on'); set(tb4_db_DV_dH_txt_btn,'Visible','on');
                set(tb4_db_DV_dS_txt,'Enable','on'); set(tb4_db_DV_dS_txt,'Visible','on');
                set(tb4_db_DV_dS_txt_str,'Enable','on'); set(tb4_db_DV_dS_txt_str,'Visible','on');
                set(tb4_db_DV_dS_txt_btn,'Enable','on'); set(tb4_db_DV_dS_txt_btn,'Visible','on');
                set(tb4_db_DV_dP_txt,'Enable','on'); set(tb4_db_DV_dP_txt,'Visible','on');
                set(tb4_db_DV_dP_txt_str,'Enable','on'); set(tb4_db_DV_dP_txt_str,'Visible','on');
                set(tb4_db_DV_dP_txt_btn,'Enable','on'); set(tb4_db_DV_dP_txt_btn,'Visible','on');
            else
                set(tb4_opl_DV_opt_popup,'Value',2);
                set(tb4_db_DV_dH_txt,'Enable','off'); set(tb4_db_DV_dH_txt,'Visible','on');
                set(tb4_db_DV_dH_txt_str,'Enable','off'); set(tb4_db_DV_dH_txt_str,'Visible','on');
                set(tb4_db_DV_dH_txt_btn,'Enable','off'); set(tb4_db_DV_dH_txt_btn,'Visible','on');
                set(tb4_db_DV_dS_txt,'Enable','off'); set(tb4_db_DV_dS_txt,'Visible','on');
                set(tb4_db_DV_dS_txt_str,'Enable','off'); set(tb4_db_DV_dS_txt_str,'Visible','on');
                set(tb4_db_DV_dS_txt_btn,'Enable','off'); set(tb4_db_DV_dS_txt_btn,'Visible','on');
                set(tb4_db_DV_dP_txt,'Enable','off'); set(tb4_db_DV_dP_txt,'Visible','on');
                set(tb4_db_DV_dP_txt_str,'Enable','off'); set(tb4_db_DV_dP_txt_str,'Visible','on');
                set(tb4_db_DV_dP_txt_btn,'Enable','off'); set(tb4_db_DV_dP_txt_btn,'Visible','on');
            end
        catch
            set(tb4_opl_DV_opt_popup,'Value',2);
            set(tb4_db_DV_dH_txt,'Enable','off'); set(tb4_db_DV_dH_txt,'Visible','on');
            set(tb4_db_DV_dH_txt_str,'Enable','off'); set(tb4_db_DV_dH_txt_str,'Visible','on');
            set(tb4_db_DV_dH_txt_btn,'Enable','off'); set(tb4_db_DV_dH_txt_btn,'Visible','on');
            set(tb4_db_DV_dS_txt,'Enable','off'); set(tb4_db_DV_dS_txt,'Visible','on');
            set(tb4_db_DV_dS_txt_str,'Enable','off'); set(tb4_db_DV_dS_txt_str,'Visible','on');
            set(tb4_db_DV_dS_txt_btn,'Enable','off'); set(tb4_db_DV_dS_txt_btn,'Visible','on');
            set(tb4_db_DV_dP_txt,'Enable','off'); set(tb4_db_DV_dP_txt,'Visible','on');
            set(tb4_db_DV_dP_txt_str,'Enable','off'); set(tb4_db_DV_dP_txt_str,'Visible','on');
            set(tb4_db_DV_dP_txt_btn,'Enable','off'); set(tb4_db_DV_dP_txt_btn,'Visible','on');
        end
        try
            hj = size(invoer.normen,1);
            if hj > 1
                cell_extra = cell(10-hj,3);
                d24 = [invoer.normen ; cell_extra];
                for o = 1:10-hj
                    d24{hj+o,1} = hj+o;
                end
            else
                d24 = invoer.normen;
            end
            set(ta24,'Data',d24);
        catch
        end
        try
            hj = size(invoer.verst,1);
            if hj > 1
                cell_extra = cell(10-hj,4);
                d25 = [invoer.verst ; cell_extra];
                for o = 1:10-hj
                    d25{hj+o,1} = hj+o;
                end
            else
                d25 = invoer.verst;
            end
            set(ta25,'Data',d25);
        catch
        end
        
        % tab5
        try %Op verzoek van HKV om te verwijderen d.d. 11 juli 2018
            set(tb5_pipfac_txt_str,'String',num2str(invoer.pip_fac));
        catch
            try
                set(tb5_pipfac_txt_str,'String','1');
            catch
            end
        end
        try %Op verzoek van HKV om te verwijderen d.d. 11 juli 2018
            set(tb5_stabfac_txt_str,'String',num2str(invoer.mac_fac));
        catch
            try
                set(tb5_stabfac_txt_str,'String','1');
            catch
            end
        end
        try
            set(tb5_dhstap_txt_str,'String',num2str(invoer.dH_stap));
        catch
            set(tb5_dhstap_txt_str,'String','0.02');
        end
        try
            set(tb5_dsstap_txt_str,'String',num2str(invoer.dS_stap));
        catch
            set(tb5_dsstap_txt_str,'String','0.2');
        end
        try
            set(tb5_dpstap_txt_str,'String',num2str(invoer.dP_stap));
        catch
            set(tb5_dpstap_txt_str,'String','0.2');
        end
        try
            set(tb5_dHmax_txt_str,'String',num2str(invoer.dH_max));
        catch
            set(tb5_dHmax_txt_str,'String','10');
        end
        try
            set(tb5_dSmax_txt_str,'String',num2str(invoer.dS_max));
        catch
            set(tb5_dSmax_txt_str,'String','100');
        end
        try
            set(tb5_dPmax_txt_str,'String',num2str(invoer.dP_max));
        catch
            set(tb5_dPmax_txt_str,'String','500');
        end
        try
            set(tb5_Qdiscstep_txt_str,'String',num2str(invoer.Qdisc_step));
        catch
            set(tb5_Qdiscstep_txt_str,'String','100');
        end
        try %Op verzoek van HKV om te verwijderen d.d. 11 juli 2018
            set(tb5_Qdiscmin_txt_str,'String',num2str(invoer.Qdisc_min));
        catch
            try
                set(tb5_Qdiscmin_txt_str,'String','100');
            catch
            end
        end
        try %Op verzoek van HKV om te verwijderen d.d. 11 juli 2018
            set(tb5_Qdisclow_txt_str,'String',num2str(invoer.Qdisc_low));
        catch
            try
                set(tb5_Qdisclow_txt_str,'String','500');
            catch
            end
        end
        try
            set(tb5_Qdischigh_txt_str,'String',num2str(invoer.Qdisc_high));
        catch
            set(tb5_Qdischigh_txt_str,'String','1000');
        end
        try
            set(tb5_refjaar_txt_str,'String',num2str(invoer.refjaar_bodemdaling));
        catch
            set(tb5_refjaar_txt_str,'String','2015');
        end
        try
            set(tb5_tvoln_txt_str,'String',num2str(invoer.t_voldoen_norm));
        catch
            set(tb5_tvoln_txt_str,'String','2050');
        end
        try %Op verzoek van HKV om te verwijderen d.d. 11 juli 2018
            set(tb5_costbdP_txt_str,'String',num2str(invoer.cost_breakeven_dP));
        catch
            try
                set(tb5_costbdP_txt_str,'String','100');
            catch
            end
        end
        
        %tab6
        try
            set(tb6_uitvoermap_txt_str,'String',invoer.uitvoer_map);
        catch
            set(tb6_uitvoermap_txt_str,'String','- niet geselecteerd -');
        end
        try
            set(tb6_tijdopgave_txt_str,'String',invoer.tijdopgave);
        catch
            set(tb6_tijdopgave_txt_str,'String','- niet geselecteerd -');
        end
        try
            if invoer.plot_pf_vs_t == 0 || invoer.plot_pf_vs_t == 9
                set(tb6_pfvst_txt_popup,'Value',2);
            else
                set(tb6_pfvst_txt_popup,'Value',invoer.plot_pf_vs_t);
            end
        catch
            %set(tb6_pfvst_txt_popup,'Value',1); % n.a.v. aanpaasing in juli 2018 is dit verwijderd
            set(tb6_pfvst_txt_popup,'Value',2);
        end
        try
            if invoer.plot_dims_vs_t == 0 || invoer.plot_dims_vs_t == 9
                set(tb6_dimvst_txt_popup,'Value',2);
            else
                set(tb6_dimvst_txt_popup,'Value',invoer.plot_dims_vs_t);
            end
        catch
            set(tb6_pfvst_txt_popup,'Value',1);
        end
        try
            if invoer.plot_pf_h == 0 || invoer.plot_pf_h == 9
                set(tb6_pfht_txt_popup,'Value',5);
            else
                set(tb6_pfht_txt_popup,'Value',invoer.plot_pf_h);
            end
        catch
            set(tb6_pfht_txt_popup,'Value',5);
        end
        try
            if invoer.plot_pf_s == 0 || invoer.plot_pf_s == 9
                set(tb6_pfstbi_txt_popup,'Value',5);
            else
                set(tb6_pfstbi_txt_popup,'Value',invoer.plot_pf_s);
            end
        catch
            set(tb6_pfstbi_txt_popup,'Value',5);
        end
        try
            if invoer.plot_pf_p == 0 || invoer.plot_pf_p == 9
                set(tb6_pfstph_txt_popup,'Value',5);
            else
                set(tb6_pfstph_txt_popup,'Value',invoer.plot_pf_p);
            end
        catch
            set(tb6_pfstph_txt_popup,'Value',5);
        end
        try
            if invoer.plot_pdf_wb == 0 || invoer.plot_pdf_wb == 9 
                set(tb6_freqws_txt_popup,'Value',4)
            else
                set(tb6_freqws_txt_popup,'Value',invoer.plot_pdf_wb);
            end
        catch
            set(tb6_freqws_txt_popup,'Value',4);
        end
        try
            if invoer.plot_dijk == 0 || invoer.plot_dijk == 9
                set(tb6_visdijk_txt_popup,'Value',2)
            else
                set(tb6_visdijk_txt_popup,'Value',invoer.plot_dijk);
            end
        catch
            set(tb6_visdijk_txt_popup,'Value',2);
        end
        try
            if invoer.exp_traject == 0 || invoer.exp_traject == 9
                set(tb6_exportcsv_txt_popup,'Value',2)
            else
                set(tb6_exportcsv_txt_popup,'Value',invoer.exp_traject);
            end
        catch
            set(tb6_exportcsv_txt_popup,'Value',2);
        end
        try
            if invoer.exp_shape == 0 || invoer.exp_shape == 9
                set(tb6_exportdbf_txt_popup,'Value',2)
            else
                set(tb6_exportdbf_txt_popup,'Value',invoer.exp_shape);
            end
        catch
            set(tb6_exportdbf_txt_popup,'Value',2);
        end
        try
            if get(tb6_exportdbf_txt_popup,'Value') == 1
                set(tb6_exportdbffile_txt_str,'String',invoer.dbf_shape);
                set(tb6_exportdbffile_txt,'Enable','on'); set(tb6_exportdbffile_txt,'Visible','on');
                set(tb6_exportdbffile_txt_str,'Enable','on'); set(tb6_exportdbffile_txt_str,'Visible','on');
                set(tb6_exportdbffile_txt_btn,'Enable','on'); set(tb6_exportdbffile_txt_btn,'Visible','on');
            else
                set(tb6_exportdbffile_txt_str,'String',invoer.dbf_shape);
                set(tb6_exportdbffile_txt,'Enable','off'); set(tb6_exportdbffile_txt,'Visible','off');
                set(tb6_exportdbffile_txt_str,'Enable','off'); set(tb6_exportdbffile_txt_str,'Visible','off');
                set(tb6_exportdbffile_txt_btn,'Enable','off'); set(tb6_exportdbffile_txt_btn,'Visible','off');
            end
        catch
            if get(tb6_exportdbf_txt_popup,'Value') == 1
                set(tb6_exportdbffile_txt_str,'String','- niet geselecteerd -');
                set(tb6_exportdbffile_txt,'Enable','on'); set(tb6_exportdbffile_txt,'Visible','on');
                set(tb6_exportdbffile_txt_str,'Enable','on'); set(tb6_exportdbffile_txt_str,'Visible','on');
                set(tb6_exportdbffile_txt_btn,'Enable','on'); set(tb6_exportdbffile_txt_btn,'Visible','on');
            else
                set(tb6_exportdbffile_txt_str,'String','- niet geselecteerd -');
                set(tb6_exportdbffile_txt,'Enable','off'); set(tb6_exportdbffile_txt,'Visible','off');
                set(tb6_exportdbffile_txt_str,'Enable','off'); set(tb6_exportdbffile_txt_str,'Visible','off');
                set(tb6_exportdbffile_txt_btn,'Enable','off'); set(tb6_exportdbffile_txt_btn,'Visible','off');
            end
        end
    end
    function invoer_aanpassen_databases(link1,link2)
        %tab1 (3)
        set(tb1_dijkvak_txt_str,'String',strrep(get(tb1_dijkvak_txt_str,'String'),link1,link2));
        set(tb1_dijkvaklen_txt_str,'String',strrep(get(tb1_dijkvaklen_txt_str,'String'),link1,link2));
        set(tb1_koswat_txt_str,'String',strrep(get(tb1_koswat_txt_str,'String'),link1,link2));
        set(tb1_constlength_txt_str,'String',strrep(get(tb1_constlength_txt_str,'String'),link1,link2));
        set(tb1_db_CBSindex_txt_str,'String',strrep(get(tb1_db_CBSindex_txt_str,'String'),link1,link2));
        %tab2 (3)
        set(tb2_htdata_txt_str,'String',strrep(get(tb2_htdata_txt_str,'String'),link1,link2));
        set(tb2_stbidata_txt_str,'String',strrep(get(tb2_stbidata_txt_str,'String'),link1,link2));
        set(tb2_stphdata_txt_str,'String',strrep(get(tb2_stphdata_txt_str,'String'),link1,link2));
        %tab3 (2 + d18 + d19)
        set(tb3_onz_txt_str,'String',strrep(get(tb3_onz_txt_str,'String'),link1,link2));
        set(tb3_bodm_txt_str,'String',strrep(get(tb3_bodm_txt_str,'String'),link1,link2));
        [m,n] = size(d18);
        for a1 = 1:m
            for a2 = 1:n
                if ischar(d18{a1,a2}) == 1
                    strr = strrep(d18{a1,a2},link1,link2);
                    d18{a1,a2} = strr;
                end
            end
        end
        set(ta18,'Data',d18);
        [m,n] = size(d19);
        for a1 = 1:m
            for a2 = 1:n
                if ischar(d19{a1,a2}) == 1
                    strr = strrep(d19{a1,a2},link1,link2);
                    d19{a1,a2} = strr;
                end
            end
        end
        set(ta19,'Data',d19);
        %tab4 (4 + d24 + d25)
        set(tb4_norm_txt_str,'String',strrep(get(tb4_norm_txt_str,'String'),link1,link2));
        set(tb4_ont_txt_str,'String',strrep(get(tb4_ont_txt_str,'String'),link1,link2));
        % Op verzoek van HKV om te verwijderen
        try
            set(tb4_maxpip_txt_str,'String',strrep(get(tb4_maxpip_txt_str,'String'),link1,link2));
        catch
        end
        set(tb4_hwbp_txt_str,'String',strrep(get(tb4_hwbp_txt_str,'String'),link1,link2));
        %tab4 extra
        set(tb4_db_corr_dH_txt_str,'String',strrep(get(tb4_db_corr_dH_txt_str,'String'),link1,link2));
        set(tb4_db_corr_dS_txt_str,'String',strrep(get(tb4_db_corr_dS_txt_str,'String'),link1,link2));
        set(tb4_db_corr_dP_txt_str,'String',strrep(get(tb4_db_corr_dP_txt_str,'String'),link1,link2));
        set(tb4_db_pipcost_txt_str,'String',strrep(get(tb4_db_pipcost_txt_str,'String'),link1,link2));
        % voor extra variabelen die niet in GUI staan
        set(tb4_db_DV_dH_txt_str,'String',strrep(get(tb4_db_DV_dH_txt_str,'String'),link1,link2));
        set(tb4_db_DV_dS_txt_str,'String',strrep(get(tb4_db_DV_dS_txt_str,'String'),link1,link2));
        set(tb4_db_DV_dP_txt_str,'String',strrep(get(tb4_db_DV_dP_txt_str,'String'),link1,link2));
        %tab5
        %(geen aanpassing)
        %tab6
        set(tb6_uitvoermap_txt_str,'String',strrep(get(tb6_uitvoermap_txt_str,'String'),link1,link2));
        set(tb6_exportdbffile_txt_str,'String',strrep(get(tb6_exportdbffile_txt_str,'String'),link1,link2));
    end
end