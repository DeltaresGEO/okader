%% Invoerfile voor Tool Kostenreductie Dijkversterking door Rivierverruiming
%% versie 2.01
%% HKV en Deltares in opdracht van RWS-WVL, maart 2017

%% Tabblad "Algemeen"
analysetype    : 1
t_begin 	   : 2015
t_eind 		   : 2125
t_stap 		   : 1
db_vakken 	   : o:\OKADER2020\Databases\Rijn\Vakken_Rijn_Totaal.txt
db_vaklengte   : o:\OKADER2020\Databases\vaklengtes.csv
db_KOSWAT 	   : o:\OKADER2020\Databases\Rijn\DatabaseKOSWAT_Rijntakken_20200108.xlsx
constlength    : 0
db_constlength : <-- pad naar bestand met db_constlength -->
discontovoet   : 4.5
basisjaar 	   : 2025
indexeren 	   : 1
db_CBSindex    : o:\OKADER2020\Databases\GWWprijsindicesCBS.csv
prijspeil	   : 2020

%% Tabblad "Dijksterkte"
faalmech_h 	 : 1
faalmech_s 	 : 1
faalmech_p 	 : 1
db_FC_h 	 : o:\OKADER2020\Databases\Rijn\FC_Rijn_Overslag_20180618.csv
db_FC_s 	 : o:\OKADER2020\Databases\Rijn\FC_Rijn_Macrostab_20180822.csv
db_FC_p 	 : o:\OKADER2020\Databases\Rijn\FC_Rijn_Piping_20180822.csv
afkap_h 	 : 1
freq_bs_h 	 : 1
afkap_s 	 : 1
freq_bs_s 	 : 1
afkap_p 	 : 1
freq_bs_p 	 : 1

%% Tabblad "Hydraulische belasting"
db_onz_toeslag 	 : o:\OKADER2020\Databases\Onzekerheidstoeslag_2020_exWalSys.csv
db_bodemdaling 	 : o:\OKADER2020\Databases\bodemdaling.csv
dh_integraal 	 : 0
extrapolatie 	 : 1

% scenarios voor QT
ns = 1
sce_id = 1
t = 2015
QT_file_1 = o:\OKADER2020\Databases\Rijn\QT\afvoerstatistiek_rijn_GRADE_aftop_corr_2015.csv
QT_file_2 = o:\OKADER2020\Databases\Rijn\QT\afvoerstatistiek_rijn_GRADE_aftop_corr_2050.csv
QT_file_3 = o:\OKADER2020\Databases\Rijn\QT\afvoerstatistiek_rijn_GRADE_aftop_corr_2100.csv

% scenarios voor QH
ns = 2
sce_id = 1
t = 2015
QH_file_1 = o:\OKADER2020\Databases\Rijn\QH\QH_rijn_DM_OI_referentie_inclNRL_2015.csv
QH_file_2 = o:\OKADER2020\Databases\Rijn\QH\QH_rijn_DM_OI_referentie_inclNRL_2050.csv
QH_file_3 = o:\OKADER2020\Databases\Rijn\QH\QH_rijn_DM_OI_referentie_inclNRL_2100.csv
sce_id = 2
t = 2025
QH_file_1 = o:\OKADER2020\Databases\Rijn\QH\QdH_rijn_ij_exIJP_2015.csv
QH_file_2 = o:\OKADER2020\Databases\Rijn\QH\QdH_rijn_ij_exIJP_2050.csv
QH_file_3 = o:\OKADER2020\Databases\Rijn\QH\QdH_rijn_ij_exIJP_2100.csv

%% Tabblad "Dijkversterking en normen"
db_norm 	    : o:\OKADER2020\Databases\Pf_Vak_Eis_20180822.csv
db_ontwerp_duur : o:\OKADER2020\Databases\ontwerplevensduur.csv
db_DVschema 	: o:\OKADER2020\Databases\DVschema_aangepast_V20190404.csv
winst_rvm 	 : 1
db_corr_dH 	 : o:\OKADER2020\Databases\correctie_dH.csv
db_corr_dS 	 : o:\OKADER2020\Databases\correctie_dS.csv
db_corr_dP 	 : o:\OKADER2020\Databases\correctie_dP.csv
db_pipcost 	 : o:\OKADER2020\Databases\pipcost_afkappen100.csv
opl_DV 		 : 0
db_DV_dH 	 : <-- pad naar bestand met opgelegde versterking dH -->
db_DV_dS 	 : <-- pad naar bestand met opgelegde versterking dS -->
db_DV_dP 	 : <-- pad naar bestand met opgelegde versterking dP -->

%% Tabblad "Berekening instellingen"
dH_stap 		 : 0.01
dS_stap 		 : 0.2
dP_stap 		 : 0.2
dH_max 			 : 5
dS_max 			 : 50
dP_max 			 : 500
Qdisc_step 		 : 100
Qdisc_high 		 : 2000
Refjaar_bodemdaling  : 2015
t_voldoen_norm 		 : 2050

%% Tabblad "Uitvoer en visualisatie"
uitvoer_map    : o:\OKADER2020\Uitvoer\Rijn_2020_ExIJP_Ref
tijdopgave 	   : 2015,2025,2050,2075,2100,2125
plot_pf_vs_t   : 1
plot_dims_vs_t : 1
plot_pf_s 	   : 0
plot_pf_p 	   : 0
plot_pf_h 	   : 0
plot_pdf_wb    : 0
plot_dijk 	   : 0
exp_traject    : 1
exp_shape 	   : 1
dbf_shape 	   : o:\OKADER2020\Databases\Vakindeling\vakken_maas_rijn_v10.dbf
